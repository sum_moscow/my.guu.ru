
<?php

	$hidden_elements = array(

		'main-admin' => $user->hasRights(array('module' => 'main', 'min_level' => User::SUPER_ADMIN_LEVEL)),
		'main-rights' => $user->hasRights(array('module' => 'main', 'min_level' => 5)),

		'has-timetable' => $user->isProfessor() || $user->isStudent(),
		//'timetable-editor' => $user->hasRights(array('module' => 'timetable', 'min_level' => StudentGroup::MIN_LEVEL_FOR_UPDATE)),

		'brs-admin' => $user->hasRights(array('module' => 'brs', 'min_level' => User::SUPER_ADMIN_LEVEL)),
		'brs-editor' => $user->hasRights(array('module' => 'timetable', 'min_level' => User::SUPER_ADMIN_LEVEL)),
		'brs' =>  $user->isProfessor() || $user->isStudent() || $user->hasRights(array( 'module' => 'institute', 'min_level' => 10)), //
		'brs-groups' => $user->isProfessor() || $user->hasRights(array( 'module' => 'institute', 'min_level' => 10)),
		'brs-my-rating' => $user->isStudent(),

		'science-my-rating' => $user->isProfessor(),
		'science-statistics' => $user->isStaff(),

		'libs' => $user->hasRights(array('module' => 'main', 'min_level' => User::SUPER_ADMIN_LEVEL)),
		'inlibs' => $user->hasRights(array('module' => 'main', 'min_level' => User::SUPER_ADMIN_LEVEL)),
		'extlibs' => $user->hasRights(array('module' => 'main', 'min_level' => User::SUPER_ADMIN_LEVEL)),

		'dorm' => $user->isStudent(),

		'institutes' => $user->hasRights(array('module' => 'institutes', 'min_level' => 10)),
		'institutes-admin' => $user->hasRights(array( 'module' => 'institutes', 'min_level' => 10)),

		'ustav' => $user->hasRights(array( 'module' => 'ustav', 'min_level' => 1)),
		'moodle' => $user->hasRights(array('module' => 'main', 'min_level' => User::SUPER_ADMIN_LEVEL)),
		'moodle-student' => $user->isStudent(),
		'moodle-professor' => $user->isProfessor(),
	);


foreach($hidden_elements as $key => $value){
		$hidden_elements[$key] = ($value == true) ? '' : 'hidden';
	}

echo "
<ul id='main-menu' class=''>
	<li class='opened active'>
		<a><i class='entypo-gauge'></i><span>Информация</span></a>
		<ul>
			<li>
				<a href='/index.php'><span>Основная информация</span></a>
			</li>
			<li>
				<a href='/catalog.php'><span>Справочник</span></a>
			</li>
			<li>
				<a href='/mobileApps.php'><span>Мобильные приложения</span></a>
			</li>

			<!--<li class=''>
				<a href='/canteens.php'><span>Меню столовых</span></a>
			</li>-->


			<!--<li>
				<a href='/studentsChangeManual.php'><span>Помощь</span></a>
			</li>-->

		</ul>
	</li>

	<!--<li>
		<a><i class='entypo-doc-text'></i><span>Заявки</span></a>
		<ul>

			<li>
				<a href='/sendRequisition.php'><span>Деканат</span></a>
			</li>

			<li class='hidden'>
				<a href='#'><span>Общежитие</span></a>
			</li>

			<li class='hidden'>
				<a href='#'><span>ЦНИТ</span></a>
			</li>

			<li>
				<a href='/myRequisitions.php'><span>Мои заявки</span></a>
			</li>

		</ul>
	</li>!-->

	<!--<li class='{$hidden_elements['has-timetable']}'>
		<a><i class='entypo-list'></i><span>Расписание</span></a>
		<ul>

			<li class='{$hidden_elements['has-timetable']}'>
				<a href='/timetable.php'><span>Мое расписание</span></a>
			</li>

		</ul>
	</li>-->

	<li class='{$hidden_elements['brs']}'>
		<a><i class='entypo-chart-line'></i><span>БРС</span></a>
		<ul>
			<li  class='{$hidden_elements['brs-groups']}'>
				<a href='/brsGroups.php'><span>Группы</span></a>
			</li>
			<li class='{$hidden_elements['brs-my-rating']}'>
				<a href='/brsStudentView.php'><span>Мои баллы</span></a>
			</li>
		</ul>
	</li>

	<li class='hidden {$hidden_elements['moodle']}'>
		<a><i class='entypo-graduation-cap'></i><span>Дистанционное обучение</span></a>
		<ul>
			<li  class='{$hidden_elements['moodle-student']}'>
				<a href='/moodleCourses.php'><span>Мои курсы</span></a>
			</li>
			<li  class='{$hidden_elements['moodle-professor']}'>
				<a href='/moodleGroups.php'><span>Мои группы</span></a>
			</li>
		</ul>
	</li>

	<li>
		<a><i class='entypo-book'></i><span>Библиотеки</span></a>
		<ul>
			<!--<li>
				<a href='#' class='hidden'><span>Внутренние ресурсы</span></a>
			</li>!-->
			<li>
				<a href='extLibs.php'><span>Внешние библиотеки</span></a>
			</li>
		</ul>
	</li>

	<li class='{$hidden_elements['institutes']}'>
		<a><i class='fa fa-group'></i><span>Институт</span></a>
		<ul>
			<li class='{$hidden_elements['institutes-admin']}'>
				<a href='instituteStudents.php'><span>Контингент</span></a>
			</li>
		</ul>
	</li>

	<li class='{$hidden_elements['science-my-rating']}'>
		<a><i class='entypo-infinity'></i><span>Аттестация</span></a>
		<ul>
			<li class='{$hidden_elements['science-my-rating']}'>
				<a href='professorCertification.php'><span>Аттестационный лист</span></a>
			</li>
			<li class='{$hidden_elements['science-statistics']}'>
				<a href='instituteStats.php'><span>Статистика</span></a>
			</li>
		</ul>
	</li>

	<!--<li class='{$hidden_elements['main-admin']}'>
		<a><i class='entypo-tools'></i><span>Администрирование</span></a>
		<ul>
			<li>
				<a href='adminRights.php'><span>Управление правами</span></a>
			</li>
			<li>
				<a href='adminSync.php'><span>Синхронизации</span></a>
			</li>
		</ul>
	</li>-->
</ul>";
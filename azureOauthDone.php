<?php
	require_once 'backend/core/bin/db.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="Laborator.co" />

	<title>Загрузка данных от Microsoft ... </title>


	<link rel="stylesheet" href="/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css"  id="style-resource-1">
	<link rel="stylesheet" href="/assets/css/font-icons/entypo/css/entypo.css"  id="style-resource-2">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic"  id="style-resource-3">
	<link rel="stylesheet" href="/assets/css/bootstrap-min.css"  id="style-resource-4">
	<link rel="stylesheet" href="/assets/css/neon-core-min.css"  id="style-resource-5">
	<link rel="stylesheet" href="/assets/css/neon-theme-min.css"  id="style-resource-6">
	<link rel="stylesheet" href="/assets/css/neon-forms-min.css"  id="style-resource-7">
	<link rel="stylesheet" href="/assets/css/custom-min.css"  id="style-resource-8">
	<link rel="stylesheet" href="assets/css/skins/blue.css"  id="style-resource-99">
	<script src="/assets/js/jquery-1.11.0.min.js"></script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->


	<!-- TS1414871640: Neon - Responsive Admin Template created by Laborator -->
</head>
<body class="page-body login-page is-lockscreen login-form-fall" data-url="">

<div class="login-container">

	<div class="login-header">

		<div class="login-content">

			<a href="#" class="logo">
				<img src="/assets/images/logo.png" alt="" width="100" />
			</a>

			<p class="description">Идет загрузка данных с серверов Microsoft...</p>
			<div class="col-md-12">
				<div class="progress progress-striped active">
					<div class="progress-bar progress-bar-info" id="progress" role="progressbar" style="width: 0%">
					</div>
				</div>

			</div>
		</div>

	</div>

</div>


<script src="/assets/js/gsap/main-gsap.js" id="script-resource-1"></script>
<script src="<?=SUM::$DOMAIN?>:8080/socket.io/socket.io.js" id="script-resource-18"></script>
<script src="/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js" id="script-resource-2"></script>
<script src="/assets/js/bootstrap.js" id="script-resource-3"></script>
<script src="/assets/js/joinable.js" id="script-resource-4"></script>
<script src="/assets/js/resizeable.js" id="script-resource-5"></script>
<script src="/assets/js/neon-api.js" id="script-resource-6"></script>
<script src="/assets/js/cookies.min.js" id="script-resource-7"></script>
<script src="/assets/js/jquery.validate.min.js" id="script-resource-8"></script>
<script src="/assets/js/neon-custom.js" id="script-resource-10"></script>
<script src="/assets/js/neon-demo.js" id="script-resource-11"></script>
<script src="/assets/js/neon-skins.js" id="script-resource-12"></script>
<script src="assets/js/node.js" id="script-resource-022"></script>

<script>
	var texts_array = [
		'Инициализация запросов к мелкомягким ...',
		'Ожидание ответа от сервера в городе Гринбоу, штат Алабама...',
		'Обработка ответа сервера ...',
		'Считаем количество конфет в коробке ...',
		'Считаем количество пингвинов в Арктике ...',
		'Считаем количество фейлов мелкомягких ...',
		'Считаем количество зябликов в Зимбабве ...',
		'Вычисляем количество произведенных в восточном Парагвае кирпичей ...',
		'Считаем количество антарктических утконосов в Австралии ...',
		'Вычисляем как сильно влияет размножение зябликов на экономику Зимбабве ...',
		'Вычислили, что точно также, как и производство кирпичей в восточном Парагвае на популяцию антарктического утконоса ...',
		'Получаем ваши личные данные ...',
		'Осталось совсем чуть чуть ...'
		],
		$pr = $('#progress'),
		$pr_text = $('.description').css('color', '#003471'),
		text_number = 0,
		first = Math.floor((Math.random() * 40) + 1),
		interval = setInterval(function(){
		var inc = Math.floor((Math.random() * 5) + 1);
			if (first < 90){
				first = first + inc;
			}else{
				window.clearInterval(interval);
			}
			$pr.width(first + '%');
			$pr_text.text(texts_array[text_number++]);
	}, 3000);
	$pr.width(first + '%');
	socket.emit('azureAuth', searchToObject());
	socket.on('auth', function(data){
		window.clearInterval(interval);
		$pr.css('width', '99%');
		$.ajax({
			url: '/auth.php',
			type: 'POST',
			data: data,
			success: function(res){
				$pr.css('width', '100%');
				if (res.status){
					window.opener.location = '/login.php';
					window.close();
				}else{
					alert('Извините, авторизация пока запрещена для Вас. Попробуйте залогиниться завтра. Мы запускаемся постепенно');
				}
			}
		});
	});
</script>

</body>
</html>



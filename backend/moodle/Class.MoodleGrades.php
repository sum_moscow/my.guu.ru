<?php

class MoodleGrades{


	static function getGrades($since, $to = null, PDO $db){
		if ($to == null){
			$to = time();
		}
		$result_array = array();
		$q_get_grades = 'SELECT moodle_grades.*, subjects.subject_1c_guid, (2 * edu_groups.course - 1) as semester_number, students.student_1c_guid
			FROM moodle_grades
			INNER JOIN moodle_activities ON moodle_activities.id = moodle_grades.moodle_activity_id
			INNER JOIN moodle_edu_groups_courses ON moodle_edu_groups_courses.id = moodle_activities.moodle_edu_groups_courses_id
			INNER JOIN moodle_courses ON moodle_courses.id = moodle_edu_groups_courses.moodle_course_id
			INNER JOIN subjects ON moodle_courses.subject_id = subjects.id
			INNER JOIN semesters ON moodle_edu_groups_courses.semester_id = semesters.id
			INNER JOIN students ON students.user_id = moodle_grades.user_id
			INNER JOIN view_users ON view_users.id = students.user_id
			INNER JOIN edu_groups ON view_users.edu_group_id = edu_groups.id
			WHERE ((moodle_grades.created_at BETWEEN FROM_UNIXTIME(:since) AND FROM_UNIXTIME(:to))
				OR (moodle_grades.updated_at BETWEEN FROM_UNIXTIME(:since) AND FROM_UNIXTIME(:to)))
			AND moodle_activities.activityid_in_moodle = "course"
			AND grade IS NOT NULL';
		$p_get_grades = $db->prepare($q_get_grades);
		$r_query = $p_get_grades->execute(array(
			':since' => $since,
			':to' => $to
		));
		if ($r_query === FALSE )throw new DBQueryException('MOODLE_CANT_GET_GRADES', $db);
		$arr = $p_get_grades->fetchAll();
		foreach($arr as $grade){
			if (!isset($result_array[$grade['subject_1c_guid']])){
				$result_array[$grade['subject_1c_guid']] = array(
					'id' => $grade['subject_1c_guid'],
					'semester' => 'Семестр' . $grade['semester_number'],
					'results' => array(),
				);
			}
			$result_array[$grade['subject_1c_guid']]['results'][] = array(
				'student' => $grade['student_1c_guid'],
				'value' => $grade['grade']
			);
		}
		if (count($result_array) > 0){
			$return_arr = array('disciplines' => array());
			foreach($result_array as $discipline){
				$return_arr['disciplines'][] = $discipline;
			}
			return new Result(true, 'Данные успешно получены', $return_arr);
		}else{
			return new Result(true, 'Нет данных для отображения');
		}
	}

	public static function getUserGrades(User $__user, PDO $db) {
		$q_get_grades = 'SELECT moodle_grades.*, subjects.subject_1c_guid, semester_number, students.student_1c_guid,
			moodle_edu_groups_courses.moodle_course_id, subjects.name, moodle_courses.courseid_in_moodle,
			moodle_activities.activityid_in_moodle, moodle_activities.grademax_in_moodle,
			moodle_activities.name as activity_name
			FROM moodle_grades
			INNER JOIN moodle_activities ON moodle_activities.id = moodle_grades.moodle_activity_id
			INNER JOIN moodle_edu_groups_courses ON moodle_edu_groups_courses.id = moodle_activities.moodle_edu_groups_courses_id
			INNER JOIN moodle_courses ON moodle_courses.id = moodle_edu_groups_courses.moodle_course_id
			INNER JOIN subjects ON moodle_courses.subject_id = subjects.id
			INNER JOIN semesters ON moodle_edu_groups_courses.semester_id = semesters.id
			INNER JOIN students ON students.user_id = moodle_grades.user_id
			WHERE moodle_grades.user_id = :user_id';
		$p_get_grades = $db->prepare($q_get_grades);
		$r_query = $p_get_grades->execute(array(
			':user_id' => $__user->getId()
		));
		if ($r_query === FALSE )throw new DBQueryException('MOODLE_CANT_GET_GRADES', $db);
		return new Result(true, '', $p_get_grades->fetchAll());
	}

	public static function getGroupGrades(Professor $professor, $moodle_edu_groups_courses_id, PDO $db){
		$q_get_grades = 'SELECT moodle_grades.*, subjects.subject_1c_guid, semester_number, students.student_1c_guid,
			moodle_edu_groups_courses.moodle_course_id, subjects.name, moodle_courses.courseid_in_moodle,
			moodle_activities.activityid_in_moodle, moodle_activities.grademax_in_moodle,
			moodle_activities.name as activity_name, users.first_name, users.last_name, users.middle_name
			FROM moodle_grades
			INNER JOIN moodle_activities ON moodle_activities.id = moodle_grades.moodle_activity_id
			INNER JOIN moodle_edu_groups_courses ON moodle_edu_groups_courses.id = moodle_activities.moodle_edu_groups_courses_id
			INNER JOIN moodle_courses ON moodle_courses.id = moodle_edu_groups_courses.moodle_course_id
			INNER JOIN subjects ON moodle_courses.subject_id = subjects.id
			INNER JOIN semesters ON moodle_edu_groups_courses.semester_id = semesters.id
			INNER JOIN students ON students.user_id = moodle_grades.user_id
			INNER JOIN users ON users.id = students.user_id
			WHERE moodle_grades.user_id IN (44800, 45333, 45427)
				AND moodle_edu_groups_courses_id = :moodle_edu_groups_courses_id
				ORDER BY activityid_in_moodle';

		$p_get_grades = $db->prepare($q_get_grades);
		$r_query = $p_get_grades->execute(array(
			':moodle_edu_groups_courses_id' => $moodle_edu_groups_courses_id
		));
		if ($r_query === FALSE )throw new DBQueryException('MOODLE_CANT_GET_GRADES', $db);
		return new Result(true, '', $p_get_grades->fetchAll());
	}
}
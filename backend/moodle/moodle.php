<?php

require_once 'Class.MoodleGrades.php';

$__modules['moodle'] = array(
	'POST' => array(
	),
	'GET' => array(
		'grades' => function() use ($__db, $__user, $__request, $__args) {
			if (isset($__args[0]) && $__args[0] == 'my'){
				return MoodleGrades::getUserGrades($__user, $__db);
			}else{
				$since = $__request['since'];
				$to = $__request['to'];
				return MoodleGrades::getGrades($since, $to, $__db);
			}
		},
		'{groups/grades/(moodle_edu_groups_courses_id:[0-9]+)}' => function($moodle_edu_groups_courses_id) use ($__db, $__user, $__request, $__args) {
				return MoodleGrades::getGroupGrades($__user->getProfessorInstance(),$moodle_edu_groups_courses_id, $__db);
		}
	)
);
<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 04.08.2015
 * Time: 17:59
 */

class Address {
    private $id;
    private $country;
    private $region;
    private $region_kladr;
    private $district;
    private $district_kladr;
    private $city;
    private $city_kladr;
    private $street;
    private $street_kladr;
    private $building;
    private $building_kladr;
    private $flat;
    private $postcode;
    
    public function __construct($id, $country, $region, $region_kladr, $district, $district_kladr, $city, $city_kladr,
                                $street, $street_kladr, $building, $building_kladr, $flat, $postcode){
        $this->id = (int)$id;
        $this->country = $country;
        $this->region = $region;
        $this->region_kladr = $region_kladr;
        $this->district = $district;
        $this->district_kladr = $district_kladr;
        $this->city = $city;
        $this->city_kladr = $city_kladr;
        $this->street = $street;
        $this->street_kladr = $street_kladr;
        $this->building = $building;
        $this->building_kladr = $building_kladr;
        $this->flat = $flat;
        $this->postcode = $postcode;
    }

    public function getInfo(){
        return array(
            'id' => $this->id,
            'country' => $this->country,
            'region' => $this->region,
            'region_kladr' => $this->region_kladr,
            'district' => $this->district,
            'district_kladr' => $this->district_kladr,
            'city' => $this->city,
            'city_kladr' => $this->city_kladr,
            'street' => $this->street,
            'street_kladr' => $this->street_kladr,
            'building' => $this->building,
            'building_kladr' => $this->building_kladr,
            'flat' => $this->flat,
            'postcode' => $this->postcode,
        );
    }
    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getBuilding(){
        return $this->building;
    }

    /**
     * @param string $building
     */
    public function setBuilding($building){
        $this->building = $building;
    }

    /**
     * @return string
     */
    public function getCity(){
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city){
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCountry(){
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country){
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getDistrict(){
        return $this->district;
    }

    /**
     * @param string $district
     */
    public function setDistrict($district){
        $this->district = $district;
    }

    /**
     * @return string
     */
    public function getFlat(){
        return $this->flat;
    }

    /**
     * @param string $flat
     */
    public function setFlat($flat){
        $this->flat = $flat;
    }

    /**
     * @return string
     */
    public function getPostcode(){
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode){
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getRegion(){
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region){
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getStreet(){
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street){
        $this->street = $street;
    }
} 
<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 22.07.2015
 * Time: 11:46
 */

abstract class AbstractStudent{
    /**
     * @return int
     */
    public abstract function getId();
    /**
     * @return int
     */
    public abstract function getUserId();
    /**
     * @return array
     */
    public abstract function getInfo();
}
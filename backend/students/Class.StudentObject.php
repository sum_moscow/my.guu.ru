<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 13.05.2015
 * Time: 19:46
 */

global $ROOT_PATH;
require_once  $ROOT_PATH . '/backend/institutes/Class.Institute.php';
require_once  'Class.AbstractStudent.php';

//TODO Добавить новые поля. getInfo
class StudentObject{
    const MODULE_NAME = 'students';
    const MIN_LEVEL_TO_WRITE = 15;
    const MIN_LEVEL_TO_READ = 10;

    const PASSPORT_RF_ID = 1;
    const BIRTH_CERTIFICATE_ID = 3;
    const FOREIGN_PASSPORT_ID = 6;
    const IMMIGRANT_REG_APP_CERTIFICATE_ID = 7;
    const TEMPORARY_IDENTITY_CARD_RF_ID = 10;

    private $id;
    protected $db;
    private $last_name;
    private $first_name;
    private $middle_name;
    private $user_id;
    private $edu_group_id;
    private $institute_id;
    private $phone_number;
    private $passport_id;
    private $registration_address_id;
    private $residence_address_id;
    private $birth_date;
    private $agreement;
    private $snils;
    private $email;
	private $document_scan_ext;
    private $photo_ext;

    public function __construct($id, PDO $db){
        $this->db = $db;
        $this->id = (int)$id;
        $q_get = 'SELECT view_students.*, students.document_scan_ext, students.photo_ext
                  FROM view_students
                  INNER JOIN students ON view_students.id = students.id
                  WHERE students.id = :id';
        $p_get = $db->prepare($q_get);
        $q_res = $p_get->execute(array(':id' => $id));

        if ($q_res === FALSE) throw new DBQueryException('', $db);
        if (!$result = $p_get->fetch()) throw new InvalidArgumentException("No student with id {$id}");

        $this->last_name = $result['last_name'];
        $this->first_name = $result['first_name'];
        $this->middle_name = $result['middle_name'];
        $this->edu_group_id = (int)$result['edu_group_id'];
        $this->institute_id = (int)$result['institute_id'];
        $this->phone_number = $result['phone_number'];
        $this->user_id = (int)$result['user_id'];
        $this->registration_address_id = (int)$result['registration_address_id'];
        $this->residence_address_id = (int)$result['residence_address_id'];
        $this->passport_id = (int)$result['passport_id'];
        $this->birth_date = $result['birth_date'];
        $this->agreement = (int)$result['agreement'];
        $this->snils = $result['snils'];
        $this->email = $result['email'];

        $this->document_scan_ext = $result['document_scan_ext'];
        $this->photo_ext = $result['photo_ext'];

    }

    /**
     * Checking rights to write
     * Возвращает true, если user имеет доступ к записи
     * @param User $user
     * @return bool
     */
    private function hasRightsToWrite(User $user){
        if (!$user->hasRights(array('module' => 'students', 'min_level' => self::MIN_LEVEL_TO_WRITE))
            && !Institute::hasRights($user, $this->institute_id))
            return false;
        else
            return true;
    }

    /**
     * Checking rights to read
     * Возвращает true, если user имеет доступ к чтению
     * @param User $user
     * @return bool
     */
    private function hasRightsToRead(User $user){
        if (!$user->hasRights(array('module' => 'students', 'min_level' => self::MIN_LEVEL_TO_READ))
            && !Institute::hasRights($user, $this->institute_id))
            return false;
        else
            return true;
    }

    //TODO хитрая проверка, что пользователь или глобальный редактор, или редактор в институте, если не задан institute_id,
    //иначе проверяется, что пользователь имеет доступ к студентам данного института.
    private static function hasRights(User $user, $institute_id = null){
        if($institute_id == null)
            $institute_id = $user->getInstituteId();
        if(!$user->hasRights(array('module' => 'students', 'min_level' => self::MIN_LEVEL_TO_READ))
            && !Institute::hasRights($user, $institute_id))
            return false;
        else
            return true;
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId(){
        return $this->user_id;
    }

    /**
     * @param User $user
     * @return int
     * @throws PrivilegesException
     */
    public function getRegistrationAddressId(User $user){
        if(!$this->hasRightsToRead($user)) throw new PrivilegesException('', $this->db);
        return $this->registration_address_id;
    }

    /**
     * @param User $user
     * @return int
     * @throws PrivilegesException
     */
    public function getResidenceAddressId(User $user){
        if(!$this->hasRightsToRead($user)) throw new PrivilegesException('', $this->db);
        return $this->residence_address_id;
    }

    /**
     * @param User $user
     * @return int
     * @throws PrivilegesException
     */
    public function getPassportId(User $user){
        if(!$this->hasRightsToRead($user)) throw new PrivilegesException('You do not have permissions', $this->db);
        return $this->passport_id;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber(){
        return $this->phone_number;
    }

    public function getInfo(){
        return new Result(true, '', array(
            'id' => $this->id,
            'last_name' => $this->last_name,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'user_id' => $this->user_id,
            'edu_group_id' => $this->edu_group_id,
        ));
    }

    /**
     * одновременный апдейт паспортов и обоих адресов
     * могут быть пустымми: post_code, snils, issue_place_code
     * @param array $params
     * @param User $user
     * @throws DBQueryException
     * @throws Exception
     * @return Result
     */
    public function setInfo(array $params, User $user){
        $trans_start = $this->db->beginTransaction();
        if($trans_start === FALSE) throw new DBQueryException('Can not start transaction', $this->db);
        try{
            $registration_address = array(
                'country' => $params['registration_country'],
                'region' => $params['registration_region'],
                'district' => $params['registration_district'],
                'city' => $params['registration_city'],
                'street' => $params['registration_street'],
                'building' => $params['registration_building'],
                'region_kladr' => $params['registration_region_kladr'],
                'district_kladr' => $params['registration_district_kladr'],
                'city_kladr' => $params['registration_city_kladr'],
                'street_kladr' => $params['registration_street_kladr'],
                'building_kladr' => $params['registration_building_kladr'],
                'postcode' => $params['registration_postcode'],
                'flat' => $params['registration_flat'],
            );
            if(array_key_exists('registration_postcode', $params)){
                $registration_address['registration_postcode'] = $params['registration_postcode'];
            }
            $residence_address = array(
                'country' => $params['residence_country'],
                'region' => $params['residence_region'],
                'district' => $params['residence_district'],
                'city' => $params['residence_city'],
                'street' => $params['residence_street'],
                'building' => $params['residence_building'],
                'region_kladr' => $params['residence_region_kladr'],
                'district_kladr' => $params['residence_district_kladr'],
                'city_kladr' => $params['residence_city_kladr'],
                'street_kladr' => $params['residence_street_kladr'],
                'building_kladr' => $params['residence_building_kladr'],
                'postcode' => $params['residence_postcode'],
                'flat' => $params['residence_flat'],
            );
            if(array_key_exists('residence_postcode', $params)){
                $residence_address['residence_postcode'] = $params['residence_postcode'];
            }
            $passport = array(
                'series' => $params['series'],
                'number' => $params['number'],
                'issue_date' => $params['issue_date'],
                'issue_place' => $params['issue_place'],
                'issue_place_code' => $params['issue_place_code'],
                'doc_type_id' => $params['doc_type_id'],
                'birth_place' => $params['birth_place'],
            );
            if( $this->getPassportId($user)){
                $this->setPassport($passport, $user);
            }
            else{
                $passport_id = $this->addPassport($passport, $user);
                $this->setPassportId($passport_id, $user);
            }
            if($this->getRegistrationAddressId($user)){
                $this->setRegistrationAddress($registration_address, $user);
            }else{
                $address_id = $this->addAddress($registration_address, $user);
                $this->setRegistrationAddressId($address_id, $user);
            }
            if($this->getResidenceAddressId($user)){
                $this->setResidenceAddress($residence_address, $user);
            }
            else{
                $address_id = $this->addAddress($residence_address, $user);
                $this->setResidenceAddressId($address_id, $user);
            }
            $this->setPhoneNumber($params['phone_number'], $user);
            $this->setAgreement($params['agreement'], $user);
            $this->setBirthDate($params['birth_date'], $user);
            $this->setEmail($params['email'], $user);
            $this->setSnils($params['snils'], $user);
            $this->setDocumentScan($params['file'], $params['file_name']);
            $this->setPhoto($params['photo_file'], $params['photo_file_name']);
            $this->update();

        }catch (Exception $e){
            $this->db->rollBack();
            throw $e;
        }
        $res = $this->db->commit();
        if(!$res) throw new DBQueryException('commit error', $this->db);
        return new Result(true);
    }

    //TODO Ограничение в базе на единственность адреса. ИЗменить логику!
    /**
     * Insert new address into database
     * @param array $address
     * @param User $user
     * @return int
     * @throws DBQueryException
     * @throws PrivilegesException
     */
    private function addAddress(array $address, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);
        $q_address = 'INSERT INTO addresses (country, region, region_kladr, district, district_kladr,
              city, city_kladr, street, street_kladr, building, building_kladr, flat, postcode, created_at)
				VALUES(:country, :region, :region_kladr, :district, :district_kladr,
				:city, :city_kladr, :street, :street_kladr, :building, :building_kladr, :flat, :postcode, NOW())';

        $p_address = $this->db->prepare($q_address);
        $q_res = $p_address->execute(array(
            ':country' => $address['country'],
            ':region' => $address['region'],
            ':region_kladr' => $address['region'],
            ':district' => $address['district'],
            ':district_kladr' => $address['district'],
            ':city' => $address['city'],
            ':city_kladr' => $address['city'],
            ':street' => $address['street'],
            ':street_kladr' => $address['street'],
            ':building' => $address['building'],
            ':building_kladr' => $address['building'],
            ':flat' => $address['flat'],
            ':postcode' => $address['postcode']
        ));
        if($q_res === FALSE) throw new DBQueryException('Can not add address into DB', $this->db);
        $address_id = $this->db->lastInsertId();
        return (int)$address_id;
    }

    /**
     * Update user's registration address or insert new and update user's registration address id, if it's empty
     * @param array $address
     * @param User $user
     * @throws DBQueryException
     * @throws PrivilegesException
     * @return Result
     */
    public function setRegistrationAddress(array $address, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);

        $q_upd_address = 'UPDATE addresses
            SET country = :country, region = :region, region_kladr = :region_kladr,
              district = :district, district_kladr = :district_kladr,
              city = :city, city_kladr = :city_kladr, street = :street, street_kladr = :street_kladr,
              building = :building, building_kladr = :building_kladr, flat = :flat, postcode = :postcode
            WHERE id = :id';
        $address_id = $this->getRegistrationAddressId( $user );
        if($address_id){
            $p_upd_address = $this->db->prepare($q_upd_address);

            $q_res = $p_upd_address->execute(array(
                ':country' => $address['country'],
                ':region' => $address['region'],
                ':region_kladr' => $address['region_kladr'],
                ':district' => $address['district'],
                ':district_kladr' => $address['district_kladr'],
                ':city' => $address['city'],
                ':city_kladr' => $address['city_kladr'],
                ':street' => $address['street'],
                ':street_kladr' => $address['street_kladr'],
                ':building' => $address['building'],
                ':building_kladr' => $address['building_kladr'],
                ':flat' => $address['flat'],
                ':postcode' => $address['postcode'],
                ':id' => $this->registration_address_id
            ));
            if($q_res === FALSE) throw new DBQueryException('Can not update reg address', $this->db);
            return new Result(true);
        }
        else{
            $address_id = $this->addAddress($address, $user);
            $this->registration_address_id = $address_id;
//            return $this->update();
            return null;
        }
    }

    /**
     * @param $registration_address_id
     * @param User $user
     * @throws DBQueryException
     * @throws PrivilegesException
     */
    public function setRegistrationAddressId($registration_address_id, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);
        $this->registration_address_id = (int)$registration_address_id;
//        $this->update();
    }

    /**
     * Update user's residence address or insert new and update user's residence address id, if it's empty
     * @param array $address
     * @param User $user
     * @throws DBQueryException
     * @throws PrivilegesException
     * @return Result
     */
    public function setResidenceAddress(array $address, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);
        $q_upd_address = 'UPDATE addresses
            SET country = :country, region = :region, region_kladr = :region_kladr,
              district = :district, district_kladr = :district_kladr,
              city = :city, city_kladr = :city_kladr, street = :street, street_kladr = :street_kladr,
              building = :building, building_kladr = :building_kladr, flat = :flat, postcode = :postcode
            WHERE id = :id';

        $address_id = $this->getResidenceAddressId( $user );
        if($address_id){
            $p_upd_address = $this->db->prepare($q_upd_address);
            $q_res = $p_upd_address->execute(array(
                ':country' => $address['country'],
                ':region' => $address['region'],
                ':region_kladr' => $address['region_kladr'],
                ':district' => $address['district'],
                ':district_kladr' => $address['district_kladr'],
                ':city' => $address['city'],
                ':city_kladr' => $address['city_kladr'],
                ':street' => $address['street'],
                ':street_kladr' => $address['street_kladr'],
                ':building' => $address['building'],
                ':building_kladr' => $address['building_kladr'],
                ':flat' => $address['flat'],
                ':postcode' => $address['postcode'],
                ':id' => $this->residence_address_id,
            ));
            if($q_res === FALSE) throw new DBQueryException('Can not update res address', $this->db);
            return new Result(true);
        }
        else{
            $address_id = $this->addAddress($address, $user);
            $this->residence_address_id = $address_id;
//            return $this->update();
            return null;
        }
    }

    /**
     * @param $residence_address_id
     * @param User $user
     * @throws DBQueryException
     * @throws PrivilegesException
     * @return null
     */
    public function setResidenceAddressId($residence_address_id, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);
        $this->residence_address_id = (int)$residence_address_id;
//        $this->update();
        return null;
    }

    /**
     * Insert new passport into database
     * @param array $passport
     * @param User $user
     * @return int
     * @throws DBQueryException
     * @throws PrivilegesException
     */
    private function addPassport( array $passport, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);

        $this->isCorrectPassport($passport);

        $q_passport = 'INSERT INTO passports(series, number, issue_date, issue_place, issue_place_code, doc_type_id, created_at)
				VALUES(:series, :number, :issue_date, :issue_place, :issue_place_code, :doc_type_id, NOW())
				ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)';
        $p_passport = $this->db->prepare($q_passport);
        $q_res = $p_passport->execute(array(
            ':series' => ($passport['series'] == '') ? null : $passport['series'],
            ':number' => ($passport['number'] == '') ? null : $passport['number'],
            ':issue_date' => ($passport['issue_date'] == '0000-00-00') ? null : $passport['issue_date'],
            ':issue_place' => ($passport['issue_place'] == '') ? null : $passport['issue_place'],
            ':issue_place_code' => ($passport['issue_place_code'] == '') ? null : $passport['issue_place_code'],
            ':doc_type_id' => ($passport['doc_type_id'] == '') ? null : $passport['doc_type_id'],
        ));
        if($q_res === FALSE) throw new DBQueryException('Can not add passport into DB', $this->db);
        $passport_id = $this->db->lastInsertId();
        return (int)$passport_id;
    }

    /**
     * @param $passport_id
     * @param User $user
     * @throws DBQueryException
     * @throws PrivilegesException
     * @return null
     */
    public function setPassportId($passport_id, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);
        $this->passport_id = (int)$passport_id;
//        $this->update();
        return null;
}
    /**
     * Update user's passport or insert new passport and update user's passport id, if it's empty
     * @param array $passport
     * @param User $user
     * @throws DBQueryException
     * @throws PrivilegesException
     * @return Result
     */
    public function setPassport(array $passport, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);

        $this->isCorrectPassport($passport);

        $q_upd_passport = 'UPDATE passports SET series = :series, number = :number,
              issue_date = :issue_date, issue_place = :issue_place, issue_place_code = :issue_place_code,
              doc_type_id = :doc_type_id, birth_place = :birth_place
            WHERE id = :id';
        $passport_id = $this->getPassportId( $user );
        if($passport_id){
            $p_upd_passport = $this->db->prepare($q_upd_passport);
            $q_res = $p_upd_passport->execute( array(
                ':series' => $passport['series'],
                ':number' => $passport['number'],
                ':issue_date' => $passport['issue_date'],
                ':issue_place' => $passport['issue_place'],
                ':issue_place_code' => $passport['issue_place_code'],
                ':doc_type_id' => $passport['doc_type_id'],
                ':birth_place' => $passport['birth_place'],
                ':id' => $passport_id,
            ));
            if($q_res === FALSE) throw new DBQueryException('Can not update passport'.$p_upd_passport->errorInfo()[2], $this->db);
            return new Result(true);
        }
        else{
            $passport_id = $this->addPassport($passport, $user);
            $this->passport_id = $passport_id;
//            return $this->update();
            return null;
        }
    }

    /**
     * проверка на корректность в соотвествии с регламентом ГУПМСР
     * @param array $passport
     * @throws InvalidArgumentException
     */
    public function isCorrectPassport(array $passport){
        $doc_type = $passport['doc_type_id'];

        if ($passport['birth_place'] == '') throw new InvalidArgumentException('Место рождения обязательно для заполнения.');

        if($doc_type != self::FOREIGN_PASSPORT_ID &&
            $doc_type != self::IMMIGRANT_REG_APP_CERTIFICATE_ID &&
            $doc_type != self::TEMPORARY_IDENTITY_CARD_RF_ID){
            //не проверяется серия свидетельства о рождении
            if ($doc_type == self::PASSPORT_RF_ID && preg_match('(\d{4})', $passport['series']) == FALSE) throw new InvalidArgumentException('incorrect series of passport');
        }
        else{
            if($passport['series'] != '')
                throw new InvalidArgumentException('Серия документа должна быть пустой для данного типа документа');
        }

        if($doc_type == self::PASSPORT_RF_ID ||
            $doc_type == self::BIRTH_CERTIFICATE_ID){
            if(preg_match('(\d{6})', $passport['number']) == FALSE)
                throw new InvalidArgumentException('Некорректный номер документа');
        }
        if($doc_type == self::BIRTH_CERTIFICATE_ID ||
            $doc_type == self::FOREIGN_PASSPORT_ID ||
            $doc_type == self::IMMIGRANT_REG_APP_CERTIFICATE_ID ||
            $doc_type == self::TEMPORARY_IDENTITY_CARD_RF_ID ||
            $doc_type == self::PASSPORT_RF_ID)
            if(preg_match('(\d{4}-\d\d-\d\d)', $passport['issue_date']) == FALSE)
                throw new InvalidArgumentException('Некорректная дата выдачи документа');

        if($passport['issue_place_code'] != '') //в заявлении нет кода подразделения
            if($doc_type == self::PASSPORT_RF_ID &&
                (preg_match('(\d\d\d-\d\d\d)', $passport['issue_place_code']) == FALSE
                || $passport['issue_place_code'] == '000-000'))
                throw new InvalidArgumentException('Некорректный код подразделения (место выдачи)');

        if(preg_match('([a-zа-я\d\xAB\xBB\x{2116},\-\'`’[:space:].]*)ui', $passport['issue_place']) == FALSE)
            throw new InvalidArgumentException('Некорректное место выдачи документа');
        return;
    }

    /**
     * @param $phone_number
     * @param User $user
     * @return Result
     * @throws DBQueryException
     * @throws PrivilegesException
     */
    public function setPhoneNumber($phone_number, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);
        if( preg_match('(((\+7)|(8))?\d{10})', $phone_number) == FALSE)
            throw new InvalidArgumentException('Некорректный номер телефона');
        $this->phone_number = $phone_number;
    }

    /**
     * @param mixed $snils
     * @param User $user
     * @throws PrivilegesException
     * @throws InvalidArgumentException
     */
    private function setSnils($snils, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);
        if ($snils == ''){
            $this->snils = null;
            return;
        }
	    if(preg_match('/\d{3}-\d{3}-\d{3} \d{2}/', $snils) == FALSE || preg_match('/000-000-000 00/', $snils) == TRUE)
            throw new InvalidArgumentException('Некорректный номер СНИЛС');
        $this->snils = $snils;
    }

    /**
     * @param mixed $birth_date
     * @param User $user
     * @throws PrivilegesException
     */
    private function setBirthDate($birth_date, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);
        $this->birth_date = $birth_date;
    }

    /**
     * @param int $agreement
     * @param User $user
     * @throws PrivilegesException
     */
    private function setAgreement($agreement, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);
        $this->agreement = $agreement;
    }

    /**
     * @param mixed $email
     * @param User $user
     * @throws PrivilegesException
     */
    private function setEmail($email, User $user){
        if(!$this->hasRightsToWrite($user)) throw new PrivilegesException('', $this->db);
        $this->email = $email;
    }

    /**
     * @return Result
     * @throws DBQueryException
     */
    private function update(){
        $q_upd = 'UPDATE students SET
            phone_number = :phone_number,
            document_scan_ext = :document_scan_ext,
            photo_ext = :photo_ext
            WHERE id = :id';
        $q_upd_addrs = 'UPDATE users SET registration_address_id = :registration_address_id, residence_address_id = :residence_address_id, passport_id = :passport_id,
                        email = :email, birth_date = :birth_date, snils = :snils, agreement = :agreement WHERE id = :id';
        $pq_upd = $this->db->prepare($q_upd);
        $p_upd_addrs = $this->db->prepare($q_upd_addrs);

        $query_params = array(
            ':registration_address_id' => $this->registration_address_id,
            ':residence_address_id' => $this->residence_address_id,
            ':passport_id' => $this->passport_id,
            ':email' => $this->email,
            ':birth_date' => $this->birth_date,
            ':snils' => $this->snils,
            ':agreement' => $this->agreement,
            ':id' => $this->user_id,
        );

        $q_res = $pq_upd->execute(array(
            ':phone_number' => $this->phone_number,
            ':id' => $this->id,
            ':document_scan_ext' => $this->document_scan_ext,
            ':photo_ext' => $this->photo_ext,
        ));
        if ($q_res === FALSE) throw new DBQueryException('UPDATE_ERROR' . $pq_upd->errorInfo()[2], $this->db, 'Извините, произошла ошибка');
        $q_res = $p_upd_addrs->execute($query_params);
        if ($q_res === FALSE)
            throw new DBQueryException('UPDATE_ERROR ' . $p_upd_addrs->errorInfo()[2], $this->db, 'Извините, произошла ошибка');
        return new Result(true);
    }

    //TODO ограничение на кол-во параметров заменить на лимит
    /**
     * @return Result
     * @param PDO $db
     * @param array $request
     * @throws DBQueryException
     * @throws InvalidArgumentException
     */
    public static function searchStudent(PDO $db, $request) {
        $available = array(
            'first_name', 'middle_name', 'last_name', 'birth_date', 'full_name', 'edu_speciality_name',
            'course', 'institute_name', 'institute_abbr', 'budget_form', 'edu_form_name', 'personal_file', 'sex'
        );
        $q_get = 'SELECT view_users.student_id, view_users.first_name, view_users.middle_name, view_users.last_name, view_users.sex,
              students.personal_file, view_users.course, view_users.institute_name, view_users.institute_abbr, view_users.budget_form, view_users.edu_form_name,
              view_users.edu_speciality_name,
              users.registration_address_id, view_users.edu_group_id, view_users.birth_date
            FROM view_users
              INNER JOIN users ON users.id = view_users.id
              INNER JOIN students ON students.user_id = users.id
            WHERE view_users.is_student = 1';

        $query_param = array();
        foreach ($request as $param => $value)
            if (in_array($param, $available)){
                if(is_null($value)){
                    $q_get .= " AND view_users.{$param} IS NULL";
                    break;
                }
                elseif(is_numeric($value) || $param == 'birth_date'){
                    $q_get .= " AND view_users.{$param} = :{$param}";
                }
                elseif($param == 'full_name'){
	                $q_get .= " AND CONCAT_WS(' ', view_users.last_name, view_users.first_name, view_users.middle_name) LIKE :{$param}";
	                $value = $value . '%';
                }
                else{
                    $q_get .= " AND view_users.{$param} LIKE :{$param}";
                }
                $query_param[':' . $param] = $value;
            }
	    $q_get .= ' ORDER BY view_users.last_name, view_users.first_name, view_users.middle_name';
        $p_get = $db->prepare($q_get);
        if(!count($query_param))
            throw new InvalidArgumentException('must have at least one parameter');
        $q_res = $p_get->execute($query_param);
        if ($q_res === FALSE)
            throw new DBQueryException('can not retrieve info from db', $db, 'Извините, произошла ошибка');
        $result = $p_get->fetchAll();
        if(count($result) != 0)
            foreach($result as &$row){
                $row['student_id'] = (int)$row['student_id'];
                $row['budget_form'] = (int)$row['budget_form'];
                $row['registration_address_id'] = (int)$row['registration_address_id'];
                $row['edu_group_id'] = (int)$row['edu_group_id'];
                $row['course'] = (int)$row['course'];
                $row['personal_file'] = (int)$row['personal_file'];
                $row['sex'] = (int)$row['sex'];
            }
        return new Result('true', '', $result);
    }

    public static function searchConfidentialInfoStudent(PDO $db, User $user, $params){
        if(!self::hasRights($user))
            throw new PrivilegesException('', $db);
        $available = array(
            'first_name', 'middle_name', 'last_name', 'birth_date', 'full_name', 'personal_file', 'edu_speciality_name',
            'edu_form_id', 'edu_qualification_id', 'institute_id', 'edu_specialty_id', 'course', 'sex', 'id'
        );
        $q_get = 'SELECT view_users.student_id, view_users.last_name, view_users.first_name, view_users.middle_name,
              view_users.sex,
              view_users.institute_id,
			  users.email,
			  users.birth_date,
			  users.snils,
			  users.agreement,
			  students.phone_number,

			  view_users.edu_form_id,
			  view_users.edu_form_name,
			  view_users.edu_qualification_id,
			  view_users.edu_qualification_name,
              view_users.institute_id,
              view_users.institute_abbr,
              view_users.institute_name,
              view_users.edu_specialty_id,
              view_users.edu_speciality_name,
              view_users.course,
              view_users.`group`,
              view_users.profile,
              view_users.program,
              view_users.edu_program_id,

			  users.registration_address_id,
			  reg_addr.country AS registration_country,
			  reg_addr.region AS registration_region,
			  reg_addr.region_kladr AS registration_region_kladr,
			  reg_addr.district AS registration_district,
			  reg_addr.district_kladr AS registration_district_kladr,
			  reg_addr.city AS registration_city,
			  reg_addr.city_kladr AS registration_city_kladr,
			  reg_addr.street AS registration_street,
			  reg_addr.street_kladr AS registration_street_kladr,
			  reg_addr.building AS registration_building,
			  reg_addr.building_kladr AS registration_building_kladr,
			  reg_addr.flat AS registration_flat,
			  reg_addr.postcode AS registration_postcode,
			  users.residence_address_id,
			  res_addr.country AS residence_country,
			  res_addr.region AS residence_region,
			  res_addr.region_kladr AS residence_region_kladr,
			  res_addr.district AS residence_district,
			  res_addr.district_kladr AS residence_district_kladr,
			  res_addr.city AS residence_city,
			  res_addr.city_kladr AS residence_city_kladr,
			  res_addr.street AS residence_street,
			  res_addr.street_kladr AS residence_street_kladr,
			  res_addr.building AS residence_building,
			  res_addr.building_kladr AS residence_building_kladr,
			  res_addr.flat AS residence_flat,
			  res_addr.postcode AS residence_postcode,
			  passports.id AS passport_id,
			  passports.series,
			  passports.number,
			  passports.issue_place,
			  passports.issue_place_code,
			  passports.issue_date,
			  passports.doc_type_id,
			  passports.birth_place,
			  passports.updated_at as last_update_time,
			  view_users.id as user_id,
			  students.document_scan_ext,
			  students.photo_ext
			FROM view_users
			  JOIN users ON view_users.id = users.id
			  LEFT JOIN passports ON users.passport_id = passports.id
			  LEFT JOIN addresses reg_addr ON reg_addr.id = users.registration_address_id
			  LEFT JOIN addresses res_addr ON res_addr.id = users.residence_address_id
			  JOIN students ON view_users.student_id = students.id
            WHERE view_users.is_student = 1';

        $query_param = array();
        foreach ($params as $param => $value)
            if (in_array($param, $available)) {
                if(is_null($value)){
                    $q_get .= " AND view_users.{$param} IS NULL";
                    break;
                }
                elseif(is_numeric($value) || $param == 'birth_date'){
                    $q_get .= " AND view_users.{$param} = :{$param}";
                }
                elseif($param == 'full_name'){
	                $q_get .= " AND CONCAT_WS(' ', view_users.last_name, view_users.first_name, view_users.middle_name) LIKE :{$param}";
	                $value = $value . '%';
                }
                else{
                    $q_get .= " AND view_users.{$param} LIKE :{$param}";
                }
                $query_param[':' . $param] = $value;
            }
	    $q_get .= ' ORDER BY view_users.last_name, view_users.first_name, view_users.middle_name';
        $p_get = $db->prepare($q_get);
        if(!count($query_param))
            throw new InvalidArgumentException('must have at least one parameter');
        $q_res = $p_get->execute($query_param);
        if ($q_res === FALSE)
            throw new DBQueryException('can not retrieve info from db', $db, 'Извините, произошла ошибка');
        $result = $p_get->fetchAll();
        if(count($result) != 0)
            foreach($result as  $key => &$row){
                $row['institute_id'] = (int)$row['institute_id'];
                if(!self::hasRights($user, $row['institute_id'])){
                    unset($result[$key]);
                    continue;
                }
                $row['student_id'] = (int)$row['student_id'];
                $row['sex'] = (int)$row['sex'];
                $row['agreement'] = (int)$row['agreement'];
                $row['registration_address_id'] = (int)$row['registration_address_id'];
                $row['residence_address_id'] = (int)$row['residence_address_id'];
                $row['passport_id'] = (int)$row['passport_id'];
                $row['doc_type_id'] = (int)$row['doc_type_id'];
                $row['user_id'] = (int)$row['user_id'];
                $row['edu_form_id'] = (int)$row['edu_form_id'];
                $row['edu_qualification_id'] = (int)$row['edu_qualification_id'];
                $row['edu_specialty_id'] = (int)$row['edu_specialty_id'];
                $row['course'] = (int)$row['course'];
                // нам надо, что был кладр, поэтому мы заставляем их перенабивать адрес
                if($row['registration_building_kladr'] == null){
                    $row['registration_country'] = null;
                    $row['registration_region'] = null;
                    $row['registration_district'] = null;
                    $row['registration_city'] = null;
                    $row['registration_street'] = null;
                    $row['registration_building'] = null;
                    $row['registration_flat'] = null;
                    $row['registration_postcode'] = null;
                }
                if($row['residence_building_kladr'] == null){
                    $row['residence_country'] = null;
                    $row['residence_region'] = null;
                    $row['residence_district'] = null;
                    $row['residence_city'] = null;
                    $row['residence_street'] = null;
                    $row['residence_building'] = null;
                    $row['residence_flat'] = null;
                    $row['residence_postcode'] = null;
                }
            }

        return new Result(true, '', $result);
    }

    public static function getModuleName(){
        return MODULE_NAME;
    }

    private function setDocumentScan($file, $file_name) {
	    if (!isset($file_name) || $file_name == ''){
            return;
	    }else{
		    $file_name_parts = explode('.', $file_name);
		    $this->document_scan_ext = end($file_name_parts);
	    }

	    $file = explode(',', $file);
        if (count($file) != 2){
            return;
        }
	    $file = $file[1];
	    if ($this->document_scan_ext != '' && $file){
		    global $ROOT_PATH;
		    $new_file_name = $ROOT_PATH . '/images/passports/' . $this->getUserId() . '.' . $this->document_scan_ext;
		    file_put_contents($new_file_name, base64_decode($file));
	    }
    }

    private function setPhoto($file, $file_name) {
	    if (!isset($file_name) || $file_name == ''){
            return;
	    }else{
		    $file_name_parts = explode('.', $file_name);
		    $this->photo_ext = end($file_name_parts);
	    }

	    $file = explode(',', $file);
        if (count($file) != 2){
            return;
        }
	    $file = $file[1];
	    if ($this->photo_ext != '' && $file){
		    global $ROOT_PATH;
		    $new_file_name = $ROOT_PATH . '/images/student_photos/' . $this->getUserId() . '.' . $this->photo_ext;
		    file_put_contents($new_file_name, base64_decode($file));
	    }
    }
}
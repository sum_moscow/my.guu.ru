<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 21.04.2015
 * Time: 20:16
 */

global $ROOT_PATH;
global $__class_name;
global $__db;
require_once 'Class.StudentObject.php';

$__modules["{$__class_name}"] = array(
    'GET' => array(
        '{/search}' => function () use ($__db, $__request){
            return StudentObject::searchStudent($__db, $__request);
        },
        '{/(id:[0-9]+)}' => function ($id) use ($__db){
            $id = (int)$id;
            $student = new StudentObject($id, $__db);
            return $student->getInfo();
        },
//        'address/(id:[0-9]+)' => function ($id) use ($__db){
//
//        }
    ),
    'POST' => array(
        '{/(id:[0-9]+)/phone_number}' => function ($id) use ($__db, $__request, $__user){
            $id = (int)$id;
            $student = new StudentObject($id, $__db);
            return $student->setPhoneNumber($__request['phone_number'], $__user);
        },
        '{/(id:[0-9]+)}' => function ($id) use ($__db, $__request, $__user){
            $id = (int)$id;
            $student = new StudentObject($id, $__db);
            return $student->setInfo($__request['payload'], $__user);
        },
    )
);
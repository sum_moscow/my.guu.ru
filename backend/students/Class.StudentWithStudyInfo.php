<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 05.08.2015
 * Time: 13:51
 */

require_once 'Class.AbstractStudent.php';
//TODO отнаследоваться от Student
class StudentWithStudyInfo extends AbstractStudent{
    private $id;
    private $user_id;
    private $last_name;
    private $first_name;
    private $middle_name;
    private $sex;
    private $phone_number;
    private $institute_name;
    private $institute_abbr;
    private $budget_form;
    private $edu_form_name;
    private $course;
    private $year;
    private $term;
    private $year_end;

    public function __construct($id, $user_id, $last_name, $first_name, $middle_name, $sex, $phone_number,
                                $institute_name, $institute_abbr, $budget_form, $edu_form_name,
                                $course, $year, $term, $year_end){
        $this->id = $id ? (int)$id : null;
        $this->user_id = $user_id ? (int)$user_id : null;
        $this->last_name = $last_name;
        $this->first_name = $first_name;
        $this->middle_name = $middle_name;
        $this->sex = (int)$sex;
        $this->phone_number = $phone_number;
        $this->institute_name = $institute_name;
        $this->institute_abbr = $institute_abbr;
        $this->budget_form = $budget_form;
        $this->edu_form_name = $edu_form_name;
        $this->course = $course ? (int)$course : null;
        $this->year = $year ? (int)$year : null;
        $this->term = $term ? (int)$term : null;
        $this->year_end = $year_end ? (int)$year_end : null;
    }
    public function getId(){
        return $this->id;
    }

    public function getUserId(){
        return $this->user_id;
    }

    public function getInfo(){
        return array(
            'id' => $this->id,
            'user_id' => $this->user_id,
            'last_name' => $this->last_name,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'sex' => $this->sex,
            'phone_number' => $this->phone_number,
            'institute_name' => $this->institute_name,
            'institute_abbr' => $this->institute_abbr,
            'budget_form' => $this->budget_form,
            'edu_form_name' => $this->edu_form_name,
            'course' => $this->course,
            'year' => $this->year,
            'term' => $this->term,
            'year_end' => $this->year_end,
        );
    }
    public static function create($id, stdClass $data){
        return new StudentWithStudyInfo(
            $id,
            $data->user_id,
            $data->last_name,
            $data->first_name,
            $data->middle_name,
            $data->sex,
            $data->phone_number,
            $data->institute_name,
            $data->institute_abbr,
            $data->budget_form,
            $data->edu_form_name,
            $data->course,
            $data->year,
            $data->term,
            $data->year_end
        );
    }
} 
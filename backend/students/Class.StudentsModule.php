<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 04.08.2015
 * Time: 18:11
 */

require_once 'Class.AbstractStudent.php';
require_once 'Class.StudentWithStudyInfo.php';

class StudentsModule extends AbstractModule{
    //public static function selectAddress(PDO $db, $id){
    //    $q_get_address = 'SELECT id, country, region, district, city, street, building, flat, postcode, kladr
    //                        FROM addresses
    //                        WHERE id = :id';
    //    $p_get_address = $db->prepare($q_get_address);
    //    $q_res = $p
    //}
    public static function selectStudentWithStudyInfo(PDO $db, $id){
        $q_get_student = 'SELECT id, user_id, last_name, first_name, middle_name, sex, phone_number,
                            institute_name, institute_abbr, budget_form,
                            edu_form_name, course, `year`, term, year_end
                          FROM view_students
                          WHERE id = :id';
        $p_get_student = $db->prepare($q_get_student);
        $q_res = $p_get_student->execute(array(':id' => $id));
        if (!$q_res) throw new DBQueryException('DB_ERROR', $db, 'Повторите позже');
        if (!$res = $p_get_student->fetchObject()) throw new InvalidArgumentException('No student with such id');
        $student = StudentWithStudyInfo::create($id, $res);
        return $student;
    }
} 
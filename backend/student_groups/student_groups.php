<?php
global $ROOT_PATH;
global $__class_name;
global $__db;
require_once 'Class.StudentGroup.php';

$__modules["{$__class_name}"] = array(
	'GET' => array(
        '{/(group_id:[0-9]+)/students}' => function ($group_id) use ($__user, $__db) {
            $group = new StudentGroup($group_id, $__db);
            return $group->getStudentsList();
        },
        '{/(group_id:[0-9]+)/errors}' => function($id) use ($__db, $__user){
            $group = new StudentGroup($id, $__db);
            return $group->getErrorRequest($__user->getStaffInstance());
        },
        '{/error_statuses}' => function() use ($__db){
            return StudentGroup::getErrorRequestStatuses($__db);
        },
        '{/(group_id:[0-9]+)}' => function ($group_id) use ($__db) {
            $group = new StudentGroup($group_id, $__db);
            return $group->getInfo();
        },
        '{/my}' => function() use ($__user, $__db, $__request){
            return StudentGroup::getStudentGroupsWithParams($__db, $__user->getStaffInstance(), $__request);
        },
	),
	'POST' => array(
        '{/(group_id:[0-9]+)/errors}' => function($id) use ($__db, $__request, $__user){
            $group = new StudentGroup($id, $__db);
            return $group->addErrorRequest($__request, $__user->getStaffInstance());
        },
	),
);

<?php

global $ROOT_PATH;
require_once $ROOT_PATH . '/backend/core/kaf/Class.AcademicDepartment.php';
require_once $ROOT_PATH . '/backend/institutes/Class.Institute.php';

class StudentGroup extends AbstractModule{

    const MODULE_NAME = 'student_groups';
    const MIN_LEVEL_FOR_READ = 5;
    const MIN_LEVEL_FOR_UPDATE = 5;

    protected $id;
    private $profile;
    private $program;
    private $edu_speciality_name;
    private $academic_department_id;
//    private $shift;
    private $course;
    private $name;
    private $number;
    private $institute_id;
    private $institute;
    private $academic_department;

    protected $db;


    /**
     * @param $group_id
     * @param PDO $db
     * @throws DBQueryException
     */
    public function __construct($group_id, PDO $db) {
        $q_get_st_group = 'SELECT view_edu_groups.*
				FROM view_edu_groups
				WHERE view_edu_groups.edu_group_id=:edu_group_id';
        $p_get = $db->prepare($q_get_st_group);
        $q_res = $p_get->execute(array(':edu_group_id' => $group_id));
        if ($q_res === FALSE) throw new DBQueryException('Can not retrieve info about student group', $db);

        if (!$r_st_group = $p_get->fetch()) throw new InvalidArgumentException("no student group with id {$group_id}");
        $this->db = $db;
        $this->id = (int)$r_st_group['edu_group_id'];
        $this->program = $r_st_group['program'];
        $this->profile = $r_st_group['profile'];
        $this->edu_speciality_name = $r_st_group['edu_speciality_name'];
        $this->number = $r_st_group['group'];
        $this->academic_department_id = (int) $r_st_group['academic_department_id'];
        $this->institute_id = (int) $r_st_group['institute_id'];
        $this->course = (int)$r_st_group['course'];
        $this->name = $this->program != '' ? $this->program : ($this->profile != '' ? $this->profile : $this->edu_speciality_name);
    }

//    /**
//     * @return mixed
//     */
//    public function getShift() {
//        return $this->shift;
//    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return Institute
     */
    public function getInstitute() {
        if ($this->institute instanceof Institute) {
            return $this->institute;
        }
        $this->institute = new Institute($this->institute_id, $this->db);
        return $this->institute;
    }
    public function getInstituteId(){
        return $this->institute_id;
    }

    /**
     * @return AcademicDepartment
     */
    private function getAcademicDepartment() {
        if ($this->academic_department instanceof AcademicDepartment) {
            return $this->academic_department;
        }
        $this->academic_department = new AcademicDepartment($this->academic_department_id, $this->db);
        return $this->academic_department;
    }

    /**
     * @return Result
     */
    public function getInfo() {
        return new Result(true, '', array(
            'name' => $this->name,
            'number' => $this->number,
            'academic_department_id' => (int)$this->academic_department_id,
//            'shift' => $this->shift,
            'course' => (int)$this->course,
            'institute_name' => $this->getInstitute()->getName(),
            'academic_department_name' => $this->getAcademicDepartment()->getName()
        ));
    }

    /**
     * @return Result
     * @throws DBQueryException
     */
    public function getStudentsList() {
        $q_get_students = 'SELECT last_name, first_name, middle_name, id as user_id, student_id
			FROM view_users
			WHERE view_users.edu_group_id = :edu_group_id
			ORDER BY last_name, first_name
		';
        $p_st = $this->db->prepare($q_get_students);
        $q_res = $p_st->execute(array(
            ':edu_group_id' => $this->getId()
        ));
        if ($q_res === FALSE) throw new DBQueryException('', $this->db);
        $result = $p_st->fetchAll();
        foreach ($result as &$row){
            $row['user_id'] = (int)$row['user_id'];
            $row['student_id'] = (int)$row['student_id'];
        }
        return new Result(true, '', $result);
    }

    /**
     * Get students list with passport and registration address info
     * @param User $user
     * @return Result
     * @throws DBQueryException
     * @throws PrivilegesException
     */
    public function getConfidentialInfoStudentsList(User $user) {
        if(!$this->hasRights($user, $this->institute_id))
            throw new PrivilegesException('Not enough rights to access this information', $this->db);

        $q_get_students = 'SELECT view_users.student_id, view_users.last_name, view_users.first_name, view_users.middle_name,
                              users.email,
                              users.birth_date,
                              users.snils,
                              users.agreement,
                              students.phone_number,

                              users.registration_address_id,
                              reg_addr.country AS registration_country,
                              reg_addr.region AS registration_region,
                              reg_addr.region_kladr AS registration_region_kladr,
                              reg_addr.district AS registration_district,
                              reg_addr.district_kladr AS registration_district_kladr,
                              reg_addr.city AS registration_city,
                              reg_addr.city_kladr AS registration_city_kladr,
                              reg_addr.street AS registration_street,
                              reg_addr.street_kladr AS registration_street_kladr,
                              reg_addr.building AS registration_building,
                              reg_addr.building_kladr AS registration_building_kladr,
                              reg_addr.flat AS registration_flat,
                              reg_addr.postcode AS registration_postcode,
                              users.residence_address_id,
                              res_addr.country AS residence_country,
                              res_addr.region AS residence_region,
                              res_addr.region_kladr AS residence_region_kladr,
                              res_addr.district AS residence_district,
                              res_addr.district_kladr AS residence_district_kladr,
                              res_addr.city AS residence_city,
                              res_addr.city_kladr AS residence_city_kladr,
                              res_addr.street AS residence_street,
                              res_addr.street_kladr AS residence_street_kladr,
                              res_addr.building AS residence_building,
                              res_addr.building_kladr AS residence_building_kladr,
                              res_addr.flat AS residence_flat,
                              res_addr.postcode AS residence_postcode,

                              passports.id AS passport_id,
                              passports.series,
                              passports.number,
                              passports.issue_place,
                              passports.issue_place_code,
                              passports.issue_date,
                              passports.doc_type_id,
                              passports.updated_at as last_update_time,
                              view_users.id as user_id,
                              view_users.course,
                              view_users.`group`,
                              view_users.edu_speciality_name,
                              view_users.profile,
                              view_users.program,
                              view_users.institute_abbr
                            FROM view_users
                              JOIN users ON view_users.id = users.id
                              LEFT JOIN passports ON users.passport_id = passports.id
                              LEFT JOIN addresses reg_addr ON reg_addr.id = users.registration_address_id
                              LEFT JOIN addresses res_addr ON res_addr.id = users.residence_address_id
                              JOIN students ON view_users.student_id = students.id
                            WHERE view_users.edu_group_id = :edu_group_id
                            ORDER BY last_name, first_name, middle_name';
        $p_get_students = $this->db->prepare($q_get_students);
        $q_res = $p_get_students->execute(array(
            ':edu_group_id' => $this->getId()
        ));
        if ($q_res === FALSE) throw new DBQueryException('Can not retrieve student information: ' . $p_get_students->errorInfo()[2], $this->db);
        $result = $p_get_students->fetchAll();
        foreach ($result as &$row){
            $row['student_id'] = (int)$row['student_id'];
            $row['agreement'] = (int)$row['agreement'];
            $row['registration_address_id'] = (int)$row['registration_address_id'];
            $row['residence_address_id'] = (int)$row['residence_address_id'];
            $row['passport_id'] = (int)$row['passport_id'];
            $row['doc_type_id'] = (int)$row['doc_type_id'];
            $row['user_id'] = (int)$row['user_id'];

            // нам надо, что был кладр, поэтому мы заставляем их перенабивать адрес
            if($row['registration_building_kladr']){
                $row['registration_country'] = null;
                $row['registration_region'] = null;
                $row['registration_district'] = null;
                $row['registration_city'] = null;
                $row['registration_street'] = null;
                $row['registration_building'] = null;
                $row['registration_flat'] = null;
                $row['registration_postcode'] = null;
            }
            if($row['residence_building_kladr']){
                $row['residence_country'] = null;
                $row['residence_region'] = null;
                $row['residence_district'] = null;
                $row['residence_city'] = null;
                $row['residence_street'] = null;
                $row['residence_building'] = null;
                $row['residence_flat'] = null;
                $row['residence_postcode'] = null;
            }
        }
        return new Result(true, '', $result);
    }


    public static function getStudentGroupsWithParams(PDO $db, AbstractUser $user, array $params){
        $q_get_groups = 'SELECT DISTINCT edu_form_id, edu_form_name, edu_qualification_id, edu_qualification_name,
        institute_id, institute_abbr, institute_name, edu_speciality_name, course FROM view_edu_groups';
        $query_params = array();
        $first = true;


        if(!Institute::hasEditorRights($user)){
            $staff = $user instanceof User ? $user->getStaffInstance() : null;
            if(!$staff)
                throw new PrivilegesException('вы не сотрудник', $db);
            if(Institute::hasRights($user, $staff->getInstituteId())){
                $query_params[':institute_id'] = $staff->getInstituteId();
                $q_get_groups .= ' WHERE institute_id = :institute_id';
                $first = false;
            }
            else
                throw new PrivilegesException('недостаточно прав', $db);
        }
        $search_params = array();
        if(array_key_exists('edu_forms-id', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':edu_form_id'] = $params['edu_forms-id'];
            $q_get_groups .= ' edu_form_id = :edu_form_id';
            $search_params['edu_form_id'] = $params['edu_forms-id'];
        }
        if(array_key_exists('edu_qualifications-id', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':edu_qualification_id'] = $params['edu_qualifications-id'];
            $q_get_groups .= ' edu_qualification_id = :edu_qualification_id';
            $search_params['edu_qualification_id'] = $params['edu_qualifications-id'];
        }
        if(array_key_exists('institutes-id', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':institute_id'] = $params['institutes-id'];
            $q_get_groups .= ' institute_id = :institute_id';
            $search_params['institute_id'] = $params['institutes-id'];
        }
        /*if(array_key_exists('edu_specialties-id', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':edu_specialty_id'] = $params['edu_specialties-id'];
            $q_get_groups .= ' edu_specialty_id = :edu_specialty_id';
            $search_params['edu_specialty_id'] = $params['edu_specialty_id'];
        }*/
        if(array_key_exists('edu_specialties-name', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':edu_speciality_name'] = $params['edu_specialties-name'];
            $q_get_groups .= ' edu_speciality_name LIKE :edu_speciality_name';
            $search_params['edu_speciality_name'] = $params['edu_specialties-name'];
        }
        if(array_key_exists('courses-id', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':course'] = $params['courses-id'];
            $q_get_groups .= ' course = :course';
            $search_params['course'] = $params['courses-id'];
        }
        $q_get_groups .= ' ORDER BY institute_abbr, edu_form_name, edu_qualification_name, edu_speciality_name, course';
        $p_get_groups = $db->prepare($q_get_groups);
        $q_res = $p_get_groups->execute($query_params);

        if ($q_res === FALSE) throw new DBQueryException('Can not retrieve student group params: ' . $p_get_groups->errorInfo()[2], $db);
        $res = $p_get_groups->fetchAll();
        $forms = array();
        $qualifications = array();
        $institutes = array();
        $specialties = array();
        $courses = array();
        foreach($res as &$row){
            $form = array(
                'id' => (int)$row['edu_form_id'],
                'name' => $row['edu_form_name'],
            );
            $qualification = array(
                'id' => (int)$row['edu_qualification_id'],
                'name' => $row['edu_qualification_name'],
            );
            $institute = array(
                'id' => (int)$row['institute_id'],
                'name' => $row['institute_name'],
                'abbr' => $row['institute_abbr'],
            );
            $specialty = array(
                'name' => $row['edu_speciality_name']
            );
            $course = array(
                'id' => (int) $row['course'],
                'name' => $row['course']
            );
            if(!in_array($form, $forms))
                $forms[] = $form;
            if(!in_array($qualification, $qualifications))
                $qualifications[] = $qualification;
            if(!in_array($institute, $institutes))
                $institutes[] = $institute;
            if(!in_array($specialty, $specialties))
                $specialties[] = $specialty;
            if(!in_array($course, $courses))
                $courses[] = $course;
        }
        $res = array(
            'edu_forms' => $forms,
            'edu_qualifications' => $qualifications,
            'institutes' => $institutes,
            'edu_specialties' => $specialties,
            'courses' => $courses,
            'groups' => StudentGroup::searchStudentsGroups($db, $search_params)->getData()
        );
        return new Result(true, '', $res);
    }

    /**
     * @return Result
     * @throws DBQueryException
     */
    public function getSubjects() {
        $q_get_sub = 'SELECT DISTINCT subject_name, subject_id
				FROM view_timetable
				WHERE edu_group_id = :edu_group_id';
        $p_subs = $this->db->prepare($q_get_sub);
        $q_res = $p_subs->execute(array(
            ':edu_group_id' => $this->id
        ));
        if ($q_res === FALSE) throw new DBQueryException('', $this->db);

        return new Result(true, '', $p_subs->fetchAll());
    }

    /** Checking rights
     * Return true if the user has rights in module students or he is a super admin
     * @param User $user
     * @param $institute_id
     * @return bool
     */
    protected function hasRights(User $user, $institute_id = null){
        if($user->hasRights(array('module' => self::getModuleName(), 'min_level' => self::MIN_LEVEL_FOR_READ))
            || Institute::hasRights($user, $institute_id))
            return true;
        else
            return false;
    }

    public static function searchStudentsGroups(PDO $db, $params) {
    //    if(!self::hasRights($user))
    //        throw new PrivilegesException('Not enough rights to access this information', $db);
        $query_params = array();
        $first = true;
        $q_get_groups = 'SELECT edu_group_id as id,
                            edu_speciality_name,
                            edu_qualification_name,
                            edu_form_name,
                            course,
                            view_edu_groups.group,
                            view_edu_groups.year,
                            institute_abbr,
                            institute_name,
                            semester,
                            program,
                            `profile`
                          FROM view_edu_groups';
        if(array_key_exists('edu_form_id', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':edu_form_id'] = $params['edu_form_id'];
            $q_get_groups .= ' edu_form_id = :edu_form_id';
        }
        if(array_key_exists('edu_qualification_id', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':edu_qualification_id'] = $params['edu_qualification_id'];
            $q_get_groups .= ' edu_qualification_id = :edu_qualification_id';
        }
        if(array_key_exists('institute_id', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':institute_id'] = $params['institute_id'];
            $q_get_groups .= ' institute_id = :institute_id';
        }
        if(array_key_exists('edu_specialty_id', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':edu_specialty_id'] = $params['edu_specialty_id'];
            $q_get_groups .= ' edu_specialty_id = :edu_specialty_id';
        }
        if(array_key_exists('edu_speciality_name', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':edu_speciality_name'] = $params['edu_speciality_name'];
            $q_get_groups .= ' edu_speciality_name LIKE :edu_speciality_name';
        }
        if(array_key_exists('course', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':course'] = $params['course'];
            $q_get_groups .= ' course = :course';
        }
        $q_get_groups .= ' ORDER BY institute_id, edu_specialty_id';
        $p_get_groups = $db->prepare($q_get_groups);
        $q_res = $p_get_groups->execute($query_params);

        if ($q_res === FALSE) throw new DBQueryException('Can not retrieve students groups: ' . $p_get_groups->errorInfo()[2], $db);
        $res = $p_get_groups->fetchAll();
        foreach($res as &$row){
            $row['id'] = (int)$row['id'];
            $row['course'] = (int)$row['course'];
            $row['group'] = (int)$row['group'];
            $row['year'] = (int)$row['year'];
            $row['semester'] = (int)$row['semester'];
            $row['year'] = (int)$row['year'];
        }
        return new Result(true, '', $res);
    }

    /**
     * @param $params
     * @param User $user
     * @return Result
     * @throws DBQueryException
     * @throws PrivilegesException
     */
    public function addErrorRequest($params, User $user) {
        if(!$this->hasRights($user))
            throw new PrivilegesException('Not enough rights to access this information', $this->db);

        $q_ins_req = 'INSERT INTO edu_groups_errors(from_user_id, edu_group_id, description, status_id, created_at)
				VALUES (:from_user_id, :edu_group_id, :description, 1, NOW())';

        $p_ins = $this->db->prepare($q_ins_req);
        $q_res = $p_ins->execute(array(
            ':from_user_id' => $user->getId(),
            ':edu_group_id' => $this->id,
            ':description' => $params['description'],
        ));
        if ($q_res === FALSE) throw new DBQueryException('Can not insert request', $this->db);
        return new Result(true, 'Заявка успешно отправлена. Скоро она будет обработана и появится в списке заявок на исправление');
    }

    public function getErrorRequest(User $user){
        if(!$this->hasRights($user))
            throw new PrivilegesException('Not enough rights to access this information', $this->db);
        $q_get = 'SELECT * FROM edu_groups_errors WHERE edu_group_id = :id';
        $p_get = $this->db->prepare($q_get);
        $q_res = $p_get->execute(array(
            ':id' => $this->id,
        ));
        if ($q_res === FALSE) throw new DBQueryException('Can not retrieve request from db', $this->db);
        $data = $p_get->fetchAll();
        if(count($data) != 0)
            foreach($data as &$request){
                $request['id'] = (int)$request['id'];
                $request['edu_group_id'] = (int)$request['edu_group_id'];
                $request['from_user_id'] = (int)$request['from_user_id'];
                $request['status_id'] = (int)$request['status_id'];
            }
        return new Result(true, '', $data);
    }
    public static function getErrorRequestStatuses(PDO $db){
        $q_get = 'SELECT * FROM edu_groups_errors_statuses';
        $p_get = $db->prepare($q_get);
        $q_res = $p_get->execute();
        if ($q_res === FALSE) throw new DBQueryException('Can not retrieve statuses from db', $db);
        $data = $p_get->fetchAll();
        if(count($data) != 0)
            foreach($data as &$status){
                $status['id'] = (int)$status['id'];
            }
        return new Result(true, '', $data);
    }

    public static function getModuleName() {
        return self::MODULE_NAME;
    }
}
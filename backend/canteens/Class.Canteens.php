<?php

class Canteens{


	public static function updateMenu(PDO $__db){

		$__db->query('INSERT INTO sync_canteens(start_time, created_at, items_added)
			VALUES (NOW(), NOW(), 0)');
		$canteen_update_id = $__db->lastInsertId();

		global $ROOT_PATH;
		$file_name = $ROOT_PATH.'/backend/canteens/menu.csv';
		if (!move_uploaded_file($_FILES['file']['tmp_name'], $file_name)){
			throw new RuntimeException('Failed to move uploaded file.');
		}else{
			$file = fopen($file_name,"r");
		}

		/*CANTEENS*/
		$p_ins_canteens = $__db->prepare('INSERT INTO canteens(canteen_name)
			VALUES (:canteen_name)
			ON DUPLICATE KEY UPDATE status = 1');

		$p_canteens_id = $__db->prepare('SELECT canteens.id
			FROM canteens
			WHERE canteen_name = :canteen_name');

		/*GROUPS*/
		$p_ins_groups = $__db->prepare('INSERT INTO canteens_meal_groups(name)
			VALUES (:meal_group_name)
			ON DUPLICATE KEY UPDATE status = 1');

		$p_group_id = $__db->prepare('SELECT canteens_meal_groups.id
			FROM canteens_meal_groups
			WHERE name = :meal_group_name');

		/*MEALS*/
		$p_ins_meals = $__db->prepare('INSERT INTO canteens_meals(name)
			VALUES (:meal_name)
			ON DUPLICATE KEY UPDATE status = 1');

		$p_meal_id = $__db->prepare('SELECT canteens_meals.id
			FROM canteens_meals
			WHERE name = :meal_name');


		$p_ins_exists = $__db->prepare('INSERT INTO canteens_exist_meals(canteen_id, meal_group_id, meal_id, status)
			VALUES (:canteen_id, :meal_group_id, :meal_id, 1)');


		$p_exist_id = $__db->prepare('SELECT id FROM canteens_exist_meals
				WHERE canteen_id = :canteen_id
				AND meal_group_id = :meal_group_id
				AND meal_id = :meal_id');

		$p_ins_meal_price = $__db->prepare('INSERT INTO canteens_meals_prices(canteen_exist_id, canteen_update_id, price)
			VALUES (:canteen_exist_id, :canteen_update_id, :price)');

		/*LOGS*/
		$meals_count = 0;


		/* Set all to 0*/
		try {
			$__db->beginTransaction();
			$__db->query('UPDATE canteens SET status = 0');
			$__db->query('UPDATE canteens_meal_groups SET status = 0');
			$__db->query('UPDATE canteens_meals SET status = 0');
			$__db->query('UPDATE canteens_exist_meals SET status = 0');
			$__db->query('UPDATE canteens_meals_prices SET status = 0');
			$__db->commit();
		}catch(Exception $e){
			$__db->rollBack();
			exit ('Ошибка обновления!');
		}

		while(! feof($file)) {
			$meals_count++;
			$a = fgetcsv($file, 0, '|');

			if (!isset($a[1]) || !isset($a[2]) || !isset($a[3])){
				continue;
			}
			$a[1] = mb_convert_encoding($a[1], "utf-8", "windows-1251");
			$a[2] = mb_convert_encoding($a[2], "utf-8", "windows-1251");
			$a[3] = mb_convert_encoding($a[3], "utf-8", "windows-1251");
			/*CANTEEN*/
			$canteen = array(':canteen_name' => $a[1]);
			$canteen_id = null;

			$p_ins_canteens->execute($canteen);
			$p_canteens_id->execute($canteen);
			$r_canteen = $p_canteens_id->fetch();
			$canteen_id = $r_canteen['canteen_id'];


			/* MEAL GROUP */

			$meal_group = array(':meal_group_name' => $a[2]);
			$group_id = null;

			$p_ins_groups->execute($meal_group);
			$p_group_id->execute($meal_group);
			$r_group = $p_group_id->fetch();
			$group_id = $r_group['meal_group_id'];

			/* MEAL */

			$meal = array(':meal_name' => $a[3]);
			$meal_id = null;

			$p_ins_meals->execute($meal);
			$p_meal_id->execute($meal);
			$r_meal = $p_meal_id->fetch();
			$meal_id = $r_meal['meal_id'];


			$exists = array(
				':canteen_id' => $canteen_id,
				':meal_group_id' => $group_id,
				':meal_id' => $meal_id);

			$p_ins_exists->execute($exists);
			$p_exist_id->execute($exists);
			$r_exist = $p_exist_id->fetch();
			$meal_exist_id = $r_exist['canteen_exist_id'];

			$p_ins_meal_price->execute(array(
				':canteen_exist_id' => $meal_exist_id,
				':canteen_update_id' => $canteen_update_id,
				':price' => $a[4]
			));
		}
		fclose($file);

		$p_upd_update = $__db->prepare('UPDATE canteen_updates
			SET items_added = :items_added,
			finish_time = NOW()
			WHERE id = :canteen_update_id');
		$p_upd_update->execute(array(
			':items_added' => $meals_count,
			':canteen_update_id' => $canteen_update_id
		));

		return new Result(true, 'Данные успешно обновлены');
	}

	public static function getLastUpdateTime(PDO $__db){
		$q_upd_time = 'SELECT start_time
				FROM canteen_updates
				ORDER BY id
				DESC LIMIT 1';
		$result = $__db->query($q_upd_time);
		$result = $result->fetch();
		return $result['start_time'];
	}

}
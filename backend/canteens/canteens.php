<?php

require_once 'Class.Canteens.php';
require_once 'Class.Canteen.php';


$__modules['canteens'] = array(
	'POST' => array(
		'update' => function() use ($__db, $__request, $__ip) {
			if ($__ip != SUM::$CANTEENS_SERVER_IP  && !true){
				throw new InvalidArgumentException('Чего эт вам надо, ааа?');
			}else{
				return Canteens::updateMenu($__db);
			}
		},
	),
	'GET' => array(
		'{id:[0-9]+}' => function($id, $act = null, $group_id = null, $meal = null, $meal_id = null) use ($__db, $__user) {
			if ($act == null){
				$can = new Canteen($id, $__db);
				return $can->getInfo();
			}elseif ($act == 'menu'){
				$can = new Canteen($id, $__db);
				return $can->getFullMenu();
			}elseif ($act == 'meal_group'){
				$can = new Canteen($id, $__db);
				$result = $can->getMealsInGroup($group_id);
				$result->addProp('canteen', $can->getInfo()->getData());
				return $result;
			}elseif ($act == 'meal'){
				$can = new Meal($id, $__db);
				$can->getMeal($group_id);
			}
		}
	)
);
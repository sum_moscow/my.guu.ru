<?php

global $ROOT_PATH;
global $__class_name;
global $__db;
require_once 'Class.Institute.php';
require_once $ROOT_PATH . '/backend/student_groups/Class.StudentGroup.php';


$__modules["{$__class_name}"] = array(
    'GET' => array(
        '{/(id:[0-9]+)/student_groups/(group_id:[0-9]+)/students}' => function ($id, $group_id) use ($__user, $__db) {
            $group = new StudentGroup($group_id, $__db);
            return $group->getConfidentialInfoStudentsList($__user);
        },
        '{/(id:[0-9]+)/student_groups/(group_id:[0-9]+)}' => function ($id, $group_id) use ($__user, $__db) {
            if (!$__user->getStaffInstance()->getInstituteId() == $id
                && !Institute::hasRights($__user, $id))
                throw new PrivilegesException('Not enough rights to access this information', $__db);

            $group = new StudentGroup($group_id, $__db);
            return $group->getInfo();
        },
        '{/(id:[0-9]+)/student_groups}' => function ($id) use ($__user, $__db, $__request) {
                $institute = new Institute($id, $__db);
                return $institute->getStudentsGroups($__user, $__request);
        },
        '{/my/student_groups/(id:[0-9]+)/students}' => function ($group_id) use ($__user, $__db) {
            $group = new StudentGroup($group_id, $__db);
            return $group->getConfidentialInfoStudentsList($__user);
        },
        '{/my/student_groups/(id:[0-9]+)}' => function ($group_id) use ($__user, $__db) {
            $group = new StudentGroup($group_id, $__db);
            return $group->getInfo();
        },
        '{/my/student_groups}' => function () use ($__user, $__db, $__request){
            $institute_id = $__user->getStaffInstance()->getInstituteId();
            if (Institute::hasEditorRights($__user)){
                return Institute::getAllStudentsGroups($__user, $__db, $__request);
            }elseif ($institute_id != null){
                $institute = new Institute($institute_id, $__db);
                return $institute->getStudentsGroups($__user, $__request);
            }else
                throw new PrivilegesException('',$__db);
        },
//        '{/my/student_groups/(id:[0-9]+)/students}' => function ($group_id) use ($__user, $__db, $__args) {
//            $student_group = new StudentGroup($group_id, $__db);
//            $institute_id = $student_group->getInstitute()->getId();
//            if (!$institute_id == $__user->getStaffInstance()->getInstituteId()
//                && !$__user->hasRights(array('module' => 'institute', 'min_level' => $__user::SUPER_ADMIN_LEVEL))
//            )
//                throw new PrivilegesException('Not enough rights to access this information', $__db);
//            return $student_group->getConfidentialInfoStudentsList($__user);
//        },
    ),
    //DEPRECATED
    //Moved to students module
    /*'POST' => array(
        'my' => function() use ($__user, $__db, $__args, $__pg_db, $__request){
            if ($__args[0] == 'student'){
                $institute = new Institute($__user->getStaffInstance()->getInstituteId(), $__db);
                return $institute->updateStudentsPassport($__request, $__pg_db);
            }elseif ($__args[0] == 'error'){
                $institute = new Institute($__user->getStaffInstance()->getInstituteId(), $__db);
                return $institute->addErrorRequest($__request);
            }
        },
        'error' => function($__user, $__db, $__args, $__pg_db, $__request){
        }
    )*/
);
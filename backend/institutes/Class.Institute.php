<?php

/**
 * Created by PhpStorm.
 * User: Инал
 * Date: 16.10.2014
 * Time: 13:38
 */
class Institute extends AbstractModule {

    const MODULE_NAME = 'institutes';
    const MIN_LEVEL_FOR_READ = 10;
    const MIN_LEVEL_FOR_READ_ALL = 15;
    const MIN_LEVEL_FOR_WRITE = 15;

    private $id;
    private $code;
    private $name;
    private $abbr;
    private $ou_id;
    private $soccardXML;
    private $created_at;
    private $updated_at;


    /**
     * @param $id
     * @param PDO $db
     * @throws DBQueryException
     * @throws InvalidArgumentException
     */
    public function __construct($id, PDO $db) {
        $this->db = $db;
        $this->id = (int)$id;
        $q_get_institute = 'SELECT institutes.*
				FROM  institutes
				WHERE institutes.id = :id';
        $p_get_institute = $db->prepare($q_get_institute);
        $q_res = $p_get_institute->execute(array(
            ':id' => $id
        ));

        if ($q_res === FALSE) throw new DBQueryException('Can not retrieve institute information', $db);
        if (!$result = $p_get_institute->fetch()) throw new InvalidArgumentException("No institute with id {$id}");

        $this->code = (int)$result['code'];
        $this->name = $result['name'];
        $this->abbr = $result['abbr'];
        $this->ou_id = (int)$result['ou_id'];
        $this->soccardXML = $result['soccardXML'];
        $this->created_at = (int)$result['created_at'];
        $this->updated_at = $result['updated_at'];
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get all information about institute
     * @return Result
     */
    public function getInfo() {
        return new Result(true, '', array(
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'abbr' => $this->abbr,
            'ou_id' => $this->ou_id,
            'soccardXML' => $this->soccardXML,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ));
    }

    /**
     * Get list of students groups in institute if user has enough rights
     * @param AbstractUser $user
     * @param $params
     * @throws PrivilegesException
     * @throws DBQueryException
     * @throws LogicException
     * @return Result
     */
    public function getStudentsGroups(AbstractUser $user, array $params = null) {
        if(!$this->hasRights($user, $this->id))
            throw new PrivilegesException('Not enough rights to access this information', $this->db);
        $query_params = array(
            ':institute_id' => $this->id,
        );
        $q_get_groups = 'SELECT edu_group_id as id, edu_group_id,
                            edu_speciality_name,
                            edu_qualification_name,
                            edu_form_name,
                            course,
                            view_edu_groups.group,
                            view_edu_groups.year,
                            semester,
                            program,
                            profile,
                            institute_name,
                            institute_abbr
                          FROM view_edu_groups
                          WHERE view_edu_groups.institute_id = :institute_id';
        if(!is_null($params)){
            if(array_key_exists('edu_qualification_id', $params)){
                $query_params[':edu_qualification_id'] = $params['edu_qualification_id'];
                $q_get_groups .= ' AND edu_qualification_id = :edu_qualification_id';
            }
            if(array_key_exists('edu_form_id', $params)){
                $query_params[':edu_form_id'] = $params['edu_form_id'];
                $q_get_groups .= ' AND edu_form_id = :edu_form_id';
            }
            if(array_key_exists('course', $params)){
                $query_params[':course'] = $params['course'];
                $q_get_groups .= ' AND course = :course';
            }
        }
	    $q_get_groups = $q_get_groups . '
	        ORDER BY edu_speciality_name, profile, program, course, view_edu_groups.group, edu_form_name
	    ';
        $p_get_groups = $this->db->prepare($q_get_groups);
        $q_res = $p_get_groups->execute($query_params);

        if ($q_res === FALSE) throw new DBQueryException('Can not retrieve students groups: ' . $p_get_groups->errorInfo()[2], $this->db);
        //if (!$res = $p_get_groups->fetchAll()) throw new LogicException('No groups in institute');
/*
        $data = array (
            'id' => $res['id'],
            'edu_speciality_name' => $res['edu_speciality_name'],
            'edu_qualification_name' => $res['edu_qualification_name'],
            'course' => $res['course'],
            'group' => $res['group'],
            'year' => $res{'year'},
            'semester' => $res['semester'],
            'program' => $res['program'],
            'profile' => $res['profile'],
        );
*/      $res = $p_get_groups->fetchAll();
        foreach ($res as &$row){
            $row['id'] = (int)$row['id'];
            $row['course'] = (int)$row['course'];
            $row['year'] = (int)$row['year'];
            $row['semester'] = (int)$row['semester'];
        }
        return new Result(true, '', $res);
    }

    public static function getAllStudentsGroups(AbstractUser $user, PDO $db, $params) {
        if(!self::hasRights($user))
            throw new PrivilegesException('Not enough rights to access this information', $db);
        $query_params = array();
        $first = true;
        $q_get_groups = 'SELECT edu_group_id as id,
                            edu_speciality_name,
                            edu_qualification_name,
                            edu_form_name,
                            course,
                            view_edu_groups.group,
                            view_edu_groups.year,
                            semester,
                            program,
                            `profile`,
                            institute_abbr,
                            institute_id
                          FROM view_edu_groups';
        if(array_key_exists('edu_qualification_id', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':edu_qualification_id'] = $params['edu_qualification_id'];
            $q_get_groups .= ' edu_qualification_id = :edu_qualification_id';
        }
        if(array_key_exists('edu_form_id', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':edu_form_id'] = $params['edu_form_id'];
            $q_get_groups .= ' edu_form_id = :edu_form_id';
        }
        if(array_key_exists('course', $params)){
            if($first){
                $q_get_groups .= " WHERE";
                $first = false;
            }
            else $q_get_groups .= " AND";
            $query_params[':course'] = $params['course'];
            $q_get_groups .= ' course = :course';
        }
	    $q_get_groups .= ' ORDER BY institute_id, edu_specialty_id';
        $p_get_groups = $db->prepare($q_get_groups);
        $q_res = $p_get_groups->execute($query_params);

        if ($q_res === FALSE) throw new DBQueryException('Can not retrieve students groups: ' . $p_get_groups->errorInfo()[2], $db);
        $res = $p_get_groups->fetchAll();
        return new Result(true, '', $res);
    }

    /**
     * Checking rights
     * Возвращает true, если user принадлежит институту или он супер администратор
     * @param AbstractUser $user
     * @param $institute_id
     * @return bool
     * @throws DBQueryException
     * @throws PrivilegesException
     */
    private static function hasInstituteEditorRights(AbstractUser $user, $institute_id) {
        if ($institute_id
            && $user instanceof Staff
            && $user->getStaffInstance()->getInstituteId() == $institute_id
            && $user->hasRights(array('module' => self::getModuleName(), 'min_level' => self::MIN_LEVEL_FOR_READ)))
            return true;
        else
            return false;
    }

//    private static function hasAdminRights(User $user){
//        if (!$user->hasRights(array('module' => self::MODULE_NAME, 'min_level' => $user::SUPER_ADMIN_LEVEL)))
//            return false;
//        else
//            return true;
//    }

    public static function hasEditorRights(AbstractUser $user){
        if (!$user->hasRights(array('module' => self::getModuleName(), 'min_level' => self::MIN_LEVEL_FOR_READ_ALL))){
            return false;
        }
        else
            return true;
    }

    public static function hasRights(AbstractUser $user, $institute_id = null){
        if(self::hasInstituteEditorRights($user, $institute_id)
            || self::hasEditorRights($user)
//            || self::hasAdminRights($user)
        )
            return true;
        else
            return false;
    }
    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    public static function getModuleName() {
        return self::MODULE_NAME;
    }
}
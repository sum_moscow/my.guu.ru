<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 29.07.2015
 * Time: 18:37
 */

class DormContractsHistory {
    private $contracts = array();

    public function addContract(DormContract $contract){
        $this->contracts[] = $contract;
    }

    public function getActive(){
        $result = array();
        if($this->contracts){
            foreach($this->contracts as $contract){
                if($contract instanceof DormContract && $contract->isActive()){
                    $result[] = $contract->getInfo();
                }
            }
        }
        return $result;
    }

    public function getList(){
        $result = array();
        if($this->contracts){
            foreach($this->contracts as $contract){
                if($contract instanceof DormContract){
                    $result[] = $contract->getInfo();
                }
            }
        }
        return $result;
    }
}
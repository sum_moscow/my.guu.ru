<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 03.08.2015
 * Time: 12:36
 */

require_once 'Class.DormContract.php';
require_once 'Class.DormContractsHistory.php';

require_once 'Class.DormRoom.php';
require_once 'Class.DormRoomList.php';

require_once 'Class.AbstractBlock.php';
require_once 'Class.DormBlock.php';
require_once 'Class.DormBlockWithRooms.php';
require_once 'Class.DormBlockList.php';
require_once 'Class.DormFloor.php';
require_once 'Class.DormResidentWithStudentInfo.php';
require_once 'Class.DormResidentList.php';

//require_once $ROOT_PATH . '/backend/students/Class.StudentWithStudyInfo.php';
//require_once $ROOT_PATH . '/backend/students/Class.AbstractStudent.php';
require_once $ROOT_PATH . '/backend/students/Class.StudentsModule.php';

class DormModule extends AbstractModule{

    /**
     * @param PDO $db
     * @param $id
     * @return DormContract
     * @throws DBQueryException
     */
    public static function selectContract(PDO $db, $id){
        $q_get = 'SELECT dorm_contracts.id, number, resident_id, type_id, type AS type_description, amount,
                    sign_date, expiry_date, is_paid, active FROM dorm_contracts
                  JOIN dorm_contracts_types ON dorm_contracts_types.id = dorm_contracts.type_id
                  WHERE dorm_contracts.id = :id';
        $p_get = $db->prepare($q_get);
        $q_res = $p_get->execute(array(
            ':id' => $id,
        ));
        if (!$q_res) throw new DBQueryException('SELECT', $db, 'Ой, мамочки, что-то сломалось!');
        if (!$res = $p_get->fetchObject()) throw new InvalidArgumentException('no contract with such id');
        $contract = DormContract::create($res->id, $res);
        return $contract;
    }
    public static function updateContract(PDO $db, DormContract $contract, $params){
        $q_upd = 'UPDATE dorm_contracts SET number = :number, type_id = :type_id, amount = :amount,
                    sign_date = :sign_date, expiry_date = :expiry_date, is_paid = :is_paid, active = :active
                    WHERE id = :id';

        !isset($params['number']) ?: $contract->setNumber($params['number']);
        !isset($params['type_id']) ?: $contract->setTypeId($params['type_id']);
        !isset($params['amount']) ?: $contract->setAmount($params['amount']);
        !isset($params['sign_date']) ?: $contract->setSignDate($params['sign_date']);
        !isset($params['expiry_date']) ?: $contract->setExpiryDate($params['expiry_date']);
        !isset($params['is_paid']) ?: $contract->setIsPaid($params['is_paid']);

        $p_upd = $db->prepare($q_upd);
        $q_res = $p_upd->execute(array(
            ":id" => $contract->getId(),
            ":number" => $contract->getNumber(),
            ":type_id" => $contract->getTypeId(),
            ":amount" => $contract->getAmount(),
            ":sign_date" => $contract->getSignDate(),
            ":expiry_date" => $contract->getExpiryDate(),
            ":is_paid" => $contract->getIsPaid(),
            ":active" => $contract->getActive(),
        ));
        if (!$q_res) throw new DBQueryException('UPDATE ERROR', $db);
        return true;
    }
    public static function insertContract(PDO $db, $params){
        $q_ins = 'INSERT INTO dorm_contracts (number, resident_id, type_id, amount,
                    sign_date, expiry_date, is_paid, active, created_at)
                  VALUES (:number, :resident_id, :type_id, :amount,
                    :sign_date, :expiry_date, :is_paid, :active, NOW())';

        $contract = DormContract::createFromArray(null, $params);

        $p_ins = $db->prepare($q_ins);
        $q_res = $p_ins->execute([
            ":number" => $contract->getNumber(),
            ":resident_id" => $contract->getResidentId(),
            ":type_id" => $contract->getTypeId(),
            ":amount" => $contract->getAmount(),
            ":sign_date" => $contract->getSignDate(),
            ":expiry_date" => $contract->getExpiryDate(),
            ":is_paid" => $contract->getIsPaid(),
            ":active" => $contract->getActive(),
        ]);
        if (!$q_res) throw new DBQueryException('can not insert contract into db', $db,
            'Ой, мы не смогли добавить новую запись, попробуйте еще раз');
        return (int)$db->lastInsertId();
    }

    public static function selectContractHistory(PDO $db, $resident_id){
        $q_get = 'SELECT dorm_contracts.id, number, resident_id, type_id, type AS type_description, amount,
                    sign_date, expiry_date, is_paid, active
                  FROM dorm_contracts
                    JOIN dorm_contracts_types ON dorm_contracts_types.id = dorm_contracts.type_id
                  WHERE resident_id = :resident_id
                  ORDER BY active DESC';
        $p_get = $db->prepare($q_get);
        $q_res = $p_get->execute(array(
            ':resident_id' => $resident_id,
        ));
        if (!$q_res) throw new DBQueryException('SELECT ERROR', $db, 'Что-то пошло не так');
        $history = new DormContractsHistory();
        while ($res = $p_get->fetchObject()){
            $contract = DormContract::create($res->id, $res);
            $history->addContract($contract);
        }
        return $history;
    }

    public static function selectRoom(PDO $db, $id){
        $q_get = 'SELECT rooms.id, number, block_id, room_area, `function`, status_id,
                  room_status AS status_description, places_count, places_reserved_count,
                    (SELECT COUNT(*)
                      FROM dorm_residents
                        INNER JOIN dorm_rooms ON dorm_rooms.id = dorm_residents.room_id
                      WHERE dorm_rooms.id = rooms.id) AS residents_count,
                    (SELECT places_count - places_reserved_count - residents_count) AS places_available_count
                  FROM dorm_rooms AS rooms
                    LEFT JOIN dorm_rooms_statuses ON dorm_rooms_statuses.id = rooms.status_id
                  WHERE rooms.id = :id';
        $p_get = $db->prepare($q_get);
        $q_res = $p_get->execute(array(':id' => $id));
        if ($q_res === FALSE) throw new DBQueryException('Can not retrieve dormitory room information', $db);
        if (!$res = $p_get->fetchObject()) throw new InvalidArgumentException('No dormitory room with such id');
        $room = DormRoom::create($id, $res);
        return $room;
    }
    public static function updateRoom(PDO $db, DormRoom $room, $params){
        $q_set = 'UPDATE dorm_rooms SET
                `number` = :number, room_area = :room_area,
                `function` = :function, places_count = :places_count,
                places_reserved_count = :places_reserved_count, status_id = :status_id
                WHERE id = :id';

        !isset($params['number']) ?: $room->setNumber($params['number']);
        !isset($params['function']) ?: $room->setFunction($params['function']);
        !isset($params['status_id']) ?: $room->setStatusId($params['status_id']);
        !isset($params['room_area']) ?: $room->setRoomArea($params['room_area']);
        !isset($params['places_reserved_count']) ?: $room->setPlacesReservedCount($params['places_reserved_count']);
        !isset($params['places_count']) ?: $room->setPlacesCount($params['places_count']);

        $p_set = $db->prepare($q_set);
        $q_res = $p_set->execute(array(
            ':id' => $room->getId(),
            ':number' => $room->getNumber(),
            ':function' => $room->getFunction(),
            ':status_id' => $room->getStatusId(),
            ':room_area' => $room->getRoomArea(),
            ':places_reserved_count' => $room->getPlacesReservedCount(),
            ':places_count' => $room->getPlacesCount(),
        ));
        if (!$q_res) throw new DBQueryException('UPDATE_ERROR', $db, 'Извините, произошла ошибка');
    }
    public static function insertRoom(PDO $db, $params){
        $q_ins = 'INSERT INTO dorm_rooms (number, block_id, room_area, `function`,
                    status_id, places_count, places_reserved_count, created_at)
                  VALUES (:number, :block_id, :room_area, :`function`,
                    :status_id, :places_count, :places_reserved_count, NOW())';

        $room = DormRoom::createFromArray(null, $params);

        $p_ins = $db->prepare($q_ins);
        $q_res = $p_ins->execute(array(
            ':number' => $room->getNumber(),
            ':block_id' => $room->getBlockId(),
            ':function' => $room->getFunction(),
            ':status_id' => $room->getStatusId(),
            ':room_area' => $room->getRoomArea(),
            ':places_reserved_count' => $room->getPlacesReservedCount(),
            ':places_count' => $room->getPlacesCount(),
        ));
        if (!$q_res) throw new DBQueryException('INSERT_ERROR', $db,
            'Что-то отвалилось?
            - Не беда: половина базы еще осталась!');
        return (int)$db->lastInsertId();
    }

    public static function selectRoomList(PDO $db, $params){
        $q_get = 'SELECT rooms.id, number, block_id, room_area, `function`,
                    status_id, room_status AS status_description,
                    places_count, places_reserved_count,
                    (SELECT COUNT(*)
                    FROM dorm_residents
                      INNER JOIN dorm_rooms ON dorm_rooms.id = dorm_residents.room_id
                    WHERE dorm_rooms.id = rooms.id) AS residents_count,
                    (SELECT IF(isnull(places_count), 0, places_count) -
                        IF(isnull(places_reserved_count), 0, places_reserved_count) -
                        IF(isnull(residents_count), 0, residents_count)
                    ) AS places_available_count
                  FROM dorm_rooms AS rooms
                  LEFT JOIN dorm_rooms_statuses ON dorm_rooms_statuses.id = rooms.status_id';

        $query_params = [];
        $first = true;
        if (isset($params['number'])){
            if ($first){
                $q_get .= " WHERE";
                $first = false;
            }else $q_get .= " AND";
            $query_params[':number'] = $params['number'];
            $q_get .= ' number = :number';
        }
        if (isset($params['block_id'])){
            if ($first){
                $q_get .= " WHERE";
                $first = false;
            }else $q_get .= " AND";
            $query_params[':block_id'] = $params['block_id'];
            $q_get .= ' block_id = :block_id';
        }
        if (isset($params['status_id'])){
            if ($first){
                $q_get .= " WHERE";
                $first = false;
            }else $q_get .= " AND";
            $query_params[':status_id'] = $params['status_id'];
            $q_get .= ' id = :status_id';
        }

        $p_get = $db->prepare($q_get);
        $q_res = $p_get->execute($query_params);
        if (!$q_res) throw new DBQueryException('SELECT_ERROR', $db, 'Извините, я не понял');
        $list = new DormRoomList();
        while ($res = $p_get->fetchObject()){
            $room = DormRoom::create($res->id, $res);
            $list->addRoom($room);
        }
        return $list;
    }

    public static function selectBlock(PDO $db, $id){
        $q_get_block = 'SELECT blocks.id, blocks.number, blocks.building_id, blocks.floor, blocks.status_id,
                            (SELECT ROUND(SUM(dorm_rooms.room_area), 2)
                              FROM dorm_rooms WHERE dorm_rooms.block_id = blocks.id) AS area,
                            (SELECT SUM(dorm_rooms.places_count)
                              FROM dorm_rooms WHERE dorm_rooms.block_id = blocks.id) AS places_count,
                            (SELECT SUM(dorm_rooms.places_reserved_count)
                              FROM dorm_rooms WHERE dorm_rooms.block_id = blocks.id) AS places_reserved_count,
                            (SELECT
                             SUM((SELECT COUNT(*)
                                  FROM dorm_residents
                                    INNER JOIN dorm_rooms ON dorm_rooms.id = dorm_residents.room_id
                                  WHERE dorm_rooms.id = rooms.id)
                                 )
                             FROM dorm_rooms AS rooms
                             WHERE rooms.block_id = blocks.id) AS residents_count,
                           (SELECT places_count - places_reserved_count - residents_count) AS places_available_count
                        FROM dorm_blocks AS blocks
                        WHERE blocks.id = :id';

        $p_get_block = $db->prepare($q_get_block);
        $q_res = $p_get_block->execute(array(':id' => $id));
        if (!$q_res) throw new DBQueryException('GET_ERROR', $db, 'Что-то пошло не так');
        if (!$res = $p_get_block->fetchObject()) throw new InvalidArgumentException('No block with such id');
        $block = DormBlock::create($id, $res);
        return $block;
    }
    public static function updateBlock(PDO $db, AbstractBlock $block, $params){
        $q_set = 'UPDATE dorm_blocks
                  SET building_id = :building_id, floor = :floor, number = :number, status_id = :status_id
                  WHERE id = :id';

        !isset($params['number']) ?: $block->setNumber($params['number']);
        !isset($params['floor']) ?: $block->setFloor($params['floor']);
        !isset($params['building_id']) ?: $block->setBuildingId($params['building_id']);
        !isset($params['status_id']) ?: $block->setStatusId($params['status_id']);

        $p_set = $db->prepare($q_set);
        $q_res = $p_set->execute(array(
            ':id' => $block->getId(),
            ':building_id' => $block->getBuildingId(),
            ':floor' => $block->getFloor(),
            ':number' => $block->getNumber(),
            ':status_id' => $block->getStatusId(),
        ));
        if (!$q_res) throw new DBQueryException('UPDATE_ERROR', $db, 'Извините, произошла ошибка');
    }
    public static function insertBlock(PDO $db, $params){
        $q_set = 'INSERT INTO dorm_blocks (number, building_id, floor, status_id, created_at)
                  VALUES (:number, :building_id, :floor, :status_id, NOW())';

        $block = DormBlock::createFromArray(null, $params);

        $p_set = $db->prepare($q_set);
        $p_set->execute(array(
            ':number' => $block->getNumber(),
            ':building_id' => $block->getBuildingId(),
            ':floor' => $block->getFloor(),
            ':status_id' => $block->getStatusId(),
        ));
        if ($p_set === FALSE) throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        return (int)$db->lastInsertId();
    }

    public static function selectBlockWithRoom(PDO $db, $id){
        $q_get_block = 'SELECT blocks.id, blocks.number, blocks.building_id, blocks.floor, blocks.status_id,
                            (SELECT ROUND(SUM(dorm_rooms.room_area), 2)
                              FROM dorm_rooms WHERE dorm_rooms.block_id = blocks.id) AS area,
                            (SELECT SUM(dorm_rooms.places_count)
                              FROM dorm_rooms WHERE dorm_rooms.block_id = blocks.id) AS places_count,
                            (SELECT SUM(dorm_rooms.places_reserved_count)
                              FROM dorm_rooms WHERE dorm_rooms.block_id = blocks.id) AS places_reserved_count,
                            (SELECT
                             SUM((SELECT COUNT(*)
                                  FROM dorm_residents
                                    INNER JOIN dorm_rooms ON dorm_rooms.id = dorm_residents.room_id
                                  WHERE dorm_rooms.id = rooms.id)
                                 )
                             FROM dorm_rooms AS rooms
                             WHERE rooms.block_id = blocks.id) AS residents_count,
                           (SELECT places_count - places_reserved_count - residents_count) AS places_available_count
                        FROM dorm_blocks AS blocks
                        WHERE blocks.id = :id';

        $p_get_block = $db->prepare($q_get_block);
        $q_res = $p_get_block->execute(array(':id' => $id));
        if (!$q_res) throw new DBQueryException('GET_ERROR', $db, 'Что-то пошло не так');
        if (!$res = $p_get_block->fetchObject()) throw new InvalidArgumentException('No block with such id');
        $block = DormBlockWithRooms::create($id, $res);
        $rooms = self::selectRoomList($db, ['block_id' => $block->getId()]);
        $block->addRooms($rooms);
        return $block;
    }

    public static function selectBlockList(PDO $db, $params){
        $q_get = 'SELECT blocks.id, blocks.number, blocks.building_id, blocks.floor, blocks.status_id,
                    (SELECT ROUND(SUM(dorm_rooms.room_area), 2)
                      FROM dorm_rooms WHERE dorm_rooms.block_id = blocks.id) AS area,
                    (SELECT SUM(dorm_rooms.places_count)
                      FROM dorm_rooms WHERE dorm_rooms.block_id = blocks.id) AS places_count,
                    (SELECT SUM(dorm_rooms.places_reserved_count)
                      FROM dorm_rooms WHERE dorm_rooms.block_id = blocks.id) AS places_reserved_count,
                    (SELECT
                      SUM((SELECT COUNT(*)
                      FROM dorm_residents
                        INNER JOIN dorm_rooms ON dorm_rooms.id = dorm_residents.room_id
                      WHERE dorm_rooms.id = rooms.id)
                      )
                     FROM dorm_rooms AS rooms
                     WHERE rooms.block_id = blocks.id) AS residents_count,
                    (SELECT places_count - places_reserved_count - residents_count) AS places_available_count
                  FROM dorm_blocks AS blocks';

        $query_params = [];
        $first = true;
        if (isset($params['number'])){
            if ($first){
                $q_get .= " WHERE";
                $first = false;
            }else $q_get .= " AND";
            $query_params[':number'] = $params['number'];
            $q_get .= ' number = :number';
        }
        if (isset($params['building_id'])){
            if ($first){
                $q_get .= " WHERE";
                $first = false;
            }else $q_get .= " AND";
            $query_params[':building_id'] = $params['building_id'];
            $q_get .= ' building_id = :building_id';
        }
        if (isset($params['status_id'])){
            if ($first){
                $q_get .= " WHERE";
                $first = false;
            }else $q_get .= " AND";
            $query_params[':status_id'] = $params['status_id'];
            $q_get .= ' status_id = :status_id';
        }
        if (isset($params['floor'])){
            if ($first){
                $q_get .= " WHERE";
                $first = false;
            }else $q_get .= " AND";
            $query_params[':floor'] = $params['floor'];
            $q_get .= ' floor = :floor';
        }

        $p_get = $db->prepare($q_get);
        $q_res = $p_get->execute($query_params);
        if (!$q_res) throw new DBQueryException('SELECT_ERROR', $db, 'Извините, я не понял');
        $list = new DormBlockList();
        while ($res = $p_get->fetchObject()){
            $block = DormBlock::create($res->id, $res);
            $list->addBlock($block);
        }
        return $list;
    }

    public static function selectFloor(PDO $db, $params){
        $q_get_blocks = 'SELECT blocks.id, blocks.number, blocks.building_id, blocks.floor, blocks.status_id,
                            (SELECT ROUND(SUM(dorm_rooms.room_area), 2)
                              FROM dorm_rooms WHERE dorm_rooms.block_id = blocks.id) AS area,
                            (SELECT SUM(dorm_rooms.places_count)
                              FROM dorm_rooms WHERE dorm_rooms.block_id = blocks.id) AS places_count,
                            (SELECT SUM(dorm_rooms.places_reserved_count)
                              FROM dorm_rooms WHERE dorm_rooms.block_id = blocks.id) AS places_reserved_count,
                            (SELECT
                             SUM((SELECT COUNT(*)
                                  FROM dorm_residents
                                    INNER JOIN dorm_rooms ON dorm_rooms.id = dorm_residents.room_id
                                  WHERE dorm_rooms.id = rooms.id)
                                 )
                             FROM dorm_rooms AS rooms
                             WHERE rooms.block_id = blocks.id) AS residents_count,
                           (SELECT places_count - places_reserved_count - residents_count) AS places_available_count
                        FROM dorm_blocks AS blocks
                        WHERE blocks.building_id = :building_id AND blocks.floor = :floor';

        $floor = DormFloor::createFromArray($params);

        if (!isset($params['with_rooms'])) $params['with_rooms'] = 'false';
        $with_room = (bool)$params['with_rooms'];

        $p_get_blocks = $db->prepare($q_get_blocks);
        $q_res = $p_get_blocks->execute(array(
            ':building_id' => $floor->getBuildingId(),
            ':floor' => $floor->getFloor(),
        ));
        if (!$q_res) throw new DBQueryException('GET_ERROR', $db, 'Помолитесь и попробуйте еще раз');
        while ($res = $p_get_blocks->fetchObject()){
            if($with_room){
                $block = DormBlockWithRooms::create($res->id, $res);
                $rooms = self::selectRoomList($db, ['block_id' => $block->getId()]);
                $block->addRooms($rooms);
            }
            else{
                $block = DormBlock::create($res->id, $res);
            }
            $floor->addBlock($block);
        }
        return $floor;
    }

    public static function selectResident(PDO $db, $id){
        $q_get_resident = 'SELECT
                            res.id,
                            student_id,
                            phone_number,
                            application_number,
                            application_date,
                            application_document,
                            student_id,
                            room_id,
                            status_id,
                            `status`.resident_status AS status_description,
                            status_reason,
                            came_from,
                            distance_from_Moscow,
                            note
                          FROM dorm_residents AS res
                            JOIN dorm_residents_statuses AS `status` ON `status`.id = res.status_id
                          WHERE res.id = :id';
        $p_get_resident = $db->prepare($q_get_resident);
        $q_res = $p_get_resident->execute(array(':id' => $id));
        if (!$q_res) throw new DBQueryException('DB_ERROR', $db, 'усё плохо!');
        if (!$res = $p_get_resident->fetchObject()) throw new InvalidArgumentException('No resident with such id');
        return DormResident::create($id, $res);
    }
    public static function updateResident(PDO $db, DormResident $resident, $params){
        !isset($params['phone_number']) ?: $resident->setPhoneNumber($params['phone_number']);
        !isset($params['status_reason']) ?: $resident->setStatusReason($params['status_reason']);
        !isset($params['note']) ?: $resident->setNote($params['note']);
        !isset($params['status_id']) ?: $resident->setStatusId($params['status_id']);
        !isset($params['distance_from_Moscow']) ?: $resident->setDistanceFromMoscow($params['distance_from_Moscow']);
        !isset($params['came_from']) ?: $resident->setCameFrom($params['came_from']);
        !isset($params['application_number']) ?: $resident->setApplicationNumber($params['application_number']);
        !isset($params['application_date']) ?: $resident->setApplicationDate($params['application_date']);
        !isset($params['application_document']) ?: $resident->setApplicationDocument($params['application_document']);
        //TODO костыль нужно, чтобы резиденту нельзя было сменить рум айди
        if(isset($params['room_id'])){
            $room = self::selectRoom($db, $params['room_id']);
            $res = $room->addResident($resident);
            if(!$res) throw new InvalidArgumentException('now available places in room');
        }

        $q_set_resident = 'UPDATE dorm_residents
                            SET
                                student_id = :student_id,
                                phone_number = :phone_number,
                                application_number = :application_number,
                                application_date = :application_date,
                                application_document = :application_document,
                                room_id = :room_id,
                                status_id = :status_id,
                                status_reason = :status_reason,
                                came_from = :came_from,
                                distance_from_Moscow = :distance_from_Moscow,
                                note = :note
                            WHERE id = :id';
        $p_set_resident = $db->prepare($q_set_resident);
        $q_res = $p_set_resident->execute(array(
            ':student_id' => $resident->getStudentId(),
            ':phone_number' => $resident->getPhoneNumber(),
            ':application_number' => $resident->getApplicationNumber(),
            ':application_date' => $resident->getApplicationDate(),
            ':application_document' => $resident->getApplicationDocument(),
            ':room_id' => $resident->getRoomId(),
            ':status_id' => $resident->getStatusId(),
            ':status_reason' => $resident->getStatusReason(),
            ':came_from' => $resident->getCameFrom(),
            ':distance_from_Moscow' => $resident->getDistanceFromMoscow(),
            ':note' => $resident->getNote(),
            ':id' => $resident->getId(),
        ));
        if(!$q_res) throw new DBQueryException('UPDATE_ERROR', $db, 'Извините, произошла ошибка');
    }
    public static function insertResident(PDO $db, $params){
        $q_ins_resident = 'INSERT INTO dorm_residents (
                            student_id, phone_number, application_number, application_date, application_document,
                            room_id, status_id, status_reason, came_from, distance_from_Moscow, note, created_at)
                          VALUES (
                            :student_id, :phone_number, :application_number, :application_date, :application_document,
                            :room_id, :status_id, :status_reason, :came_from, :distance_from_Moscow, :note, NOW())';

        $resident = DormResident::createFromArray(null, $params);

        $p_ins_resident = $db->prepare($q_ins_resident);
        $q_res = $p_ins_resident->execute(array(
            ':student_id' => $resident->getStudentId(),
            ':phone_number' => $resident->getPhoneNumber(),
            ':application_number' => $resident->getApplicationNumber(),
            ':application_date' => $resident->getApplicationDate(),
            ':application_document' => $resident->getApplicationDocument(),
            ':room_id' => $resident->getRoomId(),
            ':status_id' => $resident->getStatusId(),
            ':status_reason' => $resident->getStatusReason(),
            ':came_from' => $resident->getCameFrom(),
            ':distance_from_Moscow' => $resident->getDistanceFromMoscow(),
            ':note' => $resident->getNote(),
        ));
        if (!$q_res) throw new DBQueryException('INSERT_ERROR ', $db, '=(');
        return (int)$db->lastInsertId();
    }

    public static function selectResidentWithStudentInfo(PDO $db, $id){
        $q_get_resident = 'SELECT
                res.id,
                student_id,
                res.phone_number,
                application_number,
                application_date,
                application_document,
                room_id,
                status_id,
                resident_status AS status_description,
                status_reason,
                came_from,
                distance_from_Moscow,
                note
              FROM dorm_residents AS res
                INNER JOIN dorm_residents_statuses AS `status` ON res.status_id = `status`.id
                JOIN view_students
              WHERE res.id = :id';
        $p_get_resident = $db->prepare($q_get_resident);
        $q_res = $p_get_resident->execute(array(':id' => $id));
        if (!$q_res) throw new DBQueryException('DB_ERROR', $db, 'усё плохо!');
        if (!$res = $p_get_resident->fetchObject()) throw new InvalidArgumentException('No resident with such id');
        $student = StudentsModule::selectStudentWithStudyInfo($db, $res->student_id);
        $resident = DormResidentWithStudentInfo::create($student, $res->id, $res);
        return $resident;
    }

    public static function selectResidentList(PDO $db, $params){
        $q_get = 'SELECT
                      res.id,
                      student.id AS student_id,
                      student.user_id,
                      last_name,
                      first_name,
                      middle_name,
                      student.sex,
                      student.phone_number AS student_phone_number,
                      res.phone_number,
                      student.course,
                      institute_id,
                      student.institute_name,
                      student.institute_abbr,
                      student.budget_form,
                      student.edu_form_id,
                      student.edu_form_name,
                      student.`year`,
                      student.term,
                      student.year_end,
                      application_number,
                      application_date,
                      application_document,
                      room_id,
                      res.status_id,
                      resident_status AS status_description,
                      status_reason,
                      came_from,
                      distance_from_Moscow,
                      note
                  FROM dorm_residents AS res
                    INNER JOIN view_students AS student ON res.student_id = student.id
                    LEFT JOIN edu_terms ON edu_terms.edu_form_id = student.edu_form_id AND edu_terms.edu_qualification_id = student.edu_qualification_id
                    INNER JOIN dorm_residents_statuses AS `status` ON res.status_id = `status`.id
                    LEFT JOIN dorm_rooms ON dorm_rooms.id = res.room_id
                    LEFT JOIN dorm_blocks ON dorm_blocks.id = dorm_rooms.block_id';

        if (!isset($params['with_study_info'])) $params['with_study_info'] = true;
        elseif($params['with_study_info'] == 'true') $params['with_study_info'] = true;
        else $params['with_study_info'] = false;
        $with_study_info = $params['with_study_info'];
        $query_params = array();
        $first = TRUE;
        if(array_key_exists('last_name', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':last_name'] = $params['last_name'];
            $q_get .= ' last_name LIKE :last_name';
        }
        if(array_key_exists('first_name', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':first_name'] = $params['first_name'];
            $q_get .= ' first_name LIKE :first_name';
        }
        if(array_key_exists('middle_name', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':middle_name'] = $params['middle_name'];
            $q_get .= ' middle_name LIKE :middle_name';
        }
        if(array_key_exists('full_name', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':full_name'] = $params['full_name'] . '%';
            $q_get .= ' CONCAT_WS(\' \', view_users.last_name, view_users.first_name, view_users.middle_name) LIKE :full_name';
        }
        if(array_key_exists('application_number', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':application_number'] = $params['application_number'];
            $q_get .= ' application_number = :application_number';
        }
        if(array_key_exists('application_date', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':application_date'] = $params['application_date'];
            $q_get .= ' application_date = :application_date';
        }
        if(array_key_exists('application_document', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':application_document'] = $params['application_document'];
            $q_get .= ' application_document LIKE :application_document';
        }
        if(array_key_exists('student_id', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':student_id'] = $params['student_id'];
            $q_get .= ' student_id = :student_id';
        }
        if(array_key_exists('room_id', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':room_id'] = $params['room_id'];
            $q_get .= ' room_id = :room_id';
        }
        if(array_key_exists('status_id', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            if(is_array($params['status_id'])){
                $q_get .= ' res.status_id IN (';
                $first_in = true;
                foreach($params['status_id'] as $status){
                    if(!$first_in) $q_get .= ',';
                    $q_get .= ":status_{$status}";
                    $query_params[":status_{$status}"] = $status;
                    $first_in = false;
                }
                $q_get .= ')';
            }
            else{
                $query_params[':status_id'] = $params['status_id'];
                $q_get .= ' res.status_id = :status_id';
            }
        }
        if(array_key_exists('status_reason', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':status_reason'] = $params['status_reason'];
            $q_get .= ' status_reason LIKE :status_reason';
        }
        if(array_key_exists('distance_from_Moscow', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':distance_from_Moscow'] = $params['distance_from_Moscow'];
            $q_get .= ' distance_from_Moscow = :distance_from_Moscow';
        }
        if(array_key_exists('note', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':note'] = $params['note'];
            $q_get .= ' note LIKE :note';
        }
        if(array_key_exists('phone_number', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':phone_number'] = $params['phone_number'];
            $q_get .= ' dorm_residents.' . 'phone_number LIKE :phone_number';
        }
        if(array_key_exists('building_id', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':building_id'] = $params['building_id'];
            $q_get .= ' building_id = :building_id';
        }
        if(array_key_exists('floor', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':floor'] = $params['floor'];
            $q_get .= ' dorm_blocks.' . 'floor = :floor';
        }
        if(array_key_exists('block_id', $params)){
            if($first){
                $q_get .= " WHERE";
                $first = false;
            }
            else $q_get .= " AND";
            $query_params[':block_id'] = $params['block_id'];
            $q_get .= ' dorm_blocks.' . 'id = :block_id';
        }

        $p_get = $db->prepare($q_get);
        $q_res = $p_get->execute($query_params);
        if (!$q_res) throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        $list = new DormResidentList();
        while($res = $p_get->fetchObject()){
            if($with_study_info){
                $student = new StudentWithStudyInfo(
                    $res->student_id,
                    $res->user_id,
                    $res->last_name,
                    $res->first_name,
                    $res->middle_name,
                    $res->sex,
                    $res->student_phone_number,
                    $res->institute_name,
                    $res->institute_abbr,
                    $res->budget_form,
                    $res->edu_form_name,
                    $res->course,
                    $res->year,
                    $res->term,
                    $res->year_end);
                $resident = DormResidentWithStudentInfo::create($student, $res->id, $res);
            }
            else{
                $resident = DormResident::create($res->id, $res);
            }
            $list->addResident($resident);
        }
        return $list;
    }
}
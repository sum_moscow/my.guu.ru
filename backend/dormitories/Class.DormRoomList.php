<?php

/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 03.08.2015
 * Time: 16:17
 */
class DormRoomList{
    private $rooms = array();

    public function addRoom(DormRoom $room){
        $this->rooms[] = $room;
    }

    public function getInfo(){
        $result = array();
        if ($this->rooms){
            foreach ($this->rooms as $room){
                if ($room instanceof DormRoom){
                    $result[] = $room->getInfo();
                }
            }
        }
        return $result;
    }
} 
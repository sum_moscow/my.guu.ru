<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 29.07.2015
 * Time: 18:31
 */

//TODO change amount to decimal(12,2)
class DormContract{
    private $id;
    private $number;
    private $resident_id;
    private $type_id;
    private $type_description;
    private $amount;
    private $sign_date;
    private $expiry_date;
    private $is_paid;
    private $active;

    public function __construct($id, $number, $resident_id, $type_id, $type_description,
                                $amount, $sign_date, $expiry_date, $is_paid, $active){
        $this->id = (int)$id;
        $this->number = $number;
        $this->resident_id = (int)$resident_id;
        $this->type_id = (int)$type_id;
        $this->type_description = $type_description;
        $this->amount = (int)$amount;
        $this->sign_date = $sign_date;
        $this->expiry_date = $expiry_date;
        $this->is_paid = (int)$is_paid;
        $this->active = (int)$active;
    }

    /**
     * @return mixed
     */
    public function getActive(){
        return $this->active;
    }

    /**
     * @return mixed
     */
    public function getAmount(){
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getExpiryDate(){
        return $this->expiry_date;
    }

    /**
     * @return mixed
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIsPaid(){
        return $this->is_paid;
    }

    /**
     * @return mixed
     */
    public function getNumber(){
        return $this->number;
    }

    /**
     * @return mixed
     */
    public function getResidentId(){
        return $this->resident_id;
    }

    /**
     * @return mixed
     */
    public function getSignDate(){
        return $this->sign_date;
    }

    /**
     * @return mixed
     */
    public function getTypeDescription(){
        return $this->type_description;
    }

    /**
     * @return mixed
     */
    public function getTypeId(){
        return $this->type_id;
    }

    public function getInfo(){
        return array(
            'id' =>  $this->id,
            'number' =>  $this->number,
            'resident_id' => $this->resident_id,
            'type_id' => $this->type_id,
            'type_description' => $this->type_description,
            'amount' => $this->amount,
            'sign_date' => $this->sign_date,
            'expiry_date' => $this->expiry_date,
            'is_paid' => $this->is_paid,
            'active' => $this->active,
        );
    }

    /**
     * @param null $sign_date
     */
    public function setSignDate($sign_date){
        $this->sign_date = $sign_date;
    }

    /**
     * @param null $expiry_date
     */
    public function setExpiryDate($expiry_date){
        $this->expiry_date = $expiry_date;
    }

    /**
     * @param null $amount
     */
    public function setAmount($amount){
        $this->amount = (int)$amount;
    }

    /**
     * @param null $active
     */
    public function setActive($active){
        $this->active = (int)$active;
    }

    /**
     * @param null $is_paid
     */
    public function setIsPaid($is_paid){
        $this->is_paid = (int)$is_paid;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number){
        $this->number = $number;
    }

    /**
     * @param mixed $type_id
     */
    public function setTypeId($type_id){
        $this->type_id = $type_id;
    }

    public function isActive(){
        if($this->active == 0)
            return false;
        return true;
    }

    public static function create($id, stdClass $data){
        return new DormContract($id,
            $data->number,
            $data->resident_id,
            $data->type_id,
            $data->type_description,
            $data->amount,
            $data->sign_date,
            $data->expiry_date,
            $data->is_paid,
            $data->active
        );
    }
    public static function createFromArray($id, array $data){
        if (!isset($data['number'])) throw new InvalidArgumentException('no contract number');
        if (!isset($data['resident_id'])) throw new InvalidArgumentException('no contract resident_id');
        if (!isset($data['type_id'])) throw new InvalidArgumentException('no contract type_id');

        $contract = new DormContract($id, $data['number'], $data['resident_id'], $data['type_id'],
                        null, null, null, null, null, null);

        !isset($data['number']) ?: $contract->setNumber($data['number']);
        !isset($data['type_id']) ?: $contract->setTypeId($data['type_id']);
        !isset($data['amount']) ?: $contract->setAmount($data['amount']);
        !isset($data['sign_date']) ?: $contract->setSignDate($data['sign_date']);
        !isset($data['expiry_date']) ?: $contract->setExpiryDate($data['expiry_date']);
        !isset($data['is_paid']) ?: $contract->setIsPaid($data['is_paid']);
        return $contract;
    }

}
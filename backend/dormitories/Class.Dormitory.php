<?php

/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 26.02.2015
 * Time: 20:17
 */
class Dormitory {
    const MODULE_NAME = 'dormitories';
    private $db;
    private $id;
    private $name;
    private $abbr;

    private $floors = array();
    private $places_count;
    private $places_count_by_floor = array();
    private $places_reserved_count;
    private $places_reserved_count_by_floor = array();
    private $residents_count;
    private $residents_count_by_floor = array();
    private $places_available_count;
    private $places_available_count_by_floor = array();
    private $created_at;
    private $updated_at;
    //private static $available = array();


    /**
     * @param int $id
     * @param PDO $db
     * @throws InvalidArgumentException
     * @throws DBQueryException
     */
    public function __construct($id, PDO $db) {
        $q_get = 'SELECT build.name, build.abbr, build.created_at, build.updated_at,
                  (SELECT SUM(dorm_rooms.places_count) FROM dorm_rooms
                    INNER JOIN dorm_blocks ON dorm_blocks.id = dorm_rooms.block_id
                  WHERE dorm_blocks.building_id = build.id) AS places_count,
                  (SELECT SUM(dorm_rooms.places_reserved_count) FROM dorm_rooms
                    INNER JOIN dorm_blocks ON dorm_blocks.id = dorm_rooms.block_id
                  WHERE dorm_blocks.building_id = build.id) as places_reserved_count,
                  (SELECT
                  SUM((SELECT COUNT(*)
                       FROM dorm_residents
                         INNER JOIN dorm_rooms ON dorm_rooms.id = dorm_residents.room_id
                       WHERE dorm_rooms.id = rooms.id)
                  )
                   FROM dorm_rooms AS rooms
                     INNER JOIN dorm_blocks ON dorm_blocks.id = rooms.block_id
                     INNER JOIN buildings ON buildings.id = dorm_blocks.building_id
                   WHERE buildings.id = build.id) AS residents_count,
                  (SELECT places_count - places_reserved_count - residents_count) AS places_available_count
                FROM buildings AS build
                WHERE build.id = :id';
        $p_get = $db->prepare($q_get);
        $q_res = $p_get->execute(array(':id' => $id));

        if ($q_res === FALSE) throw new DBQueryException('', $db);
        if (!$result = $p_get->fetch()) throw new InvalidArgumentException("No dormitory with id {$id}");

        $this->id = (int)$id;
        $this->db = $db;
        $this->name = $result['name'];
        $this->abbr = $result['abbr'];
        $this->places_count = (int)$result['places_count'];
        $this->places_reserved_count = (int)$result['places_reserved_count'];
        $this->residents_count = (int)$result['residents_count'];
        $this->places_available_count = $this->places_count - $this->places_reserved_count - $this->residents_count;
        $this->created_at = $result['created_at'];
        $this->updated_at = $result['updated_at'];

        //self::getByFloor();
    }

    /**
     * @throws DBQueryException
     */
    private function getByFloor(){
        $q_get_by_floor = 'SELECT dorm_blocks.floor AS floor,
                                SUM(dorm_rooms.places_count) AS places_count_by_floor,
                                SUM(dorm_rooms.places_reserved_count) AS places_reserved_count_by_floor,
                                dorm_blocks.id AS d_bl_id,
                                SUM((SELECT COUNT(*)
                                        FROM dorm_residents
                                            INNER JOIN dorm_rooms ON dorm_rooms.id = dorm_residents.room_id
                                            INNER JOIN dorm_blocks ON dorm_blocks.id = dorm_rooms.block_id
                                        WHERE dorm_blocks.id = d_bl_id)
                                    ) AS residents_count_by_floor
                            FROM dorm_rooms
                                INNER JOIN dorm_blocks ON dorm_blocks.id = dorm_rooms.block_id
                                INNER JOIN buildings ON buildings.id = dorm_blocks.building_id
                            WHERE buildings.id = :id
                            GROUP BY dorm_blocks.floor';
        $p_get_by_floor = $this->db->prepare($q_get_by_floor);
        $q_res = $p_get_by_floor->execute(array(':id' => $this->id));

        if ($q_res === FALSE) throw new DBQueryException('Can not retrieve info about dormitory', $this->db);

        while ($res = $p_get_by_floor->fetch())
            foreach ($res as $key => $value){
                $floor = $res['floor'];
                if(!in_array($floor, $this->floors)){
                    $this->floors[] = (int)$floor;
                }
                $this->places_count_by_floor[$floor] = (int)$res['places_count_by_floor'];
                $this->places_reserved_count_by_floor[$floor] = (int)$res['places_reserved_count_by_floor'];
                $this->residents_count_by_floor[$floor] = (int)$res['residents_count_by_floor'];
                $this->places_available_count_by_floor[$floor] = $this->places_count_by_floor[$floor] - $this->places_reserved_count_by_floor[$floor] - $this->residents_count_by_floor[$floor];
            }
    }

    /**
     * @return Result
     */
    public function getInfo() {
        return new Result(true, '', array(
            'id' => $this->id,
            'name' => $this->name,
            'abbr' => $this->abbr,
            'floors' => $this->floors,
            'places_count' => $this->places_count,
            'places_count_by_floor' => $this->places_count_by_floor,
            'places_reserved_count' => $this->places_reserved_count,
            'places_reserved_count_by_floor' => $this->places_reserved_count_by_floor,
            'residents_count' => $this->residents_count,
            'residents_count_by_floor' => $this->residents_count_by_floor,
            'places_available_count' => $this->places_available_count,
            'places_available_count_by_floor' => $this->places_available_count_by_floor,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ));
    }

    /**
     * @return Result
     * @param PDO $db
     * @throws DBQueryException
     */
    public static function search($db) {
        $q_get = 'SELECT id FROM buildings WHERE name LIKE :name';
        $p_get = $db->prepare($q_get);
        $q_res = $p_get->execute(array(
            ':name' => 'Общежитие'
        ));
        if ($q_res === FALSE) throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        if ($result = $p_get->fetchAll()) return new Result(true, 'Не найдено');
        foreach($result as &$dorm)
            $dorm['id'] = (int)$dorm['id'];
        return new Result('true', '', $result);
    }

    /**
     * @param PDO $db
     * @return Result
     * @throws DBQueryException
     */
    public static function getSumInfo(PDO $db) {
        $name = "Общежитие";
        $id = array();
        $data = array(
            'places_count' => 0,
            'places_count_by_floor' => array(),
            'places_reserved_count' => 0,
            'places_reserved_count_by_floor' => array(),
            'residents_count' => 0,
            'residents_count_by_floor' => array(),
            'places_available_count' => 0,
            'places_available_count_by_floor' => array(),
        );
        $q_get = 'SELECT id FROM buildings WHERE name = :name';
        $p_get = $db->prepare($q_get);
        $q_res = $p_get->execute(array(':name' => $name));
        if ($q_res === FALSE) throw new DBQueryException('', $db);
        while ($res = $p_get->fetch())
            foreach ($res as $value)
                $id[] = $value;
        foreach ($id as $value) {
            $dorm = new Dormitory($value, $db);
            $data['places_count'] += $dorm->getPlacesCount();
            foreach ($places_count = $dorm->getPlacesCountByFloor() as $floor => $count){
                if( isset($data['places_count_by_floor'][$floor]))
                    $data['places_count_by_floor'][$floor] += $count;
                else
                    $data['places_count_by_floor'][$floor] = $count;
            }
            $data['places_reserved_count'] += $dorm->getPlacesReservedCount();
            foreach ($places_count = $dorm->getPlacesReservedCountByFloor() as $floor => $count){
                if( isset($data['places_reserved_count_by_floor'][$floor]))
                    $data['places_reserved_count_by_floor'][$floor] += $count;
                else
                    $data['places_reserved_count_by_floor'][$floor] = $count;
            }
            $data['residents_count'] += $dorm->getResidentsCount();
            foreach ($places_count = $dorm->getResidentsCountByFloor() as $floor => $count) {
                if( isset($data['residents_count_by_floor'][$floor]))
                    $data['residents_count_by_floor'][$floor] += $count;
                else
                    $data['residents_count_by_floor'][$floor] = $count;
            }
            $data['places_available_count'] += $dorm->getPlacesAvailableCount();
            foreach ($places_count = $dorm->getPlacesAvailableCountByFloor() as $floor => $count) {
                if( isset($data['places_available_count_by_floor'][$floor]))
                    $data['places_available_count_by_floor'][$floor] += $count;
                else
                    $data['places_available_count_by_floor'][$floor] = $count;
            }
        }
        return new Result(true, '', $data);
    }

    /**
     * @return int
     */
    public function getPlacesAvailableCount() {
        return $this->places_available_count;
    }

    /**
     * @return array
     */
    public function getPlacesAvailableCountByFloor() {
        return $this->places_available_count_by_floor;
    }

    /**
     * @return int
     */
    public function getPlacesCount() {
        return $this->places_count;
    }

    /**
     * @return array
     */
    public function getPlacesCountByFloor() {
        return $this->places_count_by_floor;
    }

    /**
     * @return int
     */
    public function getPlacesReservedCount() {
        return $this->places_reserved_count;
    }

    /**
     * @return array
     */
    public function getPlacesReservedCountByFloor() {
        return $this->places_reserved_count_by_floor;
    }

    /**
     * @return int
     */
    public function getResidentsCount() {
        return $this->residents_count;
    }

    /**
     * @return array
     */
    public function getResidentsCountByFloor() {
        return $this->residents_count_by_floor;
    }


    public function getFloor($floor){
        $q_get = 'SELECT id FROM dorm_blocks WHERE floor = :floor';

    }
}

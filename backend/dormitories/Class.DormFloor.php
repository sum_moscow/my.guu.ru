<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 04.08.2015
 * Time: 13:29
 */

require_once 'Class.DormBlockList.php';
require_once 'Class.AbstractBlock.php';
class DormFloor extends DormBlockList{
    private $building_id;
    private $floor;

    public function __construct($building_id, $floor){
        $this->building_id = $building_id;
        $this->floor = $floor;
    }
    public function addBlock(AbstractBlock $block){
        if($block->getFloor() != $this->floor || $block->getBuildingId() != $this->building_id)
            return false;
        return parent::addBlock($block);
    }
    public function getFloor(){
        return $this->floor;
    }
    public function getBuildingId(){
        return $this->building_id;
    }
    public static function createFromArray(array $data){
        if (!isset($data['floor'])) throw new InvalidArgumentException('no floor');
        if (!isset($data['building_id'])) throw new InvalidArgumentException('no building_id');
        return new DormFloor($data['building_id'], $data['floor']);
    }
} 
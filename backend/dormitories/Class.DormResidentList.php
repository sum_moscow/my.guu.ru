<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 05.08.2015
 * Time: 18:17
 */

require_once 'Class.AbstractResident.php';
class DormResidentList {
    private $residents = array();

    public function addResident(AbstractResident $resident){
        $this->residents[] = $resident;
    }

    public function getInfo(){
        $result = array();
        if ($this->residents){
            foreach ($this->residents as $resident){
                if ($resident instanceof AbstractResident){
                    $result[] = $resident->getInfo();
                }
            }
        }
        return $result;
    }
}
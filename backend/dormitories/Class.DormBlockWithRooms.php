<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 04.08.2015
 * Time: 13:40
 */

require_once 'Class.DormBlock.php';
require_once 'Class.DormRoomList.php';
class DormBlockWithRooms extends DormBlock{
    /**
     * @var DormRoomList
     */
    private $rooms;

    public function addRooms(DormRoomList $rooms){
        $this->rooms = $rooms;
    }
    public function getInfo(){
        $result = parent::getInfo();
        $result['rooms'] = $this->rooms->getInfo();
        return $result;
    }
}
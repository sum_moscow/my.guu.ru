<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 04.08.2015
 * Time: 14:31
 */

require_once 'Class.AbstractBlock.php';
class DormBlockList{
    private $blocks = array();

    /**
     * @param AbstractBlock $block
     * @return bool
     */
    public function addBlock(AbstractBlock $block){
        $this->blocks[] = $block;
        return true;
    }

    public function getInfo(){
        $result = array();
        if ($this->blocks){
            foreach ($this->blocks as $block){
                if ($block instanceof AbstractBlock){
                    $result[] = $block->getInfo();
                }
            }
        }
        return $result;
    }
} 
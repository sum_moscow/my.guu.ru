<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 24.02.2015
 * Time: 20:36
 */

require_once 'Class.AbstractResident.php';
class DormResident extends AbstractResident{
    private $id;
    private $student_id;
    private $phone_number;
    private $application_number;
    private $application_date;
    private $application_document;
    private $room_id;
    private $status_id;
    private $status_description;
    private $status_reason;
    private $came_from;
    private $distance_from_Moscow;
    private $note;

    public function __construct($id, $student_id, $phone_number,
                                $application_number, $application_date, $application_document,
                                $room_id, $status_id, $status_description, $status_reason,
                                $came_from, $distance_from_Moscow, $note){
        $this->id = $id ? (int)$id : null;
        $this->student_id = (int)$student_id;
        $this->phone_number = $phone_number;
        $this->application_number = $application_number ? (int)$application_number : null;
        $this->application_date = $application_date;
        $this->application_document = $application_document;
        $this->room_id = $room_id ? (int)$room_id : null;
        $this->status_id = $status_id ? (int)$status_id : null;
        $this->status_description = $status_description;
        $this->status_reason = $status_reason;
        $this->came_from = $came_from;
        $this->distance_from_Moscow = $distance_from_Moscow ? (int)$distance_from_Moscow : null;
        $this->note = $note;
    }

    public function setPhoneNumber($phone_number){
        if(!preg_match('(((\+7)|(8)){1}\d{10})', $phone_number)) throw new InvalidArgumentException('incorrect phone number');
        $this->phone_number = $phone_number;
    }
    public function setApplicationDate($application_date){
        $this->application_date = $application_date;
    }
    public function setApplicationDocument($application_document){
        $this->application_document = $application_document;
    }
    public function setApplicationNumber($application_number){
        $this->application_number = $application_number ? (int)$application_number : null;
    }
    //TODO CHECK
    public function setRoomId($room_id){
        $this->room_id = $room_id ? (int)$room_id : null;
    }
    public function setStatusId($status_id){
        $this->status_id = $status_id ? (int)$status_id : null;
    }
    public function setStatusReason($status_reason){
        $this->status_reason = $status_reason;
    }
    public function setCameFrom($came_from){
        $this->came_from = $came_from;
    }
    public function setDistanceFromMoscow($distance_from_Moscow){
        $this->distance_from_Moscow = $distance_from_Moscow ? (int)$distance_from_Moscow : null;
    }
    public function setNote($note){
        $this->note = $note;
    }
    public function getId(){
        return $this->id;
    }
    public function getStudentId(){
        return $this->student_id;
    }
    public function getPhoneNumber(){
        return $this->phone_number;
    }
    public function getApplicationDate(){
        return $this->application_date;
    }
    public function getApplicationDocument(){
        return $this->application_document;
    }
    public function getApplicationNumber(){
        return $this->application_number;
    }
    public function getRoomId(){
        return $this->room_id;
    }
    public function getStatusId(){
        return $this->status_id;
    }
    public function getStatusDescription(){
        return $this->status_description;
    }
    public function getStatusReason(){
        return $this->status_reason;
    }
    public function getCameFrom(){
        return $this->came_from;
    }
    public function getDistanceFromMoscow(){
        return $this->distance_from_Moscow;
    }
    public function getNote(){
        return $this->note;
    }

    public function getInfo(){
        return array(
            'id' => $this->id,
            'student_id' => $this->student_id,
            'phone_number' => $this->phone_number,
            'application_number' => $this->application_number,
            'application_date' => $this->application_date,
            'application_document' => $this->application_document,
            'room_id' => $this->room_id,
            'status_id' => $this->status_id,
            'status_description' => $this->status_description,
            'status_reason' => $this->status_reason,
            'came_from' => $this->came_from,
            'distance_from_Moscow' => $this->distance_from_Moscow,
            'note' => $this->note,
        );
    }

    public static function create($id, stdClass $data){
        return new DormResident(
            $id,
            $data->student_id,
            $data->phone_number,
            $data->application_number,
            $data->application_date,
            $data->application_document,
            $data->room_id,
            $data->status_id,
            $data->status_description,
            $data->status_reason,
            $data->came_from,
            $data->distance_from_Moscow,
            $data->note
        );
    }
    public static function createFromArray($id, array $data){
        if (!isset($data['student_id'])) throw new InvalidArgumentException('no resident student_id');
        if (!isset($data['application_date'])) throw new InvalidArgumentException('no resident application_date');
        $resident = new DormResident($id, $data['student_id'], null, null, $data['application_date'],
            null, null, null, null, null, null, null, null);
        !isset($data['phone_number']) ?: $resident->setPhoneNumber($data['phone_number']);
        !isset($data['application_number']) ?: $resident->setApplicationNumber($data['application_number']);
        !isset($data['application_document']) ?: $resident->setApplicationDocument($data['application_document']);
        !isset($data['room_id']) ?: $resident->setRoomId($data['room_id']);
        !isset($data['status_id']) ?: $resident->setStatusId($data['status_id']);
        !isset($data['status_reason']) ?: $resident->setStatusReason($data['status_reason']);
        !isset($data['came_from']) ?: $resident->setCameFrom($data['came_from']);
        !isset($data['distance_from_Moscow']) ?: $resident->setDistanceFromMoscow($data['distance_from_Moscow']);
        !isset($data['note']) ?: $resident->setNote($data['note']);
        return $resident;
    }

    /**
     * @return Result
     * @param PDO $db
     * @throws DBQueryException
     */
//    public static function getStatuses($db){
//        $q_get = 'SELECT id, resident_status FROM dorm_residents_statuses ORDER BY id';
//        $p_get = $db->prepare($q_get);
//        $q_res = $p_get->execute();
//        if ($q_res === FALSE) throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
//
//        $result = $p_get->fetchAll();
//        if(!$result) throw new LogicException('No statuses in db');
//        foreach($result as &$row)
//            $row['id'] = (int)$row['id'];
//        return new Result(true, "", $result);
//    }

    /**
     * DEPRECATED
     * Обновляет статусы резидентов в пакетном режиме через payload
     * Пример входных данных:
     *
        [
            {"id":6,"status_id":3},
            {"id":7,"status_id":3}
        ]
     * @param PDO $db
     * @param $params
     * @return Result
     * @throws DBQueryException
     * @throws Exception
     */
    public static function setStatusPack(PDO $db, $params){
        $data = $params['payload'];
        $q_upd = 'UPDATE dorm_residents SET status_id = :status_id WHERE id = :id';
        $p_upd = $db->prepare($q_upd);
        $trans_start = $db->beginTransaction();
        if($trans_start === FALSE) throw new DBQueryException('Can not start transaction', $db);
        try{
            foreach($data as $resident){
                $q_res = $p_upd->execute(array(
                    ':id' => $resident['id'],
                    ':status_id' => $resident['status_id'],
                ));
                if(!$q_res) throw new DBQueryException('Cannot update resident status', $db);
            }
        }catch (Exception $e){
            $db->rollBack();
            throw $e;
        }
        $res = $db->commit();
        if(!$res) throw new DBQueryException('commit error', $db);
        return new Result(true);
    }
}

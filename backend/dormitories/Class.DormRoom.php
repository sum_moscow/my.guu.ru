<?php

/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 24.02.2015
 * Time: 20:37
 */
class DormRoom{
    private $id;
    private $number;
    private $block_id;
    private $room_area;
    private $function;
    private $status_id;
    private $status_description;
    private $places_count;
    private $places_reserved_count;
    private $residents_count;
    private $places_available_count;



    public function __construct($id, $number, $block_id, $room_area, $function,
                                $status_id, $status_description,
                                $places_count, $places_reserved_count,
                                $residents_count, $places_available_count){
        $this->id = (int)$id;
        $this->number = (int)$number;
        $this->block_id = (int)$block_id;
        $this->room_area = (float)$room_area;
        $this->function = $function;
        $this->status_id = $status_id ? (int)$status_id : null;
        $this->status_description = $status_description;
        $this->places_count = (int)$places_count;
        $this->places_reserved_count = (int)$places_reserved_count;
        $this->residents_count = (int)$residents_count;
        $this->places_available_count = (int)$places_available_count;
    }

    public function getInfo(){
        return array(
            'id' => $this->id,
            'number' => $this->number,
            'block_id' => $this->block_id,
            'room_area' => $this->room_area,
            'function' => $this->function,
            'status_id' => $this->status_id,
            'status_description' => $this->status_description,
            'places_count' => $this->places_count,
            'places_reserved_count' => $this->places_reserved_count,
            'residents_count' => $this->residents_count,
            'places_available_count' => $this->places_available_count,
        );
    }

    /**
     * @param int $number
     */
    public function setNumber($number){
        $this->number = (int)$number;
    }

    /**
     * @param mixed $function
     */
    public function setFunction($function){
        $this->function = $function;
    }

    /**
     * @param int $status_id
     */
    public function setStatusId($status_id){
        $this->status_id = $status_id ? (int)$status_id : null;
    }

    /**
     * @param float $room_area
     */
    public function setRoomArea($room_area){
        $this->room_area = (float)$room_area;
    }

    /**
     * @param int $places_reserved_count
     */
    public function setPlacesReservedCount($places_reserved_count){
        $this->places_reserved_count = (int)$places_reserved_count;
    }

    /**
     * @param int $places_count
     */
    public function setPlacesCount($places_count){
        $this->places_count = (int)$places_count;
    }

    /**
     * @return bool
     */
    public function isAvailable(){
        if ($this->places_available_count > 0)
            return true;
        else
            return false;
    }

//    /**
//     * @param PDO $db
//     * @return Result
//     * @throws DBQueryException
//     */
//    public static function getStatuses(PDO $db){
//        $q_get = 'SELECT id, room_status FROM dorm_rooms_statuses';
//        $p_get = $db->prepare($q_get);
//        $q_res = $p_get->execute();
//        if ($q_res === FALSE)
//            throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
//
//        $result = $p_get->fetchAll();
//        if (!$result)
//            return new Result(true, "не найдено");
//
//        foreach ($result as &$row){
//            $row['id'] = (int)$row['id'];
//        }
//        return new Result(true, "", $result);
//    }

    public function addResident(AbstractResident $resident){
        if(!$this->isAvailable())
            return false;
        $resident->setRoomId($this->id);
        $resident->setStatusId(AbstractResident::RESIDENT_INHABITED);
        return true;
    }
    //
    //TODO статус айди и комментарий к статусу
    //public function moveOutResident($resident_id){
    //    $resident = new DormResident($resident_id, $this->db);
    //    $resident->setRoomId(null);
    //    $resident->setStatusId(self::RESIDENT_EVICTED);
    //    return new Result(true);
    //}

    /**
     * @return int
     */
    public function getPlacesAvailableCount(){
        return $this->places_available_count;
    }

    /**
     * @return int
     */
    public function getResidentsCount(){
        return $this->residents_count;
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return int
     */
    public function getBlockId(){
        return $this->block_id;
    }

    /**
     * @return mixed
     */
    public function getRoomArea(){
        return $this->room_area;
    }

    /**
     * @return mixed
     */
    public function getFunction(){
        return $this->function;
    }

    /**
     * @return int
     */
    public function getPlacesCount(){
        return $this->places_count;
    }

    /**
     * @return int
     */
    public function getPlacesReservedCount(){
        return $this->places_reserved_count;
    }

    /**
     * @return int
     */
    public function getStatusId(){
        return $this->status_id;
    }
    /**
     * @return mixed
     */
    public function getStatusDescription(){
        return $this->status_description;
    }

    /**
     * @return int
     */
    public function getNumber(){
        return $this->number;
    }

    public static function create($id, stdClass $data){
        return new DormRoom(
            $id,
            $data->number,
            $data->block_id,
            $data->room_area,
            $data->function,
            $data->status_id,
            $data->status_description,
            $data->places_count,
            $data->places_reserved_count,
            $data->residents_count,
            $data->places_available_count
        );
    }
    public static function createFromArray($id, array $data){
        if (!isset($data['number'])) throw new InvalidArgumentException('no room number');
        if (!isset($data['block_id'])) throw new InvalidArgumentException('no room block_id');
        if (!isset($data['places_count'])) throw new InvalidArgumentException('no room places_count');
        $room = new DormRoom($id, $data['number'], $data['block_id'],
            null, null, null, null, $data['places_count'], null, null, null);

        !isset($data['function']) ?: $room->setFunction($data['function']);
        !isset($data['status_id']) ?: $room->setStatusId($data['status_id']);
        !isset($data['room_area']) ?: $room->setRoomArea($data['room_area']);
        !isset($data['places_reserved_count']) ?: $room->setPlacesReservedCount($data['places_reserved_count']);
        return $room;
    }
} 
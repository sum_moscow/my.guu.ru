<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 04.08.2015
 * Time: 17:09
 */

require_once 'Class.DormResident.php';
require_once $ROOT_PATH . '/backend/students/Class.AbstractStudent.php';
class DormResidentWithStudentInfo extends DormResident{
    /**
     * @var AbstractStudent
     */
    private $student;

    public function __construct(AbstractStudent $student, $id, $student_id, $application_date, $phone_number,
                                $application_number, $application_document, $room_id,
                                $status_id, $status_description, $status_reason,
                                $came_from, $distance_from_Moscow, $note){
        parent::__construct($id, $student_id, $phone_number,
                            $application_number, $application_date, $application_document, $room_id,
                            $status_id, $status_description, $status_reason,
                            $came_from, $distance_from_Moscow, $note);
        $this->student = $student;
    }
    public function getInfo(){
        $result = $this->student->getInfo();
        $result = array_merge($result, parent::getInfo());
        return $result;
    }
    public static function create(AbstractStudent $student, $id, stdClass $data){
        return new DormResidentWithStudentInfo(
            $student,
            $id,
            $data->student_id,
            $data->application_date,
            $data->phone_number,
            $data->application_number,
            $data->application_document,
            $data->room_id,
            $data->status_id,
            $data->status_description,
            $data->status_reason,
            $data->came_from,
            $data->distance_from_Moscow,
            $data->note
        );
    }
} 
<?php

/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 26.02.2015
 * Time: 20:09
 */

require_once 'Class.AbstractBlock.php';

class DormBlock extends AbstractBlock{
    private $id;
    private $number;
    private $building_id;
    private $floor;
    private $status_id;
    private $area;
    private $places_count;
    private $places_reserved_count;
    private $residents_count;
    private $places_available_count;

    public function __construct($id, $number, $building_id, $floor, $status_id, $area,
                                $places_count, $places_reserved_count,
                                $residents_count, $places_available_count){
        $this->id = (int)$id;
        $this->number = (int)$number;
        $this->building_id = (int)$building_id;
        $this->floor = (int)$floor;
        $this->status_id = $status_id ? (int)$status_id : null;
        $this->area = (float)$area;
        $this->places_count = (int)$places_count;
        $this->places_reserved_count = (int)$places_reserved_count;
        $this->residents_count = (int)$residents_count;
        $this->places_available_count = (int)$places_available_count;
    }

    /**
     * @return float
     */
    public function getArea(){
        return $this->area;
    }

    /**
     * @return int
     */
    public function getBuildingId(){
        return $this->building_id;
    }

    /**
     * @return int
     */
    public function getFloor(){
        return $this->floor;
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return int
     */
    public function getNumber(){
        return $this->number;
    }

    /**
     * @return int
     */
    public function getPlacesAvailableCount(){
        return $this->places_available_count;
    }

    /**
     * @return int
     */
    public function getPlacesCount(){
        return $this->places_count;
    }

    /**
     * @return int
     */
    public function getPlacesReservedCount(){
        return $this->places_reserved_count;
    }

    /**
     * @return int
     */
    public function getResidentsCount(){
        return $this->residents_count;
    }

    /**
     * @return int
     */
    public function getStatusId(){
        return $this->status_id;
    }

    /**
     * @param int $status_id
     */
    public function setStatusId($status_id){
        $this->status_id = $status_id ? (int)$status_id : null;
    }

    /**
     * @param int $building_id
     */
    public function setBuildingId($building_id){
        $this->building_id = (int)$building_id;
    }

    /**
     * @param int $floor
     */
    public function setFloor($floor){
        $this->floor = (int)$floor;
    }

    /**
     * @param int $number
     */
    public function setNumber($number){
        $this->number = (int)$number;
    }



    public function getInfo(){
        return array(
            'id' => $this->id,
            'number' => $this->number,
            'building_id' => $this->building_id,
            'floor' => $this->floor,
            'status_id' => $this->status_id,
            'area' => $this->area,
            'places_count' => $this->places_count,
            'places_reserved_count' => $this->places_reserved_count,
            'residents_count' => $this->residents_count,
            'places_available_count' => $this->places_available_count,
        );
    }

    public static function create($id, stdClass $data){
        return new static(
            $id,
            $data->number,
            $data->building_id,
            $data->floor,
            $data->status_id,
            $data->area,
            $data->places_count,
            $data->places_reserved_count,
            $data->residents_count,
            $data->places_available_count
        );
    }
    public static function createFromArray($id, array $data){
        if (!isset($data['number'])) throw new InvalidArgumentException('no block number');
        if (!isset($data['building_id'])) throw new InvalidArgumentException('no block building_id');
        if (!isset($data['floor'])) throw new InvalidArgumentException('no block floor');

        $block = new static($id, $data['number'], $data['building_id'], $data['floor'],
            null, null, null, null, null, null);
        !isset($data['status_id']) ?: $block->setStatusId($data['status_id']);
        return $block;
    }
    /**
     * @param $db
     * @return Result
     * @throws DBQueryException
     */
/*    public static function getStatuses(PDO $db){
        $q_get = 'SELECT id, room_status FROM dorm_rooms_statuses';
        $p_get = $db->prepare($q_get);
        $p_get->execute();
        if ($p_get === FALSE)
            throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        else{
            $result = $p_get->fetchAll();
            if (count($result) != 0)
                foreach ($result as &$row)
                    foreach ($row as &$value)
                        if (is_numeric($value))
                            $value = (int)$value;
            return new Result(true, "", $result);
        }
    }*/

}
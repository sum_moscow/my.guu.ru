<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 24.02.2015
 * Time: 20:38
 *
 *  api
 * get
 * +   my.guu.ru/api/dormitories/<id>
 * my.guu.ru/api/dormitories/sum
 * +   my.guu.ru/api/dormitories/search
 * return dorms id
 * my.guu.ru/api/dormitories/{blocks | rooms | residents}/<id>
 * my.guu.ru/api/dormitories/{blocks | rooms | residents}/search?{param=value[…]}
 * available parameters at $available
 * if none parameters return all records
 * my.guu.ru/api/dormitories/residents/statuses
 * my.guu.ru/api/dormitories/residents/student?{param=value[…]}
 *  all parameters required
 *   post
 * my.guu.ru/api/dormitories/{blocks | rooms | residents}/<id>?{param=value[…]}
 *  available parameters at $available
 * my.guu.ru/api/dormitories/{blocks | rooms | residents}/add?{param=value[…]}
 *  required parameters at $required
 *  available parameters at $available
 */


require_once 'Class.Dormitory.php';
require_once 'Class.DormBlock.php';
require_once 'Class.DormRoom.php';
require_once 'Class.DormResident.php';
require_once 'Class.DormFloor.php';

require_once 'Class.DormModule.php';
global $ROOT_PATH;
global $__db;
require_once $ROOT_PATH . '/backend/students/Class.StudentObject.php';

$__modules[Dormitory::MODULE_NAME] = array(
    'GET' => array(
        //Dormitory::MODULE_NAME . '/{(id:[0-9]+)}' => function ($id) use ($__db){
        //    $dorm = new Dormitory($id, $__db);
        //    return $dorm->getInfo();
        //},
        '{block/(id:[0-9]+)}' => function ($id) use ($__db){
            return new Result(true, '', DormModule::selectBlock($__db, $id)->getInfo());
        },
        '{block/search}' => function () use ($__db, $__request){
            return new Result(true, '', DormModule::selectBlockList($__db, $__request)->getInfo());
        },
        //'{block/statuses}' => function () use ($__db){
        //        return DormBlock::getStatuses($__db);
        //},
        '{room/(id:[0-9]+)}' => function ($id) use ($__db){
            return new Result(true, '', DormModule::selectRoom($__db, $id)->getInfo());
        },
        '{room/search}' => function () use ($__db, $__request){
            return new Result(true, '', DormModule::selectRoomList($__db, $__request)->getInfo());
        },
        //'{room/statuses}' => function () use ($__db){
        //    return DormRoom::getStatuses($__db);
        //},
        //'{resident/statuses}' => function () use ($__db){
        //    return DormResident::getStatuses($__db);
        //},
        '{resident/search}' => function () use ($__db, $__request){
            return new Result(true, '', DormModule::selectResidentList($__db, $__request)->getInfo());
        },
        //TODO DEPRECATED
        '{resident/student}' => function () use ($__db, $__request){
            return StudentObject::searchStudent($__db, $__request);
        },
        //'{search}' => function () use ($__db){
        //    return Dormitory::search($__db);
        //},
        //'{sum}' => function () use ($__db){
        //    return Dormitory::getSumInfo($__db);
        //},
        '{contract/(id:[0-9]+)}' => function($id) use ($__db){
            return new Result(true, '', DormModule::selectContract($__db, $id)->getInfo());
        },
        '{resident/(resident_id:[0-9]+)/contract/active}' => function ($resident_id) use ($__db){
            return new Result(true, '', DormModule::selectContractHistory($__db, $resident_id)->getActive());
        },
        '{resident/(resident_id:[0-9]+)/contract/list}' => function ($resident_id) use ($__db){
            return new Result(true, '', DormModule::selectContractHistory($__db, $resident_id)->getList());
        },
        '{test/(resident_id:[0-9]+)}' => function($resident_id) use ($__db){
            return new Result(true, '', DormModule::selectResidentWithStudentInfo($__db, $resident_id)->getInfo());
        },
        '{resident/(id:[0-9]+)}' => function($id) use ($__db){
            return new Result(true, '', DormModule::selectResident($__db, $id)->getInfo());
        },
        '{floor}' => function() use ($__db, $__request){
            return new Result(true, '', DormModule::selectFloor($__db, $__request)->getInfo());
        }
    ),
    'POST' => array(
        'block' => function () use ($__db, $__args, $__request){
            return new Result(true, '', ['id' => DormModule::insertBlock($__db, $__request)]);
        },
        //'{room/(room_id:[0-9]+)/resident/(resident_id:[0-9]+)}' => function ($room_id, $resident_id) use ($__db){
        //    $room = new DormRoom($room_id, $__db);
        //    $room->addResident($resident_id);
        //},
        'room' => function () use ($__db, $__request) {
            return new Result(true, '', ['id' => DormModule::insertRoom($__db, $__request)]);
        },
        'resident' => function () use ($__db, $__request) {
            return new Result(true, '', ['id' => DormModule::insertResident($__db, $__request)]);
        },
        'contract' => function() use($__db, $__request){
            return new Result(true, '', ['id' => DormModule::insertContract($__db, $__request)]);
        }
    ),
    'PUT' => array(
        '{block/(id:[0-9]+)}' => function ($id) use ($__db, $__request){
            $block = DormModule::selectBlock($__db, $id);
            DormModule::updateBlock($__db, $block, $__request);
            return new Result(true);
        },
        '{room/(id:[0-9]+)}' => function ($id) use ($__db, $__request){
            $room = DormModule::selectRoom($__db, $id);
            DormModule::updateRoom($__db, $room, $__request);
            return new Result(true);
        },
        '{resident/(id:[0-9]+)}' => function ($id) use ($__db, $__request){
            $resident = DormModule::selectResident($__db, $id);
            DormModule::updateResident($__db, $resident, $__request);
            return new Result(true);
        },
        //TODO DEPRECATED
        '{resident/status}' => function () use ($__db, $__request){
            return DormResident::setStatusPack($__db, $__request);
        },
        '{contract/(id:[0-9]+)}' => function($id) use ($__db, $__request){
            $contract = DormModule::selectContract($__db, $id);
            DormModule::updateContract($__db, $contract, $__request);
            return new Result(true);
        }
    ),
    'DELETE' => array(
        //'{room/(room_id:[0-9]+)/resident/(resident_id:[0-9]+)}' => function($room_id, $resident_id) use ($__db, $__request){
        //    $room = new DormRoom($room_id, $__db);
        //    return $room->moveOutResident($resident_id);
        //}
    )
);

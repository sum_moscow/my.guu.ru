<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 05.08.2015
 * Time: 18:19
 */

abstract class AbstractResident{
    const RESIDENT_INHABITED = 1;
    //const RESIDENT_EVICTED = 1;
    public abstract function getId();
    public abstract function getInfo();

    public abstract function getStudentId();
    public abstract function getPhoneNumber();
    public abstract function getApplicationDate();
    public abstract function getApplicationDocument();
    public abstract function getRoomId();
    public abstract function getStatusId();
    public abstract function getStatusDescription();
    public abstract function getStatusReason();
    public abstract function getCameFrom();
    public abstract function getDistanceFromMoscow();
    public abstract function getNote();

    public abstract function setPhoneNumber($phone_number);
    public abstract function setApplicationDate($application_date);
    public abstract function setApplicationDocument($application_document);
    public abstract function setApplicationNumber($application_number);
    public abstract function setRoomId($room_id);
    public abstract function setStatusId($status_id);
    public abstract function setStatusReason($status_reason);
    public abstract function setCameFrom($came_from);
    public abstract function setDistanceFromMoscow($distance_from_Moscow);
    public abstract function setNote($note);
} 
<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 04.08.2015
 * Time: 14:16
 */

abstract class AbstractBlock{
    public abstract function getId();
    public abstract function getInfo();

    public abstract function getFloor();
    public abstract function getBuildingId();
    public abstract function getNumber();
    public abstract function getStatusId();

    public abstract function setNumber($number);
    public abstract function setFloor($floor);
    public abstract function setBuildingId($building_id);
    public abstract function setStatusId($status_id);
}
<?php

require_once 'Class.QuestionsList.php';
require_once 'Class.ProfessorsAnswers.php';
require_once 'Class.ScienceStatistics.php';

$__modules['science'] = array(
	'GET' => array(
		'questions' => function() use ($__user, $__db){
			return QuestionsList::getFullList($__db);
		},
		'answers' => function() use ($__user, $__db, $__request, $__args){
			if ($__args[0] == 'me'){
				$answers = new ProfessorsAnswers($__user->getProfessorInstance(), $__db);
				if (isset($__request['mod']) && $__request['mod'] == 'details'){
					return $answers->calculatePoints($__user->getProfessorInstance()->getStaffId());
				}else{
					return $answers->getFullList();
				}
			}elseif (is_numeric($__args[0])){
				$answers = new ProfessorsAnswers(null, $__db);

				if (isset($__request['mod']) && $__request['mod'] == 'details'){
					return $answers->calculatePoints($__args[0]);
				}else {
					return $answers->getFullList($__args[0]);
				}
			}
		},
		'stats' => function() use ($__user, $__db, $__request, $__args){
			$stats = new ScienceStatistics($__db);
			if (isset($__args[0]) && $__args[0] == 'academic_departments'){
				if (isset($__args[1]) && is_numeric($__args[1])){
					return $stats->getStaffInAcademicDepartment($__args[1]); //Academic department OU ID
				}
			}else{
				$for_graph = false;
				if (isset($__request['format']) && $__request['format'] == 'for_graph'){
					$for_graph = true;
				}
				return $stats->get($for_graph);
			}

		},
		'{rating/(staff_id:.*?)/periods/(period_id:[0-9]+)}}' => function($staff_id, $period_id) use($__user, $__db, $__request, $__args){
			if ($staff_id == 'me'){
				$ans = new ProfessorsAnswers($__user->getProfessorInstance(), $__db);
				return $ans->getRatingForStaffId(null, $period_id);
			}elseif (is_numeric($staff_id)){
				$ans = new ProfessorsAnswers(null, $__db);
				return $ans->getRatingForStaffId($staff_id, $period_id);
			}
		}
	),
	'POST' => array(
		'answers' => function() use ($__user, $__db, $__request, $__args){
			if ($__args[0] == 'me'){
				$answers = new ProfessorsAnswers($__user->getProfessorInstance(), $__db);
				return $answers->save($__request);
			}
		},
	),
	'PUT' => array(
		'{staff/(staff_id:[0-9]+)/periods/(period_id:[0-9]+)}/verification}' => function($staff_id, $period_id) use ($__user, $__db, $__request, $__args){
			$answers = new ProfessorsAnswers(null, $__db);
			return $answers->setVerified($staff_id, $period_id, $__request['value'], $__user->getStaffInstance());
		}

	)
);
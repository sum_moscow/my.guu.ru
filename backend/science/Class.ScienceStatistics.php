<?php

class ScienceStatistics{


	private $period_id;
	private $period_var;
	private $period_arr;
	private $db;

	public function __construct(PDO $db, $period_id = null){
		if ($period_id == null || $period_id == 'current'){
			$this->period_var = '(SELECT id FROM science_periods WHERE NOW() BETWEEN start_date AND finish_date)';
			$this->period_arr = array();
		}else{
			$this->period_var = ':period_id';
			$this->period_arr = array(
				':period_id' => $period_id
			);
		}
		$this->period_id = $period_id;
		$this->db = $db;
	}


	private function medianForArray($array) {
		// perhaps all non numeric values should filtered out of $array here?
		$iCount = count($array);
		if ($iCount == 0) {
			throw new DomainException('Median of an empty array is undefined');
		}
		// if we're down here it must mean $array
		// has at least 1 item in the array.
		$middle_index = (int) floor($iCount / 2);
		sort($array, SORT_NUMERIC);
		$median = $array[$middle_index];
		if ($iCount % 2 == 0) {
			$median = ($median + $array[$middle_index - 1]) / 2;
		}
		return $median;
	}

	private function normalize($data, $for_graph){
		$result_arr = array();
		$final_arr = array();
		if ($for_graph){
			foreach($data as $item){
				if (!isset($result_arr[$item['institute_ou_id']])){
					$result_arr[$item['institute_ou_id']] = array(
						'name' => $item['institute_name'],
						'institute_ou_id' => $item['institute_ou_id'],
						'knk_arr' => array(),
						'kpk_arr' => array(),
						'drilldown' => array(
							'name' => $item['institute_name'],
							'__series' => array(
								"{$item['academic_department_ou_id']}" => array(
									'name' => $item['academic_department_name'],
									'academic_department_ou_id' => $item['academic_department_ou_id'],
									'knk_arr' => array(),
									'kpk_arr' => array()
								)
							)
						)
					);
				}
				if (!isset($result_arr[$item['institute_ou_id']]['drilldown']['__series']["{$item['academic_department_ou_id']}"])){
					$result_arr[$item['institute_ou_id']]['drilldown']['__series']["{$item['academic_department_ou_id']}"] = array(
						'name' => $item['academic_department_name'],
						'academic_department_ou_id' => $item['academic_department_ou_id'],
						'knk_arr' => array(),
						'kpk_arr' => array()
					);
				}
				$key = $item['indicator_id'] == 1 ? 'kpk_arr' : 'knk_arr';
				$result_arr[$item['institute_ou_id']][$key][] = $item['value'];
				$result_arr[$item['institute_ou_id']]['drilldown']['__series']["{$item['academic_department_ou_id']}"][$key][] = $item['value'];
			}

			foreach($result_arr as $key => $item){
				$result_arr[$key]['data'] = array(
					(float) $this->medianForArray($item['knk_arr']),
					(float) $this->medianForArray($item['kpk_arr'])
				);
				$deps_count = 0;
				foreach($result_arr[$key]['drilldown']['__series'] as $dep){
					$result_arr[$key]['drilldown']['series'][] = $dep;
					$result_arr[$key]['drilldown']['series'][$deps_count]['data'] = array(
						(float) $this->medianForArray($dep['knk_arr']),
						(float) $this->medianForArray($dep['kpk_arr'])
					);
					unset($result_arr[$key]['drilldown']['__series']);
					unset($result_arr[$key]['drilldown']['series'][$deps_count]['kpk_arr']);
					unset($result_arr[$key]['drilldown']['series'][$deps_count]['knk_arr']);
					$deps_count++;
				}
				unset($result_arr[$key]['kpk_arr']);
				unset($result_arr[$key]['knk_arr']);
				$final_arr[] = $result_arr[$key];
			}
		}else{

		}

		return new Result(true, '', $final_arr);
	}

	public function get($for_graph){
		$q_get_values = 'SELECT DISTINCT view_users.id, science_indicators_values.indicator_id, science_indicators_values.value,
 			view_users.ou_id, view_users.staff_id, academic_departments.name as academic_department_name,
 			view_users.ou_id as academic_department_ou_id,
 			(SELECT organizational_units.id FROM organizational_units WHERE id =
 				(SELECT organizational_units.parent_id FROM organizational_units WHERE id = academic_department_ou_id)) as institute_ou_id,
 			(SELECT organizational_units.name FROM organizational_units WHERE id =
 				(SELECT organizational_units.parent_id FROM organizational_units WHERE id = academic_department_ou_id)) as institute_name
			FROM view_users
			INNER JOIN science_indicators_values ON science_indicators_values.staff_id = view_users.staff_id
			INNER JOIN science_periods ON science_periods.id = science_indicators_values.period_id
			INNER JOIN academic_departments ON academic_departments.ou_id = view_users.ou_id
			WHERE science_indicators_values.period_id = ' . $this->period_var;
		$p_get_values = $this->db->prepare($q_get_values);
		$p_get_values->execute($this->period_arr);
		if ($p_get_values === FALSE) throw new DBQueryException('STATS_ERROR', $this->db);
		return $this->normalize($p_get_values->fetchAll(), $for_graph);
	}

	public function getStaffInAcademicDepartment($academic_department_ou_id) {
		$q_get_info = 'SELECT view_users.first_name, view_users.last_name, view_users.middle_name,
			view_users.id, view_users.id as user_id, view_users.staff_id, science_indicators_values.indicator_id, science_indicators.name, science_indicators_values.value
			FROM view_users
			INNER JOIN science_indicators_values ON view_users.staff_id = science_indicators_values.staff_id
			INNER JOIN staff ON staff.id = science_indicators_values.staff_id
			INNER JOIN science_indicators ON science_indicators.id = science_indicators_values.indicator_id
			WHERE view_users.ou_id = :academic_department_ou_id
			AND view_users.is_professor = 1';
		$p_get_indicators = $this->db->prepare($q_get_info);
		$p_get_indicators->execute(array(':academic_department_ou_id' => $academic_department_ou_id));

		$tmp_array = array();
		$result_array = $p_get_indicators->fetchAll();

		foreach($result_array as $line){
			if (!isset($tmp_array[$line['id']])){
				$tmp_array[$line['id']] = array(
					'first_name' => $line['first_name'],
					'id' => $line['id'],
					'user_id' => $line['user_id'],
					'staff_id' => $line['staff_id'],
					'last_name' => $line['last_name'],
					'middle_name' => $line['middle_name'],
					'indicators' => array()
				);
			}
			$tmp_array[$line['id']]['indicators'][] = array(
				'indicator_id' => $line['indicator_id'],
				'name' => $line['name'],
				'value' => $line['value'],
			);
		}
		$result_array = array();
		foreach($tmp_array as $professor){
			$result_array[] = $professor;
		}

		return new Result(true, '', $result_array);
	}


}
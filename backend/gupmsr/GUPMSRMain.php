<?php

class GUPMSR{
	const UNIVERSITY_CODE = '019';

	const APPLICATION_PUBLIC_KEY = '019'; //TODO: application, that can access data
	const XMLNS = "http://university.sm.msr.com/schemas/incoming";
	const XMLXS = "http://www.w3.org/2001/XMLSchema";
	const TARGET_NAMESPACE = 'http://university.sm.msr.com/schemas/incoming';
	const XML_VERSION = '1.1';

	public static function generateXML(ApiApplication $__api_app, PDO $db){

		$today_file_number = self::startGeneration($db);

		$q_get_students = 'SELECT
			users.first_name,
			users.last_name,
			users.middle_name,
			users.birth_date,
			users.snils,
			students.id as student_id,
			 users.sex,
			 passports.*, DATE(IFNULL(passports.updated_at,passports.created_at)) as passport_update_date,
			 doc_types.soccardXML as doc_type_soccardXML,
			 students.phone_number,
			 users.ad_login,
			 students.begin_study,
			study_statuses.soccardXML as study_status_soccardXML,
			edu_groups.course,
			edu_forms.soccardXML as edu_form_soccardXML,
			institutes.soccardXML as institute_soccardXML,
			institutes.name as institute_name,

			registration_addresses.building as registration_addresses_building,
			registration_addresses.city as registration_addresses_city,
			registration_addresses.country as registration_addresses_country,
			registration_addresses.district as registration_addresses_district,
			registration_addresses.flat as registration_addresses_flat,
			registration_addresses.kladr as registration_addresses_kladr,
			registration_addresses.region as registration_addresses_region,
			registration_addresses.street as registration_addresses_street,

			residence_addresses.building as residence_addresses_building,
			residence_addresses.city as residence_addresses_city,
			residence_addresses.country as residence_addresses_country,
			residence_addresses.district as residence_addresses_district,
			residence_addresses.flat as residence_addresses_flat,
			residence_addresses.kladr as residence_addresses_kladr,
			residence_addresses.region as residence_addresses_region,
			residence_addresses.street as residence_addresses_street

			FROM students
			INNER JOIN users ON students.user_id = users.id
			INNER JOIN semesters_info ON semesters_info.student_id = students.id
			INNER JOIN edu_groups ON semesters_info.edu_group_id = edu_groups.id
			INNER JOIN edu_forms ON edu_groups.edu_form_id = edu_forms.id
			INNER JOIN passports ON users.passport_id = passports.id
			INNER JOIN addresses residence_addresses ON users.residence_address_id = residence_addresses.id
			INNER JOIN addresses registration_addresses ON users.registration_address_id = registration_addresses.id
			INNER JOIN doc_types ON doc_types.id = passports.doc_type_id
			INNER JOIN edu_programs ON edu_programs.id = edu_groups.edu_program_id
			INNER JOIN institutes ON institutes.id = edu_programs.institute_id
			INNER JOIN study_statuses ON study_statuses.id = semesters_info.study_status_id
			WHERE
				users.last_name IS NOT NULL
			AND users.first_name IS NOT NULL
			AND users.birth_date IS NOT NULL
			AND users.sex IS NOT NULL
			AND passports.number IS NOT NULL
			AND passports.series IS NOT NULL
			AND passports.issue_place IS NOT NULL
			AND passports.issue_date IS NOT NULL
			AND passports.issue_date != \'0000-00-00\'
			AND users.agreement = 1
			AND users.residence_address_id IS NOT NULL
			AND users.registration_address_id IS NOT NULL';
		$students = $db->query($q_get_students)->fetchAll();
		$students_count = count($students);

		$normalized = self::normalizeStudents($students);

		$result = array(
			'tns:file' => array(
				'@attributes' => array(
					'xmlns:tns' => self::XMLNS,
					//'xmlns:xs' => self::XMLXS,
					//'targetNamespace' => self::TARGET_NAMESPACE,
					//'version' => self::XML_VERSION,
				),
				'fileInfo' => array(
					'fileSender' => self::UNIVERSITY_CODE,
					'version' => self::XML_VERSION,
					'recordCount' => $students_count
				),
				'recordList' => array(
					'record' => $normalized['recordList']
				)
			));
		$result = new Result(true, 'Данные успешно получены', $result);
		$date = new DateTime();
		$filename = implode('-', array( //ККК-YYMMDD-XXX-NNNNN.XML
			self::UNIVERSITY_CODE, //ККК – код ВУЗа по НСИ ГУП МСР;
			$date->format('ymd'), //YYMMDD– год, месяц и дата создания файла;
			$today_file_number, //XXX – порядковый номер файла за указанную дату;
			str_pad($students_count, 5, '0', STR_PAD_LEFT) //NNNNN– количество записей в файле.
		));
		$result->setFileName($filename);
		//$result->setXMLSchemaFilePath($ROOT_PATH . '/backend/gupmsr/schema.xsd');
		self::finishGeneration($db, $filename, $today_file_number, $date->format('Y-m-d'), $normalized['students']);
		return $result;
	}

	private static function normalizeStudents(array $students) {
		$final = array();
		$counter = 1;
		$_students = array();
		foreach($students as $student){
			foreach($student as $key => $value){
				$student[$key] = (empty($value) || $value == '') ? null : $value;
			}

			$final[] = array(
				'recordId' => $counter,
				'clientInfo' => array(
					'name' => array(
						'lastName' => $student['last_name'],
						'firstName' => $student['first_name'],
						'middleName' => $student['middle_name'],
					),
					'dateOfBirth' => $student['birth_date'],
					'sex' => $student['sex'] == 1 ? 1 : 2,
					'document' => array(
						'code' => $student['doc_type_soccardXML'],
						'series' => $student['series'],
						'number' => $student['number'],
						'issueDate' => $student['issue_date'],
						'issuedBy' => $student['issue_place'],
						'unitCode' => $student['issue_place_code']
					),
					'registrationAddress' => array(
						'countryName' => $student['registration_addresses_country'],
						'regionName' => $student['registration_addresses_region'],
						'districtName' => $student['registration_addresses_district'],
						'cityName' => $student['registration_addresses_city'],
						'streetName' => $student['registration_addresses_street'],
						'house' => $student['registration_addresses_building'],
						'flat' => $student['registration_addresses_flat']
					),
					/*
					'photo' => null,
					'contacts' => array(
						'phone' => null,
						'mobilePhone' => null,//$student['phone_number'],
						'email' => null,
					),*/
					'residenceAddress' => array(
						'countryName' => $student['residence_addresses_country'],
						'regionName' => $student['residence_addresses_region'],
						'districtName' => $student['residence_addresses_district'],
						'cityName' => $student['residence_addresses_city'],
						'streetName' => $student['residence_addresses_street'],
						'house' => $student['residence_addresses_building'],
						'flat' => $student['residence_addresses_flat']
					),
					//'insuranceNumber' => null//$student['snils']
				),
				'universityInfo' => array(
					'universityCode' => self::UNIVERSITY_CODE,
					'facultyCode' => $student['institute_soccardXML'],
					'facultyName' => $student['institute_name'],
					'studentCardNumber' => null,
					'status' => array(
						'code' => $student['study_status_soccardXML'],
						'date' => $student['passport_update_date'],
					),
					'startDate' => $student['begin_study'],
					'course' => $student['course'],
					'educationType' => $student['edu_form_soccardXML'],
					/*'order' => array(
						'number' => null,
						'date' => null,
						'type' => null
					),*/
					'studentId' => $student['student_id'],
				)
			);
			$_students[] = array(
				'record_number' => $counter,
				'student_id' => $student['student_id'],
			);
			$counter++;
		}
		return array('recordList' => $final, 'students' => $_students);
	}

	private static function startGeneration(PDO $db) {
		$r_msr_sync = $db->query('SELECT file_inday_number, file_create_date, filename
			FROM sync_gupmsr_students
			WHERE file_create_date = DATE(NOW())
			ORDER BY file_inday_number DESC
			LIMIT 1')->fetch();
		return str_pad((intval($r_msr_sync['file_inday_number']) + 1), 3, '0', STR_PAD_LEFT);
	}

	private static function finishGeneration(PDO $db, $filename, $file_inday_number, $file_create_date, array $students) {
		$q_ins_sync = 'INSERT INTO sync_gupmsr_students(created_at, filename, file_create_date, file_inday_number)
			VALUES(NOW(), :filename, :file_create_date, :file_inday_number)';
		$p_ins_sync = $db->prepare($q_ins_sync);
		$result = $p_ins_sync->execute(array(
			':filename' => $filename,
			':file_create_date' => $file_create_date,
			':file_inday_number' => $file_inday_number
		));
		if ($result === FALSE) throw new DBQueryException($db, 'INS_SYNC_ERROR');
		$sync_id = $db->lastInsertId();
		self::insertStudents($db, $sync_id, $students);

	}

	private static function insertStudents(PDO $db, $sync_id, array $students){
		$sql = 'INSERT INTO gupmsr_students_records (sync_gupmsr_students_id, record_number, student_id, created_at) VALUES ';
		$insertQuery = array();
		$insertData = array();
		$n = 0;
		foreach ($students as $row) {
			$insertQuery[] = '(:sync_id, :record_number' . $n . ', :student_id' . $n . ', NOW())';
			$insertData[':record_number' . $n] = $row['record_number'];
			$insertData[':student_id' . $n] = $row['student_id'];
			$n++;
		}
		$insertData[':sync_id'] = $sync_id;
		if (!empty($insertQuery)) {
			$sql .= implode(', ', $insertQuery);
			$stmt = $db->prepare($sql);
			$stmt->execute($insertData);

		}
	}
}
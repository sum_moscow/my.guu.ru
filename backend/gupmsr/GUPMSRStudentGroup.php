<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 11.06.2015
 * Time: 17:26
 */

global $ROOT_PATH;
require_once $ROOT_PATH . '/backend/student_groups/Class.StudentGroup.php';
class GUPMSRStudentGroup extends StudentGroup{
    public function getConfidentialInfoStudentsList(User $user) {
        if(!$this->hasRights($user, $this->getInstituteId()))
            throw new PrivilegesException('Not enough rights to access this information', $this->db);

        $q_get = 'SELECT
			state.message,  `status`.student_status, state.status_id, rec.created_at AS last_sync_time
			FROM gupmsr_students_records AS rec
        	 LEFT JOIN gupmsr_students_states AS state ON rec.student_id = state.student_id
			 LEFT JOIN gupmsr_students_statuses AS `status` ON `status`.id = state.status_id
			WHERE rec.student_id = :student_id
			ORDER BY rec.created_at DESC LIMIT 1';
        $list = parent::getConfidentialInfoStudentsList($user)->getData();
        foreach ($list as &$row){
            $student_id = $row['student_id'];
            $p_get = $this->db->prepare($q_get);
            $q_res = $p_get->execute(array( ':student_id' => $student_id));
            if(!$q_res) throw new DBQueryException('Can not retrieve student status from db', $this->db);
            $res = $p_get->fetch();
            $row['status_id'] = (int)$res['status_id'];
            $row['message'] = $res['message'];
            $row['student_status'] = $res['student_status'];
            $row['last_sync_time'] = $res['last_sync_time'];
        }
        return new Result(true, '', $list);
    }
}
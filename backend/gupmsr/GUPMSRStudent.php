<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 08.06.2015
 * Time: 21:58
 */

global $ROOT_PATH;
require_once $ROOT_PATH . '/backend/students/Class.StudentObject.php';
class GUPMSRStudent extends StudentObject{
    const MIN_LEVEL_TO_READ = 10;
    const MIN_LEVEL_TO_WRITE = 10;
	const STUDENTS_STATUS_UPDATER_KEY = '307a634baf95b1be4291e0aa31090e45';

    public static function getModuleName() {
        return 'gupmsr';
    }

    public static function getStatuses(PDO $db){
        $q_get = 'SELECT id, student_status FROM gupmsr_students_statuses';
        $p_get = $db->prepare($q_get);
        $q_res = $p_get->execute();
        if ($q_res === FALSE) throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');

        $result = $p_get->fetchAll();
        if(count($result) != 0)
            foreach($result as &$row)
                $row['id'] = (int)$row['id'];
        return new Result(true, "", $result);
    }

    public function setStatus($params, ApiApplication $api_app){

	    /* User can not access, only API applications*/
        //if(!$user->hasRights(array('module' => self::getModuleName(), 'min_level' => self::MIN_LEVEL_TO_WRITE))) throw new PrivilegesException('',$this->db);

	    if ($api_app->getPublicKey() !== self::STUDENTS_STATUS_UPDATER_KEY)  throw new PrivilegesException('',$this->db);

        $q_set = 'INSERT INTO gupmsr_students_states(student_id, status_id, message, created_at)
				VALUES(:student_id, :status_id, :message, NOW()) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), student_id=:student_id, status_id=:status_id, message=:message';
        $query_params = array(
            ':student_id' => $this->getId(),
            ':status_id' => $params['status_id'],
            ':message' => implode('; ', $params['messages']),
        );
        $p_set = $this->db->prepare($q_set);
        $q_res = $p_set->execute($query_params);
        if(!$q_res) throw new DBQueryException('Can not set status', $this->db);
        return new Result(true);
    }

    public function getStatus(User $user){
        if(!$user->hasRights(array('module' => self::getModuleName(), 'min_level' => self::MIN_LEVEL_TO_READ))) throw new PrivilegesException('',$this->db);
        $q_get = 'SELECT id, student_id, status_id, message, created_at, updated_at FROM gupmsr_students_states';
        $p_get = $this->db->prepare($q_get);
        $q_res = $p_get->execute();
        if(!$q_res) throw new DBQueryException('Can not get status', $this->db);
        $result = $p_get->fetchAll();
        if(count($result) != 0)
            foreach($result as &$row){
                $row['id'] = (int)$row['id'];
                $row['student_id'] = (int)$row['student_id'];
                $row['status_id'] = (int)$row['status_id'];
            }
        return new Result(true, '', $result);
    }

    public static function ObtainStatusInfo(PDO $db, ApiApplication $api_app, $params){
        $data = $params['payload'];
        try{
            $first_name = $data['name']['firstName'];
            $middle_name = $data['name']['middleName'];
            $last_name = $data['name']['lastName'];

            $first_name = is_null($first_name) ? $first_name : str_replace('_', ' ', $data['name']['firstName']);
            $middle_name = is_null($middle_name) ? $middle_name : str_replace('_', ' ', $data['name']['middleName']);
            $last_name = is_null($last_name) ? $last_name : str_replace('_', ' ', $data['name']['lastName']);

            $sex = $data['sex'];
            $sex = $sex == 'FEMALE' ? $sex = 0 : $sex = 1;

            if(is_null($data['birthday']))
                $birthday = null;
            else{
                //convert into db date format
                $birthday = str_replace('_', '.', $data['birthday']);
                $birthday = explode('.', $birthday);
                $birthday = $birthday[2] . '-' . $birthday[1] . '-' . $birthday[0];
            }
            $status = (int)$data['state'];
            $course = is_null($data['course']) ? null : (int)$data['course'];
            $errors = array();
            if(array_key_exists('personError', $data))
                foreach($data['personError'] as $error){
                    if(is_array($error))
                        $errors[] = $error['message'];
                    else{
                        $errors[] = $data['personError']['message'];
                        break;
                    }
                }
        }
        catch(Exception $e){
            throw new InvalidArgumentException('incorrect data given');
        }
        $search_params = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'middle_name' => $middle_name,
            'birth_date' => $birthday,
            'course' => $course,
            'sex' => $sex
        );
        $search_result = parent::searchStudent($db, $search_params);
        $search_result = $search_result->getData();
        if(count($search_result) == 0)
            throw new LogicException('found no students');
        $result = false;
        foreach($search_result as $row){
            $student = new GUPMSRStudent($row['student_id'], $db);
            $result = $student->setStatus(array(
                'status_id' => $status,
                'messages' => $errors,
            ), $api_app);
        }
        return $result;
    }

    public static function searchConfidentialInfoStudent(PDO $db, User $user, $request){
        $q_get = 'SELECT
			state.message, `status`.student_status, state.status_id, rec.created_at AS last_sync_time
			FROM gupmsr_students_records AS rec
        	 LEFT JOIN gupmsr_students_states AS state ON rec.student_id = state.student_id
			 LEFT JOIN gupmsr_students_statuses AS `status` ON `status`.id = state.status_id
			WHERE rec.student_id = :student_id
			ORDER BY rec.created_at DESC LIMIT 1';
        $list = parent::searchConfidentialInfoStudent($db, $user, $request)->getData();
        foreach ($list as &$row){
            $student_id = $row['student_id'];
            $p_get = $db->prepare($q_get);
            $q_res = $p_get->execute(array(':student_id' => $student_id));
            if(!$q_res) throw new DBQueryException('Can not retrieve student status from db', $db);
            $res = $p_get->fetch();
            $row['status_id'] = (int)$res['status_id'];
            $row['message'] = $res['message'];
            $row['student_status'] = $res['student_status'];
            $row['last_sync_time'] = $res['last_sync_time'];
        }
        return new Result(true, '', $list);
    }

} 
<?php

require_once 'GUPMSRMain.php';
require_once 'GUPMSRStudent.php';
require_once 'GUPMSRStudentGroup.php';
global $__db;

$__modules['gupmsr'] = array(
	'GET' => array(
		'all' => function() use ($__api_app, $__db){
			return GUPMSR::generateXML($__api_app,$__db);
		},
        '{/statuses}' => function() use($__db){
            return GUPMSRStudent::getStatuses($__db);
        },
        '{/(id:[0-9]+)/status}' => function ($id) use ($__db, $__user){
            $id = (int)$id;
            $student = new GUPMSRStudent($id, $__db);
            return $student->getStatus($__user->getStaffInstance());
        },
        '{/student_group/(group_id:[0-9]+)/students}' => function ($group_id) use ($__db, $__user){
            $group_id = (int) $group_id;
            $institute_id = $__user->getStaffInstance()->getInstituteId();
            $student_group = new GUPMSRStudentGroup($group_id, $__db);
            return $student_group->getConfidentialInfoStudentsList($__user->getStaffInstance(), $institute_id);
        },
        '{students/search}' => function () use ($__db, $__user, $__request){
            return GUPMSRStudent::searchConfidentialInfoStudent($__db, $__user->getStaffInstance(), $__request);
        }
	),
    'POST' => array(
        '{/status}' => function() use($__db, $__request, $__api_app){
            if($__api_app == null)
                return new Result(0, 'not api application');
            return GUPMSRStudent::ObtainStatusInfo($__db, $__api_app, $__request);
        },
    ),
);
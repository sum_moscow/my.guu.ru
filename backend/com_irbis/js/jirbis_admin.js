jQuery.noConflict();
jQuery(function ($) {
	
    // Инициализируем тултип
    $( document ).tooltip();

    /**
     * GridErrorHandling
     * Обработка ошибок
     * @param gridObj
     * @return {*}
     * @constructor
     */
    var GridErrorHandling = function(gridObj){
        var self = this;

        this.gridObj = gridObj;

        // Вывод ошибок при загрузке данных
        this.gridObj.bind("jqGridLoadComplete", function(e, data){

            if(data && data.errors){
                var errorText = "";
                // Генерация текста ошибок
                $.each(data.errors, function(){
                    errorText += "Ошибка: " + this.error_number + ": " + this.error_message + "<br />";
                });
                // Вывод ошибки
                self.showError(errorText);

            }
            return true;
        });

        // Вывод ошибок при редактировании строк
        this.gridObj.bind("jqGridInlineSuccessSaveRow", function(e, response, rowid, o){

            var errorText = "";
            if(self.isValidJSON(response.responseText)){
                var data = $.parseJSON(response.responseText || "{}");
                if(data && data.errors){

                    // Генерация текста ошибок
                    $.each(data.errors, function(){
                        errorText += "Error " + this.error_number + ": " + this.error_message + "<br />";
                    });

                }
            }else if(typeof response.responseText == "string" && response.responseText) errorText = response.responseText;

            // Вывод ошибки, если есть
            if(errorText){
                self.showError(errorText);
                // Оставляем строку в режиме редактирования
                o.restoreAfterError = false;
                return [false, false];
            }

            return true;
        });

        return this;
    };
    /**
     * Проверка json на валидность
     * @return {Boolean}
     */
    GridErrorHandling.prototype.isValidJSON = function(src){
        var filtered = src;
        filtered = filtered.replace(/\\["\\\/bfnrtu]/g, '@');
        filtered = filtered.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']');
        filtered = filtered.replace(/(?:^|:|,)(?:\s*\[)+/g, '');

        return (/^[\],:{}\s]*$/.test(filtered));
    };
    /**
     * Открыто ли на данный момент диалоговое окно
     * @type {boolean}
     */
    GridErrorHandling.prototype.activeDialog = false;
    /**
     * Стек ошибок
     * @type {Array}
     */
    GridErrorHandling.prototype.stack = [];
    /**
     * Вывод ошибки
     * @param errorText - текст ошибки
     * @return {*}
     */
    GridErrorHandling.prototype.showError = function(errorText){
        var self = this,
            errorTextFull = "";

        this.stack.push(errorText);

        $.each(this.stack,function(){
            errorTextFull += this + "<br />";
        });

        // Вывод ошибки
        try {
            this.activeDialog = true;
            $.jgrid.info_dialog($.jgrid.errors.errcap,'<div class="ui-state-error">'+ errorTextFull +'</div>', $.jgrid.edit.bClose,
                {
                    buttonalign:'right',
                    onClose: function(){
                        self.stack = [];
                    }
                });

        } catch(e) {
            alert(errorText);
        }
        return this;
    };
    /**
     * Обработка ошибок при загрузке jqGrid
     * @param xhr
     * @param status
     * @param error
     */
    GridErrorHandling.prototype.loadError = function(xhr, status, error){
        if(xhr.status != 200)
            GridErrorHandling.prototype.showError("Ошибка " + xhr.status + ": " + xhr.statusText);
        else if(xhr.responseText) GridErrorHandling.prototype.showError(xhr.responseText);
    };


    
    
    
    

    /**
     * InitGrid
     * Инициализирует jqGrid таблицы
     * @return {*}
     * @constructor
     */
    var InitGrid = function(){

        var self = this,
            // Класс ячеек, которые редактируются в отдельной таблице
            _edit_class = "edit-in-table",
            // Объект для хранения всплывающих таблиц
            _edit_tables = {};

        var
            /**
             * Создание панелей редактирования таблицой
             * @param gridObj - объект jqGrid
             * @param pager - элемент для пагинации
             */
            _createNavGrid = function(gridObj, pager){
                gridObj.jqGrid('navGrid', pager, {
                    edit: false,
                    add: false,
                    del: true,
                    search: false
                })
                .jqGrid('inlineNav', pager, {
                    addParams: {
                        position: "last",
                        addRowParams: {
                            keys: true,
                            oneditfunc: function (response ) {
                                //$(gridObj.getInd(response ,true)).addClass("jgrid-new-row");
                                $("."+_edit_class).addClass("jgrid-new-row");
                                $("."+_edit_class).html("");
                                // console.log($("."+_edit_class));
                                // console.log(gridObj.getInd(response ,true)); 
                                return true;
                            },
                            successfunc: function (response ) {
                                $("."+_edit_class).html("Редактировать");
                                $("."+_edit_class).removeClass("jgrid-new-row");
                            	// Проблема заключается в том, как отсюда получить ID записи gridObj.getInd должен получать ROWID!
                            	// А RESPONSE в данной функции возвращает: Object { readyState=4, responseText="[]", status=200, ещё...} И никакого указания на ID записи здесь нет.                            	
                                //console.log(gridObj.getInd(response ,true)); 
                            	//console.log(response);
                                return true;
                            }
                        }
                    },
                    editParams: {
                        keys: true
                    }
                });
            },
            /**
             * Меняет атрибут title по значению tip в jgrid данных
             * @param grid - объект с данными инициализации jgrid
             */
            _newTitles = function(grid){
                if(grid && grid.data && grid.data.colModel)
                    $.each(grid.data.colModel, function(){
                        var self = this;

                        this.cellattr = function () {
                            return 'title="' + (self.tip ? self.tip : '') +'"';
                        }
                    });
                return grid;
            },
            /**
             * Создание таблицы во всплывающем окне
             *
             * @param row - номер строки редактируемой ячейки
             * @param col - номер столбца редактируемой ячейки
             * @param colModel - параметры редактируемой ячейки
             */
            _createEditTable = function(row, col, colModel){

                // Создание таблицы jqGrid
                var editGrid = $("#" + colModel.name),
                    grid = _edit_tables[colModel.name];

                if(!grid){
                    console.error("InitGrid: нет таблицы с именем " + colModel.name);
                    return false;
                }

                // Считаем размер ширины таблицы
                var width = 0,
                    window_width = $(window).width();

                $.each(grid.data.colModel, function(){
                    width += this.width ? parseInt(this.width) : 100;
                });
                // Прибавляем ширину отступов
                width += 33;
                // Минимальная ширина
                if(width < 350) width = 350;
                // Максимальная ширина
                if(width > window_width) width = window_width - 20;

                // Создание всплывающего окна
                $("#" + colModel.name + "_wrapper").dialog({
                    title: "Редакция",
                    dialogClass: "dialog-style",
                    resizable: false,
                    height: 350,
                    width: width,
                    modal: true,
                    buttons: {
                        "Закрыть": function() {
                            $( this ).dialog( "destroy" );
                        }
                    }
                });

                // Если таблица еще не инициализирована
                if(!grid.__init){
                    var //rowEdit = new GridRowEditClick(editGrid),
                        error = new GridErrorHandling(editGrid);
                    editGrid.jqGrid($.extend({}, grid.data, {
                        "url": grid.data.url + "&parent_id=" + row,
                        "editurl": grid.data.editurl + "&parent_id=" + row,
                        loadError: error.loadError,
                        gridComplete: function(){

                        },
                        ondblClickRow :function(row, indexRow, col){
                            // Режим редактирования для строки
                            //rowEdit.editRow(row);
                        }
                    }));
                    _createNavGrid(editGrid, grid.data.pager);
                }else{
                    // Обновляем данные таблицы
                    editGrid.jqGrid("setGridParam", {
                        "url": grid.data.url + "&parent_id=" + row,
                        "editurl": grid.data.editurl + "&parent_id=" + row
                    }).trigger("reloadGrid");
                    return self;
                }

                // Выставляем признак инициализации
                _edit_tables[colModel.name].__init = true;

                return self;
            };

        /**
         * Создание таблицы
         * @param grid_data - объект с данными для инициализации jqGrid
         */
        self.create = function(grid_data){

            // Меняем атрибут title на значение tip
            var grid = _newTitles(grid_data),
                gridObj = $("#" + grid.name);

            // Вставляем таблицу
            var //rowEdit = new GridRowEditClick(gridObj),
                error = new GridErrorHandling(gridObj);
            gridObj.jqGrid($.extend(grid.data, {
                loadError: error.loadError,
                gridComplete: function(){
                    // Для ячеек с классом '.edit-in-table' прописывает зачение Редактировать
                    $("."+_edit_class).html("Редактировать");

                },
                onCellSelect: function(row, col){

                    var colModel = gridObj.jqGrid("getGridParam", "colModel")[col];

                    // Если кликнули по ячейке, данные которые редактирубтся в отдельной таблице
                    if(colModel.classes == _edit_class){
                        if(!$(gridObj.getInd(row,true)).hasClass("jgrid-new-row")) _createEditTable(row, col, colModel);
                        else alert("Редактирование данного поля доступно только после сохранения таблицы");
                    }
                },
                ondblClickRow :function(row, indexRow, col){
                    // Режим редактирования для строки
                    //rowEdit.editRow(row);

                }
            }));
            _createNavGrid(gridObj, grid.data.pager);
        };

        /**
         * Сохранение данных для всплыващих таблиц
         * @param grid_data - объект с данными для инициализации jqGrid
         */
        self.save = function(grid_data){
            if(!grid_data || !grid_data.name){
               alert("InitGrid.save: неверные параметры");
            }
            // Меняем атрибут title на значение tip
            var grid = _newTitles(grid_data);

            _edit_tables[grid.name] = grid;
        };

        return self;
    };

    /**
     * Экспортируем
     */
    window.App = {
        InitGrid: new InitGrid()
    };

    /**
     * Инициализация элементов
     * TODO: пока так, в будущем перенести куда-нибудь
     */

    
    $(".tabs").tabs({
        cache: true,
        //hide:alert(''),
		onBeforeClick: function(event, tabIndex) {
	 
			// переменная "this" - это ссылка на API. 
			var tabPanes = this.getPanes();
	 		//console.log(tabPanes);
			/*
				Если здесь возвращаем false, то действие отменяется.
				В данном случае получается, что вкладку нельзя сделать активной, если условия соглашения ("terms") не будут приняты
			*/
			//return $(":checkbox[name=terms]").is(":checked");
		},        
        load: function () {
		    $('.button').button();
		    
		    $('form.jirbis_admin_form').submit(function (e) {
		    //	e.stopPropagation();
		/*		e.stopPropagation();
		    	e.preventDefault();		 */   
		         //$(this).find('.log').html('sdfsfsdfsddf');
		         //console.log($(this).find('.log'));
		         errors.init($(this).find('.log'));
		       // e.stopPropagation();
		        $(this).ajaxSubmit({
		            dataType: 'json',
		            type: 'GET',
		        	cache: false,
		            beforeSend: function () {
		                
		              
		            	errors.clean();
		                my_dialog.loading();
		            },
		            success: function (data) {
		            		
		                    if (!data) {
		                        errors.lo_output();
		                        return;
		                    }
		                                
							if (data.errors){
								errors.hi_output(data.errors);
							}else {
		                    	
									errors.message('Данные успешно сохранены!');
								}
		
		
		            },
		            complete: function () {
		            	my_dialog.destroy();
		            },
		            error: function (xhr) {
		                errors.lo_output(xhr);
		            }
		
		        });
		        return false; 
		    });
		    
		    $('.reload').click(function () {
		        window.location.reload();
		    });
            	
        }
    });    
});
<?php	
$letter="
Уважаемые коллеги!<br>

%s в %s ваш WEB сервер(%s) за %s секунд(ы) не ответил на запрос ИРБИС-корпорации к базе %s. Аналогичные проблемы в это время, вероятно, возникали у ваших читателей. Возможные причины этого:

<p>1. Хроническая перегрузка сервера (в таком случае следует обновить WEB ИРБИС или сам сервер, снять с него часть функций);</p>

<p>2. Удаление библиографической базы (в таком случае  следует сообщить об этом по адресу sokolinsky_k_e@mail.ru);</p>

<p>3. Удаление записи читателя из базы RDR, идентификатором которого пользуется ИРБИС-корпорация (в таком случае сообщите новый идентификатор по адресу sokolinsky_k_e@mail.ru);</p>

<p>4. Сбой, вызванный отключением электричества или другими форс-мажорными обстоятельствами. </p>

<p>В течение %s минут ваш сервер не будет участвовать работе ИРБИС-корпорации. Затем будет проведена его повторная проверка. </p>

<p>Данное письмо было сгенерировано автоматически и не требует ответа.</p>

";


class bug_mailing{

	
	public function __construct($email,$url,$timeout,$base_name,$pause,$bugs_count){
			global $CFG,$letter;
				if (($bugs_count>=$CFG['bad_connections_for_mail']) && ($bugs_count%$CFG['bad_connections_mailing_divisor']==0) && $CFG['mail_enable'] && $email){
					//Чтобы освободиться от ненужных сообщений...
					ob_start();
					$mail = new PHPMailer();
					//$mail->SetLanguage('ru');
		
					
					$mail->Body=sprintf($letter,date('Y.m.d'),date('G:i'),$url,($timeout/1000),$base_name,$pause);				
					//$mail->CharSet="Windows-1251";
					$mail->CharSet="UTF-8";
					//$mail->WordWrap = 75; 
					$mail->IsHTML(true);
					$mail->From = $CFG['mail_from'];
					$mail->FromName = 'ИРБИС-корпорация';
					$mail->AddAddress($email, '');
					$mail->Subject = 'Сбой на вашем WEB-сервере'; 
					$mail->SMTPAuth  = true;
					$mail->Username  = $CFG['mail_user_name'];
					$mail->Password  =  $CFG['mail_password'];
					$mail->Host = $CFG['mail_host'];
					$mail->Port = $CFG['mail_port'];
					$mail->Mailer = $CFG['mail_mailer'];
		
					
					if(!$mail->Send())
						ji_log::i()->w("Не удалось отправить сообщение об ошибке при поиске в базе {$url}",I_INFO);
					else 
						ji_log::i()->w("Отправлено сообщение об ошибке при поиске в базе {$url}",I_INFO);
					ob_clean();						
				}
	}
}
?>
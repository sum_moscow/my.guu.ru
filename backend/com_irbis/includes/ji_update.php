<?php

class ji_update{
 	
	public static $public_mode=true;
	
	private static  $path_constants=array(
		'JI_PATH_UPDATE_SCRIPTS_LOCAL',	
		'JI_PATH_PFT_LOCAL',
		'JI_PATH_COVERS_LOCAL',
		'JI_PATH_FORMS_LOCAL',
		'JI_PATH_INCLUDES_LOCAL',
		'JI_PATH_COMPONENT_LOCAL',
		'JI_PATH_DEBUG_LOCAL',
		'JI_PATH_TMP_LOCAL',		
		'JI_PATH_CMS_LOCAL'		
		);	
	
		
	public static function set_update_mark($user_id){
		$sql = "		
		UPDATE #__jusers
		SET update_date=NOW()
		WHERE   id = $user_id
		"; 
		idb::i()->setQuery($sql);
		idb::i()->Query();		
	}
	
	
	static public function get_serialized_users(){		

		
		$sql = "		
		SELECT *  
		FROM #__jusers
        ORDER BY id
		"; 
		//
		idb::i()->setQuery($sql);
		$sources=idb::i()->loadAssocList('id');
		
		if (idb::i()->getErrorNum()) 
			ji_ulog::i()->w('Ошибка в запросе MySQL: '.idb::i()->getErrorMsg(),I_ERROR,idb::i()->getErrorNum(),I_ERROR_LEVEL_CRITIICALY);				
			
		return urlencode(serialize($sources));
	
	}			
		
	public static function ins_upd_sql_data($table,$key_fields=array(),$recs=array()){
		
		// В связи с тем, что запрос на обновление у нас зависит от наличия или отсутствия поля, мы вынуждены выполнять запрос на каждой записи. 		
		foreach($recs as $rec){
			
			$sql_record=array();
			$sql_record_update=array();
			
			$array_keys=array_keys($rec);
			foreach ($rec as $name=>$field){
				$sql_record[]=idb::i()->Quote($field);
				// Добавляем только те поля, в которых присутствует какое-то значеня и которые при этом не являются ключевыми. (мы не должны допустить увеличения значения ID)
				if ($field && !in_array($name,$key_fields))
					$sql_record_update[]="$name=VALUES($name)";
			}
			// Обеспечиваем корректность по количеству полей. 
			//$sql_record=array_merge(array_fill(0,count($array_keys),''),$sql_record);
			

			$sql = "INSERT INTO #__$table (".implode(',',$array_keys).") VALUES ";
			$sql.="(".implode(',',$sql_record).") ON DUPLICATE KEY UPDATE ".implode(',',$sql_record_update);	
			idb::i()->setQuery($sql);				
			idb::i()->query();
			
			if (idb::i()->getErrorNum()) 
				ji_ulog::i()->w('Ошибка в запросе MySQL: '.idb::i()->getErrorMsg(),I_ERROR,idb::i()->getErrorNum(),I_ERROR_LEVEL_CRITIICALY);			
		}
		
			
		return true;
		 
	}		
		

		
	public static function truncate_table($name){
		$sql="
		TRUNCATE #__$name
		"; 
		idb::i()->setQuery($sql);
		if (!idb::i()->query()) 
			throw new Exception("Ошибка SQL при опустошении таблицы $name: ".idb::i()->getErrorMsg(),idb::i()->getErrorNum());						
		
	}
	//=================================================================================	
	/*===========================СОЗДАНИЕ ЛИСТА=======================================*/
	//=================================================================================

	public static function post($url,$port=80,$timeout=30,$fields=array()){
		
		$integrator='';		
		$full_request='';
		foreach ($fields as $par=>$value){
			$full_request.=$integrator.$par.'='.urlencode($value);
			$integrator='&';	
		}		
		
		
		$full_host=(strpos($url,'http://')===false) ? "http://$url" : $url;						
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); 
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);  		
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_PORT,$port);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $full_request); 		
		
		$body = curl_exec($ch);				
		$info=curl_getinfo($ch);   
		if (curl_errno($ch)){
			$error=curl_error($ch);
			$error_no=curl_errno($ch);			
			curl_close($ch);
			throw new Exception("Ошибка низкоуровневая при отправке данных. на $url ".$error,$error_no);
		}
		if ($info['http_code']!=200){ 			
			curl_close($ch);		
			throw new Exception("Ошибка HTTP при отправке данных по адресу $url. HTTP ответ:",$info['http_code']);
		}
		
		return $body;

	}
		
	static public function get_users_list(){		

		$condition= (self::$public_mode) ? 1: 0;
		$sql = "		
		SELECT *  
		FROM #__jusers
		WHERE crypt_enable=$condition
		AND update_enable=1
		AND url!=''
		AND login!=''
		AND password!=''
		AND licence_until>NOW() OR licence_until IS NULL
        ORDER BY id
		"; 
		//
		idb::i()->setQuery($sql);
		$sources=idb::i()->loadAssocList('id');

		return $sources;
	
	}	
	static public function write_info_array($info_array){
		if(file_put_contents(JI_PATH_COMPONENT_LOCAL.'/'.(self::$public_mode? JI_FILE_PUBLIC_FILES_LIST  : JI_FILE_PRIVATE_FILES_LIST),serialize($info_array))===false)
			ji_ulog::i()->w('Записать файл списка передачи ',I_ERROR);
			else 
			ji_ulog::i()->w('Файл списка передачи записан');
	}
	// Выполняется на севрере	
	static public function get_files_info_array(){
		ini_set('max_execution_time','0');
		$file_no_update_list=self::get_list_file(JI_PATH_COMPONENT_LOCAL.'/'.JI_FILE_NO_UPDATE_FILES_LIST);
		$file_crypt_list=self::get_list_file(JI_PATH_COMPONENT_LOCAL.'/'.JI_FILE_CRYPT_FILES_LIST);
		$file_crypt_soft_list=self::get_list_file(JI_PATH_COMPONENT_LOCAL.'/'.JI_FILE_CRYPT_SOFT_LIST);
		//Мы точно знаем, где должны находиться интересующие нас файлы, поэтому не обязаны перебирать все директории.
		$paths_array=array_merge(self::get_dir_as_array(JI_PATH_CMS_LOCAL),self::get_dir_as_array(JI_PATH_PFT_LOCAL));
		
		
		$checked_paths=array();
		foreach($paths_array as $path){
			
			if (self::is_in_file_list($path,$file_no_update_list)) 
				continue;
			
			$path_new=str_ireplace(JI_PATH_CMS_LOCAL,JI_PATH_CMS_DISTR_LOCAL,$path);
			
			if (self::is_dir_path($path)){
				@@mkdir(rtrim($path_new,'/'));
				continue;
			}
				
			if (self::is_in_file_list($path,$file_crypt_list) && self::$public_mode){
				if (($content=self::get_file_crypt_content($path,self::is_in_file_list($path,$file_crypt_soft_list) ? 0 : 1))!== false){
					ji_ulog::i()->w('Прочитан файл: '.$path.(self::$public_mode ? ': ШИФРОВАН':''));
						
				}else {
					ji_ulog::i()->w('Не удалось прочитать файл: '.$path,I_ERROR);
					continue;
				}
					
			}else{ 
				if (($content=file_get_contents($path))!== false){	
					//ji_ulog::i()->w('Прочитан файл: '.$path);
				}else {
					ji_ulog::i()->w('Не удалось прочитать и защифровать файл: '.$path,I_ERROR);
					continue;
				}	
			}

						
			if (self::$public_mode) {
				if (@file_put_contents($path_new,$content)===false)
					ji_ulog::i()->w('Не удалось записать файл '.$path);
			}
			
			$crc=crc32($content);			
			$list_path=self::get_masked_path($path);
			$checked_paths[]=array('file_path'=>$list_path,'crc'=>$crc);
		}
		return $checked_paths;
	}
	
	static public function show_array_data($array=array()){
		if 	(!$array) return;
		foreach($array as $element){
				ji_ulog::i()->w($element);
			}
	}

	//=================================================================================	
	/*===========================СОЗДАНИЕ ЛИСТА=======================================*/
	//=================================================================================
	
	// Создание каталога с обновлением
	static public function build_temp_update_dir($files_array=array()){
		ini_set('max_execution_time','0');
		
		@mkdir(JI_PATH_TEMP_UPDATE_LOCAL);
		
		$base_path=(self::$public_mode) ? JI_PATH_CMS_DISTR_LOCAL : JI_PATH_CMS_LOCAL;
		
		foreach ($files_array as $file){
			$file_path=self::get_unmasked_path($file);
			//$file_path='c:/xampp/htdocs/jirbis2/components/com_hello/controller.php';
			$file_path=(self::$public_mode) ? str_ireplace(JI_PATH_CMS_LOCAL,JI_PATH_CMS_DISTR_LOCAL,$file_path) : $file_path;
			
			// Удаляем из начала пути путь к папке CMS
			
			
			self::make_dirs_of_path($file,JI_PATH_TEMP_UPDATE_LOCAL);
			
			if(!@copy($file_path,JI_PATH_TEMP_UPDATE_LOCAL.'/'.$file))
				ji_ulog::i()->w('Не удалось выплнить копирвание файла: '.$file_path,I_ERROR);

		}
	}
	
	static public function zip(){
		ini_set('max_execution_time','0');
		$archive = new PclZip2(JI_PATH_TMP_LOCAL.'/'.JI_FILE_UPDATE);
		return $archive->create(JI_PATH_TEMP_UPDATE_LOCAL,PCLZIP_OPT_REMOVE_PATH,JI_PATH_TEMP_UPDATE_LOCAL);
	}
	

	//=================================================================================	
	/*===========================СЕРВЕРНЫЕ ФУНКЦИИ=====================================*/
	//=================================================================================

	// Серверная функция -- определение необходимых файлов
	static public function get_list_of_required_files($files_array=array()){
		ini_set('max_execution_time','0');
		$file_no_update_list=self::get_list_file(JI_PATH_COMPONENT_LOCAL.'/'.JI_FILE_NO_UPDATE_FILES_LIST);
		$needed_files=array();
		foreach ($files_array as $file){
			$file_path=self::get_unmasked_path($file['file_path']);
			
			if (self::is_in_file_list($file_path,$file_no_update_list))
				continue;
				
			if (!file_exists($file_path)) {
				$needed_files[]=$file['file_path'];
				continue;
			}
			
			if (@crc32(@file_get_contents($file_path))!=$file['crc']) 
				$needed_files[]=$file['file_path'];
		}		
		return $needed_files;
	}

	static public function unzip(){
		ini_set('max_execution_time','0');
		@mkdir(JI_PATH_TEMP_UPDATE_LOCAL);
		 $archive = new PclZip2(JI_PATH_TMP_LOCAL.'/'.JI_FILE_UPDATE);	
		 if ($archive->extract(PCLZIP_OPT_PATH, JI_PATH_TEMP_UPDATE_LOCAL) == 0) {
			return array($archive->errorInfo(true));
		}
		return array();
	}
	
	// Инсталляция апдейта
	static public function install_update_files(){
		$excute=array();
		$paths_array=self::get_dir_as_array(JI_PATH_TEMP_UPDATE_LOCAL);
		//print_r($paths_array);
		$errors=array();
		foreach($paths_array as $path){
			// Расшифровываем всё, кроме пути к временной папке. 
			$relative_temp_path=str_ireplace(JI_PATH_TEMP_UPDATE_LOCAL.'/','',$path);
			$final_path=self::get_unmasked_path($relative_temp_path);
			
			
			// Выделяем константу и расшифровываем её
			$temp=explode('/',$relative_temp_path);
			if (strlen($temp[0])>3)
				self::make_dirs_of_path($final_path,self::get_const_value($temp[0]));
			else {
				$errors[]='Не удалось выделить название конатанты для получения абсолютного пуи : '.$final_path;
				continue;
			}
			
			if(!@copy($path,$final_path)){
				$errors[]='Не удалось выплнить копирвание UPDATE версии файла: '.$final_path;
				continue;
			}
			
			if (stripos($path,'JI_PATH_UPDATE_SCRIPTS_LOCAL')){
				$excute[]=$final_path;				
			}			
		}
		
		foreach ($excute as $excute_script_name){
			$errors[]='Выполняется скрипт '.$excute_script_name;
			include($excute_script_name);
		}
		return $errors;
	}
	

	
	static public function clean_all_sessions(){		
			foreach (glob(JI_PATH_TMP_LOCAL."/*") as $filename){
		    	@unlink($filename);
		}
	}	


	static public function pft_copy_and_zip(){		
		$pft_mnu_list=self::get_list_file(JI_PATH_COMPONENT_LOCAL.'/'.JI_FILE_PFT_MNU_LIST);		
		$file_no_update_list=self::get_list_file(JI_PATH_COMPONENT_LOCAL.'/'.JI_FILE_NO_UPDATE_FILES_LIST);
		
		$pft_path_temp=JI_PATH_CMS_DISTR_LOCAL.'/'.JI_DIR_TMP.'/'.JI_DIR_UPDATE;
		@mkdir($pft_path_temp);
		
		foreach (glob(JI_PATH_PFT_LOCAL."/*.*") as $filename){
		    	if (self::is_in_file_list($filename,$pft_mnu_list) && !self::is_in_file_list($filename,$file_no_update_list)){
		    		if (!@copy($filename,$pft_path_temp.'/'.basename($filename)))
		    			ji_ulog::i()->w('Не удалось выплнить копирвание файла: '.$filename,I_ERROR);		    			
		    	}
		    	
		}
		$archive = new PclZip2(JI_PATH_CMS_DISTR_LOCAL.'/'.JI_DIR_TMP.'/'.JI_FILE_PFT_ARCHIVE);
		return $archive->create($pft_path_temp,PCLZIP_OPT_REMOVE_PATH,$pft_path_temp);
		
	}	


	
	static private function make_dirs_of_path($file_path,$start_dir){		
		
		$relative_path=trim(str_ireplace(array(trim($start_dir,'/').'/',basename($file_path)),'',$file_path),'/');		
		$dir_names_array=explode('/',$relative_path);
		
		$checked_dir=$start_dir;
		foreach($dir_names_array as $dir){
			$checked_dir.='/'.$dir;
			if (!file_exists($checked_dir)) 
				mkdir($checked_dir);
		}
				
	}
	
	static private function get_path_const_name($path_with_const){
		//echo substr($path_with_const,0,strpos('/',$path_with_const)-1);
		return substr($path_with_const,0,strpos($path_with_const,'/'));
	}
	
	
	static public function get_list_file($file_path){
		if (file_exists($file_path) && @filesize($file_path)){
			$f=fopen($file_path,"rt");
			return explode("\n",trim(fread($f,filesize($file_path))));
		}
		return array();
	}
	
	
	static private function get_dir_as_array($dir_path,$files_array=array()){
		if 	(!is_dir($dir_path))
			return array();
			
		ini_set('max_execution_time','0'); 	

		$dir = opendir($dir_path);
		while(($file = readdir($dir))){
			if (is_file("$dir_path/$file")) {
				$files_array[]="$dir_path/$file";
			} else {
			    if( is_dir("$dir_path/$file") && ($file!= ".") && ($file!= "..")){
			    	$files_array[]="$dir_path/$file/";
			    	$files_array+=self::get_dir_as_array("$dir_path/$file",$files_array);	
			    }
			
			}
		
		}
		closedir ($dir);
		return $files_array;
	}

	
	static private function is_dir_path($file_path){
		if ($file_path[strlen($file_path)-1]==='/') 
			return true;
		else 
		return false;		
	}


	static private function get_unmasked_path($file_path){		
		
		$const=self::get_path_const_name($file_path);		
		//if (strpos($const,':')) 			echo "НЕКОРРЕКТНОЕ ЗНАЧЕНИЕ ЯВЛЯЕТСЯ РЕЗУЛЬТАТОМ get_path_const_name &$const&";
			
		//echo "||||$const||||||";
		//echo '|'.str_ireplace($const,self::get_const_value($const),$file_path).'|';
		return str_ireplace($const,self::get_const_value($const),$file_path);
	}
	
	static private function get_masked_path($file_path){

		foreach(self::$path_constants as $path_constant){
			$constant_value=self::get_const_value($path_constant);
			if (strpos($file_path,$constant_value)!==false)
				return str_ireplace($constant_value,$path_constant,$file_path);
		}
		return $file_path;
	}
	
	static private function get_const_value($const_name){
		if ($const_name && defined($const_name))
			eval('$constant_value'."=$const_name;");
		else die("Не определена константа пути $const_name");
			//return '';
		return $constant_value;
	}
	
	static public  function get_file_crypt_content($file_path,$soft_enable=1){
		
		if (($content=file_get_contents($file_path))!==false){			

			return obf($content,$soft_enable);
		}
			else 
			return false;		
	}
	
	
	static private function is_in_file_list($path,$file_list){
		foreach ($file_list as $file){
			// Ситуация с регулярным выражением. Используется для фильтрации ненужных форматов. 
			if ($file[0]==='{'){
				if (preg_match($file,$path))
				return true;
				else 
				false;
			}
			
			if (strpos(strtolower($path),strtolower($file))!==false) 
				return true;			
		}
		return false;
	}
}




?>

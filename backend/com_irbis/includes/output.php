<?php 
class output{
	
	public $max_results_renew_time=2000;
	public $min_results_renew_time=1000;
	public $cache_relevance_time=90;
	public $max_sorted_records=300;	
	public $sleep_time=500000;
	public $session_wait_time=90000;
	public $indication_mode=false;
	public $st;
		
	public function __construct(){
	
	
	/*	Каждый запрос(сессия?) содержит:
	1. Время предыдущего ответа
	2. ID запроса
	3. С записи, и количество записей
	
	
	2. Количество найденных документов
	6. Количество опрошенных источников
	
	
	
	Каждый ответ должен включать:
	1. Общее количество найденных документов
	2. Количество документов для текущей страницы
	3. Собственно результат
	4. Признак финала
	5. Количество опрошенных источников
	6. Процент
	
	*/
	
	}
	
	public function get_new_records(){
		global $__db;
	
		$microtime=u::get_microtime();
		$need_reformat_wait=false;
		$recs_good=array();
		$req_id_client=mosGetParam( $_REQUEST, 'req_id_client', '' );
		

		ji_log::i()->w("OUTPUT",I_INFO,$req_id_client);	
		//sleep(4);
		for(;;){
			ji_search_session::free();
			$sd=ji_search_session::read($req_id_client,JI_LOCK_NO);
			//ji_search_session::free();
			
			// TODO: Разобраться, зачем может понадобиться убивать процесс, если сессия не создана. 
			// Например, если основной процесс ешё не создал сессию. Но ведь можно в таком случае и подождать...
			if(!$sd){
				ji_log::i()->w("Файл поисковой сессии не найден, выполнение прерывается",I_INFO,$req_id_client);				
			 	//answer::json(new stdClass());
			 	die('{}');
			}
			
			$this->st=new ji_st(JI_LOCK_NO);							
			ji_log::i()->w("Файл поисковой сессии получен!",I_INFO,$req_id_client);				

			
			
			ji_log::i()->w("selected_search_flag:{$this->st->selected_search_flag}",I_INFO,$req_id_client);					
			
			$first_number=$this->st->first_number_normalized-1;	
			$portion_output_normalized=$this->st->portion_output_normalized;
			$direction=($this->st->sort_direction='UP') ? ' DESC  ' : '';  	
			
			ji_log::i()->w("1)first_number=$first_number",I_INFO,$this->st->req_id_client);		
			// Внимание!!!! recs_outputed показывает только количество, выведенных из текущей порции
			if (
				
					$sd['recs']>=$sd['recs_need'] // Если получены все необходимые записи
					|| 
					($sd['reqs']>=$sd['reqs_need']) // Если выполнены все запросы
					|| 
					// Если увеличилось число записей и при этом текущее достигнуто минимальное время после предыдущего обновления
					($this->st->recs_outputed+($first_number)<$sd['recs'] && ($this->st->last_output_time+$this->min_results_renew_time)<=$microtime) 
					|| 
					// Если увеличилось число запросов и при этом текущее время больше максимального время после предыдущего обновления
					($this->st->reqs_outputed<$sd['reqs'] && ($this->st->last_output_time+$this->max_results_renew_time)<=$microtime)
					||
					// На случай сбоев, если не удаётся получить ожидаемый результат, да пятикратный $this->max_results_renew_time					
					((u::get_microtime()-$microtime)>($this->max_results_renew_time*5))
					//||
					// Если это возврат назад
					//($this->st->first_number+$this->st->portion_output<$this->st->recs_outputed)
					||
					// Если это последний запрос RENEW, выполняемый уже после завершения BROAD. 
					$this->st->finish_flag==='last'
					// Если завершен процесс BROAD
					||
					$sd['finish']
			){
				ji_log::i()->w("Выполнены первичные условия начала вывода 
				recs={$sd['recs']}. 
				st->recs_outputed={$this->st->recs_outputed}  
				portion_output_normalized={$portion_output_normalized} 
				first_number=$first_number",
				I_INFO,$this->st->req_id_client);
				// Если получены новые записи 
				// и, если требовалось переформатирование, все форматирования выполнены и процесс прошел
				 // 
				if (

						
					(							
						(
						// Если суммарное количество записей в текущей порции и предыдущих порциях меньше количества найденных записей
						// и если вывод должен быть перманентным
							($this->st->recs_outputed+$first_number)<$sd['recs'] 
							&&
							!$this->indication_mode
						) 
							||
							// Если это режим печати и вывод результата должен выполнятсья только после завершения работы
						(
							$this->indication_mode 
							&& 
							$this->st->finish_flag==='last'
						)
					)
					&& 						
					// В том случае, если количество выведенных записей меньше размера текущей порции  					
					($this->st->recs_outputed < $portion_output_normalized)
					&&
					// Если нет потребности в переформатировании
						(!$need_reformat_wait || 
							($sd['reformating_portions']==$sd['reformating_portions_need'] &&
							 $sd['reformating_portions_need']
							)
						)  
						
					//||
					// Возврат назад					
					//($this->st->first_number-1)+$this->st->portion_output<$sd['recs']
								
					){
				 // || $this->st->permanent_output совершенно не обязательно постоянный вывод должен быть запрещен в случае сортировки
						$profiles_type_preg='%'.implode('%',get_profile_types_as_array($this->st->profile)).'%';				 		
				 		$profile_type_condition="(#__rec.profile_types LIKE '".$profiles_type_preg."')";
				 		
						if (!$this->st->selected_search_flag){
					 		if (!$this->st->sort){
								$sql = "
								SELECT #__rec.rec_id,IF($profile_type_condition,#__rec.record,0) as record
								FROM #__req_rec 
								JOIN #__portions USING(req_id) 
								JOIN #__rec USING(rec_id) 
								WHERE #__req_rec.req_id = ".idb::i()->Quote($sd['req_id'])."
								AND UNIX_TIMESTAMP(#__rec.update_date)>".strtotime('-'.$this->cache_relevance_time.' day')."
								AND #__req_rec.bl_id IN ({$this->st->bl_id_string_selected})		
								GROUP BY #__req_rec.rec_id
								ORDER BY #__rec.counter
								LIMIT {$first_number},{$portion_output_normalized}
								"; 
							}else{
								$direction=($this->st->sort_direction='UP') ? ' DESC  ' : '';  	
								$sql = "
								SELECT #__rec.rec_id,IF($profile_type_condition,#__rec.record,0) as record
								FROM #__req_rec 
								JOIN #__rec USING(rec_id) 
								JOIN #__sort USING(rec_id)
								JOIN #__portions USING(req_id) 
								WHERE #__req_rec.req_id = ".idb::i()->Quote($sd['req_id'])."
								AND UNIX_TIMESTAMP(#__rec.update_date)>".strtotime('-'.$this->cache_relevance_time.' day')."
								AND #__req_rec.bl_id IN ({$this->st->bl_id_string_selected})		
								AND #__sort.key_type=".idb::i()->Quote($this->st->sort)."		
								GROUP BY #__req_rec.rec_id
								ORDER BY #__sort.key_value $direction ,#__rec.counter
								LIMIT {$first_number},{$portion_output_normalized}		
								"; 					
								
							}
						}else{
							if (!$this->st->sort){
								$sql = "
								SELECT #__rec.rec_id,IF($profile_type_condition,#__rec.record,0) as record
								FROM #__rec 
								WHERE #__rec.rec_id IN ({$this->st->rec_id_string_selected})
								ORDER BY #__rec.counter
								LIMIT {$first_number},{$portion_output_normalized}
								";
							}else{
								$direction=($this->st->sort_direction='UP') ? ' DESC  ' : '';  	
								$sql = "
								SELECT #__rec.rec_id,IF($profile_type_condition,#__rec.record,0) as record
								FROM #__rec 
								JOIN #__sort USING(rec_id)
								WHERE #__rec.rec_id IN ({$this->st->rec_id_string_selected})
								AND #__sort.key_type=".idb::i()->Quote($this->st->sort)."
								GROUP BY #__rec.rec_id
								ORDER BY #__sort.key_value $direction,#__rec.counter
								LIMIT {$first_number},{$portion_output_normalized}
								";								
							}
						}
						
						idb::i()->setQuery($sql);		
						//ji_log::i()->w("2)first_number=$first_number",I_INFO,$this->st->req_id_client);		
						
						$recs=idb::i()->loadAssocList('rec_id');				
						ji_log::i()->sql('OUTPUT_REQ',$sql,$recs);	
						$need_reformat_wait=false;
						
						ji_log::i()->w("Для вывода данных начиная с номера $first_number и лимитом {$portion_output_normalized} выполнен запрос {$sd['req_id']} к базе с результом: ".count($recs).", ",I_INFO,$this->st->req_id_client);
						
						
						/* Что делать, если записи не получены, но должны были быть? 
						Видимо это должно зависеть от ситуации... 
						*/
						if(!$recs){
		/*
												$sd['recs']>=$sd['recs_need'] // Если получены все необходимые записи
												|| 
												($sd['reqs']>=$sd['reqs_need']) // Если выполнены все запросы
												|| 
												// Если увеличилось число записей и при этом текущее достигнуто минимальное время после предыдущего обновления
												($this->st->recs_outputed+($first_number)<$sd['recs'] && ($this->st->last_output_time+$this->min_results_renew_time)<=$microtime) 
												|| 
												// Если увеличилось число запросов и при этом текущее время больше максимального время после предыдущего обновления
												($this->st->reqs_outputed<$sd['reqs'] && ($this->st->last_output_time+$this->max_results_renew_time)<=$microtime)
												||
												// На случай сбоев, если не удаётся получить ожидаемый результат, да пятикратный $this->max_results_renew_time					
												((u::get_microtime()-$microtime)>($this->max_results_renew_time*5))
												//||
												// Если это возврат назад
												//($this->st->first_number+$this->st->portion_output<$this->st->recs_outputed)
												||
												// Если это последний запрос RENEW, выполняемый уже после завершения BROAD. 
												$this->st->finish_flag==='last'
												// Если завершен процесс BROAD											
					*/
		
							ji_log::i()->w("Непредвиденная ситуаця: нет записей, но условия отбора сработали:
							<br>получены все необходимые записи ? sd[reqs] - {$sd['reqs']}>=sd[reqs_need] - {$sd['reqs_need']}
							<br>выполнены все запросы ? sd[recs] - {$sd['recs']}>=sd[recs_need] - {$sd['recs_need']}
							<br>это последний запрос RENEW, выполняемый уже после завершения BROAD. this->st->finish_flag==='last' -- {$this->st->finish_flag}					
							<br>завершен процесс BROAD{$sd['finish']}
							<br>увеличилось число записей и при этом текущее достигнуто минимальное время после предыдущего обновления ".($this->st->recs_outputed+($first_number)<$sd['recs'] && ($this->st->last_output_time+$this->min_results_renew_time)<=$microtime) ."							
							<br>это возврат назад ".($this->st->first_number+$this->st->portion_output<$this->st->recs_outputed)."
							<br>увеличилось число запросов и при этом текущее время больше максимального время после предыдущего обновления ".($this->st->reqs_outputed<$sd['reqs'] && ($this->st->last_output_time+$this->max_results_renew_time)<=$microtime) ."							
							<br>случай сбоев, если не удаётся получить ожидаемый результат, да пятикратный this->max_results_renew_time	 ".((u::get_microtime()-$microtime)>($this->max_results_renew_time*5)) ."							
							<br>this->st->recs_outputed+(first_number)".($this->st->recs_outputed+($first_number)<$sd['recs']) ."							
							<br>sd['recs']".$sd['recs'] ."
							", I_INFO,$this->st->req_id_client);
							die('{}');						
						}
						
											
						foreach($recs as $bl_id=>&$rec){
							// && !$sd['finish'] && $this->st->permanent_output && $sd['cache_count'] > $this->st->first_number
							if (!$rec['record']){
								if (!$sd['finish']){
										ji_log::i()->w("Задержка вывода в связи с ожиданием переформатирования",I_INFO,$this->st->req_id_client);
										$need_reformat_wait=true;
										usleep($this->sleep_time);
										break;			
								}
							}else{
								
								/*if (!@unserialize($rec['record'])) 
									ji_log::i()->w($rec['record'],I_INFO,$this->st->req_id_client);*/
																		
								if (($recs_good[$bl_id]=@unserialize($rec['record']))===false){
										$recs_good[$bl_id]=new jrecord();
										$recs_good[$bl_id]->SetSubField(200,1,'A','Ошибка при UNSERIALIZE кэшированной записи!');
										ji_log::i()->w("Ошибка при UNSERIALIZE кэшированной записи!",I_ERROR,$this->st->req_id_client);										
								}							
							}
						}
							
					}else {
						usleep($this->sleep_time);
						ji_log::i()->w("Условия отбора из базы не выполнены",I_INFO,$this->st->req_id_client);
					}
				
					
					
				if ($need_reformat_wait && !$sd['finish']){
					usleep($this->sleep_time);
					continue;
				}
				
				
				


				/*Каждый ответ должен включать:
				Как исключить из результата записи, которые ещё нуждаются в расформатировании?
				Найденные записи будут всегда иметь большие номера, чем переформатированные. 
				
				Что происходит, если запрашивается сортировка? Требуется 
				
				
				Если у нас по текущему диапазону есть кэш, то:
				1. В условиях перманентного вывода ставим флаг проверки кэша 
				
				Отбор только записей, соответствующих по профилю. Если запись не соответстсвует профилю, то 
				Проблема: как быть с соответствием шаблону
				1. Общее количество найденных документов 
				2. Количество документов для текущей страницы 
				3. Собственно результат
				4. -Признак финала
				5. -Количество опрошенных источников
				6. Процент
				*/	
				
				
				$output=array();
				$output['req_id_client']=$this->st->req_id_client;	
				$output['req_words']=$sd['req_words'];
				$output['req']=$sd['req'];
				// Вопрос: что отражает параметр? Если количество записей от нуля -- то это одно. Если количество записей от текущей порции -- другое. 

				// Если текущий запрос не ориентирован на кэширование, то 
				
				$output['recs_outputed']=count($recs_good) ? count($recs_good): $this->st->recs_outputed;
				
				//min($sd['recs'],$last_number);
				//$output['recs_outputed']=($sd['recs']>$last_number) ? $last_number : $sd['recs'];			
				//$output['recs_outputed']=(count($recs)) ? count($recs) : $this->st->recs_outputed;
				
				$output['reqs_outputed']=$sd['reqs'];
				$output['reqs']=$sd['reqs'];
				$output['last_output_time']=$microtime;
				
				$output['recs_total']=$sd['recs_total'];
				
				
				$output['percent']=($sd['reqs_need'] ? ceil((100/$sd['reqs_need'])*$sd['reqs']) : 100);
				$output['recs']=$recs_good;
				$output['first_number']=$this->st->first_number;
				$output['first_number_normalized']=$this->st->first_number_normalized;
				
				//$output['recs_outputed']=count($recs)+$this->st->first_number-1;
				//$output['portion_output']=$this->st->portion_output_normalized;
				//$output['first_number_sql']=$first_number;
				
			/*	// Если запрошенная порция превышает допустимые пределы и в результате записи получены не были
				if ($first_number>=$sd['recs_total'] && !$recs_good){
					if ($sd['recs_total']>$portion_output_normalized)
						$output['page_failure']=$sd['recs_total']-$portion_output_normalized+1;
					else 	
						$output['page_failure']=1;
				}*/
				
				ji_log::i()->w("================Выполнен вывод ответа {$this->st->finish_flag}=============",I_INFO,$this->st->req_id_client);
				
				if ($this->st->finish_flag==='last'){					
					ji_search_session::unlink_session();			
				}
				
				return 	$output;
				
			} else {
				usleep($this->sleep_time);
			}
		}
	}

	public function get_rec_by_rec_id($rec_id){
		global $__db;
		$sql="
		SELECT record
		FROM #__rec
		WHERE rec_id=$rec_id
		"; 
		idb::i()->setQuery($sql);
		$rec_text=idb::i()->loadResult();
		$rec=unserialize($rec_text);
		return $rec;
	}

	public function get_raeting_array($rec_id_array){
		global $__db;
		if (!$rec_id_array) 
			return array();
		$rec_id_string=implode(', ',$rec_id_array);
		$sql="		
		SELECT COUNT(`rate`) as counts, 
		AVG(`rate`) as avg, rec_id
		FROM `#__rec_rate` 
		WHERE rec_id IN ($rec_id_string)
		GROUP BY rec_id
		"; 
		idb::i()->setQuery($sql);
		$books_raeting_array=idb::i()->loadAssocList('rec_id'); 		
		return $books_raeting_array;
	}
	
}
?>
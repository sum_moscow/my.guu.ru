<?php

define("JI_FORM_DELIMITER","<!-- DELIMITER -->");
class ji_search_form{
	
	
	public $request_array=array();
	//FIELD PROPERTIES
	public $fp;
	public $logic_form='*';
	public $form_path=''; // Правильный путь к форме определяется И ПРОВЕРЯЕТСЯ ранее
	public $search_mode='';
	public $user=null;
	
	
	public function __construct(){
/*		$this->fp=$st->fp;
		$this->request_array=$st->request_array;
		$this->logic_form=$st->logic_form;
		$this->form_path=$st->form_path;		*/
	}
	
	
	public function get_document_type($prf){				
		$res=$this->get_dic_and_mnu_decode($prf,'hd.mnu','IBIS'); 
		return array((string)''=>(string)'')+$res;
	}

	public function get_document_form($prf){				
		$res=$this->get_dic_and_mnu_decode($prf,'vd.mnu','IBIS'); 
		return array((string)''=>(string)'')+$res;
	}

	public function get_document_language($prf){				
		$res=$this->get_dic_and_mnu_decode($prf,'jz.mnu','IBIS'); 
		return array((string)''=>(string)'')+$res;
	}	
	
	
	public function show(){
		if (strpos('//',$this->form_path)===true){
			echo  'Не удалось найти в jirbis_configuration корректрное описание выбранного режима';
			return;
		}		
		if (!file_exists($this->form_path)){
			echo  'Не найдена поисковая форма по адресу: '.$this->form_path;
			return;
		}
//echo 'ddddddd';		
		$content=file_get_contents($this->form_path);
		$content=explode('<!-- DELIMITER -->',$content);
		
		if (count($content)!=3) {
			echo  'Неправильная разметка страницы! Код формы не обрамлён двумя разделителями <!-- DELIMITER --> !';
			return ;
		}
		
		
		eval( '?>'.$this->get_js_data($content[1]).$content[1]);
		
		//return array('form'=>$content[1],'params'=>$this->get_js_data($content[1]));
	}
	
	public function build_request(){		
/*		// Устанавливаем логику, переданную в параметре запроса. 
		if (isset($this->request_array['logic_form']) and $this->request_array['logic_form']){
			$this->logic_form=$this->request_array['logic_form'];			
		}*/
		
		// Результат-- массив с содержимым полей и полным перечнем свойств
		foreach ($this->request_array as $name=>$value){
			if (isset($this->fp[$name]))
				$this->fp[$name]['value']=$value;
		}
		
		$f=new ji_field($this->fp);
		$req='';
		foreach ($this->request_array as $name=>$value){
			if (isset($this->fp[$name])){
				if ($this->fp[$name]['slave'] || (is_string($value) and !trim($value))) continue; 				
				$f_req=$f->get_field_req($name);
				$req.=($req ? $this->logic_form : '')."($f_req)";
			}
		}
		return $req;
	}
	

	
	
	// Передаём две вещи -- название функции из FP и префикс. Остальное не имеет никакого значения. 
	// Таким образом, здесь мы только формируем массив, который потом нужно будет подать на разборку. В массиве перебираем только элементы, для которых autocomplete<>''	
	//
	private function get_js_data($form_html){
		$js_array=array();
		$autocomplete_info=array();
		
		foreach ($this->fp as $name=>$value){

			if ((!strpos($form_html,'id="'.$name.'"') && !strpos($form_html,'name="'.$name.'"')) || (!$value['dic_function'] && !$value['prefix']) || $value['field_type']==='multiselect' )
				continue;
				
			$autocomplete_info[$name]=array('field_type'=>$value['field_type'],'dic_function'=>$value['dic_function'],'prefix'=>$value['prefix']);									
		}
		
		?>
		<script type="text/javascript">		
				autocomplete_info=<?php echo json_encode($autocomplete_info) ?>;
		</script>
		
		<?php 

	}
	
	public function output_like_options_object($array){
		$result=array();
		// Если первый элемент массива не пустой, значит определено какое-то значение по дефолту
		// и мы должны добавить пустой, но не выбранный элемент.
		if (u::get_first_assoc_key($array)!=='')
			$result[] = array('value'=>'','description'=>'','selected'=>false);
			
            
           	$d=0;
            foreach ($array as $value=>$description) {            	
            	
                 $result[]=array("value"=>$value, "description" => $description, "selected" => ((++$d)==1 ? true : false));
            }
        	answer::jsone($result);			
	}
	
	// Берёт нужный словарь, раскодирует по MNU и, в том случае, если пользователь авторизован, ставит значение соответствующее его записи читателя певым
	protected function get_dic_and_mnu_decode_with_rdr($prf,$mnu_name,$mnu_db,$reader_field,$reader_subfield){
		$res=array();
		// Если существут нужный нам словарь
		if (($dic_data=$this->get_dic_and_mnu_decode($prf,$mnu_name,$mnu_db))){
			
			// Если пользователь авторизован 		
			if ($this->user and ($user_key=$this->user->GetSubField($reader_field,1,$reader_subfield))){
				// Если в словаре присутствуют нужные нам значения
				$user_key=mb_strtoupper($user_key,'UTF-8');
				if (isset($dic_data[$user_key])){
					// Добавляем в начало массива соответствуюищй записи пользователя ключ и удаляем его из массива
					$res=array($user_key=>$dic_data[$user_key])+array_diff_key($dic_data,array($user_key=>''));
					return $res;
				}			
			}				
			// Если пользователь не авторизован или не найдено соответствие данным в RDR, то выдаём результат как есть	
			$res=array((string)''=>(string)'')+$dic_data;
			return $res;				
		}
		
		return array();
		
	}	
	
	// Берёт нужный словарь, раскодирует по MNU
	protected function get_dic_and_mnu_decode($prf,$mnu_name='',$mnu_db=''){
			$res=array();
			$mnu_array=array();
			
			if ($mnu_name && $mnu_db)				
				$mnu_array=ji_rec_common::get_mnu_as_assoc_array($mnu_name,$mnu_db,$this->cache_live_time);
			
			$dic_array=ji_rec_common::get_dic_terms($this->db_ko, $prf, '', 0,$this->cache_live_time);
			
			if (!is_array($dic_array))
				return 	array();
			
			foreach($dic_array as $dic_term){
				$term=$dic_term['key'];
				if (isset($mnu_array[$term]))
					$res[(string)$term]=$term.' - '.$mnu_array[$term];	
					else 		
					$res[(string)$term]=$term;
			}
		return $res;	
	}		
}


?>
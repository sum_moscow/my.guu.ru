<?php 

require_once("iwibase.php"); 


class iconsole extends iwibase  {
	public $obj_save_path;
	public $ini_latsmod_time; 	
	protected  $all_pft='all_pft';
	protected  $web_dict='autocomplete';
	protected $dummy_frame_name='dummy';
	protected $quantity_frame_name='quantity_frame';
	protected $read_rec_format='read_rec';
	private $irb_modification;
	private $console_path;	
	private $req_path;	
	private $common_pft_path;
	private $common_frames_path;
	private $ident;
	private $id_category;	
	private $ini_file;
	private $sys_fields;
	//���� �� ������������
	private $local_encoding;
	

	
	private $optimal_portion=500;
	
	
	public function __construct($irb_modification,$console_path,$ident='',$output_encoding='win'){
		
		$ini_name=($irb_modification==='i32') ? 'irbis32' :'irbis_server';
		$ini_path="$console_path$ini_name.ini";
		$this->obj_save_path=u::get_temp().'/iconsole.sav';
		$this->ini_latsmod_time=filemtime($ini_path);		
		$this->irb_modification=$irb_modification;
		$this->console_path=$console_path;	
		
		/* @var $iconsole_obj iconsole */
		@$iconsole_obj=unserialize(file_get_contents($this->obj_save_path));
		if (@$iconsole_obj->ini_latsmod_time===$this->ini_latsmod_time){
			// �������������� ������� 
			
			$this->req_path=$iconsole_obj->req_path;	
			$this->common_pft_path=$iconsole_obj->common_pft_path;
			$this->common_frames_path=$iconsole_obj->common_frames_path;
			$this->sys_fields=$iconsole_obj->sys_fields;
			$this->local_encoding=$iconsole_obj->local_encoding; 

			
		}else{
			// �������� �������
							
			$ini_file=u::ini_read($ini_path);	
			$this->req_path=u::get_temp();	
			$this->common_pft_path=$ini_file['MAIN']['DEPOSITPATH'];
			$this->common_frames_path=$ini_file['WEB']['FRAMES'];			
			$this->local_encoding=(@$ini_file['WEB']['ANSIENCODING']) ? 'win' :'utf';  			

			foreach($ini_file['PARAMETRS'] as $key=>$value){
				if ($key!=='PARCOUNT' && substr($key,0,7)!=='PARNAME'){
					if(is_numeric($value))$sys_fields[]=$value; 
				}
			}
			
			$sys_fields[]=$ini_file['PARAMETRS']['SEARCHRESULTTAG'];
			$sys_fields[]=$ini_file['PARAMETRS']['READERIDTAG'];
			$sys_fields[]=$ini_file['PARAMETRS']['DICTTERMTAG'];
			$sys_fields[]=$ini_file['PARAMETRS']['DICTNDOCSTAG'];
			$sys_fields[]=$ini_file['PARAMETRS']['URLTAG'];
			$sys_fields[]=$ini_file['PARAMETRS']['RECUPDATERESULTTAG'];
			
			$this->sys_fields=$sys_fields;

			$iconsole_serialized=serialize($this);
			file_put_contents($this->obj_save_path,$iconsole_serialized);
			
			if (!is_file("{$this->common_pft_path}{$this->all_pft}.pft")){
				file_put_contents("{$this->common_pft_path}{$this->all_pft}.pft","mfn#&uf('+0')'**********'");
			}
			if (!is_file("{$this->common_pft_path}{$this->web_dict}.pft")){
				file_put_contents("{$this->common_pft_path}{$this->web_dict}.pft","v1004'&&&'v1003'**********'");
			}
			
			if (!is_file("{$this->common_pft_path}{$this->read_rec_format}.pft")){
				file_put_contents("{$this->common_pft_path}{$this->read_rec_format}.pft","&uf('D',v1050,',@',v1080,',@all_pft')");
			}
			
			if (!is_file("{$this->common_frames_path}{$this->dummy_frame_name}.frm")){
				file_put_contents("{$this->common_frames_path}{$this->dummy_frame_name}.frm"," ");
			}
			
			if (!is_file("{$this->common_pft_path}{$this->quantity_frame_name}.frm")){
				file_put_contents("{$this->common_frames_path}{$this->quantity_frame_name}.frm","<--TOTALRECS-->*");
			}

		}
		
		$this->output_encoding=$output_encoding;
		$this->ident=$ident;


	}
	
	
		

	/* public function __sleep(){
		
		return $iconsole_obj;		
	}*/
	
	public function FindRecords($db, $search, $sequence = '', $start = 1, $count = 0){
    	
		$result=$this->fsearch($db,$search,$sequence,'@'.$this->all_pft,'','',1, $this->optimal_portion);
    	
		if (!$result) return NULL;
    	if ($result<0) return $result;
		
    	$rec_number=0;
    	$rec=explode('**********',$result);
    	for($r=0;$r<count($rec)-1;$r++){
    		
    		$str_fields=explode("\n",$rec[$r]);
    		$records[$rec_number]=new Record();			
			$records[$rec_number]->mfn=trim($str_fields[0]);		
			$records[$rec_number]->Status=0;		
			$records[$rec_number]->RecVersion=0;	    				
    		for($i=1;$i<count($str_fields)-1;$i++){
    			
					$field=explode("#",$str_fields[$i]);
					
					if(array_search($field[0],$this->sys_fields)===false)
    				  $records[$rec_number]->Content[$field[0]][]=trim($field[1]);
    		}
    		$rec_number++;
    	}
    	
    	return  ($rec_number>0) ? $records : NULL;   	
    }
    
    public function read_record($db,$mfn,$block=false){
    	
	 	$req['C21COM']='S';
	 	$req['S21COLORTERMS']='0'; 		
	 	$req['I21DBN']='RDR';	 	
		$req['S21All']="(<.>RI={$this->ident}<.>)";
		$req['S21FMT']=$this->read_rec_format;		
		$req['S21CNR']=1;
		$req['S21FRAME']=$this->dummy_frame_name;
		$req['MFN']=$mfn;
		 			
	
		$rec=$this->send_req($req);		    		
		$str_fields=explode("\n",$rec);

		if (count($str_fields)==2) return NULL;
		
		$record=new Record();
		$record->mfn=trim($str_fields[0]);		
		$record->Status=0;		
		$record->RecVersion=0;	    				
		
		// ��������� �������� -- ���������, ������� -2 			
		for($i=1;$i<count($str_fields)-2;$i++){
			
				$field=explode("#",$str_fields[$i]);			
				$record->Content[$field[0]][]=trim($field[1]);
		}
			
    	
    	return $record;
    }

    
     public function GetTermList($db, $prf='', $start_term='', $count=10, $format = '',$term_req=''){
    	
	 	$req['C21COM']='T';	 	
	 	$req['I21DBN']=$db;
	 	$req['S21ALL']=$term_req;
	 	$req['T21CNR']=(string)$count;
		$req['T21PRF']=$prf;
		$req['T21TRM']=$start_term;
		$req['S21FRAME']=$this->dummy_frame_name;

    	
		$result=$this->send_req($req);		    		

		if (!$result) return NULL;
    	if ($result<0) return $result;
		
    	$records=array();    	
    	$rec=explode('**********',$result);	
    	
    	for($r=0;$r<count($rec)-1;$r++){   		
    		$str=explode("&&&",$rec[$r]);
    		$records[$r]['key']=$str[1];			
			$records[$r]['refs']=$str[0];
    	}
    	
    	return $records;
    }
    
	public function search_list($db, $search_expression, $seq, $format, $portion = 0, $first_number = 1){
		return $this->fsearch($db, $search_expression, $seq, $format,'','',$first_number, $portion);
	}
	 public function fsearch($db, $search_expression, $seq='', $format='',$srw_name='',$sort_direction='',$first_number =  1, $portion = 0 ,$frame=''){
		
	 	$seq_format_name='';
	 	if ($seq && isset($this->common_pft_path)){
			$seq_format_name='ic'.crc32(substr($seq,0,100)); 
	 		$seq_format_full_path=$this->common_pft_path.$seq_format_name.'.pft';
			
			if (!is_file($seq_format_full_path)){
				file_put_contents($seq_format_full_path,$seq);
			}
			
		}
		
		$format_name='';
	  	if (substr($format,0,1)!=='@' && isset($this->common_pft_path)){
				$format_name='f'.crc32($format);
		 		$format_full_path=$this->common_pft_path.$format_name.'.pft';
				
				if (!is_file($format_full_path)){
					file_put_contents($format_full_path,$format);
				}
			
		}else {
			$format_name=substr($format,1);
		}
		
		

		
	 	$req['C21COM']='S';
	 	$req['S21COLORTERMS']='0'; 		
	 	$req['I21DBN']=$db;
		$req['S21All']=$search_expression;
		$req['S21FMT']=$format_name;
		$req['S21STN']=$first_number;		
		$req['S21CNR']=($portion) ? $portion : $this->optimal_portion;
		$req['S21SRW']=$srw_name;
		$req['S21SRD']=$sort_direction;
		$req['S21SCAN']=$seq_format_name;
		$req['S21FRAME']=($frame)? $frame: $this->dummy_frame_name;
		 			
		return $this->send_req($req);		
	 }
	 
	 public function export($db, $search_expression, $seq='', $portion = 0, $first_number = 1){	 	

	 	$seq_format_name='';
	 	if ($seq && isset($this->common_pft_path)){
			$seq_format_name='ic'.crc32(substr($seq,0,100)); 
	 		$seq_format_full_path=$this->common_pft_path.$seq_format_name.'.pft';
			
			if (!is_file($seq_format_full_path)){
				file_put_contents($seq_format_full_path,$seq);
			}
			
		}

	 	$req['C21COM']='E';
	 	$req['I21DBN']=$db;
		$req['S21All']=$search_expression;
		$req['S21STN']=$first_number;		
		$req['S21CNR']=($portion) ? $portion : $this->optimal_portion;
		$req['S21SCAN']=$seq_format_name;
		return $this->send_req($req);
	 }
	
	 
	
	 
	protected function send_req($req){
		
		try{
			$req['Z21ID']=$req['C21COM']<>'T' ? $this->ident: '';
			$req_path=$this->req_path.'/r'.time().rand(0,100);		
			
			if(@!$file=fopen($req_path.'.req','w+t')){
				throw new iexception('�� ������ ������� ���� �������'.$req_path.'.req',9992,0); 
			}
				foreach ($req as $rkey=>$rval){
					//i64 -- ��� ������� � UTF 
					// i32 -- � ����������� �� ���������
					if ($this->irb_modification==='i32'){
						if ($this->local_encoding==='win' && $this->output_encoding==='utf') $rval=u::utf_win($rval);
						elseif ($this->local_encoding==='utf' && $this->output_encoding==='win')$rval=u::win_utf($rval);												
					}else{
						// ���� i64
						if ($this->output_encoding==='win')$rval=u::win_utf($rval);
					}

					fwrite($file,"$rkey=$rval\n");
				}
			fclose($file);
			$exe_name=($this->irb_modification==='i32') ? 'wwwirbis32.exe' : 'wwwirbis64.exe';
			$cpath=$this->console_path.$exe_name;
							
	   		if (!$wsh_shell = new COM("WScript.Shell"))throw new iexception('�� ������ ��������� WSH ��������',9999,0); 
	   		if ($wsh_shell->Run("$cpath $req_path.req $req_path.out", 0, true)!=0) throw new iexception('�� ������ ��������� ���������� ������',9994,0);
	
	   		
			
			if(!file_exists("$req_path.out"))
				throw new iexception('�� ������ ���� ������',9993,1); 
								
			if(@($result=file_get_contents("$req_path.out"))===FALSE)
				throw new iexception('�� ������ ������� ���� ������',9991,1); 
			
			if(preg_match('{>error\d*<}',substr($result,0,120)))
				throw new iexception("������ WEB �����: $result  �������� ������ � ������ ��� �������� ���� ",9995,0); 


			@unlink("$req_path.req"); 
			@unlink("$req_path.out"); 
					
			if ($this->local_encoding==='win' && $this->output_encoding==='utf'){
				return u::win_utf($result);
			}elseif(($this->local_encoding==='utf' && $this->output_encoding==='win')){
				return u::utf_win($result);	
			}else
			return $result;			
			
		}catch(iexception $e){        				
    		$this->errors[]=$e;        	
        	return -$e->getCode();
       
		}
	}


	private function make_pft($content='',$prf='',$file_name=''){
	 		$format_name='';
		 	if ($content && isset($this->common_pft_path)){
		 		
		 		$format_name=($file_name) ? $file_name : ($prf.crc32(substr($content,0,100))); 
		 		$format_full_path=$this->common_pft_path.$format_name.'.pft';
				
				if (!is_file($format_full_path)){
					file_put_contents($format_full_path,$content);
				}
		 	}	
			return $format_name;		
	}




}



/* 
$irb=new iconsole('i32','C:/xampp/htdocs/ares/!irbis/exe/','1','win');

print_r($irb->GetTermList('NWPIL','A=','',10));


$rec=$irb->export('NWPIL','(<.>A=A$<.>)');
print_r($rec)

*/

//$rec=$irb->read_record('nwpil',1);
//print_r($rec)
//$rec=$irb->FindRecords('PASS_REQ','("RI=655$")');
//print_r($irb->errors);
//print_r($rec[0]);



//$rec[0]->AddField(200,'���� ���vbbbbbbbbbbbb���!');
//print_r($irb->RecWrite('PASS_REQ','',true,$rec[0]));

?>
<?php

class ji_titles{
	public static $ko_fields_array=array('spec'=>'C','disc_multiselect'=>'D','disc'=>'D','kaf'=>'B','sem'=>'F','purpose'=>'G');
	const DELIM='*';
	
	public static function disc(&$recs,$request){
		$titles_strings_array=self::ko($recs,'disc',$request);
		$recs_with_titles=self::convert_to_list($recs,$titles_strings_array);
		return $recs_with_titles;				
	}
	
	public static function spec_disc(&$recs,$request){
		$titles_strings_array=self::ko($recs,'disc_spec',$request);
		$recs_with_titles=self::convert_to_list($recs,$titles_strings_array);
		return $recs_with_titles;		
	}	

	private static function ko(&$recs, $titles_type,$request){
		$titles_array=array();
		foreach ($recs as $key=>$rec){
			
			if (!($occ691=$rec->GetFieldOcc(691)))
				continue;
			
			$keys_temp=array();
	//--------Проход по повторениям ОДНОЙ ЗАПИСИ------------
			for($i=1;$i<$occ691+1;$i++){						
				
				// Необходимы для ключа, поэтому являются обязательными
				if (!$rec->GetSubField(691,$i,'C') || !$rec->GetSubField(691,$i,'D'))
					continue;
								
				$coincidence=true;
				
				// -------------БЛОК ПРОВЕРКИ НА СООТВЕТСТСТВИЕ ЗАПРОСУ-------------------
				// Если в результате сопоставления связок с запросом в каком-то непустом поле было отличное от запроса значение -- 
				foreach(self::$ko_fields_array as $field_name=>$field_label){
					if (!u::ga($request,$field_name))
						continue;
					
					$req_field_str='*'.(is_array($request[$field_name]) ?  implode('*',$request[$field_name]) : $request[$field_name]).'*';							
						// Если в повторении есть хоть одно не совпавшее значение, прерываем цикл и устанавливаем совпадение в false
					if (strpos(mb_strtoupper($req_field_str,'UTF-8'),'*'.mb_strtoupper($rec->GetSubField(691,$i,$field_label),'UTF-8').'*')===false){
						$coincidence=false;										
						break;
					}	
				}
				// -------------БЛОК ПРОВЕРКИ НА СООТВЕТСТСТВИЕ ЗАПРОСУ-------------------
				
				// Здесь выполняем форм
				// В качестве ключа мы не можем использовать REC_ID, поскольку несколько заголовков может быть связано с одной записью
				if ($coincidence){ 
					$keys_temp[]=(($titles_type=='disc') ? $rec->GetSubField(691,$i,'D') : $rec->GetSubField(691,$i,'C').self::DELIM.$rec->GetSubField(691,$i,'D')).self::DELIM.$rec->GetRec_id();
				}
			}
				//\--------Проход по повторениям------------		
				
				// Уникальные значения для каждой записи добавляем в массив
				if ($keys_temp){
					$titles_array=array_merge($titles_array,array_unique($keys_temp));		
				}	
			}
		
		
			sort($titles_array);		
			
			// Вот переконвертирование. array_walk не удаётся применить из-за необходимости написания специального формата функции. 
			foreach ($titles_array as &$key_element) 
				$key_element=$key_element;
			
			return $titles_array;
	}
			
	
	
	// формирует линейный масив с записями
	private static function convert_to_list(&$recs,$titles_strings_array){
		$recs_with_titles=array();
		$titles_temp=array();
		foreach($titles_strings_array as $titles_string){			
			$titles=explode(self::DELIM,$titles_string);
			// Перебираем разбитую на заголовки строку. Формируем заголовки.
			for ($i=0;$i<count($titles)-1;$i++){
				// Если заголовок какого-то уровня перестал быть идентичным 
				if (empty($titles_temp[$i]) or $titles[$i]!==$titles_temp[$i]){					
					// Если изменился заголовок первого уровня, то мы должны заново отрисовать заголовок второго уровня, даже если аналогичный заголовок уже выводился ранее. 
					if ($i==0 && count($titles_temp))
					$titles_temp=array();
					 //array_fill(0,count($titles_temp),'');
					 					
					$recs_with_titles[]=array('title'=>$titles[$i],'level'=>$i);
					$titles_temp[$i]=$titles[$i];

					 
				}	
			}			
			//$titles_temp=$titles;
			$rec_id=$titles[count($titles)-1];
			
			$recs_with_titles[]=$recs[$rec_id];
		}
		return $recs_with_titles;
	}
	
	
	
	
}




?>
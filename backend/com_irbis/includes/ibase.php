<?php 

//FORMAT DELIMITER
define('FD','&&&&');


require_once('u.php'); 
require_once('record.php'); 
require_once('jrecord.php'); 
require_once('iexception.php'); 
require_once('ilog.php'); 


class ibase{
	
	public $errors=Array();
	public $output_encoding='utf';	
	public $host='';
	public $port_number='';
	public $timeout='';
	public $password='';
	public $login='';

	public function is_errors(){
		return $this->errors ? true : false;
	}

	public function is_crytical_errors(){
		if ($this->errors){
			foreach($this->errors as $error){
				if ($error->getLevel()>=2) 
					return true;
			}
		}
		return false;
	}
	
	public function get_errors(){
		return $this->errors;
	}
	public function add_errors($errors_exception_array){
		if ($errors_exception_array && is_array($errors_exception_array))
			$this->errors=array_merge($errors_exception_array,$this->errors);
	}
	
	public function clean_errors(){
		$this->errors=array();
	}
	
	public function get_last_error(){
		return $this->errors ? $this->errors[count($this->errors)-1]: null;
	}
	public function get_last_error_code(){
		return $this->errors ?  -abs($this->errors[count($this->errors)-1]->getCode()): 0;
	}
	public function get_last_error_level(){
		return $this->errors ?  $this->errors[count($this->errors)-1]->level : 0;
	}
	public function get_last_error_message(){
		return $this->errors ?  $this->errors[count($this->errors)-1]->getMessage() : '';
	}

}

?>
<?php


// first we include phpmorphy library
require_once(dirname(__FILE__) . '\morfology\src\common.php');
//require_once(dirname(__FILE__) . '\u.php');
// set some options
$opts = array(
    // storage type, follow types supported
    // PHPMORPHY_STORAGE_FILE - use file operations(fread, fseek) for dictionary access, this is very slow...
    // PHPMORPHY_STORAGE_SHM - load dictionary in shared memory(using shmop php extension), this is preferred mode
    // PHPMORPHY_STORAGE_MEM - load dict to memory each time when phpMorphy intialized, this useful when shmop ext. not activated. Speed same as for PHPMORPHY_STORAGE_SHM type
    'storage' => PHPMORPHY_STORAGE_MEM,
    // Enable prediction by suffix
    'predict_by_suffix' => true, 
    // Enable prediction by prefix
    'predict_by_db' => true,
    // TODO: comment this
    'graminfo_as_text' => true,
);

// Path to directory where dictionaries located

$dir = dirname(__FILE__) . '\morfology\dicts';
//file_put_contents('dir',$dir);


//setlocale(LC_CTYPE, array('ru_RU.CP1251', 'Russian_Russia.1251'));

class morfology{
	public $w;
	public $enable=false;
	private $morphy;
	
	public function __construct($word,$ru_enable=true,$en_enable=true){				   		
			global $dir,$opts;   			
		if (function_exists('mb_strtoupper')) 
		$this->w=mb_strtoupper($word,"UTF-8");
		else 
		$this->w=$word;
	
   		if(preg_match("{^[А-Я]+$}", $this->w)){
        	if ($ru_enable) {
        		$this->morphy = new phpMorphy(dirname(__FILE__) . '\morfology\dicts', 'ru_ru', $opts);		        		
        		$this->enable=true;
        	}
	     }else{
      	 	if(preg_match("{^[A-Z]+$}", $this->w)){
      			if ($en_enable){
      				$this->morphy = new phpMorphy(dirname(__FILE__) . '\morfology\dicts', 'en_en', $opts);
      				$this->enable=true;
      			}
      		}	
		}			
	}
	
	public function normalisation(){

		$result='';

		if (!$this->enable) 
			return $this->w;
			
			$result=$this->morphy->getPseudoRoot($this->w);
		
     	return (is_array($result)) ? $result[0] : $this->w;
	}
	
	public function forms(){

		$result=array();
		if (!$this->enable) 
			return array($this->w);
			
			$res=$this->morphy->getAllForms($this->w);
			
			$result=array_merge(array($this->w),$res ? $res : array());
		    $result=array_unique($result);
		    sort($result);
	    return $result;
	}
	
	public function __destruct(){
		unset($this->morphy);
	}
	
}

/*try {
	$m=new morfology('УДИВЛЯЮСЬ');
	print_r($m->forms());

} catch(phpMorphy_Exception $e) {
    die('Error occured while creating phpMorphy instance: ' . PHP_EOL . $e);
}
*/

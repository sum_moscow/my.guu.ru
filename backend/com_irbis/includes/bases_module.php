<?php

class bases_module{
	
	public $show_libraries=true;
	public $level=0;
	public $src;
	public $bl_id_array_selected;
	public $self_net_path;
	public $show_full_lib_name=false;
	
/*	public function __construct(){
		 parent::__construct(JI_LOCK_NO);
	}*/
	
	public function show_bases_module(){
	
		if ($this->show_libraries) 
			$bases_array=$this->get_lib_bases_array($this->show_full_lib_name ? 'full_name' : 'shot_name');
		else 	
			$bases_array=$this->src;
		
			
		
		?>
					<form action="<?php echo $this->self_net_path; ?>?task=set_selected_bases" method="GET" id="bases_module">
						<ul class="menu">
						<?php
						foreach ($bases_array as $lib_name=>$lib_bases_array){
							// Всё с точностью наоборот -- 
							if (!@$lib_bases_array['published']){
								if ($this->show_libraries)
									$this->show_lib_bases($lib_name,$lib_bases_array);
								else 
									$this->show_base($lib_bases_array['bnf'],$lib_bases_array['bl_id'],$lib_bases_array['access_level']);
							}
						}
						?>
						</ul>
					</form>						
				<div id="select_all" style="active deeper parent" style="text-align:center">
					<a href="#"> Выбрать все </a>
				</div>
	<?php 	
		
	}	
	
	
	private function get_lib_bases_array($key){
		$lib_bases_array=array();
		foreach($this->src as $base){
			$lib_bases_array[$base[$key]][]=$base;
		}
		return $lib_bases_array;
	}
	
	private function show_lib_bases($lib_name,$lib_bases_array){
		?>
		<li>
			<a href="#"><?php echo $lib_name; ?></a>
			<ul>		
			<?php
			foreach ($lib_bases_array as $base)
				$this->show_base($base['bnf'],$base['bl_id'],@$base['access_level']);
			?>
			</ul>
		</li>							
		<?php
	}
	
	private function show_base($base_name,$base_id,$base_level){
		if (!$this->is_base_avalable($base_level)) 
			return false;
	?>

		<li>
            <a href="javascript:void(0)">
                <label>
                    <input class="base_checkbox" name="bl_id_array_selected[<?php echo $base_id ?>]" <?php echo  (@array_search($base_id,$this->bl_id_array_selected)!==false) ? 'checked=""' : '' ?>   value="<?php echo $base_id ?>" type="checkbox">
                    <?php echo $base_name ?>
                </label>
            </a>
		</li>
	<?php
		return true;
	}

	private function is_base_avalable($base_level=0){
		if (!$base_level)
			return true;
				
		if ($base_level<=$this->level)
			return true;
		
		return false;		
	}
	
}
?>
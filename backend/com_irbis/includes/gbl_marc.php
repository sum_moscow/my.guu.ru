<?php
require_once('conv_base.php');


class gbl_marc extends conv_base{
	public function convert(jrecord &$rec,$params=array(),$bl_id=0){
		//REC_OUT
		/* @var $rec jrecord */	
		$rec->filter($this->useful_fields,$this->deleted_subfields);		
		if ($rec->GetField(922,1)==='v922') 
			$rec->DeleteField(922);

				
		if ($params){		
			//$rec->SetField(902,1,'^A'.$params['full_name'].'^S'.$rec->Get_bl_id());
			// В норме bl_id должен быть в самой записи!! 
			$rec->SetField(902,1,'^A'.$params['full_name'].'^S'.$bl_id);
		}
		return $rec;
	}
	
}
/*$rec_array=unserialize(file_get_contents('z39.res'));    	
print_r($rec_array[0]);
$c=new conv_knigafund();

echo $c->get_id('http://www.knigafund.ru/books/52');
*/
//print_r($c->convert($rec_array[0]));

?>
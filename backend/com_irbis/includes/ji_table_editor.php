<?php
// Тип ассоциативного массива
define('JI_SRC_ARRAY_MNU',0);
define('JI_SRC_ARRAY_CFG',1);
define('JI_SRC_ARRAY_CFG_ARRAY',2);


// Класс, учитывающий работу с расположением конкретных элементов J-ИРБИС
class ji_table_editor extends table_editor{
	public $itemid='';
	public 
	public function __construct($structure_params){
		parent::__construct($structure_params);
	}	
	public static function get_js_settings(){	
		$st=new stdClass();
		$st->self_net_path=JI_FILE_PATH_AUTO_REQUESTS_NET_GLOBAL_PROVIDER;
		$st->images_net_path=JI_PATH_IMAGES_NET;
		$st->ji_path_cms_net=JI_PATH_CMS_NET;
		$st->debug=(int)ji_st::i()->debug_reporting;	
		return $st;				
	}		

	// Сохранение данных в базу или исходное местоположение
	// Обновление оригинальных версий таблиц в рекурсии (при нажатии кнопки Сохранить в форме). Параметры -- базовый массив
	public function update_source_tables($structure_params=null){
		// Если отсутствует structure_params 
		if (!$structure_params)
			$structure_params=$this->structure_params;		
		//die('YES');
		$array=$this->get_cache_data($structure_params->name);	
		
		
		$serialized_list=$this->get_serialized_elements_list($structure_params->name);
		if ($serialized_list){
		// Выполняем SERIALIZE
			foreach ($array as $index=>&$record){
				foreach($serialized_list as $serialized_field_name){
					if(!empty($record[$serialized_field_name]))				
						$record[$serialized_field_name]=$this->assoc_array_index_back($record[$serialized_field_name]);				
				}						
			}
		}		
		// Для интегрированных массивов -- здесь мы возвращаем им исходную структуру и удаляем элементы с нулевым значением, которые были удалены через интерфейс 
		switch ($structure_params->src_type){			
			case  JI_SRC_ARRAY:{
				switch ($structure_params->src_array_type){
					
					case JI_SRC_ARRAY_CFG:{
						$res=$this->set_cfg_array($array,$structure_params->name);				
					}
					break;		
					case JI_SRC_ARRAY_MNU:{
						$res=$this->set_mnu_array($structure_params->name,$structure_params->itemid,$array);		
					}
					break;							
				}
			}
			break;			
			case JI_SRC_MYSQL: 			
				$res=$this->set_mysql_table($structure_params->name,$array);				
			// По дефолту может быть интегрированнй массив, который совсем не нужно кэшировать
			default:						
		}
		
		if(property_exists($structure_params,'function_excute') and $structure_params->function_excute){
			call_user_func(array(__CLASS__,$structure_params->function_excute));
		}
		
		if (isset($structure_params->ref_table))
			$this->update_source_tables($structure_params->ref_table);
	}
		
	// Кэширование.  Поскольку здесь используются специфические константы и идёт отсылка к специфическим функциям эту функцию нельзя поместить в базовый класс
	protected function cache_abstract_structure($structure_params){
		
		if (!$structure_params)
			$structure_params=$this->structure_params;

			
		switch ($structure_params->src_type){			
			case  JI_SRC_ARRAY:{
				switch ($structure_params->src_array_type){
					
					case JI_SRC_ARRAY_CFG:{
						$res=$this->get_cfg_array($structure_params->name);				
					}
					break;		
					case JI_SRC_ARRAY_MNU:{
						//TODO: здесь должно быть обращение к внешнему элементу, а не к структуре. Одна структура может описывать 
						//несколько массивов $structure_params->itemid
						$res=$this->get_mnu_array($structure_params->name,$structure_params->itemid);		
					}
					break;							
				}
			}
			break;			
			case JI_SRC_MYSQL: 			
				$res=$this->get_mysql_table($structure_params->name);				
			// По дефолту может быть интегрированнй массив, который совсем не нужно кэшировать
			default:						
		}
		
		// Не проверяем $res, так как при возникновении ошибок уже были бы выброшены эксепшены.  
		$this->set_cache_data($structure_params->name,$res);
		
		if (isset($structure_params->ref_table))
			$this->cache_abstract_structure($structure_params->ref_table);
		
	}
	
	// Извлечение стандартной MYSQL таблицы. Интегрированные массивы сразу преобразуем к обычной форме
	protected function get_mysql_table($name){
		global $__db;
	
		$sql="
		SELECT *
		FROM #__$name
		"; 
		idb::i()->setQuery($sql);
		if (!($array=idb::i()->loadAssocList())) 
		$array=array();
			//throw new Exception("Ошибка SQL при выборке из таблицы $name :".idb::i()->getErrorMsg(),idb::i()->getErrorNum());				
		
		$serialized_list=$this->get_serialized_elements_list($name);
		
		if (!$serialized_list) 
			return $array;
		// Выполняем UNSERIALIZE	
		foreach ($array as $index=>&$record){
			foreach($serialized_list as $serialized_field_name){
				//if ($serialized_field_name==='vb_references')
					
				if($record[$serialized_field_name] && $record[$serialized_field_name]){				
					$result= @unserialize($record[$serialized_field_name]);
					
						$record[$serialized_field_name]= is_array($result) ? $this->assoc_array_index_replace($result) : array();
						
				}
				//if ($serialized_field_name==='vb_references')
			}			
		}
		return $array;
	}
	// Извлечение стандартной MYSQL таблицы. Интегрированные массивы сразу преобразуем к обычной форме
	protected function set_mysql_table($name,$array){
		global $__db;
		
		$serialized_list=$this->get_serialized_elements_list($name);
		if ($serialized_list){
		// Выполняем SERIALIZE
			foreach ($array as $index=>&$record){
				if (empty($record)) 
					continue;							
				foreach($serialized_list as $serialized_field_name){
					if(!empty($record[$serialized_field_name]))				
						$record[$serialized_field_name]=@serialize($record[$serialized_field_name]);				
						else 
						$record[$serialized_field_name]='';
				}						
			}
		}
		
		$sql="
		TRUNCATE #__$name
		"; 
		idb::i()->setQuery($sql);
		if (!idb::i()->query()) 
			throw new Exception("Ошибка SQL при опустошении таблицы $name: ".idb::i()->getErrorMsg(),idb::i()->getErrorNum());				

		$sql_parts=array();
		
		$array_keys=array_keys($array[0]);		
		foreach($array as $rec){
			if (empty($rec)) 
				continue;
			$sql_record=array();
			foreach ($array_keys as $key){								
					$sql_record[]=idb::i()->Quote(u::ga($rec,$key,''));
			}
			$sql_parts[]="(".implode(',',$sql_record).")";
		}		
		$sql = "INSERT INTO #__$name (".implode(',',$array_keys).") VALUES ";
		$sql.=implode(',',$sql_parts);	
		idb::i()->setQuery($sql);
		

		if (!idb::i()->query()) {
			if (idb::i()->getErrorNum()==1062) throw new Exception("Дублетный идентификатор в таблице $name. Увеличьте значение идентификатора в добавленных записях! ",idb::i()->getErrorNum());				
			throw new Exception("Ошибка SQL при формировании таблицы $name: ".idb::i()->getErrorMsg(),idb::i()->getErrorNum());				
		}

	}
	
	
	
	// TODO: ещё не написано
	// Извлечение массива из параметров пункта меню 
	protected function get_mnu_array($name,$itemid){
		
	}
	
	// Установка массива из параметров пункта меню
	protected function set_mnu_array($name,$itemid,$value){
		
	}
	
	
	// БЕз параметров, если мы планируем отображать CFG массив в таблцие 
	protected function get_cfg_array($name=''){
		$res_array=array();
		
		
		if (isset($GLOBALS['CFG'][$name])){
			$result=$this->assoc_array_index_replace($GLOBALS['CFG'][$name]);
			return $result;
		}
		
		
		foreach ($GLOBALS['CFG_descriptions'] as $name=>$descr_array){
			// Если задан массив с описанием параметра
			if (is_array($descr_array)){
				if (isset($GLOBALS['CFG'][$name])){
					
					$res_array[$name]=array_merge(
						// Используем шаблон на случай, если какие-то элементы в DESCRIPTION отсутствуют
						array('label'=>'','default'=>'','description'=>''),
						$descr_array,
						array('value'=>$GLOBALS['CFG'][$name])
					);
					
				}elseif(substr($name,0,1)==='-') {
					$descr_array['label']=mb_strtoupper($descr_array['label'],'UTF-8');
					$res_array[$name]=array_merge(
						array('label'=>'','default'=>'','description'=>'','value'=>''),
						$descr_array
					);
				}
			}
		}
		
		return $this->assoc_array_index_replace($res_array);
	}
	
	// БЕз параметров, если мы планируем отображать CFG массив в таблцие 
	protected function set_cfg_array($array,$name=''){
		$new_data=$this->assoc_array_index_back($array);

		if (isset($GLOBALS['CFG'][$name])){
			$GLOBALS['CFG'][$name]=$new_data;
		}else {		
			foreach ($GLOBALS['CFG'] as $par_name=>&$value){
				if (isset($new_data[$par_name]))	{
					$value=$new_data[$par_name]['value'];					
				}
			}
		}
		if (!u::cfg_write(JI_PATH_COMPONENT_LOCAL.'/jirbis_configuration.php',$GLOBALS['CFG']))
			throw new Exception("Ошибка записи в jirbis_configuration.php. Возможно, файл заблокирован другим приложением",70);				
	
	}
	
	
	public static function clean_all_sessions(){
		// В принципе может выбрасывать эксепшен
		ji_update::clean_all_sessions();
	}
		
}

?>
<?php 
class ji_bo_grid{
	
/*c) Запись ИРБИС рек
d) Показывать номер
e) Показывать обложки
f) Массив с названиями секций и их адресами
g) Показывать чекбокс
h) Показывать новизну
i) Зебра разрешена
j) Значок выгрузки
*/
	

public $number_enable=true;
public $download_enable=true;
public $upload_enable=true;
public $checkbox_enable=true;
public $cover_enable=true;
public $zebra_enable=true;
public $pager_enable=true;
public $lighting_enable=true;
public $lighting_tag='span';
public $lighting_tag_param='class="bo_lighting"';
public $books_comments_enable=true;
public $books_raeting_enable=true;

public $default_no_cover_name='no_cover.jpg';
public $cover_width=152;
public $cover_height=196;
public $first_number=1;

// RECORD PRESENTATION
public $rp=array();
public $recs=array();
public $raetings=array();
public $covers_path_local='';
public $self_net_path='';
public $rec_id_array_selected=array();
public $books_raeting_array=array();

private $req_words=array();
//
public $php_correction;

	public function __construct(){
		$this->php_correction=new ji_php_user_rec_correction($this->recs,$this->rp);
	}

	public function bo_list($output){
		global $bo_grid_debug;
		
		$this->recs=$output['recs'];
		$this->req_words=$output['req_words'];
		
		if (DEBUG_BO_GRID) $this->recs=unserialize($bo_grid_debug);
		
		if (!count($this->recs)) return '';
		
		ob_start();
		
		?>
		
		<table class="records"> 
		
		<?php 

		$i=0;
		foreach($this->recs as $r){
			$i++;
			
			?>

			<tr>
				<td <?php echo ($i%2 && $this->zebra_enable) ?  'class="lighted"' : '' ; ?> >
				<?php 
				if (is_object($r))
					$this->bo_cell($r,$i); 
					else {
						echo 'Запись повреждена! ( не является объектом JRecord )';
						echo '<pre>';
						print_r($r);
						echo '</pre>';
					}
				?>
				</td>
			</tr>
			<?php
		}
		//echo u::get_microtime();		
		?>

		</table> 
		
		<?php
		return ob_get_clean(); 
	}
	
		private function bo_cell($r,$i){
		

		
		?>
			<table class="record">
			<tr>
				<?php if ($this->number_enable || $this->checkbox_enable || $this->download_enable){ ?>	
				<td class="number_cell">
					<?php if ($this->number_enable){ ?>	
					<div class="number">
						<?php echo $i+$this->first_number-1 ?>
					</div>
					<?php } ?> 
					
					<?php if ($this->checkbox_enable){ ?>	
					<div class="checkbox">
						<form>
						<input name="code" class="code_checkbox" <?php echo in_array($r->GetRec_id(),$this->rec_id_array_selected) ? ' checked=""' : '';  ?> value="<?php echo $r->GetRec_id();?>" type="checkbox">					
						</form>
					</div>						
					<?php } ?> 
					
					<?php if ($this->download_enable){ ?>	
					<div class="download_button">
							<img class="download_image" src="<?php echo JI_PATH_IMAGES_NET ?>/download.gif">
					</div>
					<?php } ?> 		
								
					<?php if ($this->upload_enable){ ?>	
					<div class="upload_button" title="<?php echo "bl_id={$r->GetBl_id()}&mfn={$r->GetMfn()}&rec_id={$r->GetRec_id()}" ?>">
							<img class="upoad_image" src="<?php echo JI_PATH_IMAGES_NET ?>/upload.gif" >
					</div>
					<?php } ?> 					
					
				</td>
				<?php } ?> 
				
				<?php if ($this->cover_enable){ ?>	
				<td  class="cover_cell">
						<?php $this->get_cover($r); ?>		
				</td>							
				<?php } ?> 
				
				<td  class="bo_cell">
					<div class="bo_tabs">
						<ul>
							<li><a href="#bo_tab<?php echo $r->GetRec_id(); ?>">Библиографическая запись</a></li>
							<?php $this->tabs($r); ?>
						</ul>
						<div id="bo_tab<?php echo $r->GetRec_id(); ?>">
								<?php if ($this->books_raeting_enable){ 
									$raeting=u::ga($this->books_raeting_array,$r->GetRec_id(),array());	
								?>	    											
				                  <div class="raeting">				                  
				                 	<input type="hidden" name="val" value="<?php echo round(u::ga($raeting,'avg',0)) ?>"/>
				                 	<input type="hidden" name="votes" value="<?php echo u::ga($raeting,'counts',0) ?>"/>
				                 	<input type="hidden" name="vote-id" value="<?php echo $r->GetRec_id(); ?>"/>				                 					                  	
				                  </div>
	    						<?php } ?> 
								
								<div class="bo_div">
									<?php $this->bo($r); ?>
								</div>

							<?php if ($this->books_comments_enable){ ?>	    			
				                  
								  <div class="comments" id="<?php echo $r->GetRec_id(); ?>">
					                  <div class="c_get" title="Комментарии">
						                  <img class="upoad_image" src="<?php echo JI_PATH_IMAGES_NET ?>/comment.png"> 
						                  <!--<span class="comments_span">Комментарии</span>-->
						              </div>
					                  <div class="c_list">
					                  </div>
				                  </div>
	    					<?php } ?> 								
								<div class="add_buttons">
									<?php $this->links($r); ?>
									<div class="add_elements">										
									</div>
								</div>   


																	
						</div>
						
							
						
					</div>
				</td>							
			</tr>
		</table>
		
	<?php
		}

		private function bo($r){
			
			$this->php_correction->show_before_bo($r);
			
			foreach ($this->rp as $format_type=>$rules){
				if ($rules['type']==0){
					if ($this->lighting_enable)
						echo $this->lighting($r->GetFormatValue($format_type));
					else 
						echo $r->GetFormatValue($format_type);
				}elseif($rules['type']==1){
					echo "<span id=\"a$format_type{$r->GetRec_id()}\" style=\"display:none\"></span>\n\n";
				}
			}
			
			$this->php_correction->show_after_bo($r);
			
		}
		
		private function lighting($bo){

			$backlight = new Backlight(array(
			    'source'    =>$bo,
			    'query'     =>$this->req_words,
			    'params'    =>array('tag'=>$this->lighting_tag,'options'=>array($this->lighting_tag_param))
			));
			
			return $backlight->display();			
		}
		
		private function links($r){
			foreach ($this->rp as $format_type=>$rules){
				if ($rules['type']==1 && $this->need_additional_element($r,$rules['indicators'])){					
					echo "<button class=\"link_bo_part\" id=\"$format_type{$r->GetRec_id()}\" title=\"{$rules['request']}&rec_id={$r->GetRec_id()}&bl_id={$r->GetBl_id()}&format_type=$format_type\">{$rules['title']}</button>\n\n";
				}
				
			}
			
		}
		
		private function tabs($r){
			$timestamp=u::get_microtime();
			
			foreach ($this->rp as $format_type=>$rules){				
				if ($rules['type']==2 && $this->need_additional_element($r,$rules['indicators'])){					
					echo "<li><a href=\"{$this->self_net_path}?{$rules['request']}&rec_id={$r->GetRec_id()}&bl_id={$r->GetBl_id()}&format_type=$format_type&_=$timestamp\">{$rules['title']}</a></li>\n\n";

				}
				
			}			

		}
		
		private function need_additional_element(&$r,$indicators){
			if (is_array($indicators)){
				foreach($indicators as $field){
					if ($r->GetField($field,1)) return true;					
				}				
			}else{
				eval("\$res=$indicators;");
				if ($res) return true;
			}
			return false;
		}
		
		private function get_cover(&$r){
			$isbn='';
			for($i=1;$r->GetField(10,$i);$i++){
				$isbn=$r->GetSubField(10,1,'A');
				if ($isbn) 
					break;
			}
			//echo $isbn;
			
			$image_path_isbn=($isbn) ? "{$this->self_net_path}?task=get_cover&isbn=$isbn" : '';			
			$image_path="{$this->covers_path_local}/{$this->default_no_cover_name}";
		
			
			echo "<img class=\"cover_img\" title=\"$image_path_isbn\" style=\"width:{$this->cover_width}px;height:{$this->cover_height}px;\" src=\"$image_path\">";
				
			
		}
		
}
?>
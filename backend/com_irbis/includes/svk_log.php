<?php
require_once('ilog.php');

class svk_log extends ilog{
	protected static $instance;  // object instance
	protected static $start_microtime=0;    
	
	public function w($msg='',$type=I_INFO,$error_number=0,$error_level=0){				
		$this->output_string($msg,$type,$error_number,$error_level);	
		
		
		parent::w($msg,$type,0);
		
		if ($type==I_ERROR){
			errors::add($error_number,$msg,$error_level);
			if ($error_level==I_ERROR_LEVEL_CRITIICALY)
				throw new Exception($msg,$error_number);
			elseif ($error_level==I_ERROR_LEVEL_ABORT) 
				die();
				
				
		}					
		
	}	
	
	/**
     * Форматирование и вывод протокола
     *
     * @return ji_ulog
     */

	//Форматирование протокола
	public function formated_output($log_array){
		
		if (!is_array($log_array))
			echo 'Данные протокола отсутствуют!';
			
		foreach($log_array as $c){
			$this->output_string($c['msg'],$c['type']);
		}
	}
	
	
	public function output_string($msg='',$type=I_INFO,$error_number=0,$error_level=0){	
		switch ($type){
			case I_ERROR: $message='<span style="background-color:red">'.$msg.'</span></br>'; break;
			case I_INFO_HEADER:	$message='<h1>'.$msg.'</h1></br>'; break;
			case I_INFO: 
			default: 
			$message=''.$msg.'';	break;
		}
		
		
		?>
		<tr>

			<td>
				<?php echo floatval((u::get_microtime()-self::$start_microtime)/10000) ?>
			</td>
							
						
			<td>
				<?php echo $message; ?>
			</td>			
			
		</tr>
		<?php
		flush();		
	}
	
	public function __construct(){
		self::$debug_path=JI_PATH_DEBUG_LOCAL;
		self::$start_microtime=u::get_microtime();
		@header('Content-Type: text/html; charset=utf-8');
		?>		
		<html>
			<head>
					<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
			</head>		
		<body>
		<table width="100%" border="1">		
		<?php

	}
	public function __destruct(){
		?>
		</table>
		</body>
		</html>
		<?php		
	}
    /**
     * Возвращает единственный экземпляр класса
     *
     * @return ji_ulog
     */
    public static function i() {
        if ( is_null(self::$instance) ) {
            self::$instance = new svk_log;
        }
        return self::$instance;
    }	
	
}

/* @var ilog::i() ilog */
/* @var ilog::i ilog */
/*jlog::$debug_enable=true;
jlog::$debug_path='C:/temp';
jlog::i()->w('main','6666','Message',I_INFO);
jlog::i()->w('temp','888','MEssage',I_INFO);
jlog::i()->formated_output(jlog::i()->get_txt_as_array());
jlog::i()->sql('SQL','sssss',array('f'));
jlog::i()->packet('PACKET','dfsdfsdf');
jlog::i()->replace_debugs();
*/


?>
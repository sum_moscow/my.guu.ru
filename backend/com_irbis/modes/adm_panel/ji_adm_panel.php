<?php

class ji_adm_panel{

	// Получение списка заказанных изданий
	public  static function output($params_array=array(),$search_mode=''){	
		$st=ji_st::i(JI_LOCK_NO);		
		$js_settings_array=ji_adm_panel::get_js_settings();
		ji_adm_panel_show::js_css();
		ji_adm_panel_show::js_settings($js_settings_array);
		$data=array();
		$data['self_net_path']=JI_PATH_COMPONENT_NET.'/modes/adm_panel/'.JI_FILE_AJAX_PROVIDER;
		ji_adm_panel_show::template(JI_PATH_COMPONENT_LOCAL.'/modes/adm_panel/template.htm',$data);	
	}

	public  static function get_js_settings(){	
		$st=new stdClass();
		$st->self_net_path=JI_FILE_PATH_AUTO_REQUESTS_NET_GLOBAL_PROVIDER;
		$st->images_net_path=JI_PATH_IMAGES_NET;
		$st->ji_path_cms_net=JI_PATH_CMS_NET;
		$st->debug=(int)ji_st::i()->debug_reporting;	
		return $st;				
	}	

	
	public static function get_tab_content(){
		
	}
	// Здесь выводим всё: и JS подключаемые слои табов. 
	public function show_adm_panel(){
		
	}
	
	// Содержимое tab 
	public function get_src_tab(){
		
	}
	
	public function get_settings_tab(){
		
	}

	public function get_search_tab(){
		
	}
	
	//Выводит HTML для конкретного поискового режима
	public function get_search_acordion_html($itmid){
		
	}
}


?>
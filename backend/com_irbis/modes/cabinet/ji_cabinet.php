<?php
class ji_cabinet {
	
	// Получение списка заказанных изданий
	public  static function output($params_array=array(),$search_mode=''){	
		$st=ji_st::i(JI_LOCK_NO);
		$debts_array=ji_cabinet::get_debts_accordion();
		$js_settings_array=ji_cabinet::get_js_settings();
		
		//Заполнение массива с данными 
		$data=array();
		$data['debts_accordion']=ji_cabinet_show::debts_accordion($debts_array);
		
		//Вывод данных
		ji_cabinet_show::js_css();
		ji_cabinet_show::js_settings($js_settings_array);
		ji_cabinet_show::template(JI_PATH_COMPONENT_LOCAL.'/modes/cabinet/template.htm',$data);	
	}
	// Получение долгов
	public  static function get_debts_accordion(){
		$debts_data=array(
			'overdued'=>array(),
			'debts'=>array(),
			'readed'=>array(),
		);
		
		
		$user_rec=ji_st::i()->user;
		
		try{
			if (!$user_rec) 
				throw new Exception('Режим доступен только для авторизованных читателей! Возможно, Вы вышли из авторизованного режима!');
			/* @var $this->user record */
			/* @var self::user record */
			
			if (!$user_rec->GetField(40))
				throw new Exception('Нет истории книговыдачи! Возможно, Вы ещё не брали литературу литературу.');
				
			for($i=1;;$i++){
			
				if (!$user_rec->IsField(40,$i)) 
					return $debts_data;
				
				$bo=array('bo'=>$user_rec->GetSubField(40,$i,'C').", Срок возврата: ".substr($user_rec->GetSubField(40,$i,'E'),6,2).'.'.substr($user_rec->GetSubField(40,$i,'E'),4,2).'.'.substr($user_rec->GetSubField(40,$i,'E'),0,4),'occ'=>$i-1);				
				
				if 	(strpos($user_rec->GetSubField(40,$i,'F'),'*')!==false){
					if (date('Ydm')>$user_rec->GetSubField(40,$i,'E'))
						$debts_data['overdued'][]=$bo;
						else 
						$debts_data['debts'][]=$bo;				
				}else{
					$debts_data['readed'][]=$bo;				
				}
			}					
			
			return $debts_data;
			
		}catch (Exception $e){
			$debts_data['overdued']='Ошибка: '.$e->getMessage();			
			return $debts_data;
		}
		
	}
	
	public  static function get_js_settings(){	
		$st=new stdClass();
		$st->self_net_path=JI_FILE_PATH_AUTO_REQUESTS_NET_GLOBAL_PROVIDER;
		$st->images_net_path=JI_PATH_IMAGES_NET;
		$st->ji_path_cms_net=JI_PATH_CMS_NET;
		$st->debug=(int)ji_st::i()->debug_reporting;	
		return $st;				
	}	

}


?>
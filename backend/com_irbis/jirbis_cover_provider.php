<?php
require_once( 'jirbis_link_libraries.php' );

$covers=new covers();
$covers->covers_path_local=JI_PATH_COVERS_LOCAL;
$covers->covers_download_enable=true;
$covers->default_no_cover_name=JI_FILE_DEFAULT_COVER_IMAGE_NAME;
$covers->timeout_google=100;



$isbn=mosGetParam( $_REQUEST, 'isbn', '5-87897-005-8' );

try{
	
	$images=$covers->get_remote_cover_urls_array($isbn);
	
	if (!$images) 
		die('no');
	
	$res=$covers->download_best_cover($images);	
	
	if (isset($res['url']))
		die($res['url']);
	
	die('no');
	

}catch (Exception $e){

	die('error');
}

?>
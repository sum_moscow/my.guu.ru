<?php

// Если скрипт запускается с целью отладки 
if (empty($CFG)&& empty($GLOBALS['CFG']))
	include_once('../jirbis_link_libraries.php');

@@define('JI_OUTPUT',0);
@@define('JI_BUTTON',1);
@@define('JI_ACCORDION',2);
try{
	//*********************ОБНОВЛЕНИЕ КОНФИГУРАЦИОННЫХ ФАЙЛОВ*************************************
	@include_once(JI_PATH_UPDATE_SCRIPTS_LOCAL.'/jirbis_configuration_template.php');
	@include_once(JI_PATH_UPDATE_SCRIPTS_LOCAL.'/jirbis_defaults_template.php');

	if(empty($CFG) && isset($GLOBALS['CFG'])) $CFG=$GLOBALS['CFG'];
	if(empty($default_settings) && isset($GLOBALS['default_settings'])) $default_settings=$GLOBALS['CFG'];
	
	if (!is_array($CFG))
		throw  new Exception('Массив $CFG не доступен');
	if (!is_array($CFG_template))
		throw  new Exception('Массив $CFG_template не доступен');
	if (!is_array($default_settings))
		throw  new Exception('Массив $default_settings не доступен');		
	if (!is_array($default_settings_template))
		throw  new Exception('Массив $default_settings_template не доступен');
	
	$cfg_old=array_merge($CFG,$default_settings);
	
	

	
	
	//Берём шаблон и дополняем его новыми значениями	
	update_cfg_array($default_settings_template,$cfg_old);
	update_cfg_array($default_settings_template,$default_settings);
	update_cfg_array($CFG_template,$cfg_old);

	$CFG_template['fp']['all']['field_type']='';
	$CFG_template['fp']['keywords']['field_type']='';
	$CFG_template['fp']['keywords_z']['field_type']='';
	$CFG_template['fp']['work_place']['dic_function']='preload';
	
	$CFG_template['fp']['document_form']['field_type']='combobox';
	$CFG_template['fp']['document_form']['dic_function']='get_document_form';
	$CFG_template['fp']['document_type']['field_type']='combobox';
	$CFG_template['fp']['document_type']['dic_function']='get_document_type';
	
	$CFG_template['ed_avalable_extensions']='*.html; *.txt; *.pdf; *.zip; *.rar; *.doc; *.rtf; *.ppt; *.jpg; *.gif; *.jpeg; *.png; *.bmp; *.ppsx; *.pptx; *.pptm';
	
	
	if ($CFG_template['forms_profile']['professional']){
		$CFG_template['forms_profile']['prof']=$CFG_template['forms_profile']['professional'];
		unset($CFG_template['forms_profile']['professional']);
	}
	
	if (!cfg_write(JI_PATH_COMPONENT_LOCAL.'/jirbis_defaults.php',$default_settings_template,'default_settings'))
		throw  new Exception('Не удалось открыть на запись jirbis_defaults.php');	
	
	if (!cfg_write(JI_PATH_COMPONENT_LOCAL.'/jirbis_configuration.php',$CFG_template,'CFG'))
		throw  new Exception('Не удалось открыть на запись jirbis_configuration.php');
//\*********************ОБНОВЛЕНИЕ КОНФИГУРАЦИОННЫХ ФАЙЛОВ*************************************

//*********************ОБНОВЛЕНИЕ СТРУКТУРЫ ТАБЛИЦ*************************************
// Ориентируемся как на старый, так и на новый вариант провайдера. Но сохраняем старый синтаксис

	$GLOBALS['db']=get_db();
	
	

	tm('mhr',"
	CHANGE COLUMN `id` `mhr_id` INT(11) NOT NULL AUTO_INCREMENT AFTER `kaf_full_name`;
	CHANGE COLUMN `comment` `comment` VARCHAR(255) NULL DEFAULT '' AFTER `mhr_id`;
	ADD COLUMN `order_enable` TINYINT(1) NULL DEFAULT '1' AFTER `comment`;
	");

	tm('req_rec','
	DROP INDEX `req_id_rec_id_i`,
	ADD UNIQUE INDEX `req_id_rec_id_bl_id_i` (`req_id`, `rec_id`, `bl_id`);	
	');
		
	
	tm('jusers',"
	CHANGE COLUMN `lib_raeting` `update_enable` INT(1) NOT NULL DEFAULT '1' AFTER `shot_name`;
	");

	
	tm('bases',"
	CHANGE COLUMN `profile_special` `profile_special` MEDIUMTEXT NOT NULL DEFAULT '' AFTER `bugs_count`,
	CHANGE COLUMN `profile_special_form` `profile_special_form` MEDIUMTEXT NOT NULL DEFAULT '' AFTER `vb_references`,	
	CHANGE COLUMN `level` `access_level` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `ordering`,
	CHANGE COLUMN `default` `search_default` TINYINT(1) NOT NULL DEFAULT '0' AFTER `access_level`,	
	CHANGE COLUMN `pause_end` `pause_end` TIMESTAMP NULL AFTER `bnf`,	
	ADD COLUMN `svk` TINYINT(1) NOT NULL DEFAULT '0' AFTER `search_default`,
	ADD COLUMN `svk_downloaded` INT NOT NULL DEFAULT '0' AFTER `svk`;
	");
	
	
	tm('libraries',"
	ADD COLUMN `is_persistent` TEXT NULL  AFTER `charset`,
	ADD COLUMN `is_piggy_back` TEXT NULL AFTER `is_persistent`,
	ADD COLUMN `avalable_attributes` TEXT NULL  AFTER `is_piggy_back`,
	ADD COLUMN `remote_format` VARCHAR(50) NULL DEFAULT '' AFTER `avalable_attributes`,
	ADD COLUMN `remote_encoding` VARCHAR(50) NULL DEFAULT 'utf' AFTER `remote_format`;
	ADD COLUMN `sigla` VARCHAR(50) NOT NULL DEFAULT '' AFTER `ordering_lib`;
	CHANGE COLUMN `avalable_attributes` `avalable_attributes` VARCHAR(3000) NOT NULL DEFAULT '' AFTER `is_piggy_back`;		
	");
	
	
	$GLOBALS['db']->setQuery("
	CREATE TABLE IF NOT EXISTS `jos_rec_rate` (
	  `rec_id` bigint(20) NOT NULL default '0',
	  `rate` int(11) unsigned NOT NULL default '0',
	  KEY `rec_id_i` (`rec_id`)
	) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	");
	$GLOBALS['db']->query();	

//*********************ОБНОВЛЕНИЕ СТРУКТУРЫ ТАБЛИЦ*************************************


//*********************КОРРЕКТИРОВКА ЛИСТА ОБНОВЛЕНИЙ(авторизацию обновляем)*************************************

$no_update_list_additions=array(
'jzko.mnu',
'JwU_H.pft',
'JWP.pft',
'Jwh_vlp.pft',
'Jwh_vl.pft',
'jw_vlp1.pft',
'jw_vlp0.pft',
'jw_H.pft',
'jw.pft',
'jFO_ELkko.MNU',
'jNAZNAC.MNU',
'{(?:deposit/[^j].+?\.)|(?:deposit/j.+?\.(?!mnu|pft))}si',
'distr_downloads_log.txt',
'send_distrib.txt',
'update_log.txt',
'jirbis_send_distrib.cmd',
'ji_distrib.php',
'get_distrib.php',
'jirbis_send_distrib.php',
'json_rpc_server_test.php',
'json_rpc_client.php',
'yaz_test.cmd',
'text_results.txt',
'jirbis_update_forced_public.cmd',
'jirbis_update_forced.cmd',
'!titles_test.php',
'!tables_ajax.php',
'!show_tables.php',
'!jstree_test.php',
);
$no_update_list=ji_update::get_list_file(JI_PATH_COMPONENT_LOCAL.'/'.JI_FILE_NO_UPDATE_FILES_LIST);

$no_update_list=array_merge($no_update_list,$no_update_list_additions);

$no_update_list=array_diff($no_update_list,array(
'/search_forms/',
'/deposit/',
'/irbis_auth/',
'/irbis_logout/',
'/components/',
'conv_knigafund.php',
'conv_base.php',
'template.htm',
'ji_upload.php',
'iz39.php',
'ji_php_user_rec_correction.php',
'jirbis_defaults.php',
'{(/[^j]{1}.\w+?\.(:?mnu|pft))}si'
));



if (!file_put_contents(JI_PATH_COMPONENT_LOCAL.'/'.JI_FILE_NO_UPDATE_FILES_LIST,implode("\r\n",$no_update_list)))
		throw  new Exception('Не удалось выполнить запись файла JI_PATH_COMPONENT_LOCAL.'/'.JI_FILE_NO_UPDATE_FILES_LIST');

//\*********************КОРРЕКТИРОВКА ЛИСТА ОБНОВЛЕНИЙ(авторизацию обновляем)*************************************

//*********************УДАЛЕНИЕ КЭША*************************************

//ji_update::clean_all_sessions();

//\*********************УДАЛЕНИЕ КЭША*************************************

}catch (Exception $e){
	if (empty($errors)){ 	
		$errors=array();
		
	}
	$errors[]='Ошибка при выполнении скрипта обновления '.$final_path.": {$e->getMessage()} ({$e->getCode()})";
}


// table_modification
function tm($table_name,$action_text){

	$commands=preg_split('{[\,\; \t]*[\n\r]+}',$action_text);	
	if (!is_array($commands)) 
		throw  new Exception('Ошибка при попытке выделения команд в тексте: '.$action_text);
		
	
	foreach ($commands as $command){
		if (!($command=trim($command))) 
			continue;
		$command='ALTER TABLE `#__'.$table_name.'` '.$command.';';
		$GLOBALS['db']->setQuery($command);
		if (!$GLOBALS['db']->query())
			$GLOBALS['errors'][]='Не удалось выполнить запрос: '.$command;	
	}
	
}

function get_db(){
	
	
	if (file_exists(JI_PATH_INCLUDES_LOCAL.'/idb.php')){
		if (!class_exists('idb'))
			include_once(JI_PATH_INCLUDES_LOCAL.'/idb.php');
		$GLOBALS['db']=idb::i();
	}elseif (isset($GLOBALS['db']) &&  class_exists('database'))
		$GLOBALS['db']=$GLOBALS['db'];
	else
		throw  new Exception('Не удаось выполнить подключение к БД');
	// Никакие сообщения об ошибках не нужны
		$GLOBALS['db']->_debug=false;
	return $GLOBALS['db'];
}


function update_cfg_array(&$cfg_new,&$cfg_old){
	foreach ($cfg_new as $key=>&$value){
		// Если это ассоциативный массив, то выполняем рекурсию. Если нет (если это не ассоциативный массив, например), то просто берём старые значения. 
		if (is_array($value) && isset($cfg_old[$key]) &&  is_assoc_array($value) ){
		//В обычном случае берётся новая версия массива и в неё добавляются старые значения. Но если состав форматов пользователя уже, чем новый, то никакого обновления не происходит
		
		// Если присутствует весь стандартный перечень форматов, 
		// но состав связок не стандартный, то мы просто используем старый набор парамеров.
			if (
				($key==='presentation_profile' 
				&& 
				array_diff(array('document_form','place_code','access_points','udk_bbk','keywords','rubrics','annotation','contents','ko'),array_keys($cfg_old[$key]))
				)
				|| 
				($key==='formats_profile' 
				&&
				array_diff(array('document_form','place_code','access_points','udk_bbk','keywords','rubrics','annotation','contents','ko'),array_keys($cfg_old[$key]))
				)
			){
				$value=$cfg_old[$key];				
			}else {
				update_cfg_array($value,$cfg_old[$key]);	
			}
			// Если это не массив, то мы имеем право устанавливать даже FALSE значение!
		}elseif(isset($cfg_old[$key])){ 
			/*if ($key==='print_titles_enable'){
			echo '';
			}*/
			$value=$cfg_old[$key];	
		}	
	}	
	return $cfg_new;
}

function cfg_write($filename,$CFG,$array_name='CFG'){
	if (!($file=@fopen($filename,'w+'))) 
	return false;
	//if (!var_export($CFG))  'Пустой файл CFG';
	fwrite($file,"<?php \n \$$array_name=\$GLOBALS['$array_name']=".var_export($CFG,true).";\n ?>\n");
	fclose($file);
	return true;
}


function is_assoc_array($array){
	return isset($array[0]) ? false :true;
}



?>
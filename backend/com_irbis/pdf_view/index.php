<?php

if (file_exists('../jirbis_link_libraries.php'))
include_once('../jirbis_link_libraries.php');

require_once(JI_PATH_PDF_VIEW_LOCAL."/lib/config.php");
require_once(JI_PATH_PDF_VIEW_LOCAL."/lib/common.php");

$configManager = new Config();

?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru" xml:lang="ru">
    <head>
        <title>Постраничный просмотр PDF файлов</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/flexpaper.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/flexpaper.js"></script>
		<script type="text/javascript" src="js/flexpaper_handlers.js"></script>
    </head>
    <body>
    

			<div id="documentViewer" class="flexpaper_viewer"></div>

			<?php
			//die();
			$ed=new ji_ed(true);		
			$fa=$ed->get_cached_file_data();
			//print_r($fa);
			
			if (!is_array($fa)|| !$fa){
				die('Некорректный вызов просмотрщика. Возможно, истёк срок действия сессии. Возможно, предпринята попытка просмотра файла, открытого, не из J-ИРБИС 2.0');
			}
	       	
			$doc = str_replace(array('.pdf','.PDF'),'',$fa['file_name']);
			$pdfFilePath = $configManager->getConfig('path.pdf');
			$full_file_path=$pdfFilePath . $doc . ".pdf";
			
			if (!file_exists($full_file_path)){
				die('Файл документа не доступен. Возможно, не было выполнено копирование во временную директорию');
			}
			
			?>
	        <div id='txt_eventlog'></div>
			 <!--style='<?php  $_REQUEST['debug_reporting'] ? "display:" : "display:none" ?>'-->
			<script type="text/javascript">
	        
		        function getDocumentUrl(document){
		        	var numPages 			= <?php echo getTotalPages($full_file_path) ?>;
					var url = "{services/view.php?doc={doc}&format={format}&page=[*,0],{numPages}}";
						url = url.replace("{doc}",document);
						url = url.replace("{numPages}",numPages);
						return url;
		        }

				var searchServiceUrl	= escape('services/containstext.php?doc=<?php echo $doc ?>&page=[page]&searchterm=[searchterm]');
				$('#documentViewer').FlexPaperViewer(
				  { config : {

						 DOC : escape(getDocumentUrl("<?php echo $doc ?>")),
						 Scale : 0.6,
						 ZoomTransition : 'easeOut',
						 ZoomTime : 0.5,
						 ZoomInterval : 0.2,
						 FitPageOnLoad : false,
						 FitWidthOnLoad : true,
						 FullScreenAsMaxWindow : false,
						 ProgressiveLoading : true,
						 MinZoomSize : 0.2,
						 MaxZoomSize : 5,
						 SearchMatchAll : false,
  						 SearchServiceUrl : searchServiceUrl,
						 InitViewMode : 'Portrait',
						 RenderingOrder : '<?php echo ($configManager->getConfig('renderingorder.primary') . ',' . $configManager->getConfig('renderingorder.secondary')) ?>',

						 ViewModeToolsVisible : true,
						 ZoomToolsVisible : true,
						 NavToolsVisible : true,
						 CursorToolsVisible : true,
						 SearchToolsVisible : true,
  						 key : '<?php echo $configManager->getConfig('licensekey') ?>',

  						 DocSizeQueryService : 'services/swfsize.php?doc=<?php echo $doc ?>',
						 jsDirectory : 'js/',
						 localeDirectory : 'locale/',
						 JSONDataType : 'jsonp',

  						 localeChain: 'en_US'
						 }}
				);
	        </script>

   </body>
</html>
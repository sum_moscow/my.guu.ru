<?php
/**
* █▒▓▒░ The FlexPaper Project 
* 
* Copyright (c) 2009 - 2011 Devaldi Ltd
*
* Commercial licenses are available. The commercial player version
* does not require any FlexPaper notices or texts and also provides
* some additional features.
* When purchasing a commercial license, its terms substitute this license.
* Please see http://flexpaper.devaldi.com/ for further details.
* 
*/

require_once("config.php");
require_once("common.php");

class pdf2swf
{
	private $configManager = null;

	/**
	* Constructor
	*/
	function __construct()
	{
		$this->configManager = new Config();
	}

	/**
	* Destructor
	*/
	function __destruct() {
        //echo "pdf2swf destructed\n";
    	}

	/**
	* Method:convert
	*/
	public function convert($doc,$page)
	{
		$output=array();
		$pdfFilePath = $this->configManager->getConfig('path.pdf') . $doc;
		$swfFilePath = $this->configManager->getConfig('path.swf') . $doc  . $page. ".swf";
		
		if($this->configManager->getConfig('splitmode'))
			$command = $this->configManager->getConfig('cmd.conversion.splitpages');
		else
			$command = $this->configManager->getConfig('cmd.conversion.singledoc');
			
		$command = str_replace("{path.pdf}",$this->configManager->getConfig('path.pdf'),$command);
		$command = str_replace("{path.swf}",$this->configManager->getConfig('path.swf'),$command);
		$command = str_replace("{pdffile}",$doc,$command);
		
		try {
			if (!$this->isNotConverted($pdfFilePath,$swfFilePath)) {
				array_push ($output, utf8_encode("[Converted]"));
				return arrayToString($output);
			}
		} catch (Exception $ex) {
			array_push ($output, "Error," . utf8_encode($ex->getMessage()));
			return arrayToString($output);
		}

		$return_var=0;
		
		if($this->configManager->getConfig('splitmode')){
			$pagecmd = str_replace("%",$page,$command);
			$pagecmd = $pagecmd . " -p " . $page;

			exec($pagecmd,$output,$return_var);
			
/*
SOKOL TODO Абсолютно бредовый фрагмент -- если команда ещё не выполнялась согласно сессии, выполняем её независимо от splitpages
$hash = getStringHashCode($command);
if(!isset($_SESSION['CONVERSION_' . $hash])){
                exec(getForkCommandStart() . $command . getForkCommandEnd());
                $_SESSION['CONVERSION_' . $hash] = true;
            }*/
		}else
			exec($command,$output,$return_var);
			
		if($return_var==0 || strstr(strtolower($return_var),"notice")){
			$s="[Converted]";
		}else{
			$s="Ошибка преобразования. Убедитесь, что есть право на запись в директорию и что для запароленых документов установлен пароль." ;
		}
		return $s;
	}

	/**
	* Method:isConverted
	*/
	public function isNotConverted($pdfFilePath,$swfFilePath)
	{
		if (!file_exists($pdfFilePath)) {
			throw new Exception("Document does not exist");
		}
		if ($swfFilePath==null) {
			throw new Exception("Document output file name not set");
		} else {
			if (!file_exists($swfFilePath)) {
				return true;
			} else {
				if (filemtime($pdfFilePath)>filemtime($swfFilePath)) return true;
			}
		}
		return false;
	}
}
?>
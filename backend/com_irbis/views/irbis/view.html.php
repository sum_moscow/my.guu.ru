<?php
/**
 * @version		$Id: view.html.php 20523 2011-02-03 01:26:20Z dextercowley $
 * @package		Joomla.Site
 * @subpackage	com_irbis
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * @package		Joomla.Site
 * @subpackage	irbis
 */
class irbisViewirbis extends JView
{
	public function display($tpl = null)
	{
		$app		= JFactory::getApplication();
		$document	= JFactory::getDocument();

		$menus	= $app->getMenu();
		$menu	= $menus->getActive();

		$params = $app->getParams();

		// because the application sets a default page title, we need to get it
		// right from the menu item itself
		$title = $params->get('page_title', '');
		if (empty($title)) {
			$title = $app->getCfg('sitename');
		}
		elseif ($app->getCfg('sitename_pagetitles', 0)) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		$this->document->setTitle($title);
/*
		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords')) 
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}*/

		$irbis = new stdClass();
		// auto height control
		if ($params->def('height_auto')) {
			$irbis->load = 'onload="iFrameHeight()"';
		} else {
			$irbis->load = '';
		}

		$irbis->url = $params->def('url', '');
		$irbis->width = $params->def('width', '');
		$irbis->height = $params->def('height', '');


		//Escape strings for HTML output
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

		$this->assignRef('params',	$params);
		$this->assignRef('irbis', $irbis);

		parent::display($tpl);
	}
}

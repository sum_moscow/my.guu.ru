<?php
/**
 * @version		$Id: default.php 20196 2011-01-09 02:40:25Z ian $
 * @package		Joomla.Site
 * @subpackage	com_irbis
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;


 
//ini_set('zlib.output_compression','On'); 
//ob_start("ob_gzhandler");
setlocale(LC_ALL, 'ru_RU.UTF-8'); 
//header('Content-Type: text/html; charset=utf-8');	



//$base_path = "C:/xampp/htdocs/jirbis2/components/com_irbis";
$base_path = JPATH_BASE."/components/com_irbis";


	//require_once( JI_PATH_INCLUDES_LOCAL.'/ji_modes_controller.php' );
	
	
	require_once( $base_path.'/jirbis_link_libraries.php' );
	

	//require_once( JI_PATH_INCLUDES_LOCAL.'/ji_personal.php' );

	$mode=($GLOBALS['CFG']['installed']) ? $this->irbis->height : 'install';
	//$params_text='',$search_mode='',$mode=''
	ji_modes_controller::run($this->irbis->width,$this->irbis->url,$mode);
?>

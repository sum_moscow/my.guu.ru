<?php

require '../bin/db.php';

	$__db->exec('DELETE  FROM classrooms');

$building_types = array(
	'АК' => 'А',
	'БЦ' => 'БЦ',
	'ГУК' => 'У',
	'ЛК' => 'ЛК',
	'ПА' => 'ПА'
);

$p_ins_classroom = $__db->prepare('INSERT INTO classrooms(building_type, room_number,
	room_type, fact_room_type, room_area, allow_student_count)
		VALUES (:building_type, :room_number,
	:room_type, :fact_room_type, :room_area, :allow_student_count)');

$file = fopen("rooms.csv","r");
while(! feof($file)) {
	$a = fgetcsv($file, 0, '|');

	if (!isset($a[1]) || !isset($a[2]) || !isset($a[3])){
		continue;
	}

	$p_ins_classroom->execute(array(
		':building_type' => $building_types[$a[0]],
		':room_number' => $a[2],
		':room_type' => $a[3],
		':fact_room_type' => $a[5],
		':room_area' => str_replace(',', '.', $a[4]),
		':allow_student_count' => $a[8],
	));
}
fclose($file);


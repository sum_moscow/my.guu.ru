<?php
/**
 * Created by PhpStorm.
 * User: Инал
 * Date: 20.10.2014
 * Time: 20:57
 */

require_once $ROOT_PATH . '/backend/core/timetable/Class.Timetable.php';
require_once $ROOT_PATH . '/backend/brs/Class.BRSProject.php';
require_once $ROOT_PATH . '/backend/student_groups/Class.StudentGroup.php';

class BRSMain extends AbstractModule{

	const MIN_LEVEL = 1;
	const CHANGE_FORM_LEVEL = 2;
	const MAX_SUM_RATING = 13; //По положению о БРС сумма баллов за посемаещмость и активность
	const MAX_EXAM_RATING = 40;


	private $db;
	private $user;
	private $edu_group_id;
	private $subject_id;

	private $project;

	public function __construct(PDO $db, User $__user){
		$this->db = $db;
		$this->user = $__user;
	}

	public function setEduGroupId($edu_group_id){
		$this->edu_group_id = $edu_group_id;
		return $this;
	}

	public function setSubjectId($subject_id){
		$this->subject_id = $subject_id;
		return $this;
	}

	public function selectForm($edu_group_id = null, $subject_id = null){
		$edu_group_id = $edu_group_id == null ? $this->edu_group_id : $edu_group_id;
		$subject_id = $subject_id == null ? $this->subject_id : $subject_id;
		$has_rights = $this->user->hasRights(
				array(
					'module' => self::getModuleName(),
					'min_level' => self::CHANGE_FORM_LEVEL
				)
			)
			|| $this->professorCanAccess($edu_group_id, $subject_id)
			|| $this->user->hasRights(
				array(
					'module' => 'institute',
					'min_level' => 6
				)
				);
		if (!$has_rights) throw new \PrivilegesException('У вас недостаточно прав', $this->db);

		//$student_group = new StudentGroup($edu_group_id, $this->db);
		//$subject = new Subject($subject_id, $this->db);

		$this->edu_group_id = $edu_group_id;
		$this->subject_id = $subject_id;
		return $this;
	}

	private function professorCanAccess($edu_group_id, $subject_id){
		$q_get_pr = 'SELECT view_timetable.professor_id
			FROM view_timetable
			WHERE view_timetable.edu_group_id = :edu_group_id
			AND view_timetable.timetable_subject_id = :subject_id
			AND professor_id = :professor_id
			AND sync_id = (SELECT MAX(id)FROM sync_timetable sync_timetable LIMIT 1)';

		$p_get_pr = $this->db->prepare($q_get_pr);
		$p_get_pr->execute(array(
			':edu_group_id' => $edu_group_id,
			':subject_id' => $subject_id,
			':professor_id' => $this->user->getId()
		));
		if ($p_get_pr === FALSE) throw new DBQueryException('QUERY_EXCEPTION', $this->db);
		return ($p_get_pr->rowCount() > 0 && $this->user->isStaff());
	}

	private function getClassesInfo(){
		$q_classes = 'SELECT brs_classes.id as class_id, date, number
			FROM brs_classes
			INNER JOIN timetable_subjects ON timetable_subjects.id =  brs_classes.timetable_subject_id
			INNER JOIN view_edu_groups ON view_edu_groups.edu_group_id =  timetable_subjects.edu_group_id
			WHERE brs_classes.timetable_subject_id = :subject_id
			AND timetable_subjects.edu_group_id = :student_group_id
			AND semester_id = :semester_id
			ORDER BY number';
		$p_classes = $this->db->prepare($q_classes);
		$p_classes->execute(array(
			':student_group_id' => $this->edu_group_id,
			':semester_id' => SUM::getCurrentSemesterId($this->db),
			':subject_id' => $this->subject_id
		));
		return new Result(true, '', $p_classes->fetchAll());
	}

	public function getFormData(){
		$student_group = new StudentGroup($this->edu_group_id, $this->db);
		$timetable = new Timetable($student_group, $this->db);
		$result_array = $timetable->get()->getData();
		$result_array['students'] = $student_group->getStudentsList()->getData();
		$result_array['classes_info'] = $this->getClassesInfo()->getData();
		$result_array['class_info'] = $this->getTimetableSubjectInfo();
		$result_array['max_ratings'] = $this->getMaxRatings();

		return new Result(true, 'Данные успешно получены', $result_array);
	}

	private function getTimetableSubjectInfo(){
		if ($this->subject_id == null) return null;
		$q_get_info = 'SELECT subjects.name as subject_name, subject_id,
 			academic_departments.name as academic_department_name,
 			academic_departments.id as academic_department_id,
 			since, till
			FROM timetable_subjects
			INNER JOIN subjects ON subjects.id = timetable_subjects.subject_id
			INNER JOIN academic_departments ON academic_departments.id = subjects.academic_department_id
			WHERE timetable_subjects.id = :id';
		$p_get_info = $this->db->prepare($q_get_info);
		$p_get_info->execute(array(':id' => $this->subject_id));
		return $p_get_info->fetch();
	}

	private function getProjectFeatures(){
		try{
			return $this->getProjectInstance()->getFeatures();
		}catch(Exception $e){
			return array();
		}
	}

	public function getProjectInstance(){
		if ($this->project instanceof BRSProject
				&& $this->project->getTimetableSubjectId() == $this->subject_id){
			return $this->project;
		}else{
			$this->project = new BRSProject($this->subject_id, $this->db);
			return $this->project;
		}
	}

	public function getProjectMarks(){
		try{
			return $this->getProjectInstance()->getMarks()->getData();
		}catch(Exception $e){
			return array();
		}
	}

	public function getProject(){
		$students_group = new StudentGroup($this->edu_group_id, $this->db);
		return new Result(true, 'Данные успешно получены', array(
				'project' => array_merge(
					$this->getProjectStatus()->getData(),
					$this->getProjectFeatures()
				),
				'students' => $students_group->getStudentsList()->getData(),
				'marks' => $this->getProjectMarks(),
				'themes' => $this->getProjectThemes()
			)
		);
	}

	public function updateProjectFeatures($features, $has_project){
		$project_status = $this->setProjectStatus($has_project);
		if ($project_status){
			return $this->getProjectInstance()->updateFeatures($features);
		}
	}

	public function getControlEvents(){
		$q_events = 'SELECT *
			FROM brs_control_events
			WHERE
			timetable_subject_id = :subject_id
			AND professor_id = :professor_id
			AND brs_control_events.status = 1
		';
		$p_events = $this->db->prepare($q_events);
		$p_events->execute(array(
			':professor_id' => $this->user->getStaffInstance()->getStaffId(),
			':subject_id' => $this->subject_id,
		));
		if ($p_events === FALSE) throw new DBQueryException('', $this->db);


		$result = array(
			'events' => $p_events->fetchAll(),
			'event_types' => $this->getControlEventTypes()->getData()
		);

		return new Result(true, '', $result);
	}

	public function updateControlEvents(array $events){
		$this->db->beginTransaction();
			try {
				$q_ins_event = 'INSERT INTO brs_control_events(id, control_type_id, professor_id, timetable_subject_id,
						description, event_date, active_till, max_rating, created_at, status)
					VALUES(:id, :control_type_id, :professor_id, :timetable_subject_id, :description,
						:event_date, :active_till, :max_rating, NOW(), 1)
					ON DUPLICATE KEY UPDATE
					control_type_id = :control_type_id,
					professor_id = :professor_id,
					timetable_subject_id = :timetable_subject_id,
					description = :description,
					event_date = :event_date,
					active_till = :active_till,
					max_rating = :max_rating,
					status = :status
					';
				$p_ins_event = $this->db->prepare($q_ins_event);

				$q_set_rating_to_max = 'UPDATE brs_control_marks
 					INNER JOIN brs_control_events ON brs_control_events.id = brs_control_marks.control_event_id
 					INNER JOIN timetable_subjects ON timetable_subjects.id = brs_control_events.timetable_subject_id
					SET brs_control_marks.mark = :max
					WHERE brs_control_marks.mark > :max
					AND brs_control_events.id = :control_event_id';
				$p_set_rating_to_max = $this->db->prepare($q_set_rating_to_max);

				foreach ($events as $event){

					$max_point = intval($event['max_rating']);
					if ($max_point < 0) $max_point = 0;
					$p_ins_event->execute(array(
						':id' => $event['control_event_id'] ? $event['control_event_id'] : null,
						':timetable_subject_id' => $this->subject_id,
						':control_type_id' => $event['control_event_type'],
						':subject_id' => $this->subject_id,
						':professor_id' => $this->user->getStaffInstance()->getStaffId(),
						':description' => $event['description'],
						':event_date' => $event['event_date'],
						':max_rating' => $event['max_rating'],
						':active_till' => $event['active_till'],
						':status' => isset($event['to_remove']) && $event['to_remove'] == '1' ? 0 : 1
					));

					if (isset($event['control_event_id']) && is_numeric($event['control_event_id'])){
						$p_set_rating_to_max->execute(array(
							':max' => $max_point,
							':control_event_id' => $event['control_event_id']
						));
					}

					if ($p_ins_event === FALSE) throw new DBQueryException('', $this->db);
				}
			}catch (Exception $e){
				$this->db->rollBack();
				throw $e;
			}
		$this->db->commit();
		return new Result(true, 'Данные успешно обновлены');
	}

	public function getControlEventTypes(){
		$q_get_types = 'SELECT
			* FROM brs_types_of_control
			ORDER BY brs_types_of_control.subject_types';
		$p_types = $this->db->prepare($q_get_types);
		$p_types->execute();
		if ($p_types === FALSE) throw new DBQueryException('', $this->db);
		return new Result(true, '', $p_types->fetchAll());
	}

	public function getProjectStatus(){
		try{
			$prj = new BRSProject($this->subject_id, $this->db);
			return new Result(true, '', array('exists' => true));
		}catch (Exception $e){
			return new Result(true, '', array('exists' => false));
		}
	}

	public function setProjectStatus($status){
		$status = ($status == 1 || $status == 'true') ? 1 : 0;
		$q_set_project_status = 'UPDATE timetable_subjects SET has_project = :has_project';
		$p_set_prj_status = $this->db->prepare($q_set_project_status);
		$p_set_prj_status->execute(array(':has_project' => $status));
		if ($p_set_prj_status === FALSE) throw new DBQueryException('PROJECT_UPD_ERROR', $this->db);
		return true;
	}

	public function updateAttendance($students, $classes){
		global $__user;
		$q_upd = 'INSERT INTO brs_attendance(student_id, class_id, mark, created_at)
			VALUES (:student_id, :class_id, :mark, NOW())
			ON DUPLICATE KEY UPDATE
			mark = :mark,
			id = LAST_INSERT_ID(id)';


		$q_upd_class = 'INSERT INTO brs_classes(date, timetable_subject_id, number, professor_id, created_at)
			VALUES (:date, :timetable_subject_id, :number, :professor_id, NOW())
			ON DUPLICATE KEY UPDATE
			date = :date,
			id = LAST_INSERT_ID(id)
			';

		$p_upd = $this->db->prepare($q_upd);
		$p_upd_class = $this->db->prepare($q_upd_class);


		$result_classes = array();
		foreach($classes as $class){
			$p_upd_class->execute(array(
				':date' => $class['date'] == '' ? null : $class['date'],
				':timetable_subject_id' => $this->subject_id,
				':number' => $class['number'],
				':professor_id' => $__user->getStaffInstance()->getId()
			));
			$class['id'] = $this->db->lastInsertId();
			$result_classes[$class['number']] = $class;
		}
		$classes = $result_classes;

		foreach($students as $student){
			$student_id = $student['student_id'];
			foreach($student['marks'] as $index => $mark){
				$_mark = (is_numeric($mark)) ? floatval($mark) : null;
				$p_upd->execute(array(
					':student_id' => $student_id,
					':class_id' => $classes[($index + 1)]['id'],
					':mark' => $_mark
				));
			}
		}
		return new Result(true, 'Данные успешно обновлены');
	}

	public function updateExam(array $marks, $exam_form_id){

		$q_upd_exam_form = 'UPDATE timetable_subjects
			SET exam_form_id = :exam_form_id
			WHERE id = :subject_id';
		$p_upd = $this->db->prepare($q_upd_exam_form);
		$p_upd->execute(array(
			':exam_form_id' => $exam_form_id,
			':subject_id' => $this->subject_id
		));


		$q_upd_exam = 'INSERT INTO brs_exams(student_id, mark, professor_id, timetable_subject_id, created_at)
			VALUES(:student_id, :mark, :professor_id, :timetable_subject_id, NOW())
			ON DUPLICATE KEY UPDATE
			mark = :mark,
			professor_id = :professor_id';
		$p_upd_exam = $this->db->prepare($q_upd_exam);
		$professor_id = $this->user->getStaffInstance()->getStaffId();
		foreach($marks as $mark){
			if ($mark['mark'] > self::MAX_EXAM_RATING){
				$mark['mark'] = self::MAX_EXAM_RATING;
			}
			$p_upd_exam->execute(array(
				':student_id' => $mark['student_id'],
				':mark' => $mark['mark'],
				':timetable_subject_id' => $this->subject_id,
				':professor_id' => $professor_id,
			));
		}
		return new Result(true, '');
	}

	public function getAttendance(){
		$q_attendance = 'SELECT brs_attendance.*, brs_classes.number as class_number
			FROM brs_attendance
			INNER JOIN brs_classes ON brs_classes.id = brs_attendance.class_id
			INNER JOIN view_users ON view_users.student_id = brs_attendance.student_id
			INNER JOIN timetable_subjects ON timetable_subjects.id = brs_classes.timetable_subject_id
			WHERE  brs_classes.timetable_subject_id = :subject_id
			AND timetable_subjects.semester_id = :semester_id
			AND view_users.edu_group_id = :student_group_id
			AND mark IS NOT NULL';

		$args = array(
			':subject_id' => $this->subject_id,
			':student_group_id' => $this->edu_group_id,
			':semester_id' => SUM::getCurrentSemesterId($this->db)
		);

		$p_attendance = $this->db->prepare($q_attendance);
		$p_attendance->execute($args);

		$attendance = $p_attendance->fetchAll();
		return new Result(true, '',$attendance);
	}

	public function getProgress(){
		$q_events = 'SELECT brs_control_events.*,  brs_types_of_control.*, brs_control_events.id as control_event_id
			FROM brs_control_events
			INNER JOIN brs_types_of_control ON brs_types_of_control.id = brs_control_events.control_type_id
			WHERE timetable_subject_id = :subject_id
			AND professor_id = :professor_id
			AND brs_control_events.status = 1';

		$students_in_group = new StudentGroup($this->edu_group_id, $this->db);

		$q_marks = 'SELECT brs_control_marks.*
			FROM brs_control_marks
			INNER JOIN brs_control_events ON brs_control_events.id = brs_control_marks.control_event_id
			WHERE
			brs_control_events.timetable_subject_id = :subject_id
			AND brs_control_events.professor_id = :professor_id
			AND brs_control_marks.mark IS NOT NULL';



		$p_events = $this->db->prepare($q_events);
		$p_events->execute(array(
			':subject_id' => $this->subject_id,
			':professor_id' => $this->user->getStaffInstance()->getStaffId()
		));

		$p_marks = $this->db->prepare($q_marks);
		$p_marks->execute(array(
			':subject_id' => $this->subject_id,
			':professor_id' => $this->user->getStaffInstance()->getStaffId()
		));

		return new Result(true, '', array(
			'events' => $p_events->fetchAll(),
			'students' => $students_in_group->getStudentsList()->getData(),
			'marks' => $p_marks->fetchAll(),
		));

	}

	public function updateProgress(array $marks){

		$q_upd_mark = 'INSERT INTO brs_control_marks
			(student_id, control_event_id, mark, professor_id, created_at)
			VALUES (:student_id, :control_event_id, :mark, :professor_id, NOW())
			ON DUPLICATE KEY UPDATE
			mark = :mark';

		$p_marks = $this->db->prepare($q_upd_mark);

		foreach ($marks as $mark){
			if ($mark['mark'] == 'NaN'){
				$mark['mark'] = null;
			}
			$p_marks->execute(array(
				':student_id' => $mark['student_id'],
				':control_event_id' => $mark['control_event_id'],
				':mark' => $mark['mark'],
				':professor_id' => $this->user->getStaffInstance()->getStaffId(),
			));
		}

		return new Result(true, 'Данные успешно обновлены');
	}

	public function updateSettings(array $req){
		//if (!isset($req['events']) && !isset($req['features'])) throw new InvalidArgumentException('Не указаны поля для обновления');
		$result = true;
		if (isset($req['events'])){
			$result = $this->updateControlEvents($req['events']);
		}
		if (isset($req['features'])){
			$result = $result && $this->updateProjectFeatures($req['features'], $req['has_project']);
		}
		if (isset($req['for_activity']) || isset($req['for_attendance'])){
			$result = $result && $this->updateMaxRatings($req['for_activity'], $req['for_attendance']);
		}
		if (isset($req['has_project'])){
			$result = $result && $this->setProjectStatus($req['has_project']);
		}
		return new Result($result, 'Данные обновлены');
	}

	public function getExam(){

		$st_group = new StudentGroup($this->edu_group_id, $this->db);

		$q_ratings_max = '
			SELECT activity_max, attendance_max, COUNT(brs_classes.id) as classes_count,
			exam_form_id
			FROM timetable_subjects
			INNER JOIN brs_classes ON timetable_subjects.id = brs_classes.timetable_subject_id
			WHERE timetable_subjects.id = :timetable_subject_id
		';
		$p_ratings_max = $this->db->prepare($q_ratings_max);
		$p_ratings_max->execute(array(':timetable_subject_id' => $this->subject_id));
		$max_ratings = $p_ratings_max->fetch();

		$q_semester_rating = '
			SELECT SUM(mark) as progress_marks_sum, brs_control_marks.student_id
			FROM brs_control_marks
			INNER JOIN brs_control_events ON brs_control_events.id = brs_control_marks.control_event_id
			WHERE brs_control_events.status = 1
			AND brs_control_marks.student_id IN (SELECT student_id FROM edu_groups WHERE edu_groups.id = :student_group_id)
			AND brs_control_events.timetable_subject_id = :timetable_subject_id
			GROUP BY student_id
		';

		$p_semester_rating = $this->db->prepare($q_semester_rating);
		$p_semester_rating->execute(array(
			':student_group_id' => $this->edu_group_id,
			':timetable_subject_id' => $this->subject_id
		));
		$progress_rows = $p_semester_rating->fetchAll();

		$q_marks = 'SELECT mark, student_id, users.id
			FROM brs_attendance
			INNER JOIN brs_classes ON brs_classes.id = brs_attendance.class_id
			INNER JOIN students ON students.id = brs_attendance.student_id
			INNER JOIN users ON users.id = students.user_id
			WHERE brs_attendance.student_id IN (SELECT student_id FROM edu_groups WHERE edu_groups.id = :student_group_id)
			AND brs_classes.timetable_subject_id = :timetable_subject_id';

		$p_marks = $this->db->prepare($q_marks);
		$p_marks->execute(array(
			':student_group_id' => $this->edu_group_id,
			':timetable_subject_id' => $this->subject_id
		));

		$q_exam_marks = 'SELECT mark, student_id, users.id, IFNULL(brs_exams.updated_at, brs_exams.created_at) as date_of_change
			FROM brs_exams
			INNER JOIN students ON students.id = brs_exams.student_id
			INNER JOIN users ON users.id = students.user_id
			WHERE students.id IN (SELECT student_id FROM edu_groups WHERE edu_groups.id = :student_group_id)
			AND brs_exams.timetable_subject_id = :timetable_subject_id';

		$p_exam_marks = $this->db->prepare($q_exam_marks);
		$p_exam_marks->execute(array(
			':student_group_id' => $this->edu_group_id,
			':timetable_subject_id' => $this->subject_id
		));

		$row_exam_marks = $p_exam_marks->fetchAll();

		$row_marks = $p_marks->fetchAll();


		$students = array();
		foreach($row_marks as $mark){
			if (!isset($students[$mark['student_id']])){
				$students[$mark['student_id']] = array(
					'attendance' => 0,
					'progress' => 0,
					'date_of_change' => null,
					'activity' => 0,
					'in_class_count' => 0,
					'out_class_count' => 0,
					'exam' => null,
					'student_id' => $mark['student_id'],
					'id' => $mark['id']
				);
			}
			$students[$mark['student_id']]['activity'] += floatval($mark['mark']);

			if ($mark['mark'] === '0'){
				$students[$mark['student_id']]['out_class_count']++;
			}else{
				$students[$mark['student_id']]['in_class_count']++;
			}
		}

		foreach($progress_rows as $progress){
			if (!isset($students[$progress['student_id']])) continue;
			$students[$progress['student_id']]['progress'] = $progress['progress_marks_sum'] == null ? '0.0' : number_format(floatval($progress['progress_marks_sum']),1);
			$att = ($max_ratings['attendance_max'] / $max_ratings['classes_count'])
																	* ($max_ratings['classes_count'] - $students[$progress['student_id']]['out_class_count']);
			$students[$progress['student_id']]['attendance'] = $att > floatval($max_ratings['attendance_max']) ? $max_ratings['attendance_max'] : number_format($att, 1);

			if ($students[$progress['student_id']]['activity'] > floatval($max_ratings['activity_max'])){
				$students[$progress['student_id']]['activity'] = $max_ratings['activity_max'];
			}
			$students[$progress['student_id']]['activity'] = number_format($students[$progress['student_id']]['activity'], 1);
		}

		foreach($row_exam_marks as $exam_mark){
			if (!isset($students[$exam_mark['student_id']])) continue;
			$students[$exam_mark['student_id']]['exam'] = $exam_mark['mark'];
			$students[$exam_mark['student_id']]['date_of_change'] = $exam_mark['date_of_change'];
		}

		return new Result(true, null, array(
			'students' => $st_group->getStudentsList()->getData(),
			'marks' => $students,
			'exam_form_id' => $max_ratings['exam_form_id']
		));
	}

	public function getSettings() {
		try{
			$project = array_merge($this->getProjectStatus()->getData(),
				$this->getProjectFeatures());
		}catch(Exception $e){
			$project = array(
				'exists' => 0
			);
		}
		return new Result(true, '', array(
			'control_events' => $this->getControlEvents()->getData(),
			'project' => $project,
			'max_ratings' => $this->getMaxRatings()
		));
	}

	private function updateMaxRatings($for_activity, $for_attendance) {
		$for_activity = intval($for_activity);
		$for_attendance = intval($for_attendance);
		if (($for_attendance + $for_activity) > self::MAX_SUM_RATING){
			$for_activity = self::MAX_SUM_RATING - $for_attendance;
		}
		$q_set_max = 'UPDATE timetable_subjects SET
			activity_max = :activity_max,
			attendance_max = :attendance_max
			WHERE timetable_subjects.id = :timetable_subject_id';
		$p_set_max = $this->db->prepare($q_set_max);
		$p_set_max->execute(array(
			':activity_max' => $for_activity,
			':attendance_max' => $for_attendance,
			':timetable_subject_id' => $this->subject_id,
		));
		if ($p_set_max === FALSE) throw new DBQueryException('RATING UPDATE FAIL', $this->db);
		return new Result(true, '');
	}

	private function getMaxRatings() {
		$q_get_ratings = 'SELECT activity_max, attendance_max
			FROM timetable_subjects
			WHERE timetable_subjects.id = :timetable_subject_id';
		$p_get_ratings = $this->db->prepare($q_get_ratings);
		$p_get_ratings->execute(array(
			':timetable_subject_id' => $this->subject_id
		));
		return $p_get_ratings->fetch();
	}

	public function updateProjectMarks($req){
		return $this->getProjectInstance()->updateProjectMarks($req);
	}

	private function getProjectThemes() {
		try{
			return $this->getProjectInstance()->getThemes()->getData();
		}catch(Exception $e){
			return array();
		}
	}

}
<?php
/**
 * Created by PhpStorm.
 * User: Инал
 * Date: 12.11.2014
 * Time: 12:11
 */

const MODULE_NAME = 'BRS';

class BRSProject{

	private $db;
	private $timetable_subject_id;


	public function __construct($timetable_subject_id, PDO $db){

		$this->db = $db;
		$q_get_prj = 'SELECT timetable_subjects.has_project, id
			FROM timetable_subjects
			WHERE timetable_subjects.id = :timetable_subject_id
			AND timetable_subjects.has_project = 1';

		$p_prj = $this->db->prepare($q_get_prj);
		$p_prj->execute(array(
			':timetable_subject_id' => $timetable_subject_id
		));

		if ($p_prj === FALSE) throw new DBQueryException('', $this->db);

		if ($p_prj->rowCount() == 0) throw new InvalidArgumentException('Курсовой работы по данной дисциплине в этом семестре у этой группы не существует');

			$project = $p_prj->fetch();
			$this->timetable_subject_id = $project['id'];
	}

	public function getFeatures(){
		$q_features = 'SELECT *
			FROM brs_project_features
			WHERE
				brs_project_features.timetable_subject_id = :timetable_subject_id
				AND brs_project_features.status = 1
			ORDER BY brs_project_features.group_number';

		$p_features = $this->db->prepare($q_features);
		$p_features->execute(array(
			':timetable_subject_id' => $this->timetable_subject_id
		));
		if ($p_features === FALSE) throw new DBQueryException('', $this->db);

		return array('features' => $p_features->fetchAll());
	}

	public function updateFeatures($features){
		$this->db->beginTransaction();
		$q_ins_event = 'INSERT INTO brs_project_features(created_at, id, timetable_subject_id, name,
						description, group_number, max_rating)
					VALUES (NOW(), :feature_id, :timetable_subject_id, :feature_name, :feature_description,
						:feature_group_number, :max_rating)
					ON DUPLICATE KEY UPDATE
					name = :feature_name,
					description = :feature_description,
					group_number = :feature_group_number,
					max_rating = :max_rating,
					status = :status';
		try {
			$p_ins_feature = $this->db->prepare($q_ins_event);

			foreach ($features as $feature) {
				if (!$feature['feature_id'] && !$feature['max_rating']){
					continue;
				}

				$p_ins_feature->execute(array(
					':feature_id' => $feature['feature_id'] ? $feature['feature_id'] : null,
					':timetable_subject_id' => $this->timetable_subject_id,
					':feature_name' => $feature['feature_name'],
					':feature_description' => $feature['description'],
					':feature_group_number' => $feature['feature_group'],
					':max_rating' => $feature['max_rating'],
					':status' => isset($feature['to_remove']) && $feature['to_remove'] == '1' ? 0 : 1
				));
				if ($p_ins_feature === FALSE) throw new DBQueryException('FEATURE_INSERT_ERROR', $this->db);
			}
		}catch (Exception $e){
			$this->db->rollBack();
			throw $e;
		}
		$this->db->commit();
		return new Result(true, 'Данные успешно обновлены');
	}

	public function updateProjectMarks($students_array){
		$q_ins_mark = 'INSERT INTO brs_project_marks(feature_id, student_id, chance_type, chance_date, created_at, mark)
			VALUES(:feature_id, :student_id, :chance_type, :chance_date, NOW(), :mark)
			ON DUPLICATE KEY UPDATE
			chance_type = :chance_type,
			chance_date = :chance_date,
			mark = :mark';
		$p_ins_mark = $this->db->prepare($q_ins_mark);

		$q_ins_theme = 'INSERT INTO brs_projects_themes(timetable_subject_id, student_id, theme, created_at)
			VALUES(:timetable_subject_id, :student_id, :theme, NOW())
			ON DUPLICATE KEY UPDATE
			theme = :theme';

		$p_ins_theme = $this->db->prepare($q_ins_theme);

		foreach($students_array as $student_data){
			$p_ins_theme->execute(array(
				':timetable_subject_id' => $this->timetable_subject_id,
				':student_id' => $student_data['student_id'],
				':theme' => $student_data['theme'],
			));
			foreach($student_data['marks'] as $mark){
				$p_ins_mark->execute(array(
					':feature_id' => $mark['id'],
					':student_id' => $student_data['student_id'],
					':chance_type' => isset($mark['chance_type']) ? $mark['chance_type'] : '1',
					':chance_date' => isset($student_data['chance_date']) ? $student_data['chance_date'] : null,
					':mark' => $mark['value'],
				));
			}
		}

		return new Result(true, '');
	}

	public function getTimetableSubjectId() {
		return $this->timetable_subject_id;
	}

	public function getMarks(){
		$q_get_marks = 'SELECT brs_project_marks.*
			FROM brs_project_marks
			INNER JOIN brs_project_features ON brs_project_features.id = brs_project_marks.feature_id
			INNER JOIN students ON students.id = brs_project_marks.student_id
			WHERE brs_project_features.timetable_subject_id = :timetable_subject_id
			AND brs_project_features.status = 1';

		$p_get_marks = $this->db->prepare($q_get_marks);
		$p_get_marks->execute(array(
			':timetable_subject_id' => $this->timetable_subject_id
		));

		return new Result(true, '', $p_get_marks->fetchAll());
	}

	public function getThemes() {
		$q_get_themes = 'SELECT timetable_subject_id, student_id, theme FROM brs_projects_themes
			WHERE timetable_subject_id = :timetable_subject_id';
		$p_get_themes = $this->db->prepare($q_get_themes);
		$p_get_themes->execute(array(
			':timetable_subject_id' => $this->timetable_subject_id
		));
		return new Result(true, '', $p_get_themes->fetchAll());
	}
}
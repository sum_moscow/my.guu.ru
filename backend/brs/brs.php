<?php

	global $ROOT_PATH;

	require_once 'Class.BRSMain.php';
	require_once 'Class.StudentView.php';
	require_once $ROOT_PATH . '/backend/student_groups/Class.StudentGroup.php';
	require_once $ROOT_PATH . '/backend/institutes/Class.Institute.php';

	$brs = new BRSMain($__db, $__user);

//api/brs/edu_groups/:group_id/subjects/:subject_id/:name/

$__modules[$__class_name] = array(
	'GET' => array(
		'{edu_groups/(edu_group_id:[0-9]+)/subjects/(subject_id:[0-9]+)}/(function:.*?($|/))}' => function($edu_group_id, $subject_id, $func_name) use ($__user, $__args, $brs, $__db){
			$brs->selectForm($edu_group_id, $subject_id);
			switch ($func_name){
				case 'form_data': {
					return $brs->getFormData();
				}
				case 'control_events': {
					return $brs->getControlEvents();
				}
				case 'settings': {
					return $brs->getSettings();
				}
				case 'attendance': {
					return $brs->getAttendance();
				}
				case 'progress': {
					return $brs->getProgress();
				}
				case 'exam': {
					return $brs->getExam();
				}
				case 'project': {
					return $brs->getProject();
				}
			}
		},
		'{edu_groups/(edu_group_id:[0-9]+)/(subjects:subjects($|/$))}' => function($edu_group_id) use ($__user, $__args, $brs, $__db){
			if ($__user->hasRights(array('module' => Institute::MODULE_NAME, 'min_level' => 10))){
				$st_group = new StudentGroup($edu_group_id, $__db);
				return  $st_group->getSubjects();
			}else{
				return $__user->getProfessorInstance()->getMySubjectsWithGroup($edu_group_id);
			}
		},
		'my_groups' =>  function() use ($__user, $brs, $__db){
			if ($__user->hasRights(array('module' => Institute::MODULE_NAME, 'min_level' => 10))){
				$institute = new Institute($__user->getInstituteId(), $__db);
				return  $institute->getStudentsGroups($__user);
			}else{
				return $__user->getProfessorInstance()->getMyGroups();
			}

		},
		//me/:semester_id/subjects/:subject_id
		'me' =>  function() use ($__user, $brs, $__db, $__args){
			if ($__args[0] == 'semesters'){
				$st_view = new StudentView($__user->getStudentInstance(), $__db);
				return $st_view->getSemesters();
			}else{
				$st_view = new StudentView($__user->getStudentInstance(), $__db, $__args[0]);
				if ($__args[1] == 'subjects'){
					if ($__args[2] != ''){
						return $st_view->getMarks($__args[2]);
					}else{
						return $st_view->getSubjects();
					}
				}
			}
		}
	),
	'PUT' => array(
		'{id:[0-9]+}' => function($group_id) use ($__args, $brs, $__request){
			if (isset($__args[0]) && isset($__args[1])){
				switch ($__args[1]){
					case 'project': {
						return $brs->selectForm($group_id, $__args[0])->setProjectStatus($__request['status']);
						break;
					}
				}
			}
		}
	),
	'POST' => array(
		'{edu_groups/(edu_group_id:[0-9]+)/subjects/(subject_id:[0-9]+)}/(function:.*?($|/))}' => function($edu_group_id, $subject_id, $func_name) use ($__user, $__args, $brs, $__db, $__request){
			$brs->selectForm($edu_group_id, $subject_id);
			switch ($func_name){
				case 'control_events': {
					return $brs->updateControlEvents($__request['events']);
				}
				case 'project_features': {
					return $brs->updateProjectFeatures($__request['features'], $__request['has_project']);
				}
				case 'settings': {
					return $brs->updateSettings($__request);
				}
				case 'attendance': {
					return $brs->updateAttendance($__request['payload']['students'], $__request['payload']['classes']);
				}
				case 'progress': {
					return $brs->updateProgress($__request['marks']);
				}
				case 'project_marks': {
					return $brs->updateProjectMarks($__request['payload']);
				}
				case 'exam': {
					return $brs->updateExam($__request['payload']['marks'],$__request['payload']['exam_form_id']);
				}
			}
		}
	),
	'DELETE' => array(
		'{id:[0-9]+}' => function($group_id) use ($__args, $brs, $__request){
			if (isset($__args[0]) && isset($__args[1])){
				switch ($__args[1]){
					case 'controlEvent': {
						//return $brs->selectForm($group_id, $__args[0])->deleteControlEvent($__args[2]);
						break;
					}
				}
			}
		}
	),
);
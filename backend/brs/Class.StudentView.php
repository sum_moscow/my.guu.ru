<?php
/**
 * Created by PhpStorm.
 * User: Инал
 * Date: 09.11.2014
 * Time: 18:37
 */

global $ROOT_PATH;
require_once $ROOT_PATH . '/backend/student_groups/Class.StudentGroup.php';

class StudentView {

	private $student;
	private $db;
	private $student_group;
	private $semester_id;

	public function __construct(Student $student, PDO $db, $semester_id = null){
		$this->student = $student;
		$this->db = $db;
		$this->semester_id = is_numeric($semester_id) ? $semester_id : SUM::getCurrentSemesterId($db);
	}

	private function getStudentGroup(){
		if ($this->student_group instanceof StudentGroup == false){
			$this->student_group = new StudentGroup($this->student->getStudentGroupId(), $this->db);
			return $this->student_group;
		}else{
			return $this->student_group;
		}
	}

	public function getSemesters(){
		$q_get_semesters = 'SELECT DISTINCT brs_attendance.semester_id, semesters.brs_semester
			FROM brs_attendance
			INNER JOIN semesters ON semesters.semester_id = brs_attendance.semester_id
			WHERE brs_attendance.student_id = :student_id';
		$p_semesters = $this->db->prepare($q_get_semesters);
		$p_semesters->execute(array(
			':student_id' => $this->student->getId()
		));
		return new Result(true, '', $p_semesters->fetchAll());
	}

	public function getSubjects(){
		$q_get_subjects = 'SELECT DISTINCT subjects.subject_id, subject_name, semester_id, professor_id
			FROM subjects
			INNER JOIN timetable ON timetable.subject_id = subjects.subject_id
			WHERE student_group_id = :student_group_id
			AND semester_id = :semester_id';
		$p_subjects = $this->db->prepare($q_get_subjects);
		$p_subjects->execute(array(
			':student_group_id' => $this->getStudentGroup()->getId(),
			':semester_id' => $this->semester_id
		));
		if (!$p_subjects) throw new DBQueryException('', $this->db);
		return new Result(true, '', $p_subjects->fetchAll());
	}

	public function setSemesterId($semester_id = null){
		$this->semester_id = is_numeric($semester_id) ? $semester_id : SUM::getCurrentSemesterId($this->db);
	}

	public function getNewMarks(){
		$q_marks = 'SELECT brs_attendance.subject_id, mark, brs_attendance.changer_id as cuid,
			subjects.subject_name, brs_attendance.change_date,
			(SELECT CONCAT(first_name, " ", last_name, " ", middle_name) FROM users WHERE id = cuid) as changer_name
			FROM brs_attendance
			INNER JOIN users ON users.id = brs_attendance.student_id
			INNER JOIN subjects ON subjects.subject_id = brs_attendance.subject_id
			WHERE student_id = :student_id
			AND change_date > users.last_my_brs_view
			AND mark IS NOT NULL
			ORDER BY brs_attendance.change_date DESC';
		$p_marks = $this->db->prepare($q_marks);
		$p_marks->execute(array(
			':student_id' => $this->student->getId()
		));
		return new Result(true, '', $p_marks->fetchAll());
	}

	public function getAttendanceMarks($subject_id){
		$q_get_marks = 'SELECT brs_attendance.*, brs_classes.class_date, brs_classes.class_number, brs_classes.class_id
			FROM brs_attendance
			LEFT OUTER JOIN brs_classes ON brs_classes.semester_id = brs_attendance.semester_id
				AND brs_classes.class_number = brs_attendance.class_number
			WHERE brs_attendance.subject_id = :subject_id
				AND brs_attendance.semester_id = :semester_id
				AND brs_classes.student_group_id = :student_group_id
				AND student_id = :student_id';

		$p_marks = $this->db->prepare($q_get_marks);
		$p_marks->execute(array(
			':student_group_id' => $this->getStudentGroup()->getId(),
			':subject_id' => $subject_id,
			':student_id' => $this->student->getId(),
			':semester_id' => $this->semester_id
		));
		if (!$p_marks) throw new DBQueryException('', $this->db);
		return new Result(true, '', $p_marks->fetchAll());
	}

	public function getControlEventsMarks($subject_id){
		$q_get_ce_marks = 'SELECT brs_control_events.*, brs_types_of_control.name as control_event_name,
				brs_control_marks.mark
			FROM brs_control_events
			LEFT OUTER JOIN brs_control_marks ON brs_control_marks.control_event_id = brs_control_events.control_event_id
				AND brs_control_marks.student_id = :student_id
			LEFT OUTER JOIN brs_types_of_control ON brs_types_of_control.control_type_id = brs_control_events.control_type_id
			WHERE brs_control_events.subject_id = :subject_id
				AND brs_control_events.student_group_id = :student_group_id
				AND brs_control_events.status = 1
				AND brs_control_events.semester_id = :semester_id';

		$p_marks = $this->db->prepare($q_get_ce_marks);
		$p_marks->execute(array(
			':student_id' => $this->student->getId(),
			':subject_id' => $subject_id,
			':semester_id' => $this->semester_id,
			':student_group_id' => $this->getStudentGroup()->getId()
		));
		if (!$p_marks) throw new DBQueryException('', $this->db);

		return new Result(true, '', $p_marks->fetchAll());

	}

	public function getMarks($subject_id){
		return new Result(true, '', array(
			'attendance' => $this->getAttendanceMarks($subject_id)->getData(),
			'control_events' => $this->getControlEventsMarks($subject_id)->getData()
		));
	}
}
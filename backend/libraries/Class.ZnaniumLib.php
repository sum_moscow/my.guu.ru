<?php


class ZnaniumLib{

	const PRIVATE_KEY = '783582a231907200275ab21fa4a96085';


	public static function getAutoSignOnData(User $user){
		$timestamp = date('YmdHis');
		return new Result(true, '', array(
			'link' => 'https://znaniumcom.my.guu.ru/autosignon.php',
			'domain' => '*.guu.ru',
			'id' => $user->getId(),
			'login' => $user->getLogin(),
			'name' => $user->getFirstName(),
			'patr' => $user->getMiddleName(),
			'lname' => $user->getLastName(),
			'time' => $timestamp,
			'sign' => md5( $user->getId() . self::PRIVATE_KEY . $timestamp )
		));
	}


	public static function getAutoSignOnLink(User $user){
		$link = self::getAutoSignOnData($user)->getData();
		$params = array();

		foreach($link as $key => $value){
			if ($key == 'link') continue;
			$params[] = $key . '=' . $value;
		}
		return $link['link'] . '?' . implode('&', $params);
	}

}
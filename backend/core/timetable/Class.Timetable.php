<?php

	require_once $ROOT_PATH . '/backend/student_groups/Class.StudentGroup.php';
	require_once  $ROOT_PATH . '/backend/core/timetable/Class.Subject.php';


	class Timetable{

		private $db;
		private $student_group;
		private $check_prepared;

		const MODULE_NAME = 'timetable';

		public function __construct(StudentGroup $group = null, PDO $db){
			$this->student_group = $group;
			$this->db = $db;
		}


		//TODO: Add to groups_name institute abbrs after student_groups table schema will change
		private function getCheckPrepared($name){
			if ($this->check_prepared != null){
				return $this->check_prepared[$name];
			}
			$this->check_prepared = array(
				'allow_student_count' => $this->db->prepare('
					SELECT allow_student_count,
						(SELECT COUNT(*)
							FROM students
							WHERE students.student_group_id IN (SELECT student_group_id FROM timetable WHERE timetable.building_type=:building_type AND timetable.room = :room AND timetable.status = 1)
								AND students.student_group_id != :current_student_group_id
						) AS current_count,
						(SELECT COUNT(*) FROM students WHERE student_group_id = :current_student_group_id) AS current_student_group_students
						FROM classrooms
						WHERE building_type = :building_type AND room = :room'
				),
				'in_classroom' => $this->db->prepare('
					SELECT timetable.*, subjects.subject_name, CONCAT(" ", student_groups.student_group_name, " ", student_groups.course, " ", student_groups.student_group_number) AS groups_name,
					CONCAT(professors_view.first_name, " ", professors_view.last_name, " ", professors_view.middle_name) AS professor_name
						FROM timetable
						INNER JOIN student_groups ON student_groups.student_group_id = timetable.student_group_id
						LEFT JOIN professors_view ON professors_view.id = timetable.professor_id
						LEFT JOIN subjects ON timetable.subject_id = subjects.subject_id
						WHERE
							timetable.building_type = :building_type
							AND timetable.room=:room
							AND student_groups.shift = (SELECT shift FROM student_groups WHERE student_group_id=:current_student_group_id)
							AND timetable.day_type = :day_type
							AND timetable.week_type = :week_type
							AND timetable.class_type = :class_type
							AND timetable.student_group_id != :current_student_group_id
							AND timetable.status = 1'
				),
				'with_professor' => $this->db->prepare(
					'SELECT timetable.*, subjects.subject_name, CONCAT(" ",student_groups.student_group_name, " ", student_groups.course, " ", student_groups.student_group_number) AS groups_name,
						 CONCAT(professors_view.first_name, " ", professors_view.last_name, " ", professors_view.middle_name) AS professor_name
						FROM timetable
						INNER JOIN student_groups ON student_groups.student_group_id = timetable.student_group_id
						LEFT JOIN professors_view ON professors_view.id = timetable.professor_id
						LEFT JOIN subjects ON timetable.subject_id = subjects.subject_id
						WHERE
							timetable.professor_id = :professor_id
							AND timetable.day_type = :day_type
							AND timetable.week_type = :week_type
							AND timetable.class_type = :class_type
							AND timetable.student_group_id != :current_student_group_id
							AND student_groups.shift = (SELECT shift FROM student_groups WHERE student_group_id=:current_student_group_id)
							AND timetable.status = 1'
				)
			);
			return $this->check_prepared[$name];
		}

		private function checkItemInfo($item){

			$class_formats = array(
				'Л' => 0,
				'ПЗ' => 1,
				'ЛЗ' => 2
			);

			$result = array(
				'warnings' => array(),
				'errors' => array()
			);
			is_numeric($item[':student_group_id']) == false ? ($result['errors'][] = 'Не указана группа') : true;

			if ($item[':subject_id'] != 0){
				$item[':building_type'] == '' ? ($result['warnings'][] = 'Не указан корпус') : true;
				$item[':room'] == '' ? ($result['warnings'][] = 'Не указана аудитория') : true;

				if (isset($item[':professor_id']) && is_numeric($item[':professor_id'])){

					if ($item[':building_type'] != '' && $item[':room'] != ''){
						/* Another classes info */
						$p_with_professor = $this->getCheckPrepared('in_classroom');
						$p_with_professor->execute(array(
							':building_type' => $item[':building_type'],
							':room' => $item[':room'],
							':day_type' => $item[':day_type'],
							':week_type' => $item[':week_type'],
							':class_type' => $item[':class_type'],
							':current_student_group_id' => $this->student_group->getId()
						));

						if ($p_with_professor !== FALSE){
							$classes = $p_with_professor->fetchAll();
							foreach($classes as $class){
								if ($class['professor_id'] != $item[':professor_id'] || $class_formats[$item[':class_format']] != 0){
									$result['warnings'][] = 'В данной аудитории у преподавателя ' . ($class['professor_name'])
										. ' занятие ('. self::$class_formats_text[$item[':class_format']]
										.') по дисциплине "' . ($class['subject_name'])
										. '" с группами: ' . ($class['groups_name']);
								}
							}
						}
					}



					/* Another classes of professor */
					$p_with_professor = $this->getCheckPrepared('with_professor');
					$p_with_professor->execute(array(
						':day_type' => $item[':day_type'],
						':week_type' => $item[':week_type'],
						':class_type' => $item[':class_type'],
						':professor_id' => $item[':professor_id'],
						':current_student_group_id' => $this->student_group->getId()
					));

					if ($p_with_professor !== FALSE){
						$classes = $p_with_professor->fetchAll();
						foreach($classes as $class){
							if ($class['building_type'] != $item[':building_type'] || $item[':room'] != $item[':room']){
								$result['warnings'][] = 'У преподавателя ' . ($class['professor_name'])
									. ' занятие ('. self::$class_formats_text[$item[':class_format']]
									.') по дисциплине "' . ($class['subject_name'])
									. '" с группами: ' . ($class['groups_name']) . ' в другой аудитории ' . $class['building_type'] . $class['room'];
							}
						}
					}

					$p_allow_st_count = $this->getCheckPrepared('allow_student_count');
					$p_allow_st_count->execute(array(
						':building_type' => $item[':building_type'],
						':room' => $item[':room'],
						':current_student_group_id' => $this->student_group->getId()
					));
					if ($p_allow_st_count !== FALSE && $p_allow_st_count->rowCount() == 1){
						$r_allow_count = $p_allow_st_count->fetch();
						if ($r_allow_count['allow_student_count'] < ($r_allow_count['current_count'] + $r_allow_count['current_student_group_students'])){
							$result['warnings'][] = 'Превышено максимальное количество студентов в аудитории (' . $r_allow_count['allow_student_count']
								.'). Сейчас ' . $r_allow_count['current_count']
								. ' + в данной группе ' . $r_allow_count['current_student_group_students'];
						}
					}
				}else{
					$result['warnings'][] = 'Не указан преподаватель';
				}
			}
			return $result;
		}


		/*
		 * We'll store with key {day}-{class}-{week}
		 * */
		private static function normalize(array $tt, $change_index = true){
			$result = array();
			foreach($tt as $key => $item){
				if ($change_index){
					$result["{$item['day_number']}-{$item['class_number']}-{$item['week_number']}"] = $item;
				}else{
					$result[$key] = $item;
				}
			}
			return $result;
		}


		public function get($normalize = true){
			if ($this->student_group instanceof StudentGroup == false)
				throw new LogicException('Не задана студенческая группа');
			$q_get_tt = 'SELECT view_timetable.*
				FROM view_timetable
				WHERE
					semester_id = :semester_id
				AND edu_group_id = :edu_group_id
				AND sync_id = (SELECT MAX(id) FROM sync_timetable LIMIT 1)
				ORDER BY view_timetable.day_number, view_timetable.class_number, view_timetable.week_number';
			$p_tt = $this->db->prepare($q_get_tt);
			$p_tt->execute(array(
				':semester_id' => SUM::getCurrentSemesterId($this->db),
				':edu_group_id' => $this->student_group->getId()
			));
			if ($p_tt === FALSE) throw new DBQueryException('', $this->db);

			$data = $normalize ? (self::normalize($p_tt->fetchAll())) : ($p_tt->fetchAll());
			return new Result(true, 'Данные успешно получены',
				array(
					'timetable' => $data,
					'group_info' => $this->student_group->getInfo()->getData(),
				));
		}


		static public function getProfessorsTimetable(Professor $professor, PDO $db){
			$q_get = 'SELECT timetable.*, kafs_view.kaf_name, professors_view.first_name, subjects.subject_name,
				professors_view.last_name, professors_view.middle_name, student_groups.shift,
				student_groups.student_group_name, student_groups.course, student_groups.institute_id,
				institutes_view.abbr, student_groups.student_group_number
				FROM timetable
				INNER JOIN kafs_view ON timetable.kaf_id = kafs_view.kaf_id
				INNER JOIN professors_view ON timetable.professor_id=professors_view.id
				INNER JOIN subjects ON subjects.subject_id = timetable.subject_id
				INNER JOIN student_groups ON timetable.student_group_id = student_groups.student_group_id
				INNER JOIN institutes_view ON student_groups.institute_id = institutes_view.institute_id
				AND semester_id = :semester_id
				AND timetable.professor_id = :professor_id
				AND timetable.status = 1';
			$p_get = $db->prepare($q_get);
			$p_get->execute(array(
				':semester_id' => SUM::getCurrentSemesterId($db),
				':professor_id' => $professor->getId()
			));
			return new Result(true, '', self::normalize($p_get->fetchAll(), false));
		}
	}
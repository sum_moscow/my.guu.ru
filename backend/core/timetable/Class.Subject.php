<?php

class Subject{

	private $subject_id;
	private $subject_name;
	private $db;



	public function __construct($subject_id, PDO $db){
		$q_get_subject = 'SELECT * FROM subjects WHERE id = :subject_id';
		$p_subject = $db->prepare($q_get_subject);
		$p_subject->execute(array(
			':subject_id' => $subject_id
		));
		if ($p_subject === FALSE) throw new DBQueryException('', $db);


	}


	public static function normalizeName($subject_name){
		$subject_name = (mb_strtoupper($subject_name) == $subject_name) ? $subject_name : mb_strtolower($subject_name);
		$subject_name = trim($subject_name);
		$fc = mb_strtoupper(mb_substr($subject_name, 0, 1));
		$subject_name = $fc.mb_substr($subject_name, 1);
		$subject_name = preg_replace('!\s+!', ' ', $subject_name);
		return $subject_name;
	}



	public static function add($subject_name, PDO $db){

		if ($subject_name == ''){
			return 0;
		}
		$subject_name = self::normalizeName($subject_name);
		$q_ins_subject_name = 'INSERT INTO subjects (subject_name) VALUES (:subject_name)';
		$p_ins_sn = $db->prepare($q_ins_subject_name);
		$result = $p_ins_sn->execute(array(':subject_name' => $subject_name));

		if ($result){
			return $db->lastInsertId();
		}else{
			$q_get_sn = 'SELECT subject_id FROM subjects WHERE subject_name = :subject_name';
			$p_get_sn = $db->prepare($q_get_sn);
			$p_get_sn->execute(array(':subject_name' => $subject_name));
			if ($p_get_sn === FALSE || $p_get_sn->rowCount() == 0) throw new LogicException('Ошибка добавления дисциплины');
			$r_s_id = $p_get_sn->fetch();
			return $r_s_id['subject_id'];
		}
	}

}
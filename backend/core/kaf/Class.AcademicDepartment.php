<?php

	class AcademicDepartment{

		const HEAD_LEVEL_IN_MODULE = 10;

		private $institute_id;
		private $academic_department_id;
		private $name;
		private $db;




		public function __construct($kaf_id, PDO $db){
			$q_get_kaf = 'SELECT academic_departments.*, institutes.id as institute_id
				FROM  academic_departments
				LEFT JOIN organizational_units ON organizational_units.id = academic_departments.ou_id
				LEFT JOIN institutes ON institutes.ou_id = organizational_units.parent_id
				WHERE academic_departments.id = :id';
			$p_get_kaf = $db->prepare($q_get_kaf);
			$p_get_kaf->execute(array(
				':id' => $kaf_id
			));
			if ($p_get_kaf === FALSE) throw new DBQueryException('Кафедра не найдена', $db);
			$r_kaf = $p_get_kaf->fetch();
			$this->kaf_id = $r_kaf['id'];
			$this->institute_id = $r_kaf['institute_id'];
			$this->name = $r_kaf['name'];
			$this->db = $db;
		}

		/**
		 * @return mixed
		 */
		public function getKafId() {
			return $this->kaf_id;
		}

		/**
		 * @return mixed
		 */
		public function getName() {
			return $this->name;
		}


		public function getInstituteId(){
			return $this->institute_id;
		}


		public function getProfessorsList(){
			$q_get_professors = 'SELECT users.*
				FROM users
				INNER JOIN professors_view ON professors_view.id=users.id
				INNER JOIN staff ON staff.id=users.id
				WHERE staff.ou_id = :kaf_id';
			$p_professors = $this->db->prepare($q_get_professors);
			$p_professors->execute(array(
				':kaf_id' => $this->getKafId()
			));
			if ($p_professors === FALSE) throw new DBQueryException('', $this->db);
			return new Result(true, 'Данные успешно получены', $p_professors->fetchAll());
		}
	}
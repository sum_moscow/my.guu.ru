<?php

/*abstract class AbstractException extends Exception{

	protected $error_name;
	protected $user_message;
	protected $http_status;
	protected $db;
	protected $error_code;

	//ALLOWED_ERROR_CODES

	const FILE_SIZE_LIMIT = 1001;
	const INVALID_FILE = 1002;
	const FILE_NOT_FOUND = 1003;
	const DB_QUERY_ERROR = 1004;
	const PRIVILEGES_ERROR = 1005;
	const BUSINESS_LOGIC_ERROR = 1006;
	const INVALID_ARGUMENT = 1007;
	const INVALID_MODULE = 1008;
	const MODULE_DEPENDENCY_NOT_FOUND = 1009;
	const XML_GENERATION_ERROR = 1010;
	const XML_PARSE_ERROR = 1011;
	const UNSUPPORTED_OPERATION = 1012;


	public function write(){
		$this->db->prepare('');
	}

	public function __construct(PDO $db, $error_name, $error_code, $http_status, $user_message = ''){
		parent::__construct($error_name);
		$this->user_message = $user_message;
		$this->error_name= $error_name;
		$this->db = $db;
		$this->http_status = $http_status;
		$this->error_code = $error_code;
	}
	public function getErrorCode() {
		return $this->error_code;
	}


	public function getUserMessage(){
		return $this->user_message;
	}

	public function getDescription(){
		return "
			FILE: {$this->getFile()}. \n
			LINE: {$this->getLine()}. \n
			ERROR_NAME: {$this->getErrorName()}. \n
			USER_MESSAGE: {$this->getUserMessage()}. \n
			ERROR_CODE: {$this->getErrorCode()}. \n
			TRACE: {$this->getTraceAsString()}. \n
			HTTP_STATUS: {$this->getHTTPStatus()}
		";
	}

	public function getErrorName(){
		return $this->error_name;
	}

	public function __toString(){
		return "
			FILE: {$this->getFile()}. \n
			LINE: {$this->getLine()}. \n
			ERROR_NAME: {$this->getErrorName()}. \n
			USER_MESSAGE: {$this->getUserMessage()}. \n
			ERROR_CODE: {$this->getErrorCode()}. \n
			TRACE: {$this->getTraceAsString()}. \n
			HTTP_STATUS: {$this->getHTTPStatus()}
		";
	}

	private function getHTTPStatus() {
		return $this->http_status;
	}
}*/

abstract class AbstractException extends \Exception{
	protected $user_message;
	private $db;

	public function __construct($message, PDO $db, $user_m = ''){
		parent::__construct($message);
		$this->user_message = $user_m;
		$this->db = $db;
	}
	public function write(){
		$this->db->prepare('');
	}

	public function getUserMessage(){
		return $this->user_message;
	}

	public function __toString(){
		return "errorMessage: {$this->user_message} \n\r
				errorCode: {$this->getCode()}";
	}
}
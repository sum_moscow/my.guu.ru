<?php

	require_once 'Class.Dictionary.php';

	$__modules[Dictionary::MODULE_NAME] = array(

	'GET' => array(
		'kafs' => function() use ($__db) {
			$dic = new Kafs($__db);
			return $dic->all();
		},
		'institutes' => function() use ($__db) {
			$dic = new Institutes($__db);
			return $dic->all();
		}
	),
);
<?php

	abstract class Dictionary{

		protected $db;
		const MODULE_NAME = 'dictionary';

		public function __construct(PDO $db){
			$this->db = $db;
		}

		abstract public function all();

	}


	class Institutes extends Dictionary{

		public function all(){
			$q_get_all = 'SELECT * FROM institutes_view';
			$r_all = $this->db->prepare($q_get_all);
			$r_all->execute(array());
			return new Result(true, '', $r_all->fetchAll());
		}

	}

	class Kafs extends Dictionary{

		public function all(){
			$q_get_all = 'SELECT * FROM kafs_view';
			$r_all = $this->db->prepare($q_get_all);
			$r_all->execute(array());
			return new Result(true, '', $r_all->fetchAll());
		}

	}
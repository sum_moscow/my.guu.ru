<?php

	require_once 'Class.Users.php';


	$__modules[Users::MODULE_NAME] = array(
		'GET' => array(
			'stars' => function() use ($__db, $__user) {
				$users = new Users($__db);
				return $users->getStars();
			},
			'search' => function() use ($__db, $__user, $__request) {
				$users = new Users($__db);
				//$filter = isset($__request['type']) ? $__request['type'] : array();
				return $users->searchByName($__request['q'], $__request['group'], $__user);
			},
			'loginIsNull' => function() use ($__db, $__user, $__api_app) {
				$users = new Users($__db);
				return $users->getUsersWithoutLogin($__api_app);
			},
			'setStudentsLoginToNull' => function() use ($__db, $__user, $__api_app) {
				$users = new Users($__db);
				return $users->setStudentsLoginToNull($__api_app);
			},
			'changedSince' => function() use ($__db, $__user, $__api_app, $__request) {
				$users = new Users($__db);
				return $users->getUsersChangedSince($__api_app, $__request['since']);
			},
			'{(id:[0-9]+)}' => function($id) use ($__db, $__user, $__args) {
				$users = new Users($__db);
				return $users->getUserInfo($id, $__user);
			},
			'me' => function() use ($__db, $__user, $__args) {
				if (isset($__args[0]) && $__args[0] == 'fullInfo'){
					return $__user->getProfileInfo();
				}elseif(isset($__args[0]) && $__args[0] == 'dashInfo'){
					return $__user->getDashInfo();
				}elseif(isset($__args[0]) && $__args[0] == 'timetable'){
					if ($__user->isStudent()){
						return $__user->getStudentTimetable();
					}else{
						return $__user->getMyTimetable();
					}
				}elseif(isset($__args[0]) && $__args[0] == 'notifications'){
					return $__user->getNotifications();
				}else{
					$users = new Users($__db);
					return $users->getUserInfo($__user->getId(), $__user);
				}
			}
		),
		'POST' => array(
			'me' => function() use ($__db, $__user, $__args, $__request) {
				if ($__args[0] == 'unreadMessages'){
					$__user->setToNullUnreadMessages();
					return $__user->update();
				}elseif($__args[0] == 'avatar'){
					return $__user->setAvatar($__request['file'], $__request['file_name']);
				}elseif($__args[0] == 'fullInfo'){
					return $__user->updateProfileInfo($__request);
				}else{
					return $__user->updateProfileInfo($__request);
				}
			}
		),
		'PUT' => array(
			'{(id:[0-9]+)}' => function($id) use ($__request, $__args, $__api_app) {
				return $__api_app->setLogin($id, $__request['login']);
			},
		)
	);
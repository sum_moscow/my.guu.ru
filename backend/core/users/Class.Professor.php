<?php


class Professor extends Staff{


	public function __call($name, $arguments){
		if (!$this->isProfessor()) throw new PrivilegesException('Извините, вы не являетесь сотрудником', $this->db);
		return $this->$name($arguments);
	}


	private function getMyGroups(){
		$q_get_groups = 'SELECT DISTINCT view_edu_groups.*, view_timetable.sync_id
			FROM view_edu_groups
			INNER JOIN view_timetable ON view_timetable.edu_group_id = view_edu_groups.edu_group_id
			WHERE view_timetable.professor_id = :user_id
			AND view_timetable.semester_id = :semester_id
			AND view_timetable.sync_id = (SELECT MAX(id) FROM sync_timetable LIMIT 1)';
		$p_groups = $this->db->prepare($q_get_groups);
		$p_groups->execute(array(
			':user_id' => $this->getId(),
			':semester_id' => SUM::getCurrentSemesterId($this->db)
		));
		if ($p_groups === FALSE) throw new DBQueryException('', $this->db);
		return new Result(true, '', $p_groups->fetchAll());
	}

	private function getMySubjectsWithGroup($param){
		$q_get_subjects = 'SELECT DISTINCT
			sync_id, subject_name, timetable_subject_id as subject_id
			FROM view_timetable
			WHERE professor_id = :user_id
				AND semester_id = :semester_id
				AND edu_group_id = :edu_group_id
				AND sync_id = (SELECT MAX(id) FROM sync_timetable LIMIT 1)
		';
		$p_subjects = $this->db->prepare($q_get_subjects);
		$p_subjects->execute(array(
			':edu_group_id' => $param[0],
			':user_id' => $this->getId(),
			':semester_id' => Sum::getCurrentSemesterId($this->db)
		));
		if ($p_subjects === FALSE) throw new DBQueryException('', $this->db);

		return new Result(true, '', $p_subjects->fetchAll());
	}

	private function getTimetable(){
		global $ROOT_PATH;
		require_once $ROOT_PATH . '/backend/core/timetable/Class.Timetable.php';
		$tt_result = Timetable::getProfessorsTimetable($this, $this->db);
		return new Result(true, '', array(
			'is_professor' => $this->isProfessor(),
			'timetable' => $tt_result->getData())
		);
	}

}
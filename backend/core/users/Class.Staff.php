<?php

//require_once $ROOT_PATH . '/backend/core/users/Class.User.php';

class Staff extends User{

	private $department;
	private $role;
	private $ou_id;
	private $staff_id;

	private function jobInfo(){
		return new Result(true, '', array(
			'role' => $this->role,
			'ou_id' => $this->ou_id,
			'department' => $this->department
		));
	}

	public function __call($name, $arguments){
		global $__user;
		if (!$__user->isStaff()) throw new PrivilegesException('Извините, вы не являетесь сотрудником', $this->db);

		return $this->$name($arguments);
	}

	public function getJobInfo(){
		if ($this->department && $this->role) return $this->jobInfo();
		$q_get_path = 'SELECT roles.name as role, roles.ou_id, organizational_units.name as department
				FROM staff
				LEFT JOIN users_roles ON users_roles.user_id = staff.user_id
				LEFT JOIN roles ON roles.id = users_roles.role_id
				LEFT JOIN organizational_units ON organizational_units.id = roles.ou_id
				WHERE staff.user_id = :id';
		$p_path = $this->db->prepare($q_get_path);
		$p_path->execute(array(
			':id' => $this->getId()
		));
		if ($p_path === FALSE) throw new DBQueryException('', $this->db);
		$r_info = $p_path->fetch();
		$this->department = $r_info['department'];
		$this->role = $r_info['role'];
		$this->ou_id = $r_info['ou_id'];
		return $this->jobInfo();
	}

	private function getMyGroups() {

	}

	protected function updateAdditionalInfo($req){
		$q_upd_additional_info = 'UPDATE staff SET
			science_degree = :science_degree,
			science_title = :science_title,
			science_interests = :science_interests,
			project_descriptions = :projects_description,
			office_tel = :office_tel,
			room = :room,
			courses_info = :courses_info,
			education_info = :education_info
			WHERE user_id = :id';
		$p_upd_info = $this->db->prepare($q_upd_additional_info);
		$p_upd_info->execute(array(
			':science_degree' => (isset($req['science_degree'])) ? $req['science_degree'] : NULL,
			':science_title' => (isset($req['science_title'])) ? $req['science_title'] : NULL,
			':science_interests' => (isset($req['science_interests'])) ? $req['science_interests'] : NULL,
			':projects_description' => (isset($req['projects_description'])) ? $req['projects_description'] : NULL,
			':office_tel' => (isset($req['office_tel'])) ? $req['office_tel'] : NULL,
			':room' => (isset($req['room'])) ? $req['room'] : NULL,
			':courses_info' => (isset($req['courses_info'])) ? $req['courses_info'] : NULL,
			':education_info' => (isset($req['education_info'])) ? $req['education_info'] : NULL,
			':id' => $this->getId()
		));
		if ($p_upd_info === FALSE) throw new DBQueryException('', $this->db);
	}

	private function getAdditionalInfo(){
		$q_get = 'SELECT staff.*
			FROM staff
			WHERE staff.user_id = :id';
		$p_info = $this->db->prepare($q_get);
		$p_info->execute(array(
			':id' => $this->getId()
		));
		if ($p_info === FALSE) throw new DBQueryException('', $this->db);
		$result = array();
		if ($p_info->rowCount() == 1){
			$result = array_merge($p_info->fetch(), $this->getJobInfo()->getData());
		}
		return array_merge(array(), $result);
	}

	public function getStaffId(){
		if ($this->staff_id && is_numeric($this->staff_id)) return $this->staff_id;
		$additional_info = $this->getAdditionalInfo();
		$this->staff_id = $additional_info['id'];
		return $this->staff_id;
	}

	public function getInstituteId(){
		$q_get_institute = 'SELECT id
			FROM institutes
			WHERE id IN (SELECT ou_id FROM staff WHERE id = :id)
			ORDER BY id DESC
			LIMIT 1';
		$p_get_inst = $this->db->prepare($q_get_institute);
		$p_get_inst->execute(array(
			':id' => $this->getId()
		));
		$r = $p_get_inst->fetch();
		if ($p_get_inst === FALSE) throw new DBQueryException('', $this->db);
		if ($p_get_inst->rowCount() == 0) return null;
		return $r['institute_id'];
	}
}
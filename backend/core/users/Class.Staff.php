<?php

//require_once $ROOT_PATH . '/backend/core/users/Class.User.php';

class Staff extends User{

	private $department;
	private $role;
	private $ou_id;
	private $staff_id;

	private function jobInfo(){
		return new Result(true, '', array(
			'role' => $this->role,
			'ou_id' => $this->ou_id,
			'department' => $this->department
		));
	}

	public function __call($name, $arguments){
		global $__user;
		if (!$__user->isStaff()) throw new PrivilegesException('Извините, вы не являетесь сотрудником', $this->db);

		return $this->$name($arguments);
	}

	public function getJobInfo(){
		if ($this->department && $this->role) return $this->jobInfo();
		$q_get_path = 'SELECT view_users.role, view_users.ou_id, organizational_units.name as department
				FROM view_users
				LEFT JOIN organizational_units ON organizational_units.id = view_users.ou_id
				WHERE staff.user_id = :id';
		$p_path = $this->db->prepare($q_get_path);
		$p_path->execute(array(
			':id' => $this->getId()
		));
		if ($p_path === FALSE) throw new DBQueryException('', $this->db);
		$r_info = $p_path->fetch();
		$this->department = $r_info['department'];
		$this->role = $r_info['role'];
		$this->ou_id = $r_info['ou_id'];
		return $this->jobInfo();
	}

	private function getMyGroups() {

	}

	protected function updateAdditionalInfo($req){
		$q_upd_additional_info = 'UPDATE staff SET
			science_degree = :science_degree,
			science_title = :science_title,
			science_interests = :science_interests,
			project_descriptions = :projects_description,
			office_tel = :office_tel,
			room = :room,
			courses_info = :courses_info,
			education_info = :education_info
			WHERE user_id = :id';
		$p_upd_info = $this->db->prepare($q_upd_additional_info);
		$p_upd_info->execute(array(
			':science_degree' => (isset($req['science_degree'])) ? $req['science_degree'] : NULL,
			':science_title' => (isset($req['science_title'])) ? $req['science_title'] : NULL,
			':science_interests' => (isset($req['science_interests'])) ? $req['science_interests'] : NULL,
			':projects_description' => (isset($req['projects_description'])) ? $req['projects_description'] : NULL,
			':office_tel' => (isset($req['office_tel'])) ? $req['office_tel'] : NULL,
			':room' => (isset($req['room'])) ? $req['room'] : NULL,
			':courses_info' => (isset($req['courses_info'])) ? $req['courses_info'] : NULL,
			':education_info' => (isset($req['education_info'])) ? $req['education_info'] : NULL,
			':id' => $this->getId()
		));
		if ($p_upd_info === FALSE) throw new DBQueryException('', $this->db);
	}

	private function getAdditionalInfo(){
		$q_get = 'SELECT staff.*
			FROM staff
			WHERE staff.user_id = :id';
		$p_info = $this->db->prepare($q_get);
		$p_info->execute(array(
			':id' => $this->getId()
		));
		if ($p_info === FALSE) throw new DBQueryException('', $this->db);
		$result = array();
		if ($p_info->rowCount() == 1){
			$result = array_merge($p_info->fetch(), $this->getJobInfo()->getData());
		}
		return array_merge(array(), $result);
	}

	public function getStaffId(){
		if ($this->staff_id && is_numeric($this->staff_id)) return $this->staff_id;
		$additional_info = $this->getAdditionalInfo();
		$this->staff_id = $additional_info['id'];
		return $this->staff_id;
	}

	public function getInstituteId(){
		$result = null;
		$q_get_ou_id = 'SELECT ou_id FROM view_users WHERE id = :id';
		$p_get_ou_id = $this->db->prepare($q_get_ou_id);
		$res = $p_get_ou_id->execute(
			array(':id' => $this->getId())
		);
		if ($res === FALSE) throw new DBQueryException('CANT_GET_USER_INSTITUTE', $this->db);
		$row = $p_get_ou_id->fetch();
		if ($p_get_ou_id->rowCount() == 0) return $result;
		if ($row['ou_id'] == null) return $result;

		$ou_id = $row['ou_id'];

		$q_get_institute = 'SELECT institutes.id as institute_id, organizational_units.id, parent_id
			FROM organizational_units
			LEFT JOIN institutes ON institutes.ou_id = organizational_units.id
			WHERE organizational_units.id  = :ou_id';
		$p_get_inst = $this->db->prepare($q_get_institute);

		do{
			$res = $p_get_inst->execute(array(
				':ou_id' => $ou_id
			));
			if ($res === FALSE) throw new DBQueryException('', $this->db);
			$r = $p_get_inst->fetch();
			$ou_id = $r['parent_id'];
			$institute_id = $r['institute_id'];
		}while(is_null($institute_id) && !is_null($ou_id));
		$result = $institute_id;
		return $result;
	}
}
<?php


class Student extends User{

	private $work_path;
	private $role;

	public function __construct(PDO $db, User $user = null){
		parent::__construct($db, $user);
	}

	private function getStudentGroupId(){
		$q_get_id = 'SELECT view_users.edu_group_id
			FROM view_users
			WHERE view_users.id = :id';
		$p_get_id = $this->db->prepare($q_get_id);
		$p_get_id->execute(array(
			':id' => $this->getId()
		));
		if ($p_get_id === FALSE) throw new DBQueryException('', $this->db);
		$r_id = $p_get_id->fetch();
		return $r_id['edu_group_id'];
	}

	private function jobInfo(){
		return new Result(true, '', array(
			'role' => $this->role,
			'work_full_path' => $this->work_path
		));
	}

	public function __call($name, $arguments){
		global $__user;
		if (!$__user->isStudent()) throw new PrivilegesException('Извините, вы не являетесь сотрудником', $this->db);

		return $this->$name($arguments);
	}

	private function getStudentInfo(){
		if ($this->work_path && $this->role) return $this->jobInfo();
		$q_get_path = 'SELECT users.id,
				GROUP_CONCAT(organizational_units.ou_name SEPARATOR  "; ") AS work_full_path,
				(SELECT organizational_units.ou_name
					FROM organizational_units
					WHERE organizational_units.ou_id = users.low_level_ou_id
				) AS role
				FROM users
				INNER JOIN staff ON staff.id=users.id
				INNER JOIN organizational_units ON organizational_units.ou_id=staff.ou_id
				WHERE users.id=:id';
		$p_path = $this->db->prepare($q_get_path);
		$p_path->execute(array(
			':id' => $this->getId()
		));
		if ($p_path === FALSE) throw new DBQueryException('', $this->db);
		$r_info = $p_path->fetch();
		$this->work_path = $r_info['work_full_path'];
		$this->role = $r_info['role'];
		return $this->jobInfo();
	}

	private function getTimetable(){
		global $ROOT_PATH;
		require_once $ROOT_PATH . '/backend/student_groups/Class.StudentGroup.php';
		require_once $ROOT_PATH . '/backend/core/timetable/Class.Timetable.php';
		require_once $ROOT_PATH . '/backend/core/users/Class.Users.php';
		$student_group = new StudentGroup($this->getStudentGroupId(), $this->db);
		$timetable = new Timetable($student_group, $this->db);
		$data = $timetable->get(false)->getData();
		$data = $data['timetable'];
		$users = new Users($this->db);
		foreach($data as $key => $item){
			if (is_numeric($item['user_id'])){
				$prof = $users->getUserInfo($item['professor_id'], $this)->getData();
				$data[$key]['professor'] = $prof[0];
			}else{
				$data[$key] = null;
			}
		}
		$res = array('timetable' => $data);
		return new Result(true, '', array_merge(
			array( 'is_student' => true),
			$res
		));
	}

    //TODO не работает [42S02][1146] Table 'ru.guu.my.dev.institutes_view' doesn't exist
	public function getInstituteId(){
		$q_get_institute = 'SELECT institutes_view.institute_id
			FROM institutes_view
			INNER JOIN student_groups ON student_groups.institute_id=institutes_view.institute_id
			INNER JOIN students ON students.student_group_id = student_groups.student_group_id
			ORDER BY institute_id DESC
			LIMIT 1';
		$p_get_inst = $this->db->prepare($q_get_institute);
		$q_res = $p_get_inst->execute(array(
			':id' => $this->getId()
		));
		if (!$q_res) throw new DBQueryException('', $this->db);
		if ($p_get_inst->rowCount() == 0) return null;
        $r = $p_get_inst->fetch();
		return $r['institute_id'];
	}

}
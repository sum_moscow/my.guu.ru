<?php

	class Users{

		private $db;
		const MODULE_NAME = 'users';


		public function __construct(PDO $db){
			$this->db = $db;
		}

		private function normalizeUsers(array $users, $add_ou = false){
			$result = array();
			$p_get_ou = $this->db->prepare('SELECT organizational_units.id, organizational_units.name,
 				organizational_units.1c_guid, organizational_units.parent_id
				FROM organizational_units
				WHERE organizational_units.id = :id');

			$p_get_st = $this->db->prepare('SELECT edu_groups.course, edu_forms.name as edu_form_name,
				edu_qualifications.name as qualification_name, edu_programs.profile, edu_programs.program,
				edu_specialties.name as speciality, edu_groups.group, institutes.abbr as institute,
				students.personal_file, students.begin_study
 				FROM students
				INNER JOIN semesters_info ON semesters_info.student_id = students.id
				INNER JOIN edu_groups ON semesters_info.edu_group_id = edu_groups.id
				LEFT JOIN edu_programs ON edu_groups.edu_program_id = edu_programs.id
				LEFT JOIN edu_forms ON edu_groups.edu_form_id = edu_forms.id
				LEFT JOIN edu_specialties ON edu_specialties.id = edu_programs.edu_specialty_id
				LEFT JOIN edu_qualifications ON edu_qualifications.id = edu_specialties.edu_qualification_id
				LEFT JOIN institutes ON institutes.id = edu_programs.institute_id
				WHERE students.id = :student_id');
			foreach($users as $user){
				$user['avatar'] = (isset($user['avatar_ext']) && $user['avatar_ext']) ? '/images/avatars/' . $user['ad_login'] . '.' .$user['avatar_ext'] : '/assets/images/avatar.png';
				$user['is_staff'] = (isset($user['is_staff']) && $user['is_staff'] == 1) || (isset($user['ou_id']) && is_numeric($user['ou_id']));
				$user['is_student'] = (isset($user['is_student']) && $user['is_student'] == 1) || (isset($user['student_id']) && is_numeric($user['student_id']));
				$user['active'] = ($user['active'] == 1);

				if ($add_ou){
					if ($user['is_staff'] == true){
						$user['OU'] = array();
						$ou_id = $user['ou_id'];
						do{
							$p_get_ou->execute(array(':id' => $ou_id));
							if ($p_get_ou === FALSE) throw new DBQueryException('', $this->db);
							if ($p_get_ou->rowCount() > 0){
								$res = $p_get_ou->fetch();
								$user['OU'][] = $res;
								$ou_id = is_numeric($res['parent_id']) ? $res['parent_id'] : null;
							}
						}while($ou_id != null);
						$user['OU'] = array_reverse($user['OU']);
						$user['work_full_path'] = array();
						foreach($user['OU'] as $ou){
							$user['work_full_path'][] = $ou['name'];
						}
						$user['ad_work_full_path'] = array_reverse($user['work_full_path']);
						$user['work_full_path'] = implode('; ', $user['work_full_path']);

						$user['ad_work_full_path'] = implode(',OU=', $user['ad_work_full_path']);
						$user['ad_work_full_path'] = 'OU=' . $user['ad_work_full_path'];
					}elseif ($user['is_student'] == true){
						$p_get_st->execute(array(':student_id' => $user['student_id']));
						if ($p_get_st === FALSE) throw new DBQueryException('Student not found', $this->db);
						$student = $p_get_st->fetch();
						$user['role'] = $student['qualification_name'] . ', ' . $student['edu_form_name'] . ' отделение';
						$user['qualification_name'] = $student['qualification_name'];
						$user['edu_form_name'] = $student['edu_form_name'];
						$full_path_arr = array();
						if ($student['speciality']){
							$full_path_arr[] = $student['speciality'];
						}
						if ($student['profile']){
							$full_path_arr[] = $student['profile'];
						}
						if ($student['program']){
							$full_path_arr[] = $student['program'];
						}
						$full_path_arr[] = $student['course'] . ' курс '. $student['group'] . ' группа';
						$user['course'] = $student['course'];
						$user['group'] = $student['group'];
						$user['profile'] = $student['profile'];
						$user['program'] = $student['program'];
						$user['institute'] = $student['institute'];
						$user['personal_file'] = $student['personal_file'];
						$user['speciality'] = $student['speciality'];
						$user['begin_study'] = $student['begin_study'];

						$user['work_full_path'] = implode('; ', $full_path_arr);
					}

				}
				$result[] = $user;
			}

			$all_arr = array();

			if ($add_ou){
				foreach($result as $user){
					if ($user['is_staff']){
						if (!isset($all_arr[$user['id']])){
							$all_arr[$user['id']] = $user;
							$all_arr[$user['id']]['staff'] = array();
						}
						$all_arr[$user['id']]['staff'][] = array(
							'OU' => $user['OU'],
							'role' => $user['role'],
							'main_role' => $user['main_role'] == '1',
							'active' => $user['role_active'] == '1',
							'work_full_path' => $user['work_full_path'],
							'ad_work_full_path' => $user['ad_work_full_path'],
						);
						unset($all_arr[$user['id']]['OU']);
					}else{
						$all_arr[$user['id']] = $user;
					}
				}
				$result = array();
				foreach($all_arr as $u){
					$result[] = $u;
				}
			}


			return $result;
		}

		public function getStars() {
			$q_get_stars = '
				SELECT
				view_users.student_id, view_users.ou_id, view_users.role, users.active,
				view_users.first_name, view_users.last_name, view_users.id,
				users.avatar_ext, users.birth_date, users.phone_number, users.ad_login, view_users.middle_name,
				users.id as user_id, users.last_auth_time, users.additional_info,
				users.vk_url, users.fb_url, staff.room,
				(SELECT COUNT(*) AS request_times FROM stats_users_requests
					WHERE stats_users_requests.user_id = user_id
					AND (requesting_user_id != id)
				) AS request_times
				FROM view_users
				INNER JOIN users ON users.id = view_users.id
				LEFT JOIN staff ON staff.user_id = users.id
				WHERE users.active = 1
				GROUP BY users.id
				ORDER BY users.last_name, users.first_name ASC
				LIMIT 10';
			$p_stars = $this->db->prepare($q_get_stars);
			$p_stars->execute(array());
			if ($p_stars === FALSE)
				throw new DBQueryException('', $this->db);
			$users = $this->normalizeUsers($p_stars->fetchAll(), true);
			return new Result(true, '', $users);
		}

		private function buildFilterData(array $filters){
			$result = array(
				'having' => array(),
				'args' => array()
			);
			foreach($filters as $type => $value){
				if ($type == 'group'){
					switch($value){
						case 'all':{
							//$result[''] = ;
							break;
						}
						case 'students':{

							break;
						}
						case 'staff':{

							break;
						}
					}
				}
			}
		}

		/**
		 * @param $q
		 * @param $group @string Строка для фильтрации, доступны 3 группы - все, студенты, сотрудники
		 * @param AbstractUser $__user
		 * @return Result
		 * @throws DBQueryException
		 */
		public function searchByName($q, $group, AbstractUser $__user){
			//$having = array();
			//if (is_array($filters)){
			//	$filter_data = $this->buildFilterData($filters);
			//}
            $groups = array(
                'all' => null,
                'staff AND view_users.is_staff = 1',
                'students' => ' AND view_users.is_student = 1'
            );
			if (isset($groups[$group])){
				$group = $groups[$group];
			}else{
				$group = $groups['all']; //default value
			}
			$q_get_users = "
				SELECT
				view_users.student_id, view_users.ou_id, view_users.role,
				view_users.first_name, view_users.last_name, view_users.id,
				CONCAT_WS(' ', view_users.last_name, view_users.first_name, view_users.middle_name) AS full_name,
				users.avatar_ext, users.birth_date, users.phone_number, users.ad_login, view_users.middle_name,
				users.id as user_id, users.last_auth_time, users.additional_info,
				users.vk_url, users.fb_url, staff.room, users.active,
				(SELECT COUNT(*) AS request_times FROM stats_users_requests
					WHERE stats_users_requests.user_id = user_id
					AND (requesting_user_id != id)
				) AS request_times
				FROM view_users
				INNER JOIN users ON users.id = view_users.id
				LEFT JOIN staff ON staff.user_id=users.id
				WHERE users.active = 1 {$group}
				HAVING (
					view_users.first_name LIKE :q
					OR view_users.last_name LIKE :q
					OR view_users.middle_name LIKE :q
					OR full_name LIKE :q
					OR view_users.role LIKE :q)
				ORDER BY users.id, request_times, users.last_name, users.first_name ASC
				LIMIT 10";
			$p_get_users = $this->db->prepare($q_get_users);
			$p_get_users->execute(array(
//rray(
				':id' => $__user->getId(),
				':q' => $q . '%'
			));
			if ($p_get_users === FALSE) throw new DBQueryException('', $this->db);
			$users = $this->normalizeUsers($p_get_users->fetchAll(), true);
			return new Result(true, '', $users);
		}

		private function saveRequest($user_id, User $__user){
			$q_ins_req = 'INSERT INTO stats_users_requests(user_id, requesting_user_id, created_at)
				VALUES (:id, :requesting_user_id, NOW())';
			$p_req = $this->db->prepare($q_ins_req);
			$p_req->execute(array(
				':id' => $user_id,
				':requesting_user_id' => $__user->getId(),
			));

		}

		public function getUserInfo($user_id, User $__user){
			$q_get_user = '
				SELECT users.avatar_ext, birth_date, users.phone_number, email, view_users.middle_name, users.id as user_id,
				view_users.first_name, view_users.last_name, users.last_auth_time, view_users.ad_login, users.additional_info,
				 vk_url, fb_url, users.active,  view_users.student_id, users.id,
				 staff.science_interests, staff.office_tel, staff.room,
				 staff.science_degree_id, staff.science_title, staff.id as staff_id,
				CONCAT_WS(" ", view_users.last_name, view_users.first_name, view_users.middle_name) AS full_name,
				ou_id, staff.education_info, staff.science_title,
				view_users.role, staff.science_degree, staff.courses_info
				FROM users
				LEFT JOIN view_users ON view_users.id = users.id
				LEFT JOIN staff ON view_users.staff_id = staff.id
				WHERE users.id = :id
				AND users.active = 1';

			$p_user = $this->db->prepare($q_get_user);
			$p_user->execute(array(
				':id' => $user_id
			));
			if ($p_user === FALSE) throw new DBQueryException('', $this->db);
			$users = $this->normalizeUsers($p_user->fetchAll(), true);
			$this->saveRequest($user_id, $__user);
			return new Result(true, '', $users);
		}

		public function getUsersWithoutLogin(ApiApplication $app){
			$q_get_users = 'SELECT DISTINCT users.id, users.first_name, staff.1c_id,
				users.middle_name, users.last_name, roles.name as role, roles.ou_id as ou_id, users.active,
				students.id as student_id,
				IF(staff.id IS NULL, 0, 1) as is_staff
				FROM users
				LEFT JOIN staff ON staff.user_id = users.id
				LEFT JOIN users_roles ON users_roles.user_id = users.id
				LEFT JOIN roles ON roles.id = users_roles.role_id
				LEFT JOIN organizational_units ON organizational_units.id = roles.ou_id
				LEFT JOIN students ON students.user_id = users.id
				WHERE users.ad_login IS NULL
				AND users.active = 1';
			$p_get_users = $this->db->prepare($q_get_users);
			$p_get_users->execute();
			if ($p_get_users === FALSE) throw new DBQueryException('Ошибка получения данных', $this->db);
			$users = $p_get_users->fetchAll();
			return new Result(true, 'Данные успешно получены', $this->normalizeUsers($users, true));
		}

		public function getUsersChangedSince(ApiApplication $app, $since){
			$q_get_users = 'SELECT DISTINCT users.id, users.first_name, staff.1c_id,
				users.middle_name, users.last_name, roles.name as role, roles.ou_id as ou_id, users.active,
				users_roles.active as role_active, users_roles.main_role,
				students.id as student_id,
				IF(staff.id IS NULL, 0, 1) as is_staff,
				IF(students.id IS NULL, 0, 1) as is_student
				FROM users
				LEFT JOIN staff ON staff.user_id = users.id
				LEFT JOIN users_roles ON users_roles.user_id = users.id
				LEFT JOIN roles ON roles.id = users_roles.role_id
				LEFT JOIN organizational_units ON organizational_units.id = roles.ou_id
				LEFT JOIN students ON students.user_id = users.id
				WHERE
				users.updated_at > FROM_UNIXTIME(:since)
				OR users.created_at > FROM_UNIXTIME(:since)';
			$p_get_users = $this->db->prepare($q_get_users);
			$p_get_users->execute(array(
				':since' => $since
			));
			if ($p_get_users === FALSE) throw new DBQueryException('Ошибка получения данных', $this->db);
			$users = $p_get_users->fetchAll();
			return new Result(true, 'Данные успешно получены', $this->normalizeUsers($users, true));
		}

	}
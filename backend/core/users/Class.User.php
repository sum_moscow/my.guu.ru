<?php


require_once 'Class.AbstractUser.php';

class User extends AbstractUser{

	const SUPER_ADMIN_LEVEL = 99;
	const MODULE_NAME = 'users';

	/*
	 * const STUDENT_GROUP_ID = 1;
	 * const STAFF_GROUP_ID = 2;
	 * const PROFESSOR_GROUP_ID = 4;
	*/

	private $password;
	protected $id;
	protected $email;
	protected $db;
	protected $first_name;
	protected $last_name;
	protected $middle_name;
	protected $avatar;
	protected $login;
	protected $vk_url;
	protected $fb_url;
	protected $avatar_ext;
	protected $birthday;
	protected $phone_number;
	protected $ad_login;
	protected $birth_date;
	protected $additional_info;
	private $is_staff;
	private $is_student;
	private $is_professor;

	private $staff_instance;
	private $student_instance;
	private $professor_instance;

	private $rights;



	public function __construct(PDO $db, $build_param = null){

		$query_main_part = 'SELECT DISTINCT
			users.email, users.fb_url, users.vk_url,
			view_users.phone_number,
			view_users.ad_login, users.token, view_users.id,
			view_users.first_name, view_users.last_name, view_users.middle_name, users.avatar_ext,
			 view_users.is_staff,
			 view_users.is_student,
			 view_users.is_professor,
			 users.birth_date,
			 users.additional_info
			FROM view_users
			INNER JOIN users ON users.id = view_users.id
			INNER JOIN azure_tokens ON users.ad_login = azure_tokens.ad_login
			LEFT JOIN users_tokens ON users.id = users_tokens.user_id ';

		if ($build_param == null){
			if (!isset($_SESSION['login']) || trim($_SESSION['login']) == ''
				|| !isset($_SESSION['password']) || trim($_SESSION['password']) == ''){
				throw new LogicException('Пользователь с такими данными не найден');
			}
			$q_get_user = $query_main_part.
			' WHERE users.ad_login = :login AND users.token = :password
			AND azure_tokens.expires_on > NOW()';
			$p_get_user = $db->prepare($q_get_user);
			$p_get_user->execute(array(
				':login' => $_SESSION['login'],
				':password' => $_SESSION['password']
			));
		}elseif ($build_param instanceof User){
			$q_get_user = $query_main_part .'
				WHERE view_users.id = :id
				AND azure_tokens.expires_on > NOW()';
			$p_get_user = $db->prepare($q_get_user);
			$p_get_user->execute(array(
				':id' => $build_param->getId()
			));
		}else{
			$q_get_user = $query_main_part .'
				WHERE users_tokens.token = :token
				AND users_tokens.active_till < NOW()
				AND azure_tokens.expires_on > NOW()
				AND users_tokens.id = (SELECT MAX(id) FROM users_tokens WHERE user_id = view_users.id)';
			$p_get_user = $db->prepare($q_get_user);
			$p_get_user->execute(array(
				':token' => $build_param
			));
		}

		if ($p_get_user === FALSE) throw new DBQueryException('USER_NOT_EXIST', $db);
		if ($p_get_user->rowCount() !== 1) throw new LogicException('Пользователь с такими данными не найден');
		$r_get_user = $p_get_user->fetch();
		$this->id = $r_get_user['id'];
		$this->email = $r_get_user['email'];
		$this->is_staff = $r_get_user['is_staff'] == 1;
		$this->is_student = $r_get_user['is_student'] == 1;
		$this->is_professor = $r_get_user['is_professor'] == 1;
		$this->password = $r_get_user['token'];
		$this->login = $r_get_user['ad_login'];
		$this->first_name = $r_get_user['first_name'];
		$this->last_name = $r_get_user['last_name'];
		$this->middle_name = $r_get_user['middle_name'];
		$this->avatar_ext = $r_get_user['avatar_ext'];
		$this->avatar = $r_get_user['avatar_ext'] ? '/images/avatars/' . $this->login .'.' .$r_get_user['avatar_ext'] : './assets/images/avatar.png';
		$this->phone_number = $r_get_user['phone_number'];
		$this->vk_url = $r_get_user['vk_url'];
		$this->fb_url = $r_get_user['fb_url'];
		$this->ad_login = $r_get_user['ad_login'];
		$this->birth_date = $r_get_user['birth_date'];
		$this->additional_info = $r_get_user['additional_info'];
		$this->db = $db;
	}

	/**
	 * @return mixed
	 */
	public function getAvatarExt() {
		return $this->avatar_ext;
	}

	/**
	 * @param $file
	 * @param $file_name
	 * @return \Result
	 * @internal param mixed $avatar_ext
	 */
	public function setAvatar($file, $file_name){
		/* Check file size. Max allowed = 5 MB*/
		$start_memory = memory_get_usage();
		$tmp = unserialize(serialize($file));
		$img_size = memory_get_usage() - $start_memory;

		if ($img_size / 1024 > 6144){ // CMP with 6 MB, coz var is not only image data
			throw new InvalidArgumentException('Файл слишком большого размера. Максимальный размер - 4МБ');
		}

		if (!isset($file_name) || $file_name == ''){
			$this->avatar_ext = '';
		}else{
			$this->avatar_ext = end(explode('.', $file_name));
		}

		$file = explode(',', $file);
		$file = $file[1];
		if ($this->avatar_ext != '' && $file){
			global $ROOT_PATH;
			$new_file_name = $ROOT_PATH . '/images/avatars/' . $this->login . '.' . $this->avatar_ext;
			file_put_contents($new_file_name, base64_decode($file));
			$this->update();
			return new Result(true, 'Данные успешно обновлены');
		}else{
			return new Result(false, 'Ошибка обновлени данных. Не указан файл или название.');
		}
	}

	/**
	 * @return mixed
	 */
	public function getOfficeTel() {
		return $this->office_tel;
	}

	/**
	 * @param mixed $office_tel
	 */
	public function setOfficeTel($office_tel) {
		$this->office_tel = $office_tel;
	}

	/**
	 * @return mixed
	 */
	public function getPhoneNumber() {
		return $this->phone_number;
	}

	/**
	 * @param mixed $cell_tel
	 * @return $this
	 */
	public function setPhoneNumber($cell_tel) {
		$this->phone_number = $cell_tel;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getBirthday() {
		return $this->birthday;
	}

	/**
	 * @param mixed $birthday
	 */
	public function setBirthday($birthday) {
		$this->birthday = $birthday;
	}

    /**
     * return array of all user's groups and rights in it
     * @return array
     * @throws DBQueryException
     */
	protected function getRights(){
		if ($this->rights != null) return $this->rights;
		$q_get_rights = 'SELECT groups.id as group_id,
			groups.name, module, level_in_module
			FROM groups
			INNER JOIN memberof ON memberof.group_id = groups.id
			WHERE memberof.user_id = :id
			AND memberof.status = 1';
		$p_rights = $this->db->prepare($q_get_rights);
		$q_res = $p_rights->execute(array(':id' => $this->getId()));
		if ($q_res === FALSE) throw new DBQueryException('', $this->db);
		$this->rights = $p_rights->fetchAll();
		return $this->rights;
	}


	public function getName(){
		return "{$this->first_name} {$this->last_name}";
	}
	public function getAvatarUrl(){
		return $this->avatar;
	}
	public function getId(){
		return $this->id;
	}
	public function getAuthInfo(){
		return array(
			'id' => $this->id,
			'password' => $this->password,
			'login' => $this->ad_login,
			'name' => $this->getName(),
			'first_name' => $this->getFirstName(),
			'last_name' => $this->getLastName(),
			'middle_name' => $this->getMiddleName(),
			'is_student' => $this->isStudent(),
			'is_professor' => $this->isProfessor(),
			'is_staff' => $this->isStaff()
		);
	}

	protected function getMainInfo(){
		return array(
			'avatar' => $this->getAvatarUrl(),
			'user_name' => $this->getName(),
			'email' => $this->email,
			'ad_login' => $this->ad_login,
			'is_staff' => $this->isStaff(),
			'is_student' => $this->isStudent(),
			'first_name' => $this->first_name,
			'last_name' => $this->last_name,
			'middle_name' => $this->middle_name,
			'phone_number' => $this->phone_number,
			'vk_url' => $this->vk_url,
			'fb_url' => $this->fb_url,
			'birth_date' => $this->birth_date,
			'additional_info' => $this->additional_info,
		);
	}

	/**
	 * @return Result
	 * @throws DBQueryException
	 */
	public function getProfileInfo(){

		try{
			$staff = $this->getStaffInstance();
			$staff_info = $staff->getAdditionalInfo();
		}catch (Exception $e){
			$staff_info = array();
		}

		$student_info = array();
		/*try{
			$student = $this->getStudentInstance();
			$student_info = $student->getAdditionalInfo()->getData();
		}catch (Exception $e){
			$student_info = '';
		}*/
		return new Result(true, '', array(
			'main_info' => $this->getMainInfo(),
			'staff_info' => $staff_info,
			'student_info' => $student_info
		));
	}

	protected function getCanteensMenu(){
		return new Result(true, '', array());
		/*
		global $ROOT_PATH;
		require_once $ROOT_PATH . '/backend/canteens/Class.Canteens.php';
		$canteens = new Canteens($this->db);
		return $canteens->getFullMenu();*/
	}

	protected function setLastMyTimetableView(){
		$time_now = new DateTime();
		$this->last_my_timetable_view = $time_now->format('Y-m-d H:i:s');
	}

	public function getDashInfo(){
		return new Result(true, '', array_merge(
			$this->getAuthInfo(),
			$this->getCanteensMenu()->getData()
		));
	}

	public function setToNullUnreadMessages() {
		$this->unread_messages = null;
	}

	protected function getDB(){
		return $this->db;
	}

	protected function updateAdditionalInfo($req){}

	public function updateProfileInfo($req){
		$q_upd_profile = 'UPDATE users
			SET phone_number = :phone_number,
			additional_info = :additional_info,
			birth_date = :birth_date,
			vk_url = :vk_url,
			fb_url = :fb_url
			WHERE id = :id';
		$p_upd_profile = $this->db->prepare($q_upd_profile);

		$birth_date = DateTime::createFromFormat('Y-m-d', $req['birth_date']);

		$result = $p_upd_profile->execute(array(
			':phone_number' => isset($req['phone_number']) ? $req['phone_number'] : NULL,
			':additional_info' => isset($req['additional_info']) ? $req['additional_info'] : NULL,
			':vk_url' => isset($req['vk_url']) ? $req['vk_url'] : NULL,
			':fb_url' => isset($req['fb_url']) ? $req['fb_url'] : NULL,
			':birth_date' => $birth_date ? $req['birth_date'] : NULL,
			':id' => $this->getId()
		));

		if ($result === FALSE) throw new DBQueryException('', $this->db);

		if ($this->isProfessor()){
			$this->getProfessorInstance()->updateAdditionalInfo($req);
		}elseif($this->isStaff()){
			$this->getStaffInstance()->updateAdditionalInfo($req);
		}else{
			$this->getStudentInstance()->updateAdditionalInfo($req);
		}
		return new Result(true, 'Данные успешно обновлены');
	}

    /**
     * check if user has enough rights in module or he is a super admin in it
     * min rights -> ( module => "module name", min_level => "required rights in the module to do something by int" )
     * @param array $min_rights
     * @return bool
     * @throws DBQueryException
     */
	public function hasRights(array $min_rights){
		$rights = $this->getRights();
		$has_rights = false;
		foreach($rights as $right){
			if (trim(strtolower($min_rights['module'])) == trim(strtolower($right['module']))){
				if ((int)$min_rights['min_level'] <= (int)$right['level_in_module']){
					$has_rights = true;
				}
			}

			/* check for super admin rights*/
			if (trim(strtolower($right['module'])) == 'main'){
				if ((int)$right['level_in_module'] >= self::SUPER_ADMIN_LEVEL){
					$has_rights = true;
				}
			}
		}
		return $has_rights;
	}

	public function update(){
		$q_upd_user = 'UPDATE users SET
			avatar_ext = :avatar_ext,
			phone_number = :phone_number,
			birth_date = :birth_date
		WHERE id=:id';
		$r_upd = $this->db->prepare($q_upd_user)->execute(array(
			':avatar_ext' => $this->getAvatarExt(),
			':phone_number' => $this->getPhoneNumber(),
			':birth_date' => $this->getBirthday(),
			':id' => $this->id
		));

		if ($r_upd == FALSE) throw new DBQueryException('Ошибка обновления ваших персональных данных', $this->db);

		return new Result(1, 'Данные успешно обновлены');
	}

	public function getProfessorInstance(){
		if (!$this->isProfessor()) throw new PrivilegesException('Недостаточно прав для совершения действия', $this->db);

		if ($this->professor_instance instanceof Professor == false) {
			$this->professor_instance = new Professor($this->getDB());
		}
		return $this->professor_instance;
	}

	public function getStaffInstance(){
		if (!$this->isStaff()) throw new PrivilegesException('Недостаточно прав для совершения действия', $this->db);
		if ($this->staff_instance instanceof Staff == false) {
			$this->staff_instance = new Staff($this->getDB());
		}
		return $this->staff_instance;
	}

	public function getStudentInstance(){
		if (!$this->isStudent()) throw new PrivilegesException('Недостаточно прав для совершения действия', $this->db);

		if ($this->student_instance instanceof Student == false) {
			$this->student_instance = new Student($this->getDB(), $this);
		}

		return $this->student_instance;
	}

	public function isStaff(){
		return $this->is_staff > 0 || $this->hasRights(array('module' => 'main', 'min_level' => 99));
	}

	public function isStudent(){
		return $this->is_student > 0 || $this->hasRights(array('module' => 'main', 'min_level' => 99));
	}

	public function isProfessor(){
		return ( $this->is_professor > 0 && $this->isStaff());
	}

	public function getMyTimetable($save_view_time = true){
		if ($this->isStudent()){
			$student = $this->getStudentInstance();
			$r = $student->getTimetable();
		}elseif($this->isProfessor()){
			$professor = $this->getProfessorInstance();
			$r = $professor->getTimetable();
		}else{
			throw new PrivilegesException('У вас нет прав для данного действия', $this->db);
		}
		if ($save_view_time){
			$this->setLastMyTimetableView();
			$this->update();
		}
		return $r;
	}

	//TODO: remove later
	public function getStudentTimetable(){
		$student = $this->getStudentInstance();
		$r = $student->getTimetable();
		return $r;
	}

	private function getTimetableNotifications(){
		$res = array();
		if (!$this->isProfessor() && !$this->isStudent()) return $res;
		$tt = $this->getMyTimetable(false)->getData();
		foreach($tt['timetable'] as $key => $value){
			if (new DateTime($this->last_my_timetable_view) < new DateTime($value['last_change_time'])){
				$res[] = $value;
			}
		}
		return $res;
	}

	private function getRequisitionsNotifications(){
		$q_get_requisitions = 'SELECT * FROM institute_requisitions
			INNER JOIN requisition_statuses ON requisition_statuses.status_id = institute_requisitions.requisition_status_id
			WHERE
				(from_user_id = :current_user_id
					AND change_date != create_date
					AND changer_id != :current_user_id)';
		$p_requisitions = $this->db->prepare($q_get_requisitions);

		$p_requisitions->execute(array(
			':current_user_id' => $this->getId()
		));

		$result['my'] = $p_requisitions->fetchAll();

		if ($this->isStaff() && $this->hasRights(array('module' => 'institute', 'min_level' => 9))){
			$q_get_requisitions = 'SELECT * FROM institute_requisitions
				INNER JOIN requisition_statuses ON requisition_statuses.status_id = institute_requisitions.requisition_status_id
				WHERE institute_id = :institute_id';
			$p_requisitions = $this->db->prepare($q_get_requisitions);

			$p_requisitions->execute(array(
				':institute_id' => $this->getStaffInstance()->getInstituteId()
			));

			$result['all'] = $p_requisitions->fetchAll();
		}else{
			$result['all'] = array();
		}

		return new Result(true, '', array('institute' => $result));
	}

	public function getNotifications(){
		global $ROOT_PATH;
		require_once $ROOT_PATH . '/backend/brs/Class.StudentView.php';
		$arr = array();
		if ($this->isStudent()){
			$user_inst = $this->getStudentInstance();
			$st_view = new StudentView($user_inst, $this->db);
			$arr['brs'] =  $st_view->getNewMarks()->getData();
		}else{
			$arr['brs'] = array();
		}
		if ($this->isProfessor()){
			$user_inst = $this->getProfessorInstance();
		}

		return new Result(true, '', array_merge(array(
			'timetable' => array(), //$user_inst->getTimetableNotifications(),
			'requisitions' => array()//$user_inst->getRequisitionsNotifications()->getData()
		), $arr));
	}

	public function getInstituteId(){
		if ($this instanceof Professor || $this instanceof Student || $this instanceof Staff){
			return $this->getInstituteId();
		}else{
            if ($this->isProfessor()){
                return $this->getProfessorInstance()->getInstituteId();
            }elseif ($this->isStaff()){
                return $this->getStaffInstance()->getInstituteId();
            }elseif ($this->isStudent()){
				return $this->getStudentInstance()->getInstituteId();
			}else
                return null;
		}
	}

	public function getLogin(){
		return $this->login;
	}

	public function getLastName() {
		return $this->last_name;
	}

	public function getMiddleName() {
		return $this->middle_name;
	}

	public function getFirstName() {
		return $this->first_name;
	}

	public static function logout(){
		session_unset();
	}

	public function getEmail() {
		return $this->email;
	}

	public function generateToken(ApiApplication $app){
		$p_add_token = $this->db->prepare('INSERT INTO users_tokens(token, user_id, api_application_id, created_at, active_till)
			VALUES(:token, :user_id, :api_application_id, NOW(), :active_till)');

		$token = hash('sha512', $app->getPrivateKey() . $this->getId() . $app->getPublicKey() .time());
		$p_add_token->execute(array(
			':token' => $token,
			':user_id' => $this->getId(),
			':active_till' => date('M d, Y', strtotime("+14 day")),
			':api_application_id' => $app->getId()
		));
		return new Result(true, '', array('token' => $token));
	}

	public function getToken(ApiApplication $app){
		$p_get_valid_token = $this->db->prepare('SELECT *
			FROM users_tokens
			WHERE user_id = :user_id
			AND api_application_id = :api_application_id
			AND active_till < NOW()
			ORDER BY created_at DESC
			LIMIT 1');
		$p_get_valid_token->execute(array(
			':api_application_id' => $app->getId(),
			':user_id' => $this->getId()
		));
		if ($p_get_valid_token === FALSE) throw new DBQueryException('', $this->db);
		if ($p_get_valid_token->rowCount() == 0){
			return $this->generateToken($app);
		}else{
			$result = $p_get_valid_token->fetch();
			return new Result(true, '', array('token' => $result['token']));
		}
	}
}
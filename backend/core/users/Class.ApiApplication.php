<?php


class ApiApplication extends AbstractUser{


	private $id;
	private $name;
	private $description;
	private $owner_id;
	private $private_key;
	private $public_key;
	private $active;
	private $created_at;
	private $updated_at;
	private $allow_ip;
	private $db;


	public function __construct(PDO $db, $public_key, $timestamp, $token){
		$q_get_app = 'SELECT id, active, allow_ip, created_at, description, name, owner_id, private_key,
 			updated_at, public_key
			FROM api_applications
			WHERE public_key = :public_key';
		$p_get = $db->prepare($q_get_app);
		$q_res = $p_get->execute(array(
			':public_key' => $public_key,
		));
		if ($q_res === FALSE) throw new DBQueryException('', $db);
		if ($p_get->rowCount() != 1) throw new InvalidArgumentException('Неверный ключ');
		$result = $p_get->fetch();
		if (md5($timestamp . $result['private_key']) !== $token)  throw new InvalidArgumentException('Неверный ключ');

		$this->active = $result['active'];
		$this->name = $result['name'];
		$this->description = $result['description'];
		$this->owner_id = $result['owner_id'];
		$this->private_key = $result['private_key'];
		$this->public_key = $result['public_key'];
		$this->id = $result['id'];
		$this->db = $db;
		$this->allow_ip = $result['allow_ip'];
		$this->created_at = $result['created_at'];
		$this->updated_at = $result['updated_at'];
	}

	public function hasRights(array $min_rights){
		return true;
	}

	public function setLogin($user_id, $login){
		$q_upd_user = 'UPDATE users SET ad_login = :login
			WHERE users.active = 1
			AND users.id = :id
			AND users.ad_login IS NULL';
		$p_upd_login = $this->db->prepare($q_upd_user);
		$p_upd_login->execute(array(
			':login' => $login,
			':id' => $user_id
		));
		if ($p_upd_login === FALSE) throw new DBQueryException('', $this->db);
		if ($p_upd_login->rowCount() != 1) throw new InvalidArgumentException();
		return new Result(true, 'Данные успешно обновлены');
	}

	public function getId(){
		return $this->id;
	}

	public function getPrivateKey() {
		return $this->private_key;
	}

	public function getPublicKey() {
		return $this->public_key;
	}
}
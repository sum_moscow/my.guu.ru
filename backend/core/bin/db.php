<?php

	header('Content-Type: text/html; charset=utf-8');
	@mb_internal_encoding("UTF-8");
	@session_set_cookie_params(2592000);
	@session_start();
	date_default_timezone_set('Europe/Moscow');
	$ROOT_PATH = $_SERVER['DOCUMENT_ROOT'];
	require_once 'app_config.php';

	SUM::init();

	$db = new PDO('mysql:host='. SUM::$DB_SERVER.';dbname='. SUM::$DB_NAME .';charset=utf8', SUM::$DB_USER, SUM::$DB_PASSWORD);
	$db->exec("SET GLOBAL general_log = 1;");
	$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	$__db = $db;


	class DBDate{
		public static function toReadable($date, $with_time = false){
			$_date = new DateTime($date);
			if ($date == '0000-00-00')
				$date = 'Не указана';
			else{
				$date = strtotime($date);
				$months = array('января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря');
				$date = date("j",$date).' '.$months[date("n",$date)-1].' '.date("Y",$date);
				if ($with_time){
					$date = $date . ' ' .$_date->format('H:i:s');
				}
			}
			return $date;
		}
	}

/**
 * Array2XML: A class to convert array in PHP to XML
 * It also takes into account attributes names unlike SimpleXML in PHP
 * It returns the XML in form of DOMDocument class for further manipulation.
 * It throws exception if the tag name or attribute name has illegal chars.
 *
 * Author : Lalit Patel
 * Website: http://www.lalit.org/lab/convert-php-array-to-xml-with-attributes
 * License: Apache License 2.0
 *          http://www.apache.org/licenses/LICENSE-2.0
 * Version: 0.1 (10 July 2011)
 * Version: 0.2 (16 August 2011)
 *          - replaced htmlentities() with htmlspecialchars() (Thanks to Liel Dulev)
 *          - fixed a edge case where root node has a false/null/0 value. (Thanks to Liel Dulev)
 * Version: 0.3 (22 August 2011)
 *          - fixed tag sanitize regex which didn't allow tagnames with single character.
 * Version: 0.4 (18 September 2011)
 *          - Added support for CDATA section using @cdata instead of @value.
 * Version: 0.5 (07 December 2011)
 *          - Changed logic to check numeric array indices not starting from 0.
 * Version: 0.6 (04 March 2012)
 *          - Code now doesn't @cdata to be placed in an empty array
 * Version: 0.7 (24 March 2012)
 *          - Reverted to version 0.5
 * Version: 0.8 (02 May 2012)
 *          - Removed htmlspecialchars() before adding to text node or attributes.
 *
 * Usage:
 *       $xml = Array2XML::createXML('root_node_name', $php_array);
 *       echo $xml->saveXML();
 */

class Array2XML {

	private static $xml = null;
	private static $encoding = 'UTF-8';

	/**
	 * Initialize the root XML node [optional]
	 * @param $version
	 * @param $encoding
	 * @param $format_output
	 */
	public static function init($version = '1.0', $encoding = 'UTF-8', $format_output = true) {
		self::$xml = new DomDocument($version, $encoding);
		self::$xml->formatOutput = $format_output;
		self::$encoding = $encoding;
	}

	/**
	 * Convert an Array to XML
	 * @param string $node_name - name of the root node to be converted
	 * @param array $arr - aray to be converterd
	 * @return DomDocument
	 */
	public static function &createXML($node_name, $arr=array()) {
		$xml = self::getXMLRoot();
		$xml->appendChild(self::convert($node_name, $arr));

		self::$xml = null;    // clear the xml node in the class for 2nd time use.
		return $xml;
	}

	/**
	 * Convert an Array to XML
	 * @param string $node_name - name of the root node to be converted
	 * @param array $arr - aray to be converterd
	 * @return DOMNode
	 */
	private static function &convert($node_name, $arr=array()) {

		//print_arr($node_name);
		$xml = self::getXMLRoot();
		if ($node_name == 'response'){
			$node = $xml->createElementNS('http://my.guu.ru/api/moodle/grades', $node_name);
		}else{
			$node = $xml->createElement($node_name);
		}

		if(is_array($arr)){
			// get the attributes first.;
			if(isset($arr['@attributes'])) {
				foreach($arr['@attributes'] as $key => $value) {
					if(!self::isValidTagName($key)) {
						throw new Exception('[Array2XML] Illegal character in attribute name. attribute: '.$key.' in node: '.$node_name);
					}
					$node->setAttribute($key, self::bool2str($value));
				}
				unset($arr['@attributes']); //remove the key from the array once done.
			}

			// check if it has a value stored in @value, if yes store the value and return
			// else check if its directly stored as string
			if(isset($arr['@value'])) {
				$node->appendChild($xml->createTextNode(self::bool2str($arr['@value'])));
				unset($arr['@value']);    //remove the key from the array once done.
				//return from recursion, as a note with value cannot have child nodes.
				return $node;
			} else if(isset($arr['@cdata'])) {
				$node->appendChild($xml->createCDATASection(self::bool2str($arr['@cdata'])));
				unset($arr['@cdata']);    //remove the key from the array once done.
				//return from recursion, as a note with cdata cannot have child nodes.
				return $node;
			}
		}

		//create subnodes using recursion
		if(is_array($arr)){
			// recurse to get the node for that key
			foreach($arr as $key=>$value){
				if(!self::isValidTagName($key)) {
					throw new Exception('[Array2XML] Illegal character in tag name. tag: '.$key.' in node: '.$node_name);
				}
				if(is_array($value) && is_numeric(key($value))) {
					// MORE THAN ONE NODE OF ITS KIND;
					// if the new array is numeric index, means it is array of nodes of the same kind
					// it should follow the parent key name
					foreach($value as $k=>$v){
						$node->appendChild(self::convert($key, $v));
					}
				} else {
					// ONLY ONE NODE OF ITS KIND
					$node->appendChild(self::convert($key, $value));
				}
				unset($arr[$key]); //remove the key from the array once done.
			}
		}

		// after we are done with all the keys in the array (if it is one)
		// we check if it has any text value, if yes, append it.
		if(!is_array($arr)) {
			$node->appendChild($xml->createTextNode(self::bool2str($arr)));
		}

		return $node;
	}

	/*
	 * Get the root XML node, if there isn't one, create it.
	 */
	private static function getXMLRoot(){
		if(empty(self::$xml)) {
			self::init();
		}
		return self::$xml;
	}

	/*
	 * Get string representation of boolean value
	 */
	private static function bool2str($v){
		//convert boolean to text value.
		$v = $v === true ? 'true' : $v;
		$v = $v === false ? 'false' : $v;
		return $v;
	}

	/*
	 * Check if the tag name or attribute name contains illegal characters
	 * Ref: http://www.w3.org/TR/xml/#sec-common-syn
	 */
	private static function isValidTagName($tag){
		$pattern = '/^[a-z_]+[a-z0-9\:\-\.\_]*[^:]*$/i';
		return preg_match($pattern, $tag, $matches) && $matches[0] == $tag;
	}
}

/*
 *
 *
 * */
	class Result{
		private $status;
		private $text;
		private $data;
		private $error_code;
		private $format;

		/*
		 * */
		public function __construct($status, $text = '', $data = '', $error_code = 0, $format = 'json'){
			$this->setStatus($status);
			$this->setText($text);
			$this->setErrorCode($error_code);
			$this->setData($data);
			$this->setFormat($format);
		}

		public function setStatus($status){
			$this->status = ($status == true || $status == 1) ? true : false;
		}

		public function setText($text){
			if (empty($text)){
				$text = $this->status ? 'Данные успешно получены' : 'Извините, произошла ошибка';
			}
			$this->text = $text;
		}

		public function setErrorCode($error_code){
			$this->error_code = $error_code;
		}

		public function setData($data){
			$this->data = $data;
		}

		public function getData(){
			return $this->data;
		}

		public function addProp($name, $value){
			$this->data[$name] = $value;
		}

		public function __toString(){
			if ($this->format == 'json'){
				header("Content-Type: application/json");
				$arr = array('status' => $this->status, 'text' => $this->text, 'data' => $this->data);
				return json_encode($arr);
			}else{
				header("Content-Type: application/xml");
				$arr = array('status' => $this->status, 'text' => $this->text);
				if ($this->data != null){
					$arr['data'] = $this->data;
				}
				$xmlStr = Array2XML::createXML('response', $arr);
				return $xmlStr->saveXML();
			}
		}

		public function setFormat($format) {
			$this->format = $format == 'xml' ? 'xml' : 'json';
		}
	}

	abstract class AbstractException extends \Exception{
		protected $user_message;
		private $db;

		public function __construct($message, PDO $db, $user_m = ''){
			parent::__construct($message);
			$this->user_message = $user_m;
			$this->db = $db;
		}
		public function write(){
			$this->db->prepare('');
		}

		public function getUserMessage(){
			return $this->user_message;
		}

		public function __toString(){
			return "errorMessage: {$this->user_message} \n\r
				errorCode: {$this->getCode()}";
		}
	}

	class DBQueryException extends AbstractException{
		public function __construct($message, \PDO $db, $user_message = '', $file = '', $file_line = 0){
			if (is_array($message)){
				$message = implode(';', $message);
			}elseif ($message == '' || $message == null){
				$message = implode(';', $db->errorInfo());
			}
			if ($user_message == ''){
				$this->user_message = 'Извините, произошла ошибка. Повторите, пожалуйста, еще раз';
			}else{
				$this->user_message = $user_message;
			}
			$description = "FILE:\n {$this->getFile()}\n LINE: \n {$this->getLine()}\n Description: \n {$this->message}.\n Trace: \n {$this->getTraceAsString()}";
			$prep = $db->prepare('INSERT INTO errors(type, id, description, status) VALUES(:type, :id, :description, :status)');
			$prep->execute(array(':type' => get_class(), ':id' => '0', ':description' => $description, ':status' => 1));
			parent::__construct($message, $db, $file, $file_line);
		}
	}

	class PrivilegesException extends AbstractException{
		public function __construct($message = 'Извините, у вас нет прав для данного действия', PDO $db, $user_message = '', $file = '', $file_line = 0){
			if (is_array($message)){
				$message = implode(';', $message);
			}
			if (!$user_message){
				$this->user_message = 'Извините, произошла ошибка. Мы уже исправляем ситуацию.';
			}else{
				$this->user_message = $user_message;
			}
			parent::__construct($this->user_message, $db, $file, $file_line);
		}
	}

	class DataFormatException extends AbstractException{
		public function __construct($message, \PDO $db, $user_message ='', $file = '', $file_line = 0){
			if (is_array($message)){
				$message = implode(';', $message);
			}
			if ($user_message == ''){
				$this->user_message = 'Извините, произошла ошибка. Мы уже исправляем ситуацию.';
			}else{
				$this->user_message = $user_message;
			}
			parent::__construct($message, $db, $file, $file_line);
		}
	}

	class UserObjException extends AbstractException{
		public function __construct($message = 'Извините, но мы не смогли распознать в Вас пользователя', PDO $db, $user_message ='', $file = '', $file_line = 0){
			parent::__construct($message, $db, $file, $file_line);
		}
	}

	class InvalidFileException extends AbstractException{
		public function __construct($message = 'Извините, но мы не смогли открыть этот файл', PDO $db, $user_message ='', $file = '', $file_line = 0){
			parent::__construct($message, $db, $file, $file_line);
		}
	}
<?php

	header('Content-Type: text/html; charset=utf-8');
	@mb_internal_encoding("UTF-8");
	@session_set_cookie_params(2592000);
	@session_start();
	date_default_timezone_set('Europe/Moscow');
	$ROOT_PATH = $_SERVER['DOCUMENT_ROOT'];
	require_once 'app_config.php';

	SUM::init();

	$db = new PDO('mysql:host='. SUM::$DB_SERVER.';dbname='. SUM::$DB_NAME .';charset=utf8', SUM::$DB_USER, SUM::$DB_PASSWORD);
	$db->exec("SET GLOBAL general_log = 1;");
	$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	$__db = $db;


	class DBDate{
		public static function toReadable($date, $with_time = false){
			$_date = new DateTime($date);
			if ($date == '0000-00-00')
				$date = 'Не указана';
			else{
				$date = strtotime($date);
				$months = array('января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря');
				$date = date("j",$date).' '.$months[date("n",$date)-1].' '.date("Y",$date);
				if ($with_time){
					$date = $date . ' ' .$_date->format('H:i:s');
				}
			}
			return $date;
		}
	}
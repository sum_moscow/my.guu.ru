<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 08.08.2015
 * Time: 18:17
 */

require_once 'C:\Workspace\my.guu.ru\backend\dormitories\Class.DormContract.php';
class DormContractTest extends PHPUnit_Framework_TestCase {
    /**
     * @var DormContract
     */
    private $contract;
    private $id = 5;
    private $number = 'rer434';
    private $resident_id = 1;
    private $type_id = 1;
    private $type_description = 'коммуналка';
    private $amount = 100;
    private $sign_date = '2015-09-09';
    private $expiry_date = '2016-09-09';
    private $is_paid = 0;
    private $active = 1;

    public function setUp(){
        $this->contract = new DormContract(
            $this->id,
            $this->number,
            $this->resident_id,
            $this->type_id,
            $this->type_description,
            $this->amount,
            $this->sign_date,
            $this->expiry_date,
            $this->is_paid,
            $this->active
        );
    }
    public function testConstruct(){
        $this->assertEquals($this->id, $this->contract->getId());
        $this->assertEquals($this->number, $this->contract->getNumber());
        $this->assertEquals($this->resident_id, $this->contract->getResidentId());
        $this->assertEquals($this->type_id, $this->contract->getTypeId());
        $this->assertEquals($this->type_description, $this->contract->getTypeDescription());
        $this->assertEquals($this->amount, $this->contract->getAmount());
        $this->assertEquals($this->sign_date, $this->contract->getSignDate());
        $this->assertEquals($this->expiry_date, $this->contract->getExpiryDate());
        $this->assertEquals($this->is_paid, $this->contract->getIsPaid());
        $this->assertEquals($this->active, $this->contract->getActive());
    }
    public function testCreateFromArray(){
        $data = [
            'number' => $this->number,
            'resident_id' => $this->resident_id,
            'type_id' => $this->type_id,
        ];
        $contract = DormContract::createFromArray(null, $data);
        $this->assertEquals(null, $contract->getId());
        $this->assertEquals($this->number, $contract->getNumber());
        $this->assertEquals($this->resident_id, $contract->getResidentId());
        $this->assertEquals($this->type_id, $contract->getTypeId());
    }
    public function testCreateConstraint(){
        $this->setExpectedException('InvalidArgumentException');
        $data = [];
        $contract = DormContract::createFromArray(null, $data);
        $this->assertEquals(null, $contract->getId());
    }
    public function testIsActive(){
        $this->assertEquals(true, $this->contract->getActive());
    }
    public function testGetInfo(){
        $res = $this->contract->getInfo();
        $this->assertEquals($this->id, $res['id']);
        $this->assertEquals($this->number, $res['number']);
        $this->assertEquals($this->resident_id, $res['resident_id']);
        $this->assertEquals($this->type_id, $res['type_id']);
        $this->assertEquals($this->type_description, $res['type_description']);
        $this->assertEquals($this->amount, $res['amount']);
        $this->assertEquals($this->sign_date, $res['sign_date']);
        $this->assertEquals($this->expiry_date, $res['expiry_date']);
        $this->assertEquals($this->is_paid, $res['is_paid']);
        $this->assertEquals($this->active, $res['active']);
    }
}
 
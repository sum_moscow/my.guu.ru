<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 09.08.2015
 * Time: 23:34
 */

require_once 'C:\Workspace\my.guu.ru\backend\students\Class.StudentWithStudyInfo.php';
class StudentWithStudyInfoTest extends PHPUnit_Framework_TestCase {
    /**
     * @var StudentWithStudyInfo
     */
    private $student;
    private $id = 9;
    private $user_id = 304;
    private $last_name = 'Лолов';
    private $first_name = 'Лалка';
    private $middle_name = 'Лолыч';
    private $sex = 1;
    private $phone_number = '82282281488';
    private $institute_name = 'Божественный Институт';
    private $institute_abbr = 'BBC';
    private $budget_form = 1;
    private $edu_form_name = 'Дневное';
    private $course = 14;
    private $year = 1941;
    private $term = 5;
    private $year_end = 1945;

    public function setUp(){
        $this->student = new StudentWithStudyInfo(
            $this->id,
            $this->user_id,
            $this->last_name,
            $this->first_name,
            $this->middle_name,
            $this->sex,
            $this->phone_number,
            $this->institute_name,
            $this->institute_abbr,
            $this->budget_form,
            $this->edu_form_name,
            $this->course,
            $this->year,
            $this->term,
            $this->year_end
        );
    }
    public function testConstruct(){
        $this->assertEquals($this->id, $this->student->getId());
        $this->assertEquals($this->user_id, $this->student->getUserId());
    }
    public function testGetInfo(){
        $res = $this->student->getInfo();
        $this->assertEquals($this->id, $res['id']);
        $this->assertEquals($this->user_id, $res['user_id']);
        $this->assertEquals($this->last_name, $res['last_name']);
        $this->assertEquals($this->first_name, $res['first_name']);
        $this->assertEquals($this->middle_name, $res['middle_name']);
        $this->assertEquals($this->sex, $res['sex']);
        $this->assertEquals($this->phone_number, $res['phone_number']);
        $this->assertEquals($this->institute_name, $res['institute_name']);
        $this->assertEquals($this->institute_abbr, $res['institute_abbr']);
        $this->assertEquals($this->budget_form, $res['budget_form']);
        $this->assertEquals($this->edu_form_name, $res['edu_form_name']);
        $this->assertEquals($this->course, $res['course']);
        $this->assertEquals($this->year, $res['year']);
        $this->assertEquals($this->term, $res['term']);
        $this->assertEquals($this->year_end, $res['year_end']);
    }
}
 
<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 09.08.2015
 * Time: 20:58
 */

require_once 'C:\Workspace\my.guu.ru\backend\dormitories\Class.DormResident.php';
class DormResidentTest extends PHPUnit_Framework_TestCase{
    /**
     * @var DormResident
     */
    private $resident;
    private $id = 15;
    private $student_id = 10040;
    private $phone_number = '89008007060';
    private $application_number = 54;
    private $application_date = '2006-04-12';
    private $application_document = 'document';
    private $room_id = 1;
    private $status_id = 1;
    private $status_description = '';
    private $status_reason = 'лалка';
    private $came_from = 'Нижневартовск';
    private $distance_from_Moscow = 100500;
    private $note = 'заметочка';
    public function setUp(){
        $this->resident = new DormResident(
            $this->id,
            $this->student_id,
            $this->phone_number,
            $this->application_number,
            $this->application_date,
            $this->application_document,
            $this->room_id,
            $this->status_id,
            $this->status_description,
            $this->status_reason,
            $this->came_from,
            $this->distance_from_Moscow,
            $this->note
        );
    }
    public function testConstruct(){
        $this->assertEquals($this->id, $this->resident->getId());
        $this->assertEquals($this->student_id, $this->resident->getStudentId());
        $this->assertEquals($this->phone_number, $this->resident->getPhoneNumber());
        $this->assertEquals($this->application_number, $this->resident->getApplicationNumber());
        $this->assertEquals($this->application_date, $this->resident->getApplicationDate());
        $this->assertEquals($this->application_document, $this->resident->getApplicationDocument());
        $this->assertEquals($this->room_id, $this->resident->getRoomId());
        $this->assertEquals($this->status_id, $this->resident->getStatusId());
        $this->assertEquals($this->status_description, $this->resident->getStatusDescription());
        $this->assertEquals($this->status_reason, $this->resident->getStatusReason());
        $this->assertEquals($this->came_from, $this->resident->getCameFrom());
        $this->assertEquals($this->distance_from_Moscow, $this->resident->getDistanceFromMoscow());
        $this->assertEquals($this->note, $this->resident->getNote());
    }
    public function testCreateFromArray(){
        $data = [
            'student_id' => $this->student_id,
            'application_date' => $this->application_date,
        ];
        $resident = DormResident::createFromArray(null, $data);
        $resident->setPhoneNumber($this->phone_number);
        $resident->setApplicationNumber($this->application_number);
        $resident->setApplicationDate($this->application_date);
        $resident->setApplicationDocument($this->application_document);
        $resident->setRoomId($this->room_id);
        $resident->setStatusId($this->status_id);
        $resident->setStatusReason($this->status_reason);
        $resident->setCameFrom($this->came_from);
        $resident->setDistanceFromMoscow($this->distance_from_Moscow);
        $resident->setNote($this->note);


        $this->assertEquals(null, $resident->getId());
        $this->assertEquals($this->student_id, $resident->getStudentId());
        $this->assertEquals($this->phone_number, $resident->getPhoneNumber());
        $this->assertEquals($this->application_number, $resident->getApplicationNumber());
        $this->assertEquals($this->application_date, $resident->getApplicationDate());
        $this->assertEquals($this->application_document, $resident->getApplicationDocument());
        $this->assertEquals($this->room_id, $resident->getRoomId());
        $this->assertEquals($this->status_id, $resident->getStatusId());
        $this->assertEquals($this->status_reason, $resident->getStatusReason());
        $this->assertEquals($this->came_from, $resident->getCameFrom());
        $this->assertEquals($this->distance_from_Moscow, $resident->getDistanceFromMoscow());
        $this->assertEquals($this->note, $resident->getNote());
    }
    public function testCreateConstraint(){
        $this->setExpectedException('InvalidArgumentException');
        $data = [];
        $resident = DormResident::createFromArray(null, $data);
    }
    public function testGetInfo(){
        $res = $this->resident->getInfo();
        $this->assertEquals($this->id, $res['id']);
        $this->assertEquals($this->student_id, $res['student_id']);
        $this->assertEquals($this->phone_number, $res['phone_number']);
        $this->assertEquals($this->application_number, $res['application_number']);
        $this->assertEquals($this->application_date, $res['application_date']);
        $this->assertEquals($this->application_document, $res['application_document']);
        $this->assertEquals($this->room_id, $res['room_id']);
        $this->assertEquals($this->status_id, $res['status_id']);
        $this->assertEquals($this->status_description, $res['status_description']);
        $this->assertEquals($this->status_reason, $res['status_reason']);
        $this->assertEquals($this->came_from, $res['came_from']);
        $this->assertEquals($this->distance_from_Moscow, $res['distance_from_Moscow']);
        $this->assertEquals($this->note, $res['note']);
    }
    public function testSetPhoneNumber(){
        $this->setExpectedException('InvalidArgumentException');
        $this->resident->setPhoneNumber('9008007060');
    }
}
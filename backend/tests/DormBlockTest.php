<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 09.08.2015
 * Time: 20:49
 */

require_once 'C:\Workspace\my.guu.ru\backend\dormitories\Class.DormBlock.php';
class DormBlockTest extends PHPUnit_Framework_TestCase {
    /**
     * @var DormBlock
     */
    private $block;
    private $id = 4;
    private $number = 405;
    private $building_id = 8;
    private $floor = 4;
    private $status_id = 1;
    //TODO description
    private $status_description = '';
    private $area = 16;
    private $places_count = 4;
    private $places_reserved_count = 1;
    private $residents_count = 1;
    private $places_available_count = 2;
    
    public function setUp(){
        $this->block = new DormBlock(
            $this->id,
            $this->number,
            $this->building_id,
            $this->floor,
            $this->status_id,
            $this->area,
            $this->places_count,
            $this->places_reserved_count,
            $this->residents_count,
            $this->places_available_count
        );
    }
    public function testConstruct(){
        $this->assertEquals($this->id, $this->block->getId());
        $this->assertEquals($this->number, $this->block->getNumber());
        $this->assertEquals($this->building_id, $this->block->getBuildingId());
        $this->assertEquals($this->floor, $this->block->getFloor());
        $this->assertEquals($this->status_id, $this->block->getStatusId());
        $this->assertEquals($this->area, $this->block->getArea());
        $this->assertEquals($this->places_count, $this->block->getPlacesCount());
        $this->assertEquals($this->places_reserved_count, $this->block->getPlacesReservedCount());
        $this->assertEquals($this->residents_count, $this->block->getResidentsCount());
        $this->assertEquals($this->places_available_count, $this->block->getPlacesAvailableCount());
    }
    public function testCreateFromArray(){
        $data = [
            'number' => $this->number,
            'building_id' => $this->building_id,
            'floor' => $this->floor,
        ];
        $block = DormBlock::createFromArray(null, $data);
        $block->setStatusId($this->status_id);

        $this->assertEquals(null, $block->getId());
        $this->assertEquals($this->number, $block->getNumber());
        $this->assertEquals($this->building_id, $block->getBuildingId());
        $this->assertEquals($this->floor, $block->getFloor());
        $this->assertEquals($this->status_id, $block->getStatusId());
    }
    public function testCreateConstraint(){
        $this->setExpectedException('InvalidArgumentException');
        $data = [];
        $block = DormBlock::createFromArray(null, $data);
    }
    public function testGetInfo(){
        $res = $this->block->getInfo();
        $this->assertEquals($this->id, $res['id']);
        $this->assertEquals($this->number, $res['number']);
        $this->assertEquals($this->building_id, $res['building_id']);
        $this->assertEquals($this->floor, $res['floor']);
        $this->assertEquals($this->status_id, $res['status_id']);
        $this->assertEquals($this->area, $res['area']);
        $this->assertEquals($this->places_count, $res['places_count']);
        $this->assertEquals($this->places_reserved_count, $res['places_reserved_count']);
        $this->assertEquals($this->residents_count, $res['residents_count']);
        $this->assertEquals($this->places_available_count, $res['places_available_count']);
    }
}
 
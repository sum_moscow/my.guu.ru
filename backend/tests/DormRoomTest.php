<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 09.08.2015
 * Time: 17:28
 */

require_once 'C:\Workspace\my.guu.ru\backend\dormitories\Class.DormRoom.php';
class DormRoomTest extends PHPUnit_Framework_TestCase{
    /**
     * @var DormRoom
     */
    private $room;
    private $id = 5;
    private $number = 1;
    private $block_id = 2;
    private $room_area = 10.2;
    private $function = '';
    private $status_id = 1;
    private $status_description = '';
    private $places_count = 4;
    private $places_reserved_count = 1;
    private $residents_count = 3;
    private $places_available_count = 0;

    public function setUp(){
        $this->room = new DormRoom(
            $this->id,
            $this->number,
            $this->block_id,
            $this->room_area,
            $this->function,
            $this->status_id,
            $this->status_description,
            $this->places_count,
            $this->places_reserved_count,
            $this->residents_count,
            $this->places_available_count
        );
    }
    public function testConstruct(){
        $this->assertEquals($this->id, $this->room->getId());
        $this->assertEquals($this->number, $this->room->getNumber());
        $this->assertEquals($this->block_id, $this->room->getBlockId());
        $this->assertEquals($this->room_area, $this->room->getRoomArea());
        $this->assertEquals($this->function, $this->room->getFunction());
        $this->assertEquals($this->status_id, $this->room->getStatusId());
        $this->assertEquals($this->status_description, $this->room->getStatusDescription());
        $this->assertEquals($this->places_count, $this->room->getPlacesCount());
        $this->assertEquals($this->places_reserved_count, $this->room->getPlacesReservedCount());
        $this->assertEquals($this->residents_count, $this->room->getResidentsCount());
        $this->assertEquals($this->places_available_count, $this->room->getPlacesAvailableCount());
    }
    public function testCreateFromArray(){
        $data = [
            'number' => $this->number,
            'block_id' => $this->block_id,
            'places_count' => $this->places_count,
        ];
        $room = DormRoom::createFromArray(null, $data);
        $room->setRoomArea($this->room_area);
        $room->setFunction($this->function);
        $room->setStatusId($this->status_id);
        $room->setPlacesReservedCount($this->places_reserved_count);

        $this->assertEquals(null, $room->getId());
        $this->assertEquals($this->number, $room->getNumber());
        $this->assertEquals($this->block_id, $room->getBlockId());
        $this->assertEquals($this->places_count, $room->getPlacesCount());
        $this->assertEquals($this->room_area, $room->getRoomArea());
        $this->assertEquals($this->function, $room->getFunction());
        $this->assertEquals($this->status_id, $room->getStatusId());
        $this->assertEquals($this->places_reserved_count, $room->getPlacesReservedCount());
        $this->assertEquals(0, $room->getResidentsCount());
        $this->assertEquals(0, $room->getPlacesAvailableCount());
    }
    public function testCreateConstraint(){
        $this->setExpectedException('InvalidArgumentException');
        $data = [];
        $room = DormRoom::createFromArray(null, $data);
    }
    public function testIsActive(){
        $this->assertEquals(false, $this->room->isAvailable());
    }
    public function testGetInfo(){
        $res = $this->room->getInfo();
        $this->assertEquals($this->id, $res['id']);
        $this->assertEquals($this->number, $res['number']);
        $this->assertEquals($this->block_id, $res['block_id']);
        $this->assertEquals($this->room_area, $res['room_area']);
        $this->assertEquals($this->function, $res['function']);
        $this->assertEquals($this->status_id, $res['status_id']);
        $this->assertEquals($this->status_description, $res['status_description']);
        $this->assertEquals($this->places_count, $res['places_count']);
        $this->assertEquals($this->places_reserved_count, $res['places_reserved_count']);
        $this->assertEquals($this->residents_count, $res['residents_count']);
        $this->assertEquals($this->places_available_count, $res['places_available_count']);
    }
}
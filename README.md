Добро пожаловать в репозиторий my.guu.ru.

Чтобы перейти непосредственно к личному кабинету - нажмите [ТУТ](https://my.guu.ru)

Личный кабинет работает в режиме Beta теста.


Полезные ссылки:
[Wiki проекта](https://bitbucket.org/sum_moscow/my.guu.ru/wiki/Home)
[Исходные коды](https://bitbucket.org/sum_moscow/my.guu.ru/src)
[Обновления](https://bitbucket.org/sum_moscow/my.guu.ru/commits)

## О проблемах с личным кабинетом сообщайте на email my@guu.ru ##

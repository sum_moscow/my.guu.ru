# ALTER TABLE timetable_subjects ADD has_project BOOLEAN DEFAULT 0 NULL;
#
# DROP TABLE brs_project_marks;
# DROP TABLE brs_project_features;
# DROP TABLE brs_projects;
#
# CREATE TABLE `brs_project_features` (
#   `id`                   INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
#   `timetable_subject_id` INT(11) UNSIGNED NOT NULL,
#   `name`                 TEXT,
#   `description`          TEXT,
#   `group_number`         INT(3) UNSIGNED  NOT NULL,
#   `max_rating`           INT(11)          NOT NULL,
#   `status`               TINYINT(1)       NOT NULL DEFAULT '1',
#   `created_at`           DATETIME         NOT NULL,
#   `updated_at`           TIMESTAMP        NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#   PRIMARY KEY (`id`),
#   CONSTRAINT `brs_project_features_ibfk_1` FOREIGN KEY (`timetable_subject_id`) REFERENCES `timetable_subjects` (`id`)
# )
#   ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;
#
# CREATE TABLE `brs_project_marks` (
#   `id`          INT(11) UNSIGNED    NOT NULL AUTO_INCREMENT,
#   `feature_id`  INT(11) UNSIGNED    NOT NULL,
#   `student_id`  INT(11) UNSIGNED    NOT NULL,
#   `chance_type` ENUM('1', '2', '3') NOT NULL DEFAULT '1',
#   `chance_date` DATETIME                     DEFAULT NULL,
#   `created_at`  DATETIME            NOT NULL,
#   `updated_at`  TIMESTAMP           NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#   PRIMARY KEY (`id`),
#   UNIQUE KEY `feature_id` (`feature_id`, `student_id`, `chance_type`),
#   CONSTRAINT `brs_project_marks_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `brs_project_features` (`id`),
#   CONSTRAINT `brs_project_marks_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`)
# )
#   ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;
#
# CREATE TABLE brs_projects_themes (
#   `id`                 INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
#   timetable_subject_id INT(11) UNSIGNED NOT NULL,
#   `student_id`         INT(11) UNSIGNED NOT NULL,
#   theme                TEXT,
#   `created_at`         DATETIME         NOT NULL,
#   `updated_at`         TIMESTAMP        NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#   PRIMARY KEY (`id`),
#   UNIQUE KEY (`timetable_subject_id`, `student_id`),
#   FOREIGN KEY (`student_id`) REFERENCES `students` (`id`)
# )
#   ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;
#
# CREATE TABLE `dorm_rooms_statuses` (
#   `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
#   `room_status` VARCHAR(255)     NOT NULL DEFAULT '',
#   `created_at`  DATETIME         NOT NULL,
#   `updated_at`  TIMESTAMP        NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#   PRIMARY KEY (`id`),
#   UNIQUE KEY `room_status` (`room_status`)
# )
#   ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;
#
# ALTER TABLE `dorm_rooms` DROP `status`;
# ALTER TABLE `dorm_rooms` ADD `status_id` INT(11) UNSIGNED NULL
# AFTER places_reserved_count;
# ALTER TABLE `dorm_rooms` ADD CONSTRAINT `dorm_rooms_fk_2` FOREIGN KEY (`status_id`) REFERENCES `dorm_rooms_statuses` (`id`);
#
# ALTER TABLE `dorm_blocks` ADD `status_id` INT(11) UNSIGNED NULL
# AFTER floor;
# ALTER TABLE `dorm_blocks` ADD CONSTRAINT `dorm_blocks_fk_2` FOREIGN KEY (`status_id`) REFERENCES `dorm_rooms_statuses` (`id`);
#
# ALTER TABLE brs_project_marks ADD mark FLOAT DEFAULT NULL  NULL;
#
# ALTER TABLE brs_control_events ADD FOREIGN KEY (professor_id) REFERENCES staff (id);
#
# ALTER TABLE users_roles ADD main_role BOOLEAN DEFAULT TRUE  NOT NULL;
#
# DROP VIEW view_users;
#
# CREATE VIEW `view_users` AS
#   SELECT
#     `users`.`id`                                  AS `id`,
#     `users`.`first_name`                          AS `first_name`,
#     `staff`.`1c_id`                               AS `1c_id`,
#     `users`.`ad_login`                            AS `ad_login`,
#     `users`.`middle_name`                         AS `middle_name`,
#     `users`.`last_name`                           AS `last_name`,
#     `roles`.`name`                                AS `role`,
#     `roles`.`ou_id`                               AS `ou_id`,
#     `users`.`active`                              AS `active`,
#     if(isnull(`staff`.`id`), 0, 1)                AS `is_staff`,
#     if(isnull(`students`.`id`), 0, 1)             AS `is_student`,
#     if(isnull(`view_professors`.`user_id`), 0, 1) AS `is_professor`,
#     `students`.`begin_study`                      AS `begin_study`,
#     `students`.`id`                               AS `student_id`,
#     `students`.`personal_file`                    AS `personal_file`,
#     `students`.`phone_number`                     AS `phone_number`,
#     `view_edu_groups`.`academic_department_id`    AS `academic_department_id`,
#     `view_edu_groups`.`edu_specialty_id`          AS `edu_specialty_id`,
#     `view_edu_groups`.`profile`                   AS `profile`,
#     `view_edu_groups`.`profile_code`              AS `profile_code`,
#     `view_edu_groups`.`program`                   AS `program`,
#     `view_edu_groups`.`program_code`              AS `program_code`,
#     `view_edu_groups`.`institute_id`              AS `institute_id`,
#     `view_edu_groups`.`course`                    AS `course`,
#     `view_edu_groups`.`edu_form_id`               AS `edu_form_id`,
#     `view_edu_groups`.`edu_program_id`            AS `edu_program_id`,
#     `view_edu_groups`.`group`                     AS `group`,
#     `view_edu_groups`.`year`                      AS `year`,
#     `view_edu_groups`.`semester`                  AS `semester`,
#     `view_edu_groups`.`edu_form_name`             AS `edu_form_name`,
#     `view_edu_groups`.`edu_form_code`             AS `edu_form_code`,
#     `view_edu_groups`.`edu_qualification_id`      AS `edu_qualification_id`,
#     `view_edu_groups`.`edu_speciality_name`       AS `edu_speciality_name`,
#     `view_edu_groups`.`edu_speciality_code`       AS `edu_speciality_code`,
#     `view_edu_groups`.`edu_qualification_name`    AS `edu_qualification_name`,
#     `view_edu_groups`.`edu_qualification_code`    AS `edu_qualification_code`,
#     `view_edu_groups`.`institute_code`            AS `institute_code`,
#     `view_edu_groups`.`institute_abbr`            AS `institute_abbr`,
#     `view_edu_groups`.`institute_name`            AS `institute_name`,
#     `view_edu_groups`.`edu_group_id`              AS `edu_group_id`,
#     `semesters_info`.`budget_form`                AS `budget_form`,
#     `semesters_info`.`enrollment_date`            AS `enrollment_date`,
#     `staff`.`id`                                  AS `staff_id`,
#     `view_professors`.`professor_id`              AS `professor_id`
#   FROM ((((((((`users`
#     LEFT JOIN `staff` ON ((`users`.`id` = `staff`.`user_id`))) LEFT JOIN `users_roles`
#       ON ((`users`.`id` = `users_roles`.`user_id` AND users_roles.main_role = 1))) LEFT JOIN `roles`
#       ON ((`roles`.`id` = `users_roles`.`role_id`))) LEFT JOIN `organizational_units`
#       ON ((`organizational_units`.`id` = `roles`.`ou_id`))) LEFT JOIN `students`
#       ON ((`students`.`user_id` = `users`.`id`))) LEFT JOIN `view_professors`
#       ON ((`view_professors`.`user_id` = `users`.`id`))) LEFT JOIN `semesters_info`
#       ON ((`students`.`id` = `semesters_info`.`student_id`))) LEFT JOIN `view_edu_groups`
#       ON ((`view_edu_groups`.`edu_group_id` = `semesters_info`.`edu_group_id`)))
#   WHERE users_roles.active = 1 OR students.id IS NOT NULL;
#
#
# DROP VIEW view_professors;
#
# CREATE VIEW `view_professors` AS
#   SELECT DISTINCT
#     `users`.`id` AS `user_id`,
#     `users`.`id` AS `professor_id`
#   FROM ((`staff`
#     JOIN `memberof` ON ((`memberof`.`user_id` = `staff`.`user_id`))) JOIN `users`
#       ON ((`users`.`id` = `staff`.`user_id`)))
#   WHERE (`memberof`.`group_id` = 2 OR `memberof`.`group_id` = 3);
#
#
# ALTER TABLE staff ADD courses_info TEXT DEFAULT NULL NULL;
# ALTER TABLE staff ADD education_info TEXT DEFAULT NULL NULL;
#
#
# CREATE TABLE sync_staff(
#   id INT(11) UNSIGNED AUTO_INCREMENT,
#   start_time DATETIME NULL,
#   finish_time DATETIME NULL,
#   errors_count INT UNSIGNED DEFAULT 0,
#   `created_at`  DATETIME         NOT NULL,
#   `updated_at`  TIMESTAMP        NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#   PRIMARY KEY (id)
# )
#   ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;
#
#
# CREATE TABLE science_verifications(
#   id INT(11) UNSIGNED AUTO_INCREMENT,
#   verified BOOLEAN DEFAULT FALSE,
#   staff_id INT(11) UNSIGNED NOT NULL,
#   period_id INT(11) UNSIGNED NOT NULL,
#   verified_by_staff_id INT(11) UNSIGNED NOT NULL,
#   `created_at`  DATETIME         NOT NULL,
#   `updated_at`  TIMESTAMP        NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#   PRIMARY KEY (id),
#   UNIQUE KEY (staff_id, period_id),
#   FOREIGN KEY (staff_id) REFERENCES staff(id),
#   FOREIGN KEY (period_id) REFERENCES science_periods(id),
#   FOREIGN KEY (verified_by_staff_id) REFERENCES staff(id)
# )
#   ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;;
#


#UPDATE users
#SET passport_id=NULL
#WHERE id IN (44485, 46169, 46564, 45909, 49373, 49298, 43937, 43936);
#update users
#SET passport_id = 6869
#WHERE id = 51859;
#update users
#SET passport_id = 5521
#WHERE id = 50297;
#update users
#SET passport_id = 3462
#WHERE id = 51667;
#update users
#SET passport_id = 9479
#WHERE id = 52223;
#DELETE FROM passports WHERE id IN (3383, 8957, 4038, 7587, 6660, 6735, 9149, 9488, 9604, 9601);
#ALTER TABLE passports ADD CONSTRAINT uk_passport UNIQUE (series ,number);
#ALTER TABLE users
#ADD FOREIGN KEY (passport_id)
#REFERENCES passports(id);
#
#ALTER TABLE dorm_residents ADD phone_number VARCHAR(255);
#INSERT INTO groups ( name, module, level_in_module, only_manually, created_at ) VALUES ('admin', 'students', 99, 1, NOW());
#INSERT INTO groups ( name, module, level_in_module, only_manually, created_at ) VALUES ('admin', 'student_groups', 99, 1, NOW());
#UPDATE groups SET only_manually=1 WHERE name LIKE 'admin';
##ABOVE IS DONE ON 03.06.2015
#
##ALTER TABLE groups ADD only_manually BOOLEAN DEFAULT FALSE NOT NULL;
#ALTER TABLE memberof ADD added_manually BOOLEAN DEFAULT FALSE  NOT NULL;
#ALTER TABLE academic_departments ADD additional_name TEXT DEFAULT NULL NULL;
#
#UPDATE academic_departments
#SET additional_name = 'Кафедра государственного и муниципального управления'
#WHERE id = 18;
#UPDATE academic_departments
#SET additional_name = 'Кафедра мировой экономики'
#WHERE id = 139;
#UPDATE academic_departments
#SET additional_name = 'Кафедра упр-ния в сфере культуры,кино,телевидения и инд.развлечений'
#WHERE id = 121;
#UPDATE academic_departments
#SET additional_name = 'Кафедра управления инновациями'
#WHERE id = 152;
#UPDATE academic_departments
#SET additional_name = 'Кафедра экономики труда и управления социально-экономическими отнош.'
#WHERE id = 149;
#UPDATE academic_departments
#SET additional_name = 'Кафедра управления природопользованием и экологической безопасностью'
#WHERE id = 14;
#
#ALTER TABLE timetable CHANGE room room VARCHAR(50);
#ALTER TABLE timetable MODIFY room VARCHAR(50) DEFAULT NULL;
#ALTER TABLE timetable MODIFY COLUMN room VARCHAR(50) NULL;
#
#ALTER TABLE subjects MODIFY COLUMN name VARCHAR(250) NOT NULL;
#

#ABOVE IS DONE ON 04.06.2015
/*
ALTER TABLE timetable CHANGE room room VARCHAR(50);
ALTER TABLE timetable MODIFY room VARCHAR(50) DEFAULT NULL;
ALTER TABLE timetable MODIFY COLUMN room VARCHAR(50) NULL;

ALTER TABLE subjects MODIFY COLUMN name VARCHAR(250) NOT NULL;

DROP VIEW view_users;
CREATE VIEW `view_users` AS
  SELECT DISTINCT
    `users`.`id`                                  AS `id`,
    `users`.`first_name`                          AS `first_name`,
    `staff`.`1c_id`                               AS `1c_id`,
    `users`.`ad_login`                            AS `ad_login`,
    `users`.`middle_name`                         AS `middle_name`,
    `users`.`last_name`                           AS `last_name`,
    `roles`.`name`                                AS `role`,
    `roles`.`ou_id`                               AS `ou_id`,
    `users`.`active`                              AS `active`,
    if(isnull(`staff`.`id`), 0, 1)                AS `is_staff`,
    if(isnull(`students`.`id`), 0, 1)             AS `is_student`,
    if(isnull(`view_professors`.`user_id`), 0, 1) AS `is_professor`,
    `students`.`begin_study`                      AS `begin_study`,
    `students`.`id`                               AS `student_id`,
    `students`.`personal_file`                    AS `personal_file`,
    `students`.`phone_number`                     AS `phone_number`,
    `view_edu_groups`.`academic_department_id`    AS `academic_department_id`,
    `view_edu_groups`.`edu_specialty_id`          AS `edu_specialty_id`,
    `view_edu_groups`.`profile`                   AS `profile`,
    `view_edu_groups`.`profile_code`              AS `profile_code`,
    `view_edu_groups`.`program`                   AS `program`,
    `view_edu_groups`.`program_code`              AS `program_code`,
    `view_edu_groups`.`institute_id`              AS `institute_id`,
    `view_edu_groups`.`course`                    AS `course`,
    `view_edu_groups`.`edu_form_id`               AS `edu_form_id`,
    `view_edu_groups`.`edu_program_id`            AS `edu_program_id`,
    `view_edu_groups`.`group`                     AS `group`,
    `view_edu_groups`.`year`                      AS `year`,
    `view_edu_groups`.`semester`                  AS `semester`,
    `view_edu_groups`.`edu_form_name`             AS `edu_form_name`,
    `view_edu_groups`.`edu_form_code`             AS `edu_form_code`,
    `view_edu_groups`.`edu_qualification_id`      AS `edu_qualification_id`,
    `view_edu_groups`.`edu_speciality_name`       AS `edu_speciality_name`,
    `view_edu_groups`.`edu_speciality_code`       AS `edu_speciality_code`,
    `view_edu_groups`.`edu_qualification_name`    AS `edu_qualification_name`,
    `view_edu_groups`.`edu_qualification_code`    AS `edu_qualification_code`,
    `view_edu_groups`.`institute_code`            AS `institute_code`,
    `view_edu_groups`.`institute_abbr`            AS `institute_abbr`,
    `view_edu_groups`.`institute_name`            AS `institute_name`,
    `view_edu_groups`.`edu_group_id`              AS `edu_group_id`,
    `semesters_info`.`budget_form`                AS `budget_form`,
    `semesters_info`.`enrollment_date`            AS `enrollment_date`,
    `staff`.`id`                                  AS `staff_id`,
    `view_professors`.`professor_id`              AS `professor_id`
  FROM `users`
    LEFT JOIN `staff` ON `users`.`id` = `staff`.`user_id` LEFT JOIN `users_roles`
      ON `users`.`id` = `users_roles`.`user_id` LEFT JOIN `roles`
      ON `roles`.`id` = `users_roles`.`role_id` LEFT JOIN `organizational_units`
      ON `organizational_units`.`id` = `roles`.`ou_id` LEFT JOIN `students`
      ON `students`.`user_id` = `users`.`id` LEFT JOIN `view_professors`
      ON `view_professors`.`user_id` = `users`.`id` LEFT JOIN `semesters_info`
      ON `students`.`id` = `semesters_info`.`student_id` LEFT JOIN `view_edu_groups`
      ON `view_edu_groups`.`edu_group_id` = `semesters_info`.`edu_group_id`
  WHERE (`users_roles`.`active` = 1 AND users_roles.main_role = 1) OR (`students`.`id` IS NOT NULL);
*/
#
#CREATE TABLE `sync_gupmsr_students` (
#  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#  `created_at` datetime NOT NULL,
#  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#  `filename` varchar(50) DEFAULT NULL,
#  `file_create_date` date DEFAULT NULL,
#  `file_inday_number` int(11) unsigned NOT NULL,
#  PRIMARY KEY (`id`)
#) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
#
#CREATE TABLE `gupmsr_students_records` (
#  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#  `sync_gupmsr_students_id` int(11) unsigned NOT NULL,
#  `record_number` int(11) unsigned NOT NULL,
#  `student_id` int(11) unsigned NOT NULL,
#  `created_at` datetime NOT NULL,
#  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#  PRIMARY KEY (`id`),
#  UNIQUE KEY `sync_gupmsr_students_id` (`sync_gupmsr_students_id`,`record_number`,`student_id`),
#  KEY `student_id` (`student_id`),
#  CONSTRAINT `gupmsr_students_records_ibfk_1` FOREIGN KEY (`sync_gupmsr_students_id`) REFERENCES `sync_gupmsr_students` (`id`),
#  CONSTRAINT `gupmsr_students_records_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`)
#) ENGINE=InnoDB AUTO_INCREMENT=53902 DEFAULT CHARSET=utf8;
#
#
#CREATE TABLE brs_exams(
#  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#  student_id INT(11) UNSIGNED NOT NULL,
#  mark FLOAT,
#  professor_id INT(11) UNSIGNED NOT NULL,
#  timetable_subject_id INT(11) UNSIGNED NOT NULL,
#  PRIMARY KEY (id),
#  FOREIGN KEY(timetable_subject_id) REFERENCES timetable_subjects(id),
#  FOREIGN KEY (professor_id) REFERENCES staff(id),
#  FOREIGN KEY (student_id) REFERENCES students(id),
#  UNIQUE KEY (timetable_subject_id, student_id),
#  `created_at` datetime NOT NULL,
#  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
#)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
#
#CREATE TABLE exam_forms(
#  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#  name TEXT,
#  PRIMARY KEY (id),
#  `created_at` datetime NOT NULL,
#  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
#)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
#
#ALTER TABLE timetable_subjects ADD COLUMN exam_form_id INT(11) UNSIGNED NULL DEFAULT NULL;
#ALTER TABLE timetable_subjects ADD FOREIGN KEY (exam_form_id) REFERENCES exam_forms(id);

#ABOVE IS DONE ON 04.06.2015

#DROP VIEW view_users;
#CREATE VIEW `view_users` AS
#  SELECT DISTINCT
#    `users`.`id`                                  AS `id`,
#    `users`.`ad_login`                            AS `ad_login`,
#    `users`.`last_name`                           AS `last_name`,
#    `users`.`first_name`                          AS `first_name`,
#    `users`.`middle_name`                         AS `middle_name`,
#    `users`.`phone_number`                        AS `phone_number`,
#    `users`.`birth_date`                          AS `birth_date`,
#    if(isnull(`staff`.`id`), 0, 1)                AS `is_staff`,
#    if(isnull(`students`.`id`), 0, 1)             AS `is_student`,
#    if(isnull(`view_professors`.`user_id`), 0, 1) AS `is_professor`,
#    `staff`.`id`                                  AS `staff_id`,
#    `staff`.`1c_id`                               AS `1c_id`,
#    `roles`.`name`                                AS `role`,
#    `roles`.`ou_id`                               AS `ou_id`,
#    `users`.`active`                              AS `active`,
#    `students`.`id`                               AS `student_id`,
#    `students`.`personal_file`                    AS `personal_file`,
#    `students`.`begin_study`                      AS `begin_study`,
#    `view_edu_groups`.`academic_department_id`    AS `academic_department_id`,
#    `view_edu_groups`.`edu_specialty_id`          AS `edu_specialty_id`,
#    `view_edu_groups`.`profile`                   AS `profile`,
#    `view_edu_groups`.`profile_code`              AS `profile_code`,
#    `view_edu_groups`.`program`                   AS `program`,
#    `view_edu_groups`.`program_code`              AS `program_code`,
#    `view_edu_groups`.`institute_id`              AS `institute_id`,
#    `view_edu_groups`.`course`                    AS `course`,
#    `view_edu_groups`.`edu_form_id`               AS `edu_form_id`,
#    `view_edu_groups`.`edu_program_id`            AS `edu_program_id`,
#    `view_edu_groups`.`group`                     AS `group`,
#    `view_edu_groups`.`year`                      AS `year`,
#    `view_edu_groups`.`semester`                  AS `semester`,
#    `view_edu_groups`.`edu_form_name`             AS `edu_form_name`,
#    `view_edu_groups`.`edu_form_code`             AS `edu_form_code`,
#    `view_edu_groups`.`edu_qualification_id`      AS `edu_qualification_id`,
#    `view_edu_groups`.`edu_speciality_name`       AS `edu_speciality_name`,
#    `view_edu_groups`.`edu_speciality_code`       AS `edu_speciality_code`,
#    `view_edu_groups`.`edu_qualification_name`    AS `edu_qualification_name`,
#    `view_edu_groups`.`edu_qualification_code`    AS `edu_qualification_code`,
#    `view_edu_groups`.`institute_code`            AS `institute_code`,
#    `view_edu_groups`.`institute_abbr`            AS `institute_abbr`,
#    `view_edu_groups`.`institute_name`            AS `institute_name`,
#    `view_edu_groups`.`edu_group_id`              AS `edu_group_id`,
#    `semesters_info`.`budget_form`                AS `budget_form`,
#    `semesters_info`.`enrollment_date`            AS `enrollment_date`,
#    `view_professors`.`professor_id`              AS `professor_id`
#  FROM `users`
#    LEFT JOIN `staff` ON `users`.`id` = `staff`.`user_id` LEFT JOIN `users_roles`
#      ON `users`.`id` = `users_roles`.`user_id` LEFT JOIN `roles`
#      ON `roles`.`id` = `users_roles`.`role_id` LEFT JOIN `organizational_units`
#      ON `organizational_units`.`id` = `roles`.`ou_id` LEFT JOIN `students`
#      ON `students`.`user_id` = `users`.`id` LEFT JOIN `view_professors`
#      ON `view_professors`.`user_id` = `users`.`id` LEFT JOIN `semesters_info`
#      ON `students`.`id` = `semesters_info`.`student_id` LEFT JOIN `view_edu_groups`
#      ON `view_edu_groups`.`edu_group_id` = `semesters_info`.`edu_group_id`
#  WHERE (`users_roles`.`active` = 1 AND users_roles.main_role = 1) OR (`students`.`id` IS NOT NULL);
#
#ALTER TABLE addresses ADD COLUMN postcode VARCHAR(255) NULL AFTER flat;
#
#CREATE VIEW `view_students` AS
#  SELECT DISTINCT
#    students.id,
#    `users`.`ad_login` AS `ad_login`,
#    `users`.`last_name` AS `last_name`,
#    `users`.`first_name` AS `first_name`,
#    `users`.`middle_name` AS `middle_name`,
#    users.id AS user_id,
#    users.email AS email,
#    students.phone_number AS phone_number,
#    users.birth_date AS birth_date,
#    users.snils AS snils,
#    users.agreement,
#    view_edu_groups.edu_group_id,
#    users.registration_address_id,
#    residence_address_id,
#    passport_id
#  FROM users
#    INNER JOIN students ON users.id = students.user_id
#    INNER JOIN `semesters_info` ON `students`.`id` = `semesters_info`.`student_id`
#    INNER JOIN `view_edu_groups` ON `view_edu_groups`.`edu_group_id` = `semesters_info`.`edu_group_id`;
#
#CREATE TABLE `gupmsr_students_statuses` (
#  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#  `student_id` int(11) UNSIGNED NOT NULL,
#  `status_id` int(11) UNSIGNED NOT NULL,
#  `message` VARCHAR(255) DEFAULT '',
#  `created_at` DATETIME NOT NULL,
#  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#  PRIMARY KEY (`id`),
#  UNIQUE KEY `gupmsr_students_statuses_uk` (`student_id`),
#  CONSTRAINT `gupmsr_students_statuses_fk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
#  CONSTRAINT `gupmsr_students_statuses_fk_2` FOREIGN KEY (`status_id`) REFERENCES `gupmsr_statuses` (`id`)
#);
#
#CREATE TABLE `gupmsr_statuses` (
#  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
#  `student_status` VARCHAR(255) NOT NULL,
#  `created_at` datetime NOT NULL,
#  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#  PRIMARY KEY (`id`),
#  UNIQUE KEY `gupmsr_statuses_uk` (`student_status`)
#);
#
#CREATE TABLE `edu_groups_errors`(
#  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
#  `edu_group_id` INT(11) UNSIGNED NOT NULL,
#  `from_user_id` INT(11) UNSIGNED NOT NULL,
#  `status_id` INT(11) UNSIGNED NOT NULL,
#  `description` VARCHAR(255) DEFAULT '',
#  `reject_reason` VARCHAR(255) DEFAULT '',
#  `created_at` datetime NOT NULL,
#  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#  PRIMARY KEY (`id`),
#  CONSTRAINT `edu_groups_errors_fk_1` FOREIGN KEY (`edu_group_id`) REFERENCES `edu_groups` (`id`),
#  CONSTRAINT `edu_groups_errors_fk_2` FOREIGN KEY (`from_user_id`) REFERENCES `users` (`id`),
#  CONSTRAINT `edu_groups_errors_fk_3` FOREIGN KEY (`status_id`) REFERENCES `edu_groups_errors_statuses` (`id`)
#);
#
#CREATE TABLE `edu_groups_errors_statuses`(
#  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
#  `status` VARCHAR(255) NOT NULL,
#  `created_at` datetime NOT NULL,
#  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#  PRIMARY KEY (`id`),
#  UNIQUE KEY `edu_groups_errors_statuses_uk` (`status`)
#);
#DROP TABLE `edu_groups_errors`;
#INSERT INTO `edu_groups_errors_statuses`(status, created_at) VALUES ('На рассмотрении', NOW());
#INSERT INTO `edu_groups_errors_statuses`(status, created_at) VALUES ('Решается', NOW());
#INSERT INTO `edu_groups_errors_statuses`(status, created_at) VALUES ('Решено', NOW());
#INSERT INTO `edu_groups_errors_statuses`(status, created_at) VALUES ('Отказано', NOW());
#
#INSERT INTO gupmsr_statuses (id, student_status, created_at) VALUES (1, 'Модерация', NOW());
#INSERT INTO gupmsr_statuses (id, student_status, created_at) VALUES (2, 'Карточка отбракована', NOW());
#INSERT INTO gupmsr_statuses (id, student_status, created_at) VALUES (3, 'Карточка ожидает подписания', NOW());
#INSERT INTO gupmsr_statuses (id, student_status, created_at) VALUES (4, 'Карточка подписана', NOW());

#ABOVE IS DONE ON 23.06.2015

#ALTER TABLE students ADD UNIQUE KEY `students_uk_1` (`personal_file`);
#
#ALTER TABLE brs_classes ADD COLUMN timetable_item_id INT(11) UNSIGNED NULL DEFAULT NULL;
#ALTER TABLE brs_classes ADD FOREIGN KEY (timetable_item_id) REFERENCES timetable(id);
#ALTER TABLE brs_classes ADD COLUMN status BOOLEAN DEFAULT TRUE;
#
#RENAME TABLE gupmsr_students_statuses TO gupmsr_students_states;
#RENAME TABLE gupmsr_statuses TO gupmsr_students_statuses;
#
#CREATE OR REPLACE VIEW view_users AS
#  SELECT DISTINCT
#    `users`.`id` AS `id`,
#    `users`.`ad_login` AS `ad_login`,
#    `users`.`last_name` AS `last_name`,
#    `users`.`first_name` AS `first_name`,
#    `users`.`middle_name` AS `middle_name`,
#    `users`.`sex` AS `sex`,
#    `users`.`phone_number` AS `phone_number`,
#    `users`.`birth_date` AS `birth_date`,
#    IF(ISNULL(`staff`.`id`),
#       0,
#       1) AS `is_staff`,
#    IF(ISNULL(`students`.`id`),
#       0,
#       1) AS `is_student`,
#    IF(ISNULL(`view_professors`.`user_id`),
#       0,
#       1) AS `is_professor`,
#    `staff`.`id` AS `staff_id`,
#    `staff`.`1c_id` AS `1c_id`,
#    `roles`.`name` AS `role`,
#    `roles`.`ou_id` AS `ou_id`,
#    `users`.`active` AS `active`,
#    `students`.`id` AS `student_id`,
#    `students`.`personal_file` AS `personal_file`,
#    `students`.`begin_study` AS `begin_study`,
#    `view_edu_groups`.`academic_department_id` AS `academic_department_id`,
#    `view_edu_groups`.`edu_specialty_id` AS `edu_specialty_id`,
#    `view_edu_groups`.`profile` AS `profile`,
#    `view_edu_groups`.`profile_code` AS `profile_code`,
#    `view_edu_groups`.`program` AS `program`,
#    `view_edu_groups`.`program_code` AS `program_code`,
#    `view_edu_groups`.`institute_id` AS `institute_id`,
#    `view_edu_groups`.`course` AS `course`,
#    `view_edu_groups`.`edu_form_id` AS `edu_form_id`,
#    `view_edu_groups`.`edu_program_id` AS `edu_program_id`,
#    `view_edu_groups`.`group` AS `group`,
#    `view_edu_groups`.`year` AS `year`,
#    `view_edu_groups`.`semester` AS `semester`,
#    `view_edu_groups`.`edu_form_name` AS `edu_form_name`,
#    `view_edu_groups`.`edu_form_code` AS `edu_form_code`,
#    `view_edu_groups`.`edu_qualification_id` AS `edu_qualification_id`,
#    `view_edu_groups`.`edu_speciality_name` AS `edu_speciality_name`,
#    `view_edu_groups`.`edu_speciality_code` AS `edu_speciality_code`,
#    `view_edu_groups`.`edu_qualification_name` AS `edu_qualification_name`,
#    `view_edu_groups`.`edu_qualification_code` AS `edu_qualification_code`,
#    `view_edu_groups`.`institute_code` AS `institute_code`,
#    `view_edu_groups`.`institute_abbr` AS `institute_abbr`,
#    `view_edu_groups`.`institute_name` AS `institute_name`,
#    `view_edu_groups`.`edu_group_id` AS `edu_group_id`,
#    `semesters_info`.`budget_form` AS `budget_form`,
#    `semesters_info`.`enrollment_date` AS `enrollment_date`,
#    `view_professors`.`professor_id` AS `professor_id`
#  FROM
#    ((((((((`users`
#      LEFT JOIN `staff` ON ((`users`.`id` = `staff`.`user_id`)))
#      LEFT JOIN `users_roles` ON ((`users`.`id` = `users_roles`.`user_id`)))
#      LEFT JOIN `roles` ON ((`roles`.`id` = `users_roles`.`role_id`)))
#      LEFT JOIN `organizational_units` ON ((`organizational_units`.`id` = `roles`.`ou_id`)))
#      LEFT JOIN `students` ON ((`students`.`user_id` = `users`.`id`)))
#      LEFT JOIN `view_professors` ON ((`view_professors`.`user_id` = `users`.`id`)))
#      LEFT JOIN `semesters_info` ON ((`students`.`id` = `semesters_info`.`student_id`)))
#      LEFT JOIN `view_edu_groups` ON ((`view_edu_groups`.`edu_group_id` = `semesters_info`.`edu_group_id`)))
#  WHERE
#    (((`users_roles`.`active` = 1)
#      AND (`users_roles`.`main_role` = 1))
#     OR (`students`.`id` IS NOT NULL));
#
#ALTER TABLE dorm_rooms ADD COLUMN `number` int(1) NOT NULL AFTER id;
#CREATE OR REPLACE
#VIEW `view_students` AS
#  SELECT
#    `students`.`id`								AS `id`,
#    `users`.`ad_login`							AS `ad_login`,
#    `users`.`last_name`							AS `last_name`,
#    `users`.`first_name`						AS `first_name`,
#    `users`.`middle_name`						AS `middle_name`,
#    `users`.`id`								AS `user_id`,
#    `users`.`sex`								AS `sex`,
#    `users`.`email`								AS `email`,
#    `students`.`phone_number`					AS `phone_number`,
#    `users`.`birth_date`						AS `birth_date`,
#    `users`.`snils`								AS `snils`,
#    `users`.`agreement`							AS `agreement`,
#    `users`.`registration_address_id` 			AS `registration_address_id`,
#    `users`.`residence_address_id` 				AS `residence_address_id`,
#    `users`.`passport_id` 						AS `passport_id`,
#    `students`.`personal_file`					AS `personal_file`,
#    `students`.`begin_study`					AS `begin_study`,
#    `view_edu_groups`.`institute_id` 			AS `institute_id`,
#    `view_edu_groups`.`institute_abbr` 			AS `institute_abbr`,
#    `view_edu_groups`.`institute_name` 			AS `institute_name`,
#    `view_edu_groups`.`institute_code` 			AS `institute_code`,
#    `view_edu_groups`.`academic_department_id`	AS `academic_department_id`,
#    `view_edu_groups`.`edu_group_id` 			AS `edu_group_id`,
#    `view_edu_groups`.`course`					AS `course`,
#    `view_edu_groups`.`group`					AS `group`,
#    `view_edu_groups`.`year`					AS `year`,
#    `edu_terms`.`term`            AS `term`,
#    (`view_edu_groups`.`year` + edu_terms.term) AS year_end,
#    `semesters_info`.`budget_form` AS `budget_form`,
#    `view_edu_groups`.`edu_form_id`				AS `edu_form_id`,
#    `view_edu_groups`.`edu_form_name` 			AS `edu_form_name`,
#    `view_edu_groups`.`edu_form_code` 			AS `edu_form_code`,
#    `view_edu_groups`.`edu_qualification_id`	AS `edu_qualification_id`,
#    `view_edu_groups`.`edu_qualification_name` 	AS `edu_qualification_name`,
#    `view_edu_groups`.`edu_qualification_code` 	AS `edu_qualification_code`,
#    `view_edu_groups`.`edu_specialty_id`		AS `edu_speciality_id`,
#    `view_edu_groups`.`edu_speciality_name`		AS `edu_speciality_name`,
#    `view_edu_groups`.`edu_speciality_code`		AS `edu_speciality_code`,
#    `view_edu_groups`.`profile` 				AS `profile`,
#    `view_edu_groups`.`profile_code` 			AS `profile_code`,
#    `view_edu_groups`.`program` 				AS `program`,
#    `study_statuses`.`id`						AS `study_status_id`,
#    `study_statuses`.`name`						AS `study_status_name`,
#    `study_statuses`.`code`						AS `study_status_code`
#  FROM
#    `users`
#    JOIN `students` ON `users`.`id` = `students`.`user_id`
#    JOIN `semesters_info` ON `students`.`id` = `semesters_info`.`student_id`
#    JOIN `view_edu_groups` ON `view_edu_groups`.`edu_group_id` = `semesters_info`.`edu_group_id`
#    JOIN `study_statuses` ON `semesters_info`.`study_status_id` = `study_statuses`.`id`
#    JOIN edu_terms ON `edu_terms`.`edu_form_id` = `view_edu_groups`.`edu_form_id` AND `edu_terms`.`edu_qualification_id` = `view_edu_groups`.`edu_qualification_id`;
#
#DROP TABLE `dorm_residents`;
#CREATE TABLE `dorm_residents` (
#  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#  `student_id` int(11) unsigned NOT NULL,
#  `phone_number` varchar(255) DEFAULT NULL,
#  `application_number` int(11) unsigned DEFAULT NULL,
#  `application_date` date NOT NULL,
#  `application_document` varchar(255) DEFAULT NULL,
#  `room_id` int(11) unsigned DEFAULT NULL,
#  `status_id` int(11) unsigned DEFAULT NULL,
#  `status_reason` varchar(255) DEFAULT NULL,
#  `came_from` varchar(255) DEFAULT NULL,
#  `distance_from_Moscow` int(5) unsigned DEFAULT NULL,
#  `note` varchar(255) DEFAULT NULL,
#  `created_at` datetime NOT NULL,
#  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#  PRIMARY KEY (`id`),
#  UNIQUE KEY `dorm_residents_uk_1` (`student_id`),
#  KEY `dorm_residents_k_1` (`student_id`),
#  KEY `dorm_residents_k_2` (`room_id`),
#  KEY `dorm_residents_k_3` (`status_id`),
#  CONSTRAINT `dorm_residents_fk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
#  CONSTRAINT `dorm_residents_fk_2` FOREIGN KEY (`room_id`) REFERENCES `dorm_rooms` (`id`),
#  CONSTRAINT `dorm_residents_fk_3` FOREIGN KEY (`status_id`) REFERENCES `dorm_residents_statuses` (`id`)
#);
#CREATE TABLE `dorm_contracts_types`(
#  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#  `type` varchar(255) NOT NULL,
#  `created_at` datetime NOT NULL,
#  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#  PRIMARY KEY (`id`)
#);
#DROP TABLE `dorm_contracts`;
#CREATE TABLE `dorm_contracts` (
#  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
#  `number` varchar(255) NOT NULL,
#  `resident_id` int(11) UNSIGNED NOT NULL,
#  `type_id` int(11) UNSIGNED NOT NULL,
#  `amount` int(11) UNSIGNED DEFAULT NULL,
#  `sign_date` DATE DEFAULT NULL,
#  `expiry_date` DATE DEFAULT NULL,
#  `is_paid` tinyint(1) NOT NULL DEFAULT 0,
#  `active` tinyint(1) NOT NULL DEFAULT 1,
#  `created_at` datetime NOT NULL,
#  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#  PRIMARY KEY (`id`),
#  UNIQUE KEY (`number`, `type_id`),
#  KEY `dorm_contracts_k_1` (`resident_id`),
#  KEY `dorm_contracts_k_2` (`type_id`),
#  CONSTRAINT `dorm_contracts_fk_1` FOREIGN KEY (`resident_id`) REFERENCES `dorm_residents` (`id`),
#  CONSTRAINT `dorm_contracts_fk_2` FOREIGN KEY (`type_id`) REFERENCES `dorm_contracts_types` (`id`)
#);
#
#
#ALTER TABLE dorm_blocks ADD CONSTRAINT dorm_blocks_uk_1 UNIQUE (number, building_id, floor);
#ALTER TABLE dorm_rooms ADD CONSTRAINT dorm_rooms_uk_1 UNIQUE (number, block_id);

#ABOVE IS DONE ON 08.08.2015

ALTER TABLE students ADD COLUMN document_scan_ext VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE passports ADD COLUMN birth_place TEXT DEFAULT NULL  NULL;

ALTER TABLE addresses ADD COLUMN region_kladr VARCHAR(255) AFTER region;
ALTER TABLE addresses ADD COLUMN district_kladr VARCHAR(255) AFTER district;
ALTER TABLE addresses ADD COLUMN city_kladr VARCHAR(255) AFTER city;
ALTER TABLE addresses ADD COLUMN street_kladr VARCHAR(255) AFTER street;

ALTER TABLE addresses CHANGE `kladr` `building_kladr` VARCHAR(255);

ALTER TABLE students ADD COLUMN photo_ext VARCHAR(255);
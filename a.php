<?php

/*	$_SERVER['ENV'] = 'prod';
	require 'backend/core/bin/db.php';

	$q_get_all_filled = "SELECT users.last_name, users.first_name, users.middle_name, science_indicators_values.value, users.id,
		  science_indicators_values.indicator_id, roles.ou_id, b.name as kaf_name,
		  (SELECT name FROM organizational_units WHERE id = b.parent_id) as institute_name,
		  b.parent_id as institute_id, users_roles.main_role
		  FROM users INNER JOIN staff ON users.id = staff.user_id
		  LEFT JOIN science_indicators_values ON staff.id = science_indicators_values.staff_id
		    LEFT JOIN users_roles ON users.id = users_roles.user_id
		    LEFT JOIN roles ON roles.id = users_roles.role_id
		    LEFT JOIN organizational_units b ON b.id = roles.ou_id
		WHERE (users.id IN (SELECT user_id FROM log_requests WHERE method_name = 'answers' AND method = 'POST')
		OR users.id IN(SELECT staff.user_id FROM science_indicators_values INNER JOIN staff ON staff.id = science_indicators_values.staff_id))
		AND users_roles.main_role = 1 and users_roles.active = 1 and users.active= 1
		AND staff.id IN(SELECT staff_id FROM timetable_items_professors)
		GROUP BY users.id, indicator_id, period_id;";

if (isset($_REQUEST['bad'])){
	$q_get_all_filled = "SELECT users.last_name, users.first_name, users.middle_name, science_indicators_values.value, users.id,
		  science_indicators_values.indicator_id, roles.ou_id, b.name as kaf_name,
		  (SELECT name FROM organizational_units WHERE id = b.parent_id) as institute_name,
		  b.parent_id as institute_id, users_roles.main_role
		  FROM users INNER JOIN staff ON users.id = staff.user_id
		  LEFT JOIN science_indicators_values ON staff.id = science_indicators_values.staff_id
		    LEFT JOIN users_roles ON users.id = users_roles.user_id
		    LEFT JOIN roles ON roles.id = users_roles.role_id
		    LEFT JOIN organizational_units b ON b.id = roles.ou_id
		WHERE (users.id NOT IN (SELECT user_id FROM log_requests WHERE method_name = 'answers' AND method = 'POST')
			AND users.id NOT IN (SELECT staff.user_id FROM science_indicators_values INNER JOIN staff ON staff.id = science_indicators_values.staff_id))
		AND ou_id IN (SELECT ou_id FROM academic_departments)
		AND users_roles.main_role = 1 and users_roles.active =1 and users.active=1
		AND staff.id IN (SELECT staff_id FROM timetable_items_professors WHERE timetable_items_professors.id > 0)
		GROUP BY users.id, indicator_id, period_id
		ORDER BY ou_id";
}
if (isset($_REQUEST['very_bad'])){
	$q_get_all_filled = "SELECT users.ad_login, users.last_name, users.first_name, users.middle_name, science_indicators_values.value, users.id,
		  science_indicators_values.indicator_id, roles.ou_id, b.name as kaf_name,
		  (SELECT name FROM organizational_units WHERE id = b.parent_id) as institute_name,
		  b.parent_id as institute_id, users_roles.main_role
		  FROM users INNER JOIN staff ON users.id = staff.user_id
		  LEFT JOIN science_indicators_values ON staff.id = science_indicators_values.staff_id
		    LEFT JOIN users_roles ON users.id = users_roles.user_id
		    LEFT JOIN roles ON roles.id = users_roles.role_id
		    LEFT JOIN organizational_units b ON b.id = roles.ou_id
		WHERE users.ad_login NOT IN (SELECT DISTINCT ad_login FROM azure_tokens)
		AND ou_id IN (SELECT ou_id FROM academic_departments)
		AND users_roles.main_role = 1 and users_roles.active = 1 and users.active = 1
		AND value IS NULL
		AND staff.id IN(SELECT staff_id FROM timetable_items_professors)
		GROUP BY users.id, indicator_id, period_id
		ORDER BY ou_id";
}


	$stat_chils = 'SELECT academic_departments.name, COUNT(*) as ccc, academic_departments.ou_id, organizational_units.parent_id, organizational_units.parent_id as inst_id,
	 (SELECT name FROM organizational_units WHERE id = inst_id) as institute_name
	FROM view_users
	INNER JOIN academic_departments ON view_users.ou_id = academic_departments.ou_id
	  INNER JOIN organizational_units ON organizational_units.id = academic_departments.ou_id
	  WHERE academic_departments.university_code IS NOT NULL
	  AND view_users.is_professor = 1 AND view_users.active = 1
	GROUP BY academic_departments.ou_id;';

$staff = $db->query($q_get_all_filled)->fetchAll();
$items = $db->query($stat_chils)->fetchAll();

$norm_staff = array();
foreach($staff as $user){
	if (!isset($norm_staff[$user['id']])){
		$norm_staff[$user['id']] = array(
			'last_name' => $user['last_name'],
			'first_name' => $user['first_name'],
			'middle_name' => $user['middle_name'],
			'institute_name' => $user['institute_name'],
			'institute_id' => $user['institute_id'],
			'kaf_name' => $user['kaf_name'],
			'ou_id' => $user['ou_id'],
		);
	}
	if ($user['indicator_id'] == 1){
		$norm_staff[$user['id']]['kpk'] = $user['value'];
	}elseif($user['indicator_id'] == 2){
		$norm_staff[$user['id']]['knk'] = $user['value'];
	}else{
		if (isset($_REQUEST['bad']) || isset($_REQUEST['very_bad'])){
			$norm_staff[$user['id']]['kpk'] = '';
			$norm_staff[$user['id']]['knk'] = '';
		}
	}
}

$norm_institutes = array();
foreach($items as $item){
	if(!isset($norm_institutes[$item['inst_id']])){
		$norm_institutes[$item['inst_id']] = array(
			'name' => $item['institute_name'],
			'staff_count' => (int)  0,
			'filled_count' => (int) 0,
			'kafs' => array()
		);
	}
	$norm_institutes[$item['inst_id']]['staff_count'] += $item['ccc'];
	$norm_institutes[$item['inst_id']]['kafs'][$item['ou_id']] = array(
		'count' => $item['ccc'],
		'name' => $item['name'],
		'filled_count' => (int) 0,
		'staff' => array()
	);
}


if (isset($_REQUEST['tree'])){
	echo "<table>";
	foreach($norm_staff as $id => $st){
		if (!isset($st['kpk']) || !isset($st['knk'])) continue;
		if (!isset($norm_institutes[$st['institute_id']])) continue;
		if (!isset($norm_institutes[$st['institute_id']]['kafs'][$st['ou_id']])) continue;
		$norm_institutes[$st['institute_id']]['filled_count']++;
		$norm_institutes[$st['institute_id']]['kafs'][$st['ou_id']]['filled_count']++;
		$norm_institutes[$st['institute_id']]['kafs'][$st['ou_id']]['staff'][] = array(
			'name' => $st['last_name'] . ' ' . $st['first_name']. ' ' . $st['middle_name'],
			'kpk' => $st['kpk'],
			'knk' => $st['knk']
		);
	}


	foreach($norm_institutes as $key => $institute){
		echo "<tr><th>{$institute['name']}</th><td>{$institute['filled_count']}</td><td>{$institute['staff_count']}</td></tr>";
		foreach($institute['kafs'] as $kaf){
			echo "<tr><td></td><th>{$kaf['name']}</th><td>{$kaf['filled_count']}</td><td>{$kaf['count']}</td></tr>";
			foreach($kaf['staff'] as $user){
				echo "<tr><td></td><td></td><th>{$user['name']}</th><td>{$user['kpk']}</td><td>{$user['knk']}</td></tr>";
			}
		}
	}
	echo "</table>";
}else{
	$counter = 1;
	echo "<html><table><tr><th>#</th><th>ID</th><th>ФИО</th><th>КНК</th><th>КПК</th><th>Кафедра</th><th>Институт</th></tr>";
	foreach($norm_staff as $id => $st){
		if (isset($st['kpk']) && isset($st['knk'])){
			echo "<tr><td>{$counter}</td><td>{$id}</td><td>{$st['last_name']} {$st['first_name']} {$st['middle_name']}</td>
				<td>{$st['knk']}</td><td>{$st['kpk']}</td><td>{$st['kaf_name']}</td><td>{$st['institute_name']}</td></tr>";
			$counter++;
		}
	}
	echo "</table></html>";
}
*/
/*$p_get_avatars = $db->query('SELECT log_requests.body, log_requests.user_id as uid, users.ad_login, avatar_ext
FROM log_requests
  INNER JOIN users ON users.id = log_requests.user_id
WHERE args = \'["avatar"]\'
AND log_requests.created_at =
    (SELECT MAX(created_at) FROM log_requests a WHERE a.user_id = log_requests.user_id AND args = \'["avatar"]\')');

$avatars = $p_get_avatars->fetchAll();

global $ROOT_PATH;
foreach($avatars as $avatar){

	$file = json_decode($avatar['body'])->file;
	$file = explode(',', $file);
	$file = $file[1];
	echo $file, "\n";

	$new_file_name = $ROOT_PATH . '/images/avatars/' . $avatar['ad_login'] . '.' . $avatar['avatar_ext'];
	file_put_contents($new_file_name, base64_decode($file));

}*/



/*
class A
{
    private $param1;
    private $param2;

    public function add( $arr ){
        foreach( $arr as $key => $value ){
            $this->$key = $value;
            echo $key;
            echo '<br>';
            echo $this->$key;
            echo '<br>';
        }
    }
    public function output(){
        echo $this->param1;
        echo '<br>';
        echo $this->param2;
    }
}

$arr = array(
    'param1' => 11,
    'param2' => 22,
);

$cla = new A();
$cla->add($arr);
$cla->output();
*/
/*
	$array = json_decode('[{"group_id":317,"free_days":[1,6]},{"group_id":457,"free_days":[]},{"group_id":234,"free_days":[5,6]},{"group_id":460,"free_days":[4,6]},{"group_id":142,"free_days":[1,6]},{"group_id":147,"free_days":[3,4]},{"group_id":258,"free_days":[]},{"group_id":11,"free_days":[6]},{"group_id":148,"free_days":[3,4]},{"group_id":402,"free_days":[5,6]},{"group_id":182,"free_days":[5,6]},{"group_id":468,"free_days":[]},{"group_id":408,"free_days":[]},{"group_id":256,"free_days":[]},{"group_id":217,"free_days":[6]},{"group_id":443,"free_days":[]},{"group_id":170,"free_days":[2,5,6]},{"group_id":65,"free_days":[3,4]},{"group_id":309,"free_days":[6]},{"group_id":452,"free_days":[]},{"group_id":218,"free_days":[5,6]},{"group_id":12,"free_days":[3,6]},{"group_id":437,"free_days":[3]},{"group_id":238,"free_days":[]},{"group_id":296,"free_days":[]},{"group_id":128,"free_days":[]},{"group_id":455,"free_days":[]},{"group_id":180,"free_days":[1,2,6]},{"group_id":185,"free_days":[3,4]},{"group_id":501,"free_days":[]},{"group_id":412,"free_days":[]},{"group_id":264,"free_days":[]},{"group_id":384,"free_days":[]},{"group_id":475,"free_days":[4,6]},{"group_id":379,"free_days":[]},{"group_id":116,"free_days":[]},{"group_id":13,"free_days":[6]},{"group_id":481,"free_days":[]},{"group_id":221,"free_days":[5,6]},{"group_id":51,"free_days":[6]},{"group_id":366,"free_days":[]},{"group_id":146,"free_days":[1,2]},{"group_id":442,"free_days":[]},{"group_id":135,"free_days":[]},{"group_id":339,"free_days":[]},{"group_id":250,"free_days":[]},{"group_id":367,"free_days":[]},{"group_id":462,"free_days":[]},{"group_id":335,"free_days":[]},{"group_id":285,"free_days":[]},{"group_id":262,"free_days":[]},{"group_id":302,"free_days":[]},{"group_id":192,"free_days":[]},{"group_id":144,"free_days":[1,2]},{"group_id":513,"free_days":[]},{"group_id":268,"free_days":[]},{"group_id":30,"free_days":[5,6]},{"group_id":465,"free_days":[]},{"group_id":231,"free_days":[5,6]},{"group_id":352,"free_days":[6]},{"group_id":154,"free_days":[3,4]},{"group_id":3,"free_days":[6]},{"group_id":306,"free_days":[]},{"group_id":318,"free_days":[5,6]},{"group_id":152,"free_days":[6]},{"group_id":442,"free_days":[]},{"group_id":150,"free_days":[5,6]},{"group_id":355,"free_days":[]},{"group_id":187,"free_days":[2,5]},{"group_id":245,"free_days":[]},{"group_id":130,"free_days":[3,4]},{"group_id":118,"free_days":[6]},{"group_id":356,"free_days":[]},{"group_id":431,"free_days":[]},{"group_id":503,"free_days":[]},{"group_id":491,"free_days":[]},{"group_id":282,"free_days":[6]},{"group_id":211,"free_days":[]},{"group_id":487,"free_days":[]},{"group_id":232,"free_days":[5,6]},{"group_id":413,"free_days":[]},{"group_id":380,"free_days":[]},{"group_id":474,"free_days":[]},{"group_id":14,"free_days":[2,6]},{"group_id":210,"free_days":[]},{"group_id":253,"free_days":[]},{"group_id":433,"free_days":[]},{"group_id":341,"free_days":[]},{"group_id":466,"free_days":[]},{"group_id":349,"free_days":[]},{"group_id":451,"free_days":[]},{"group_id":139,"free_days":[2,6]},{"group_id":442,"free_days":[]},{"group_id":281,"free_days":[]},{"group_id":383,"free_days":[]},{"group_id":137,"free_days":[5,6]},{"group_id":203,"free_days":[5,6]},{"group_id":514,"free_days":[]},{"group_id":299,"free_days":[]},{"group_id":200,"free_days":[1,6]},{"group_id":260,"free_days":[]},{"group_id":303,"free_days":[]},{"group_id":227,"free_days":[1,6]},{"group_id":164,"free_days":[4,6]},{"group_id":438,"free_days":[]},{"group_id":399,"free_days":[5,6]},{"group_id":346,"free_days":[]},{"group_id":127,"free_days":[3,4]},{"group_id":440,"free_days":[]},{"group_id":125,"free_days":[5,6]},{"group_id":286,"free_days":[]},{"group_id":292,"free_days":[]},{"group_id":166,"free_days":[3,4]},{"group_id":446,"free_days":[5]},{"group_id":454,"free_days":[]},{"group_id":488,"free_days":[]},{"group_id":240,"free_days":[]},{"group_id":174,"free_days":[]},{"group_id":430,"free_days":[]},{"group_id":315,"free_days":[3,5,6]},{"group_id":456,"free_days":[6]},{"group_id":214,"free_days":[5,6]},{"group_id":246,"free_days":[]},{"group_id":298,"free_days":[]},{"group_id":248,"free_days":[]},{"group_id":278,"free_days":[]},{"group_id":301,"free_days":[]},{"group_id":300,"free_days":[]},{"group_id":239,"free_days":[]},{"group_id":141,"free_days":[1,2]},{"group_id":482,"free_days":[]},{"group_id":168,"free_days":[1,3]},{"group_id":372,"free_days":[]},{"group_id":396,"free_days":[]},{"group_id":401,"free_days":[]},{"group_id":347,"free_days":[]},{"group_id":167,"free_days":[1,6]},{"group_id":445,"free_days":[]},{"group_id":149,"free_days":[3,4]},{"group_id":266,"free_days":[]},{"group_id":290,"free_days":[]},{"group_id":297,"free_days":[]},{"group_id":158,"free_days":[]},{"group_id":138,"free_days":[]},{"group_id":244,"free_days":[5]},{"group_id":428,"free_days":[]},{"group_id":492,"free_days":[]},{"group_id":136,"free_days":[3,4]},{"group_id":169,"free_days":[]},{"group_id":241,"free_days":[]},{"group_id":183,"free_days":[]},{"group_id":191,"free_days":[1,3]},{"group_id":344,"free_days":[]},{"group_id":132,"free_days":[1,2]},{"group_id":287,"free_days":[]},{"group_id":176,"free_days":[6]},{"group_id":494,"free_days":[]},{"group_id":393,"free_days":[]},{"group_id":442,"free_days":[]},{"group_id":471,"free_days":[]},{"group_id":403,"free_days":[]},{"group_id":497,"free_days":[]},{"group_id":512,"free_days":[]},{"group_id":467,"free_days":[]},{"group_id":289,"free_days":[6]},{"group_id":157,"free_days":[2,6]},{"group_id":310,"free_days":[6]},{"group_id":489,"free_days":[]},{"group_id":333,"free_days":[6]},{"group_id":193,"free_days":[6]},{"group_id":251,"free_days":[]},{"group_id":447,"free_days":[]},{"group_id":172,"free_days":[3,4]},{"group_id":449,"free_days":[5,6]},{"group_id":252,"free_days":[]},{"group_id":124,"free_days":[5]},{"group_id":159,"free_days":[3,4]},{"group_id":216,"free_days":[]},{"group_id":181,"free_days":[1,2]},{"group_id":486,"free_days":[6]},{"group_id":498,"free_days":[6]},{"group_id":464,"free_days":[]},{"group_id":336,"free_days":[]},{"group_id":410,"free_days":[5,6]},{"group_id":363,"free_days":[]},{"group_id":414,"free_days":[]},{"group_id":151,"free_days":[]},{"group_id":332,"free_days":[]},{"group_id":369,"free_days":[]},{"group_id":198,"free_days":[]},{"group_id":17,"free_days":[4,6]},{"group_id":291,"free_days":[6]},{"group_id":224,"free_days":[1,5,6]},{"group_id":439,"free_days":[4,6]},{"group_id":202,"free_days":[1,2]},{"group_id":453,"free_days":[]},{"group_id":374,"free_days":[]},{"group_id":442,"free_days":[]},{"group_id":195,"free_days":[6]},{"group_id":230,"free_days":[1,6]},{"group_id":161,"free_days":[6]},{"group_id":126,"free_days":[1,2]},{"group_id":95,"free_days":[2,6]},{"group_id":397,"free_days":[6]},{"group_id":509,"free_days":[4,6]},{"group_id":321,"free_days":[]},{"group_id":502,"free_days":[]},{"group_id":348,"free_days":[]},{"group_id":204,"free_days":[]},{"group_id":145,"free_days":[]},{"group_id":212,"free_days":[6]},{"group_id":441,"free_days":[]},{"group_id":2,"free_days":[5,6]},{"group_id":215,"free_days":[]},{"group_id":257,"free_days":[]},{"group_id":219,"free_days":[]},{"group_id":358,"free_days":[]},{"group_id":448,"free_days":[]},{"group_id":9,"free_days":[4,6]},{"group_id":237,"free_days":[6]},{"group_id":470,"free_days":[]},{"group_id":194,"free_days":[]},{"group_id":197,"free_days":[]},{"group_id":223,"free_days":[6]},{"group_id":205,"free_days":[6]},{"group_id":155,"free_days":[3,4]},{"group_id":179,"free_days":[1,2]},{"group_id":72,"free_days":[6]},{"group_id":119,"free_days":[]},{"group_id":206,"free_days":[6]},{"group_id":7,"free_days":[5,6]},{"group_id":495,"free_days":[]},{"group_id":387,"free_days":[]},{"group_id":434,"free_days":[]},{"group_id":171,"free_days":[1,2,6]},{"group_id":411,"free_days":[5,6]},{"group_id":409,"free_days":[]},{"group_id":76,"free_days":[]},{"group_id":484,"free_days":[]},{"group_id":442,"free_days":[]},{"group_id":196,"free_days":[2,6]},{"group_id":64,"free_days":[1,3,4]},{"group_id":259,"free_days":[]},{"group_id":254,"free_days":[]},{"group_id":199,"free_days":[]},{"group_id":160,"free_days":[1,6]},{"group_id":29,"free_days":[3,6]},{"group_id":243,"free_days":[6]},{"group_id":90,"free_days":[2]},{"group_id":338,"free_days":[]},{"group_id":500,"free_days":[5,6]},{"group_id":483,"free_days":[]},{"group_id":354,"free_days":[]},{"group_id":337,"free_days":[]},{"group_id":236,"free_days":[]},{"group_id":143,"free_days":[]},{"group_id":27,"free_days":[5,6]},{"group_id":279,"free_days":[]},{"group_id":385,"free_days":[]},{"group_id":153,"free_days":[3,4]},{"group_id":201,"free_days":[]},{"group_id":340,"free_days":[]},{"group_id":15,"free_days":[5,6]},{"group_id":156,"free_days":[1,2,6]},{"group_id":361,"free_days":[]},{"group_id":225,"free_days":[5,6]},{"group_id":458,"free_days":[]},{"group_id":267,"free_days":[]},{"group_id":134,"free_days":[1,2]},{"group_id":435,"free_days":[]},{"group_id":288,"free_days":[]},{"group_id":479,"free_days":[]},{"group_id":177,"free_days":[]},{"group_id":184,"free_days":[1,2]},{"group_id":175,"free_days":[3,4]},{"group_id":188,"free_days":[3,4]},{"group_id":165,"free_days":[2,5,6]},{"group_id":133,"free_days":[1,2]},{"group_id":371,"free_days":[]},{"group_id":382,"free_days":[]},{"group_id":295,"free_days":[5,6]},{"group_id":477,"free_days":[5,6]},{"group_id":162,"free_days":[3,4]},{"group_id":304,"free_days":[]},{"group_id":360,"free_days":[]},{"group_id":213,"free_days":[6]},{"group_id":342,"free_days":[1,6]},{"group_id":480,"free_days":[3]},{"group_id":255,"free_days":[]},{"group_id":381,"free_days":[]},{"group_id":1,"free_days":[5,6]},{"group_id":450,"free_days":[5,6]},{"group_id":469,"free_days":[]},{"group_id":186,"free_days":[2,4,5,6]},{"group_id":493,"free_days":[]},{"group_id":242,"free_days":[]},{"group_id":334,"free_days":[]},{"group_id":129,"free_days":[1,2,6]},{"group_id":490,"free_days":[]},{"group_id":178,"free_days":[6]},{"group_id":378,"free_days":[1,6]},{"group_id":228,"free_days":[5,6]},{"group_id":235,"free_days":[]},{"group_id":94,"free_days":[2,6]},{"group_id":463,"free_days":[]},{"group_id":84,"free_days":[]},{"group_id":247,"free_days":[]},{"group_id":10,"free_days":[4,6]},{"group_id":163,"free_days":[6]},{"group_id":265,"free_days":[]}]');

	$days = $db->query('SELECT * FROM timetable_days')->fetchAll();
	$prep = $db->prepare('SELECT *,
		(SELECT COUNT(*) FROM view_users WHERE edu_group_id = :id) as students_count
 		FROM view_edu_groups WHERE edu_group_id = :id');
	$_days = array(
		'1' => array(),
		'2' => array(),
		'3' => array(),
		'4' => array(),
		'5' => array(),
		'6' => array()
	);

foreach($array as $group){
	if (count($group->free_days) != 0){
		foreach($group->free_days as $free_day){
			$prep->execute(array(
				':id' => $group->group_id
			));

			$_days["$free_day"][] = $prep->fetch();

		}
	}
};
?>
<table>
	<tr>
		<th>День недели</th>
		<th>Институт</th>
		<th>Форма обучения</th>
		<th>Направление</th>
		<th>Профиль</th>
		<th>Программа</th>
		<th>Курс</th>
		<th>Группа</th>
		<th>Количество студентов</th>
	</tr>
<?php
foreach($_days as $day_index => $day){
	$groups_count = count($day) + 1; //+итоговая строка
	$counter = 0;
	$all_students = 0;
	foreach($day as $group){
		$line = "
			<td>{$group['institute_abbr']}</td>
			<td>{$group['edu_qualification_name']}</td>
			<td>{$group['edu_speciality_name']}</td>
			<td>{$group['program']}</td>
			<td>{$group['profile']}</td>
			<td>{$group['course']}</td>
			<td>{$group['group']}</td>
			<td>{$group['students_count']}</td>";
		$all_students += $group['students_count'];
		if ($counter == 0){
			$line = "<tr><td rowspan='{$groups_count}'>{$day_index}</td>" . $line . "</tr>";
		}else{
			$line = "<tr>{$line}</tr>";
		}
		echo $line;
		$counter++;
	}
	echo "<tr><td colspan='5'><b>Итого студентов:<b></td><td colspan='5'>{$all_students}</td></tr>";
}

?>
</table>
*/

	require 'backend/core/bin/db.php';

	$q_get = 'SELECT id, profile, profile_code,
			program, program_code, institute_id, institute_abbr, edu_form_name,
			edu_form_id, edu_qualification_code, edu_qualification_name, edu_qualification_id,
			edu_speciality_name, edu_specialty_id
	FROM view_users
	WHERE is_student = 1
	AND
		id NOT IN (SELECT id FROM view_users WHERE view_users.edu_qualification_id = 1 AND view_users.course = 4 AND view_users.edu_form_id = 1)
  	AND
		id NOT IN (SELECT id FROM view_users WHERE view_users.edu_qualification_id = 3 AND (view_users.course = 5 OR view_users.course = 6) AND view_users.edu_form_id = 1)
    AND
		id NOT IN (SELECT id FROM view_users WHERE view_users.edu_qualification_id = 2 AND view_users.course = 2)

	';

	$p_get = $__db->query($q_get);
	$rows = $p_get->fetchAll();

	$result = array(
		'institutes' => array()
	);

	foreach($rows as $row){
		$institute_id = $row['institute_id'];
		$edu_form_id = $row['edu_form_id'];
		$edu_qualification_id = $row['edu_qualification_id'];
		$edu_specialty_id = $row['edu_specialty_id'];


		if (!isset($result['institutes'][$institute_id])){
			$result['institutes'][$institute_id] = array(
				'institute_abbr' => $row['institute_abbr'],
				'count' => 1,
				'edu_forms' => array());
		}else{
			$result['institutes'][$institute_id]['count']++;
		}


		if (!isset($result['institutes'][$institute_id]['edu_forms'][$edu_form_id])){
			$result['institutes'][$institute_id]['edu_forms'][$edu_form_id] = array(
				'edu_form_name' => $row['edu_form_name'], 'count' => 1, 'edu_qualifications' => array());
		}else{
			$result['institutes'][$institute_id]['edu_forms'][$edu_form_id]['count']++;
		}


		if (!isset($result['institutes'][$institute_id]['edu_forms'][$edu_form_id]['edu_qualifications'][$edu_qualification_id])){
			$result['institutes'][$institute_id]['edu_forms'][$edu_form_id]['edu_qualifications'][$edu_qualification_id] = array('edu_qualification_name' => $row['edu_qualification_name'], 'count' => 1, 'edu_specialities' => array());
		}else{
			$result['institutes'][$institute_id]['edu_forms'][$edu_form_id]['edu_qualifications'][$edu_qualification_id]['count']++;
		}


		if (!isset($result['institutes'][$institute_id]['edu_forms'][$edu_form_id]['edu_qualifications'][$edu_qualification_id]['edu_specialities'][$edu_specialty_id])){
			$result['institutes'][$institute_id]['edu_forms'][$edu_form_id]['edu_qualifications'][$edu_qualification_id]['edu_specialities'][$edu_specialty_id] = array('edu_speciality_name' => $row['edu_speciality_name'], 'count' => 1);
		}else{
			$result['institutes'][$institute_id]['edu_forms'][$edu_form_id]['edu_qualifications'][$edu_qualification_id]['edu_specialities'][$edu_specialty_id]['count']++;
		}
	}

echo "<table>
<tr>
<th>Институт</th>
<th>Форма обучения</th>
<th>Квалификация</th>
<th>Направление</th>+
<th>Количество</th>
</tr>";

	foreach($result['institutes'] as $institute){
		$inst_name = $institute['institute_abbr'];
		foreach($institute['edu_forms'] as $edu_form){
			$edu_form_name = $edu_form['edu_form_name'];
				foreach($edu_form['edu_qualifications'] as $edu_speciality){
					$edu_qualification_name = $edu_speciality['edu_qualification_name'];
					foreach($edu_speciality['edu_specialities'] as $final){
						echo "<tr>
<td>{$inst_name}</td>
<td>{$edu_form_name}</td>
<td>{$edu_qualification_name}</td>
<td>{$final['edu_speciality_name']}</td>
<td>{$final['count']}</td>
</tr>";
					}
				}
		}
	}
echo "</table>";
<?php
require 'backend/core/bin/db.php';
require 'backend/core/users/Class.User.php';


try {
	$user = new User($db);
	$filename = basename(__FILE__, '.php');
	if (!file_exists('tmpl/' . $filename . '.html')) {
		header("HTTP/1.0 404 Not Found");
		exit();
	}
}
catch (Exception $e) {
	header('Location: ./login.php', TRUE, 303);
	exit();
}

$logo_added_info = $user->getId() == '2040' ? '_struzhkin' : '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<title>Ваш профиль</title>


	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="/assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="assets/css/bootstrap-min.css">
	<link rel="stylesheet" href="assets/css/neon-core-min.css">
	<link rel="stylesheet" href="assets/css/neon-theme-min.css">
	<link rel="stylesheet" href="assets/css/neon-forms-min.css">
	<link rel="stylesheet" href="assets/css/custom-min.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/css/skins/blue.css">
	<link rel="stylesheet" href="assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="assets/js/labelauty/jquery-labelauty.css">
	<link rel="stylesheet" href="assets/js/rickshaw/rickshaw.min.css">
	<link rel="stylesheet" href="assets/js/ladda/ladda-themeless.min.css">
	<link rel="stylesheet" href="assets/js/table-sortable/bootstrap-sortable.css">
	<link rel="stylesheet" href="assets/js/vertical-timeline/css/component.css">
	<link rel="stylesheet" href="assets/js/datatables/responsive/css/datatables.responsive.css">
	<link rel="stylesheet" href="assets/js/daterangepicker/daterangepicker-bs3.css">
	<link rel="stylesheet" href="assets/js/kladr/css/jquery.kladr.min.css">

	<!--<link rel="stylesheet" href="assets/js/vertical-timeline/css/patch.css"> -->


	<!--[if lt IE 9]>
	<script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body class="page-body page-fade">
<div class="noty-container" style="position: fixed; z-index: 9999999; float: right; left: 80%;">

</div>
<div class="page-container">

	<div class="sidebar-menu hidden-print">


		<header class="logo-env">

			<div class="sidebar-collapse">
				<a href="#" class="sidebar-collapse-icon handled">
					<!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
					<i class="entypo-menu"></i>
				</a>
			</div>


			<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
			<div class="sidebar-mobile-menu visible-xs">
				<a href="#"><!-- add class "with-animation" to support animation -->
					<i class="entypo-menu"></i>
				</a>
			</div>

			<!-- logo collapse icon -->
			<div class="logo">
				<a href="/index.php">
					<img src="assets/images/logo<?= $logo_added_info ?>.png" width="50%" alt=""/>
				</a>
			</div>
			<!-- logo -->


		</header>

		<div class="sidebar-user-info">
			<div class="sui-normal">
				<a href="#" class="user-link">
					<img src="<?= $user->getAvatarUrl() ?>" style="max-width: 50px" class="img-circle avatar-img">
					<span>Здравствуйте,</span>
					<strong><?= $user->getFirstName() . ' ' . $user->getMiddleName() ?></strong> </a>
			</div>
			<div class="sui-hover inline-links animate-in">
				<a href="profile.php"> <i class="entypo-pencil"></i>
					Личные данные
				</a>
				<a href="http://outlook.office365.com" target="_blank"> <i class="entypo-mail"></i>
					Email
				</a>
				<span class="close-sui-popup">×</span></div>
		</div>

		<?php require "tmpl/menu.php" ?>

	</div>
	<div class="main-content">


		<div class="row hidden-print">

			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix ">

				<ul class="user-info pull-left pull-right-xs pull-none-xsm notification-bar">

					<!-- BRS Notifications -->
					<li class="notifications dropdown brs-notifications hidden">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
						   data-close-others="true">
							<i class="entypo-chart-bar"></i>
							<span class="badge badge-info header brs-new-marks-count">-</span>
						</a>

						<ul class="dropdown-menu">
							<li>
								<ul class="dropdown-menu-list scroller brs-new-marks">
								</ul>
							</li>

							<li class="external">
								<a href="brsStudentView.php">Открыть БРС</a>
							</li>
						</ul>
					</li>

					<!-- Timetable Notifications -->
					<li class="notifications dropdown timetable-notifications hidden">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
						   data-close-others="true">
							<i class="entypo-list"></i>
							<span class="badge badge-info header timetable-changes-count">-</span>
						</a>

						<ul class="dropdown-menu">
							<li>
								<ul class="dropdown-menu-list scroller timetable-changes">
								</ul>
							</li>
							<li class="external">
								<a href="timetable.php">Открыть расписание</a>
							</li>
						</ul>
					</li>

					<!-- Requisitions Notifications -->
					<li class="notifications dropdown requisitions-notifications hidden">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
						   data-close-others="true">
							<i class="entypo-list"></i>
							<span class="badge badge-info header requisitions-count">-</span>
						</a>

						<ul class="dropdown-menu">
							<li>
								<ul class="dropdown-menu-list scroller timetable-changes">
								</ul>
							</li>
							<li class="external">
								<a href="timetable.php">Открыть расписание</a>
							</li>
						</ul>
					</li>


					<!-- Email Notifications -->
					<li class="notifications dropdown email-notifications hidden">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
						   data-close-others="true">
							<i class="entypo-mail"></i>
							<span class="badge badge-info header unread-emails-count">-</span>
						</a>

						<ul class="dropdown-menu">

							<li>
								<ul class="dropdown-menu-list scroller unread-emails">
								</ul>
							</li>
							<li class="external">
								<a href="https://outlook.office365.com/owa/#path=/mail" target="_blank" class="handled">Все
									сообщения</a>
							</li>
						</ul>
					</li>


				</ul>

			</div>


			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs hidden-print">

				<ul class="list-inline links-list pull-right">

					<li class="hidden">
						<a href="#" data-toggle="chat" data-animate="1" data-collapse-sidebar="1" class="handled">
							<i class="entypo-chat"></i>
							Чат

							<span class="badge badge-success chat-notifications-badge is-hidden">0</span>
						</a>
					</li>

					<li class="sep"></li>

					<li>
						<a href="#" class="handled main-exit-btn">
							Выйти <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>

			</div>
		</div>

		<hr/>
		<div id="page-content">
		</div>

		<!-- Footer -->
		<footer class="main">

			<div class="pull-right">
				<strong>ЦНИТ ГУУ ||</strong>
				<a href="https://bitbucket.org/sum_moscow/my.guu.ru"
				   target="_blank"><strong>Bitbucket</strong></a>
			</div>

			&copy; 2015
		</footer>
	</div>


	<div id="chat" class="fixed" data-current-user="" data-order-by-status="1" data-max-chat-history="25">

		<div class="chat-inner">

			<h2 class="chat-header">
				<a href="#" class="chat-close handled"><i class="entypo-cancel"></i></a>

				<i class="entypo-users"></i>
				Чат
				<span class="badge badge-success is-hidden">0</span>
			</h2>

			<div class="col-md-12">
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" class="form-control search-people-in-chat" id="chat-search"
						       placeholder="Поиск...">
					</div>
				</div>
			</div>

			<div class="chat-group" id="group-1">
				<strong>Чат в режиме теста!</strong>
			</div>

		</div>

		<!-- conversation template -->
		<div class="chat-conversation">
			<div class="conversation-header">
				<a href="#" class="conversation-close"><i class="entypo-cancel"></i></a>

				<span class="user-status"></span>
				<a href="#" style="color: #fff;"><span class="display-name"></span></a>
				<small></small>
				<p class="user-role"></p>
			</div>

			<ul class="conversation-body">
			</ul>
			<a type="button" class="btn btn-primary btn-icon send-email-btn btn-block" target="_blank">Отправить E-mail
				сообщение <i class="entypo-mail"></i></a>

			<div class="chat-textarea">
				<textarea class="form-control autogrow" placeholder="Введите сообщение..."></textarea>
			</div>

		</div>

	</div>

	<!-- User info modal -->
	<div class="modal fade custom-width" id="user-info-modal">
	</div>
</div>


<script type="template/javascript" id="tmpl-user-info-modal">
	<div>
		<div class="modal-dialog" style="max-width: 990px;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Информация о пользователе {last_name} {first_name} {middle_name}</h4>
				</div>
				<div class="modal-body">
					{sticker}
					<div class="row">
						<div class="col-sm-3"><img src="{avatar}" style="max-width: 200px; margin-left: 25%"
						                           class="img-rounded"></div>
						<div class="col-sm-9" style="text-align: center;">
							<h3 class="role"> {last_name} {first_name} {middle_name}</h3>
							<h4 class="role"><i class="entypo-briefcase"></i> {role}</h4>

							<p><i class="entypo-box"></i> <span class="department">{work_full_path}</span></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary" data-collapsed="0">
								<div class="panel-heading">
									<div class="panel-title">
										Дополнительная информация о пользователе
									</div>
								</div>

								<div class="panel-body">

									<form role="form" class="form-horizontal form-groups-bordered">

										<div class="form-group">
											<label for="field-1" class="col-sm-3 control-label">Мобильный телефон</label>

											<div class="col-sm-5">
												{phone_number}
											</div>
										</div>

										<div class="form-group {staff_visible}">
											<label for="field-2" class="col-sm-3 control-label">Рабочий телефон</label>

											<div class="col-sm-5">
												{office_tel}
											</div>
										</div>

										<div class="form-group {staff_visible}">
											<label for="field-3" class="col-sm-3 control-label">Кабинет</label>

											<div class="col-sm-5">
												{room}
											</div>
										</div>

										<div class="form-group">
											<label for="field-3" class="col-sm-3 control-label">Вконтакте</label>

											<div class="col-sm-5">
												<a href="{vk_url}" class="handled">{vk_url}</a>
											</div>
										</div>

										<div class="form-group">
											<label for="field-3" class="col-sm-3 control-label">Facebook</label>

											<div class="col-sm-5">
												<a href="{fb_url}" class="handled">{fb_url}</a>
											</div>
										</div>

										<div class="form-group {staff_visible}">
											<label for="field-3" class="col-sm-3 control-label">КНК общий</label>
											<div class="col-sm-5 text-center">
												<span class="knk-label"></span>
												<div class="progress knk-progress progress-striped"> <div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style=""> <span class="sr-only"></span> </div> </div>
											</div>
										</div>
										<div class="form-group {staff_visible}">
											<label for="field-3" class="col-sm-3 control-label">КПК общий</label>
											<div class="col-sm-5 text-center">
												<span class="kpk-label"></span>
												<div class="progress kpk-progress progress-striped"> <div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style=""> <span class="sr-only"></span> </div> </div>
											</div>
										</div>
										<div class="form-group {staff_visible}">
											<label for="field-3" class="col-sm-3 control-label">Ученая степень</label>

											<div class="col-sm-5">
												{science_degree}
											</div>
										</div>

										<div class="form-group {staff_visible}">
											<label for="field-3" class="col-sm-3 control-label">Ученое звание</label>

											<div class="col-sm-5">
												{science_title}
											</div>
										</div>

										<div class="form-group">
											<label for="field-3" class="col-sm-3 control-label">Научные интересы</label>

											<div class="col-sm-5">
												{science_interests}
											</div>
										</div>

										<div class="form-group">
											<label for="field-3" class="col-sm-3 control-label">Дополнительная информация</label>

											<div class="col-sm-5">
												{additional_info}
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					<a href="mailto:{ad_login}" class="btn btn-info btn-icon"><i class="entypo-mail"></i>
						Отправить Email</a>
					<button type="button" class="btn btn-success btn-icon disabled"><i class="entypo-comment"></i>
						Написать в чат
					</button>
				</div>
			</div>
		</div>
	</div>
</script>

<script type="template/javascript" id="tmpl-email-dropdown-notification">
	<li class="unread notification-info">
		<a href="#">
				<span class="line">
					<strong>{from_name} ({from_email})</strong> - {received_time}
				</span>
				<span class="line small">
				{subject}
				</span>
		</a>
	</li>
</script>

<script type="template/javascript" id="tmpl-brs-dropdown-notification">
	<li class="unread notification-info">
		<a href="#">
				<span class="line">
					<strong>{subject_name}</strong> - {mark} балл(а/ов)
				</span>
				<span class="line small">
				Поставил {changer_name} {change_date_readable}
				</span>
		</a>
	</li>
</script>

<script type="template/javascript" id="tmpl-timetable-dropdown-notification">
	<li class="unread notification-info">
		<a href="#">
				<span class="line">
					<strong>Теперь в {day_name} по {week_type} неделям {class_type} парой у Вас {class_format_text} по
						{subject_name}</strong>
				</span>
				<span class="line small">
				Ведет {first_name} {last_name} {father_name} c {kaf_name} в {building_type} {room}
				</span>
		</a>
	</li>
</script>

<script src="assets/js/jquery-1.11.0.min.js"></script>
<script src="assets/js/gsap/main-gsap.js"></script>
<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/joinable.js"></script>
<script src="assets/js/resizeable.js"></script>
<script src="assets/js/neon-api.js"></script>
<script src="assets/js/cookies.min.js"></script>
<script src="assets/js/toastr.js"></script>
<script src="assets/js/neon-custom.js"></script>
<script src="<?= SUM::$DOMAIN ?>:8080/socket.io/socket.io.js"></script>
<script src="assets/js/main_info.js.php"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/node.js"></script>
<script src="assets/js/typeahead.min.js"></script>
<script src="assets/js/ladda/spin.min.js"></script>
<script src="assets/js/ladda/ladda.min.js"></script>
<script src="assets/js/jquery.inputmask.bundle.min.js"></script>
<script src="assets/js/neon-chat.js"></script>
<script src="assets/js/chat.js"></script>
<script src="assets/js/select2/select2.min.js"></script>
<script src="assets/js/bootstrap-switch.min.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/daterangepicker/moment.min.js"></script>
<script src="assets/js/daterangepicker/daterangepicker.js"></script>
<script src="assets/js/photobooth_min.js"></script>
<script src="assets/js/highcharts/highcharts.js"></script>
<script src="assets/js/highcharts/data.js"></script>
<script src="assets/js/highcharts/drilldown.js"></script>
<script src="assets/js/highcharts/highslide.js"></script>
<script src="assets/js/labelauty/jquery-labelauty.js"></script>
<script src="assets/js/table-sortable/bootstrap-sortable.js"></script>
<script src="assets/js/globalize.min.js"></script>
<script src="assets/js/moment.ru.js"></script>
<script src="assets/js/kladr/js/jquery.kladr.min.js"></script>

<!-- Yandex.Metrika counter -->
<noscript><div><img src="//mc.yandex.ru/watch/29425770" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>

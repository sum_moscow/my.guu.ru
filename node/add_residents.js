/**
 * Created by Дмитрий on 23.07.2015.
 */
var server = require('http'),
    rest = require('restler'),
    winston = require('winston'),
    Sync = require('./sync.js'),
    mysql = require('mysql'),
    fs = require("fs"),
    csv = require('fast-csv'),
    config = {};

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

config = fs.readFileSync('../config.json');
config = JSON.parse(config);

var
    config_index = 'local',//process.env.ENV ? process.env.ENV : 'local';
    real_config = config[config_index],
    logger = new (winston.Logger)({
        transports: [
            new winston.transports.File({name: 'info-file', filename: __dirname + '/add_residents.log', level: 'info', json: false}),
            new winston.transports.File({name: 'error-file', filename: __dirname + '/add_residents_error.log', level: 'error', json: false})
        ],
        exitOnError: true
    }),
    connection = mysql.createConnection(real_config.db);
//var log_info = logger.transports['info-file'];

var all_count = 0,
    count = 0;

function addResidents(file_name, format){
    var stream = fs.createReadStream(__dirname + '/residents/' + file_name);
    var csvStream = csv({delimiter:';'})
        .on("data", function(data){
            //setTimeout(function(){
                normalizeData(data, format, ++count, file_name);
            //}, 300);
            //console.log(data);
        })
        .on("end", function(){
            logger.log('info', "done");
        });

    stream.pipe(csvStream);
}

function normalizeData(record, format, count, file_name) {
    function normalizePhoneNumber(phone){
        var new_phone = phone.replace(/[^\d]/g, '');
        if (new_phone.length < 10)
            new_phone = null;
        else if (new_phone.length > 11)
            new_phone = null;
        else if (new_phone.length == 10)
            new_phone = '8' + String(new_phone);
        else if (new_phone[0] == 9 || new_phone[0] == 7)
            new_phone = '8' + new_phone.substring(1);
        return new_phone;
    }

    function normalizeDate(date) {
        date = date.replace(/,/g, '.');
        date = date.split('.');
        date = date[2] + '-' + date[1] + '-' + date[0];
        return date;
    }

    function searchStudent(count, record, resident, file_name) {
        var q = 'http://dev.my.guu.ru/api/students/search?token=3634db2592dc68b0f7a03f8fc42bdd1b&public_key=3a770a24036e95283d29bd64b2dcbb32&time=1' +
            '&full_name=' + encodeURIComponent(resident.full_name) + '&sex=' + resident.sex;
        setTimeout(function(){
            rest
                .get(q)
                .on('complete', function (response){
                    //console.log(q);
                    var data = response.data;
                    //console.log(response);
                    if(data.length != 1){
                        searchStudentWithInstitute(count, record, resident, file_name);
                    }
                    else
                        insertResident(count, record, resident, data, file_name)
                });
        }, 600 * count);
    }
    function searchStudentWithInstitute(count, record, resident, file_name) {
        var q = 'http://dev.my.guu.ru/api/students/search?token=3634db2592dc68b0f7a03f8fc42bdd1b&public_key=3a770a24036e95283d29bd64b2dcbb32&time=1' +
            '&full_name=' + encodeURIComponent(resident.full_name) + '&sex=' + resident.sex + '&institute_abbr=' + resident.institute_abbr;
        setTimeout(function(){
            rest
                .get(q)
                .on('complete', function (response){
                    var data = response.data;
                    if(data.length != 1){
                        logger.log('error', file_name + ' | ' + 'not found' + ' | ' + record);
                        console.log(count);
                    }
                    else
                        insertResident(count, record, resident, data, file_name)
                });
        }, 200 );
    }

    function insertResident(count, record, resident, data, file_name){
        //console.log(resident);
        //console.log(data[0]);
        data = data[0];
        var q = 'http://dev.my.guu.ru/api/dormitories/resident?token=3634db2592dc68b0f7a03f8fc42bdd1b&public_key=3a770a24036e95283d29bd64b2dcbb32&time=1' +
            '&student_id=' + data.student_id + '&phone_number=' + resident.phone_number +
            '&application_number=' + resident.application_number + '&application_date=' + resident.application_date + '&application_document=' + encodeURIComponent(resident.application_document) +
            '&status_id=' + resident.status_id + '&status_reason=' + encodeURIComponent(resident.status_reason) + '&distance_from_Moscow=' + resident.distance_from_Moscow +
            '&note=' + encodeURIComponent(resident.note) + '&came_from=' + encodeURIComponent(resident.came_from);
        setTimeout(function(){
            rest
                .post(q)
                .on('complete', function (response){
                    //console.log(q);
                    if(!response)
                        console.log('WTF');
                    var status = response.status;
                    if(!status){
                        console.log(response.status, response.text);
                        logger.log('error', file_name + ' | ' + 'not added: ' + response.text + ' | ' + record);
                    }
                    console.log(count, response.status, response.text);
                });
        }, 300 );
    }
    var status_types = {
            'заселён': 1,
            'заселен': 1,
            'нуждается': 3,
            'нарушитель': 4,
            'не пришел': 5,
            'отказ': 2,
            'отклонено': 2,
            'отчислен': 6,
            'не пришла': 5,
            'не пришёл': 5,
            'не заселилась в установленный срок': 5,
            'иностранец': 1
        },
        index = {};
    if (format)
    //2014-2015
        index = {
            'application_date': 1,
            'fio': 2,
            'sex': 3,
            'institute_abbr': 4,
            'course': 5,
            'phone_number': 6,
            'application_document': 7,
            'status': 8,
            'status_reason': 9,
            'budget_form': 10,
            'distance_from_Moscow': 11,
            "came_from": 12,
            'note': 13,
            'study_begin': 14,
            'term': 15,
            'study_end': 16,
            'application_number': 17
        };
    else
        index = {
            'application_date': 1,
            'fio': 2,
            'sex': 3,
            'institute_abbr': 4,
            'course': 5,
            'phone_number': 6,
            'application_document': 7,
            'status': 8,
            'status_reason': 9,
            'budget_form': 10,
            'edu_form_name': 11,
            "came_from": 12,
            'note': 13,
            'study_begin': 14,
            'term': 15,
            'study_end': 16
        };
    //console.log(record);
    var
        application_date = record[index.application_date].length != 0 ? normalizeDate(record[index.application_date]) : '2013-09-01',
        fio = record[index.fio].trim().replace(/\s\s/g, ' ').split(' ');
    if (!(fio.length == 2 || fio.length == 3)) {
        logger.log('error', file_name + ' | ' + 'error in name' + ' | ' + record);
        return
    }
    var first_name = fio[1].trim().toLowerCase(),
        middle_name = fio.length == 3 ? fio[2].trim().toLowerCase() : '',
        last_name = fio[0].trim().toLowerCase(),
        sex = record[index.sex].trim() == 'ж' ? 0 : 1,
        institute_abbr = record[index.institute_abbr].trim(), //много хреноты вместо институтов
        course = record[index.course].trim(),
        phone_number = normalizePhoneNumber(record[index.phone_number].trim()),
        application_document = record[index.application_document].trim(),
        status_id = status_types[record[index.status].trim().toLowerCase()],
        status_reason = record[index.status_reason].trim(),
        budget_form = record[index.budget_form].trim(),
        distance_from_Moscow = null,
        came_from = record[index.came_from].trim(),
        note = record[index.note].trim(),
        study_begin = record[index.study_begin].trim(),
        term = record[index.term].trim(),
        study_end = record[index.study_end].trim(),
        application_number = null;
    if (index.hasOwnProperty('application_number')){
        application_number = record[index.application_number].trim();
    }
    if (index.hasOwnProperty('distance_from_Moscow')) {
        distance_from_Moscow = record[index.distance_from_Moscow].trim();
    }
    var resident = {
        "first_name": first_name,
        "middle_name": middle_name,
        "last_name": last_name,
        "full_name": last_name + " " + first_name + " " + middle_name,
        "sex": sex,
        "institute_abbr": institute_abbr,
        "course": course,
        "phone_number": phone_number,
        "application_document": application_document,
        "application_date": application_date,
        "status_id": status_id,
        "status_reason": status_reason,
        "budget_form": budget_form,
        "distance_from_Moscow": distance_from_Moscow,
        "came_from": came_from,
        "note": note,
        "study_begin": study_begin,
        "term": term,
        "study_end": study_end,
        "application_number": application_number
    };
    //console.log(resident);
    //console.log(current + ' ' + first_name.capitalize() + ' ' + middle_name.capitalize() + ' ' + last_name.capitalize() + ' ' + application_id + ' ' + application_date + ' ' + status_id);
    searchStudent(count, record, resident, file_name);
}



var files = ['2013-2014_1.csv', '2013-2014_2.csv', '2013-2014_not.csv'],
    file = '2014-2015.csv';
addResidents(file, 1);
for(var i = 0; i < 3; i++){
    addResidents(files[i], 0);
}
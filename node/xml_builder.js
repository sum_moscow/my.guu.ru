var server = require('http'),
    rest = require('restler'),
    winston = require('winston'),
    Sync = require('./sync.js'),
    mysql = require('mysql'),
    _fs = require("fs"),
    builder = require('xmlbuilder'),
    moment = require('moment'),
    bpay = require('bpay'),
    config = {};

config = _fs.readFileSync('../config.json');
config = JSON.parse(config);

var
    config_index = 'prod',//process.env.ENV ? process.env.ENV : 'local';
    real_config = config[config_index],
    connection = mysql.createConnection(real_config.db);

const
	EDU_CODE_IN_MSR = '019',
	BARCODE_NATIONAL_STANDART = '9',
	BARCODE_RUSSIA_CODE = '644',
	BARCODE_UNIVERSITY_CODE = '99',
	BARCODE_DEFAULT_ARM_CODE = '01',
	RUSSIA_COUNTRY_CODE = '643',
	TYPE_CODE_AND_WORKPOST = 'СТУДЕНТ';

function generateNumberWithLeadingZeros(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}

var xml = builder.create('CLIENTS');
connection.query(
    ' SELECT view_users.*, students.phone_number AS mobile_phone, ' +
    ' users.sex,' +
    ' users.snils, users.birth_place,' +
    ' students.personal_file,' +
    ' view_edu_groups.*,' +
    ' passports.*,' +
    ' doc_types.name as doc_type_name,' +
    ' a.kladr AS registration_kladr,' +
    ' a.flat as registration_flat,' +
    ' b.kladr AS residence_kladr,' +
    ' b.flat as residence_flat' +
    ' FROM view_users' +
    ' INNER JOIN view_students ON view_students.user_id = view_users.id' +
    ' INNER JOIN students ON students.user_id = view_users.id' +
    ' INNER JOIN addresses a ON a.id = view_students.registration_address_id' +
    ' INNER JOIN addresses b ON b.id = view_students.residence_address_id' +
    ' INNER JOIN view_edu_groups ON view_students.edu_group_id = view_edu_groups.edu_group_id' +
    ' INNER JOIN passports ON passports.id = view_students.passport_id' +
    ' INNER JOIN doc_types ON passports.doc_type_id = doc_types.id' +
    ' INNER JOIN users ON view_users.id = users.id' +
    ' WHERE is_student = 1' +
    ' AND a.kladr IS NOT NULL' +
    ' AND b.kladr IS NOT NULL' +
    ' AND users.snils IS NOT NULL' +
    ' AND issue_place_code IS NOT NULL' +
    ' AND view_students.phone_number IS NOT NULL' +
    ' AND passports.issue_date IS NOT NULL' +
    ' LIMIT 1', function(err, rows){
        console.log(rows.length);
        rows.forEach(function(student){
            var _restler = rest;
            _restler
                .json('http://kladr-api.ru/api.php?contentType=building&limit=1&withParent=1&buildingId=' + student.residence_kladr)
                .on('complete', function(residence){
                    _restler
                        .json('http://kladr-api.ru/api.php?contentType=building&limit=1&withParent=1&buildingId=' + student.registration_kladr)
                        .on('complete', function(registration){


                            var identification = BARCODE_NATIONAL_STANDART +
                                    BARCODE_RUSSIA_CODE +
                                    BARCODE_UNIVERSITY_CODE +
                                    BARCODE_DEFAULT_ARM_CODE +
                                    generateNumberWithLeadingZeros(student.personal_file, 6) +
                                    moment().format('MMYY');
                            identification += bpay.getLuhnCheckDigit(identification);
		                    var client = { CLIENTS:
                            {
                                CLIENT:{
                                    ANKETA_ID: {'#text': String(student.id)},
                                    ACTION: {'#text': 0},
                                    IDENTIFICATION: {'#text': identification},
                                    SNILS: {'#text': String(student.snils)},
                                    BARCODE: {'#text': identification.slice(-15)},
                                    TYPE_CODE: {'#text': TYPE_CODE_AND_WORKPOST},
                                    LASTNAME: {'#text': student.last_name},
                                    FIRSTNAME: {'#text': student.first_name},
                                    MIDNAME: {'#text': student.middle_name},
                                    SEX: {'#text': student.sex == '1' ? 'M' : 'Ж'},
                                    BIRTHDAY: {'#text': moment(student.birth_date).format('YYYY-MM-DD[T]HH:mm:ss')},
                                    BIRTHPLACE: {'#text': ''},
	                                EDUCATIONID2: {'#text': EDU_CODE_IN_MSR},
                                    EDUCATIONFORM: {'#text': student.edu_form_name},
                                    EDUCATIONDOCUMENT: {'#text': String(student.personal_file)},
                                    EDUCATIONBEGIN: {'#text': '2014-09-01T00:00:00'},
                                    EDUCATIONEND: {'#text': '2018-09-01T00:00:00'},
                                    EDUCATIONFACILITY: {'#text': student.institute_name},
                                    EDUCATIONYEAR: {'#text': student.course},
                                    DOCUMENT: {
                                        DOC_CODE: {'#text': student.doc_type_name},
                                        DOC_SERIYA: {'#text': student.series},
                                        DOC_NUM: {'#text': student.number},
                                        DOC_WHEN: {'#text': moment(student.issue_date).format('YYYY-MM-DD[T]HH:mm:ss')},
                                        DOC_WHO: {'#text': student.issue_place},
                                        DOC_WHO2: {'#text': student.issue_place_code}
                                    },
	                                MOBILE_PHONE: {'#text': student.mobile_phone},
                                    EMAIL: {'#text': student.ad_login},
                                    REZIDENT: {'#text': '1'},
                                    NATIONALTY_COD: {'#text': RUSSIA_COUNTRY_CODE},
	                                COUNTRY_CODE: {'#text': RUSSIA_COUNTRY_CODE},
                                    WORKNAME: {'#text': ''},
                                    WORKADDR: {'#text': ''},
                                    WORKPOST: {'#text': TYPE_CODE_AND_WORKPOST},
                                    ADDR_REG: {
                                        COUNTRY_CODE: {'#text': RUSSIA_COUNTRY_CODE},
                                        INDEX: {'#text': registration.result[0].zip},
                                        T_REGION: {'#text': registration.result[0].parents[0].typeShort},
                                        REGION: {'#text': registration.result[0].parents[0].name},
                                        T_AREA: {'#text': registration.result[0].parents[1].typeShort},
                                        AREA: {'#text': registration.result[0].parents[1].name},
                                        T_CITY: {'#text': registration.result[0].parents[2].typeShort},
                                        CITY: {'#text': registration.result[0].parents[2].name},
                                        T_STREET: {'#text': registration.result[0].parents[3].typeShort},
                                        STREET: {'#text': registration.result[0].parents[3].name},
                                        T_BUILDING: {'#text': registration.result[0].typeShort},
                                        BUILDING: {'#text': registration.result[0].name},
                                        T_FLAT: {'#text': student.registration_flat.replace(/[0-9]/ig, '')},
                                        FLAT: {'#text': student.registration_flat.replace(/[a-zA-Zа-яА-ЯёЁ]/ig, '').trim()}
                                    },
                                    ADDR_FACT: {
                                        COUNTRY_CODE: {'#text': RUSSIA_COUNTRY_CODE},
                                        INDEX: {'#text': residence.result[0].zip},
                                        T_REGION: {'#text': residence.result[0].parents[0].typeShort},
                                        REGION: {'#text': residence.result[0].parents[0].name},
                                        T_AREA: {'#text': residence.result[0].parents[1].typeShort},
                                        AREA: {'#text': residence.result[0].parents[1].name},
                                        T_CITY: {'#text': residence.result[0].parents[2].typeShort},
                                        CITY: {'#text': residence.result[0].parents[2].name},
                                        T_STREET: {'#text': residence.result[0].parents[3].typeShort},
                                        STREET: {'#text': residence.result[0].parents[3].name},
                                        T_BUILDING: {'#text': residence.result[0].typeShort},
                                        BUILDING: {'#text': residence.result[0].name},
                                        T_FLAT: {'#text': student.residence_flat.replace(/[0-9]/ig, '')},
                                        FLAT: {'#text': student.residence_flat.replace(/[a-zA-Zа-яА-ЯёЁ]/ig, '').trim()}
                                    },
                                    ADDR_POST: {
                                        COUNTRY_CODE: {'#text': 643},
                                        INDEX: {'#text': registration.result[0].zip},
                                        T_REGION: {'#text': registration.result[0].parents[0].typeShort},
                                        REGION: {'#text': registration.result[0].parents[0].name},
                                        T_AREA: {'#text': registration.result[0].parents[1].typeShort},
                                        AREA: {'#text': registration.result[0].parents[1].name},
                                        T_CITY: {'#text': registration.result[0].parents[2].typeShort},
                                        CITY: {'#text': registration.result[0].parents[2].name},
                                        T_STREET: {'#text': registration.result[0].parents[3].typeShort},
                                        STREET: {'#text': registration.result[0].parents[3].name},
                                        T_BUILDING: {'#text': registration.result[0].typeShort},
                                        BUILDING: {'#text': registration.result[0].name},
                                        T_FLAT: {'#text': student.registration_flat.replace(/[0-9]/ig, '')},
                                        FLAT: {'#text': student.registration_flat.replace(/[a-zA-Zа-яА-ЯёЁ]/ig, '').trim()}
                                    },
                                    DOC_COPY:{'#text': ''}
                                }
                            }};
                            //console.log(JSON.stringify(client));

                            _fs.readFile('img.jpg', function(err, data) {
                                client.CLIENTS.CLIENT.DOC_COPY = new Buffer(data).toString('base64');
                                _fs.writeFile(student.id + '.xml', builder.create(client).end({ pretty: true}));
                            });

                        });
                });
        });
});
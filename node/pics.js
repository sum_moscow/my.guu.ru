var server = require('http'),
    rest = require('restler'),
    winston = require('winston'),
    Sync = require('./sync.js'),
    mysql = require('mysql'),
    _fs = require("fs"),
    recursive = require('recursive-readdir'),
    config = {};

config = _fs.readFileSync('../config.json');
config = JSON.parse(config);

var
    config_index = 'prod',//process.env.ENV ? process.env.ENV : 'local';
    real_config = config[config_index],
    connection = mysql.createConnection(real_config.db);

recursive('./БАЗА', function (err, files) {
    // Files is an array of filename
    var files_count = files.length,
        done = 0, success = 0, actual_students_count = 0, staff_count = 0,
        unresolved_students = 0;
    files.forEach(function(name){
        var name_parts = name.split('/');
        name_parts = name_parts[name_parts.length - 1]
            .replace(/\s+/ig,' ')
            .replace('.jpg', '').trim();
        connection.query('SELECT id, is_student, course, edu_form_id, edu_qualification_id FROM view_users' +
        ' WHERE CONCAT_WS(" ", last_name, first_name, middle_name)' +
        ' = ' + connection.escape(name_parts), function(err, rows){
            done++;

            if (rows.length == 1){
                success++;
                var rr = rows[0];
                if (rr.is_student){

                    var max_year = null;

                    if (rr.edu_form_id == 1 && rr.edu_qualification_id == 1){ //бакалавр - день
                        max_year = 3;
                    }else if (rr.edu_form_id == 1 && rr.edu_qualification_id == 2){ // магистр - день
                        max_year = 1;
                    }else if (rr.edu_form_id == 2 && rr.edu_qualification_id == 1){ // бакалавр - вечер
                        max_year = 4;
                    }else if (rr.edu_form_id == 2 && rr.edu_qualification_id == 2){ // магистр - вечер
                        max_year = 2;
                    }else if (rr.edu_form_id == 1 && rr.edu_qualification_id == 3){ //спецалист - день
                        max_year = 4;
                    }else if (rr.edu_form_id == 2 && rr.edu_qualification_id == 3){ //специалист - вечер
                        max_year = 5;
                    }else if (rr.edu_qualification_id == 4){ //аспирант
                        max_year = 2;
                    }else{
                        unresolved_students++;
                    }

                    if (max_year != null && rr.course <= max_year){
                        actual_students_count++;
                    }
                }else{
                    staff_count++;
                }
            }

            console.log({
                dd: done/files_count,
                done: done,
                files_count:files_count,
                success:success,
                staff_count: staff_count,
                actual_students_count:actual_students_count,
                unresolved_students: unresolved_students
            });
        });
    })
});
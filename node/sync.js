var server = require('http'),
	winston = require('winston'),
	csv = require('fast-csv'),
	//dbfkit = require("dbfkit"),
	node_dbf = require('node-dbf'),
	fs = require('fs'),
	iconv = require('iconv-lite'),
	FTPClient = require('ftp'),
	SMB2 = require('smb2'),
	_logger,
	moodle;


function Sync(mysql_connection, restler, config) {

	this.parse_files = {
		SUBJECTS: '../sync/edu/FDIS.DBF',
		ACADEMIC_DEPARTMENTS: '../sync/edu/academic_deps.csv',
		DPV: '../sync/edu/dpv_with_header.csv',
		STUDENTS: '../sync/edu/PERSON.DBF'
	};

	this.smb_settings = config.smb;

	this.rest = restler;

	this.lmsys = config.lmsys;

	this.moodle_settings = config.moodle;

	_logger = new (winston.Logger)({
		transports: [
			new (winston.transports.Console)(),
			new winston.transports.File({filename: __dirname + '/sync.log', json: true})
		],
		exceptionHandlers: [
			new winston.transports.File({filename: __dirname + '/syncexceptions.log', json: false})
		],
		exitOnError: true
	});

	this.connection = mysql_connection;

	//this.DBFParser = dbfkit.DBFParser;

	this.parser = new node_dbf(this.parse_files.STUDENTS);

	this.csv_parser_options = {
		headers: true,
		ignoreEmpty: true,
		delimiter: "\t"
	};

	this.students_ftp = new FTPClient(config.students);
}

Sync.prototype.addPaddingToNumber = function addPaddingToNumber(number, size) {
	var s = number + "";
	while(s.length < size) s = "0" + s;
	return s;
};

Sync.prototype.normalizeSubjectName = function normalizeSubjectName(name) {
	name = name.trim().replace(/\s{2,}/g, ' '); //replace multiple spaces
	name = name.replace(/[^\da-zA-ZА-Яа-яёЁ\s,.\*]\s/gi, function(match) {
		return match.replace(/\s/g, '')
	}); // replace all spaces after NOT letters and numbers (#,$,
	name = name.charAt(0).toUpperCase() + name.slice(1); // capitalize
	return name;
};

Sync.prototype.updateStaff = function updateStaff(callbacks) {

	callbacks = callbacks ? callbacks : {};
	callbacks.onProgress = callbacks.onProgress ? callbacks.onProgress : null;
	callbacks.onDone = callbacks.onDone ? callbacks.onDone : null;
	this.smb2Client = new SMB2(this.smb_settings);

	var q_ins_sync = 'INSERT INTO sync_staff(start_time, finish_time, errors_count, created_at) ' +
		' VALUES(NOW(), NULL, NULL, NOW())',
		users_arr = {}, main_jobs = {};

	var
		smb_client = this.smb2Client,
		parser_options = JSON.parse(JSON.stringify(this.csv_parser_options)),
		mysql_connection = this.connection,
		ou_list_str = '',
		staff_list_str = '',
		ou_count = 0,
		ou_done_count = 0,
		roles_count = 0,
		roles_done_count = 0,
		roles_done = false,
		ou_done = false,
		errors_count = 0,
		staff_sync_id,

		STAFF_LAST_NAME_INDEX = 2,
		STAFF_FIRST_NAME_INDEX = 3,
		STAFF_MIDDLE_NAME_INDEX = 4,
		OU_GUID_INDEX = 6,
		JOB_TITLE_INDEX = 7,
		STAFF_ID_INDEX = 8,
		STAFF_ENABLED_INDEX = 9,
		STAFF_GROUP_TYPE_INDEX = 10,
		STAFF_MAIN_JOB_INDEX = 12,
		STAFF_BIRTH_DATE = 13,
		STAFF_SEX_INDEX = 14;


	function insertRoleAndStaffDone(err) {
		roles_done_count++;
		if (callbacks.onProgress instanceof Function) {
			callbacks.onProgress({
				all: roles_count,
				done: roles_done_count
			});
		}

		console.log({
			all: roles_count,
			done: roles_done_count
		});
		if (err) {
			_logger.error('ERROR', err, {sync_id: staff_sync_id});
			errors_count++;
		}
		if (roles_done_count == roles_count && roles_done) {
			_logger.info('DONE', 'STAFF sync IS DONE');
			var q_upd_sync = 'UPDATE sync_staff SET finish_time = NOW(), errors_count = ' + mysql_connection.escape(errors_count)
				+ ' WHERE id = ' + mysql_connection.escape(staff_sync_id);
			if (callbacks.onDone instanceof Function){
				callbacks.onDone();
			}
			mysql_connection.query(q_upd_sync);
		}
	}

	function setActiveFalseToAllRoles(){
		mysql_connection.query('UPDATE users_roles SET active = 0', function(){
			updateRoles();
		});
	}

	function updateRoles() {

		function setUserMainJob(guid, role, status){
			var key = guid + '-' + role;
			if (main_jobs.hasOwnProperty(key)){
				if (status == 'true'){
					main_jobs[key] = 'true';
				}
			}else{
				main_jobs[key] = status;
			}
			return main_jobs[key];
		}

		function setUserStatus(guid, status){
			if (users_arr.hasOwnProperty(guid)){
				if (status == 'true'){
					users_arr[guid] = 'true';
				}
			}else{
				users_arr[guid] = status;
			}
			return users_arr[guid];
		}


		var lines = staff_list_str.split("\r\n");
		roles_count = lines.length;
		lines.forEach(function(line){
			var data = line.split("\t");
			var q_get_ou_id = 'SELECT id FROM organizational_units WHERE 1c_guid = ' + mysql_connection.escape(data[OU_GUID_INDEX]);

			/*if (data[STAFF_ENABLED_INDEX] == 'false'){
				insertRoleAndStaffDone({text: 'ROLE_INSERT_ENABLED_FALSE'});
				return;
			}*/

			var user_id,
				role_id;

			function insertRole() {
				var enabled = data[STAFF_ENABLED_INDEX] == 'true' ? 1 : 0,
					main_role = setUserMainJob(data[STAFF_ID_INDEX], role_id , data[STAFF_MAIN_JOB_INDEX]) == 'true' ? 1 : 0,
					q_ins_role = 'INSERT INTO users_roles (created_at, user_id, role_id, main_role, active) ' +
						' VALUES ( NOW(), ' +
						mysql_connection.escape(user_id) + ', ' +
						mysql_connection.escape(role_id)+ ', ' +
						main_role + ', ' + enabled +')' +
						' ON DUPLICATE KEY UPDATE ' +
						' active = ' + enabled +
						' ,main_role = ' + main_role;

				mysql_connection.query(q_ins_role, function(role_err) {
					if (role_err) {
						insertRoleAndStaffDone({text: 'ROLE INSERT', err: role_err, query: q_ins_role});
					} else {
						insertRoleAndStaffDone();
					}
				});

			}

			function insertGroup() {
				var
					enabled = data[STAFF_ENABLED_INDEX] == 'true' ? 1 : 0,
					q_ins_groups = 'INSERT INTO memberof (created_at, group_id, user_id, status) ' +
						' SELECT NOW() as created_at, id as group_id, ' +
						mysql_connection.escape(user_id) + ' as user_id,' +
						mysql_connection.escape(enabled) + ' as status' +
						' FROM groups ' +
						' WHERE groups.name = ? OR groups.name = ? AND only_manually = 0' +
						' ON DUPLICATE KEY UPDATE status = ' + enabled;

				mysql_connection.query(q_ins_groups, [data[STAFF_GROUP_TYPE_INDEX], data[JOB_TITLE_INDEX].trim()], function(err) {
					if (err) {
						_logger.error('ERROR', {
							text: 'GROUP NOT FOUND',
							query: q_ins_groups,
							key: data[STAFF_GROUP_TYPE_INDEX]
						});
					}
					insertRole();
				});
			}

			mysql_connection.query(q_get_ou_id, function(err, rows) {
				if (err) {
					insertRoleAndStaffDone({text: 'ORGANIZATIONAL UNIT INSERT', err: err, query: q_get_ou_id});
					return;
				}
				if (!rows || rows.length == 0) {
					insertRoleAndStaffDone({text: 'ORGANIZATIONAL UNIT INSERT', err: err, query: q_get_ou_id});
					return;
				}
				var q_ins_roles = 'INSERT INTO roles (created_at, ou_id, name) VALUES (' +
					'NOW(), ' +
					mysql_connection.escape(rows[0]['id']) + ',' +
					mysql_connection.escape(data[JOB_TITLE_INDEX]) +
					')' +
					' ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID (id)';

				mysql_connection.query(q_ins_roles, function(roles_err, role_res) {
					if (roles_err) {
						insertRoleAndStaffDone({text: 'ROLES INSERT', err: roles_err, query: q_ins_roles});
						return;
					}
					role_id = role_res.insertId;
					var q_check_user = 'SELECT staff.user_id FROM staff WHERE staff.1c_id = ' + mysql_connection.escape(data[STAFF_ID_INDEX]);

					mysql_connection.query(q_check_user, function(check_user_err, users) {
						if (check_user_err) {
							insertRoleAndStaffDone({
								text: 'ROLES INSERT',
								err: check_user_err,
								query: q_check_user
							});
							return;
						}
						var q_ins_user,
							is_active = (setUserStatus(data[STAFF_ID_INDEX], data[STAFF_ENABLED_INDEX]) == 'true') ? 1 : 0;
						if (users && users.length == 1) {
							user_id = users[0]['user_id'];
							q_ins_user = 'UPDATE users SET ' +
							' first_name = ' + mysql_connection.escape(data[STAFF_FIRST_NAME_INDEX]) + ', ' +
							' last_name = ' + mysql_connection.escape(data[STAFF_LAST_NAME_INDEX]) + ', ' +
							' middle_name = ' + mysql_connection.escape(data[STAFF_MIDDLE_NAME_INDEX]) + ', ' +
							' birth_date = ' + mysql_connection.escape(data[STAFF_BIRTH_DATE]) + ', ' +
							' sex = ' + mysql_connection.escape(data[STAFF_SEX_INDEX].toLowerCase() == 'м' ? 1 : 0) + ', ' +
							' active = ' + is_active +
							' WHERE users.id = ' + mysql_connection.escape(user_id);

							mysql_connection.query(q_ins_user, function(user_upd_err) {
								if (user_upd_err) {
									insertRoleAndStaffDone({
										text: 'USER CHECK',
										err: user_upd_err,
										query: q_ins_user
									});
								} else {
									insertGroup();
								}
							});

						} else {
							mysql_connection.beginTransaction(function(transaction_err) {
								if (transaction_err) {
									insertRoleAndStaffDone({text: 'TRANSACTION', err: transaction_err});
									return;
								}

								var q_ins_user = ' INSERT INTO users (created_at, first_name, last_name, middle_name, active, birth_date, sex)' +
									' VALUES(NOW(), ' +
									mysql_connection.escape(data[STAFF_FIRST_NAME_INDEX]) + ',' +
									mysql_connection.escape(data[STAFF_LAST_NAME_INDEX]) + ',' +
									mysql_connection.escape(data[STAFF_MIDDLE_NAME_INDEX]) + ',' +
									mysql_connection.escape(is_active) + ',' +
									mysql_connection.escape(data[STAFF_BIRTH_DATE]) + ',' +
									mysql_connection.escape(data[STAFF_SEX_INDEX].toLowerCase() == 'м' ? 1 : 0) +
									'); ';
								//console.log(q_ins_user);
								mysql_connection.query(q_ins_user, function(transaction_err, user) {
									user_id = user.insertId;

									//console.log(user.insertId);
									if (transaction_err) {
										mysql_connection.rollback(function() {
											insertRoleAndStaffDone({text: 'TRANSACTION', err: transaction_err});
										});
										return;
									}

									var q_ins_staff = ' INSERT INTO staff (created_at, user_id, 1c_id)' +
										' VALUES(NOW(),' +
										mysql_connection.escape(user_id) + ',' +
										mysql_connection.escape(data[STAFF_ID_INDEX]) + ');';

									mysql_connection.query(q_ins_staff, function(err) {
										if (err) {
											mysql_connection.rollback(function() {
												insertRoleAndStaffDone({text: 'TRANSACTION', err: err});
											});
											return
										}
										mysql_connection.commit(function(err) {
											if (err) {
												mysql_connection.rollback(function() {
													insertRoleAndStaffDone({text: 'TRANSACTION', err: err});
												});
											} else {
												insertGroup();
											}
										});
									});
								});
							});
						}
					});

				});
			});
		});
	}

	function updateOUList() {

		function unitDone(data) {
			ou_done_count++;
			if (data.err) _logger.error('ERROR', {query: data.query});
			if (ou_count == ou_done_count && ou_done) {
				setActiveFalseToAllRoles();
			}
		}

		csv
			.fromString(ou_list_str, parser_options)
			.on("data", function(data) {
				var line = [],
					q_ins_ou = null,
					q_parent_id,
					parent_id = null;

				ou_count++;

				for(var key in data) {
					if (data.hasOwnProperty(key)) {
						line.push(mysql_connection.escape(data[key]));
					}
				}
				line.push(line[4] == 'false' ? 1 : 0);
				q_parent_id = 'SELECT id FROM organizational_units WHERE 1c_guid = ' + line[3];
				mysql_connection.query(q_parent_id, function(err, result) {
					if (err) throw err;
					if (result.length == 1) {
						parent_id = result[0]['id'];
					} else {
						parent_id = null;
					}
					q_ins_ou = 'INSERT INTO organizational_units (created_at, name, parent_id, 1c_guid, active)' +
					' VALUES (NOW(),' +
					line[0] + ',' +
					parent_id + ',' +
					line[1] + ',' +
					line[5] +
					') ' +
					'ON DUPLICATE KEY UPDATE ' +
					' name = ' + line[0] + ',' +
					' parent_id = ' + parent_id + ',' +
					' active = ' + line[5];
					mysql_connection.query(q_ins_ou, function(err, result) {
						unitDone({
							err: err,
							result: result,
							query: q_ins_ou
						});
					});
				});
			})
			.on('end', function() {
				ou_done = true;
			});
	}

	function readOUListFile(files) {
		smb_client.readFile(files.OU, {encoding: 'ucs2'}, function(err, data) {
			if (err) throw err;
			ou_list_str = data;
			readStaffFile(files);
		})
	}

	function readStaffFile(files) {
		smb_client.readFile(files.ROLES, {encoding: 'ucs2'}, function(err, data) {
			if (err) throw err;
			staff_list_str = data;
			updateOUList();
		});
	}

	var _smb_client = this.smb2Client;

	mysql_connection.query(q_ins_sync, function(err, row){
		staff_sync_id = row.resultId;

		_smb_client.readdir('', function(err, files) {
			var
				ou_max_date = 0,
				roles_max_date = 0,
				roles_res_name = '',
				ou_res_name = '',
				file_name_pat = /^(\d\d\.\d\d\.\d\d\d\d \d\d\.\d\d\.\d\d\.csv)/gi;
			files.forEach(function(file_name) {
				var ou_date_items,
					roles_curr_date,
					ou_curr_date,
					ou_name,
					roles_name,
					roles_date_items = '';
				if (file_name.indexOf('OU_') != -1) {
					ou_name = file_name.replace('OU_', '').split(' ');
					ou_date_items = ou_name[0].split('.');
					ou_curr_date = new Date().setFullYear(parseInt(ou_date_items[2]), parseInt(ou_date_items[1]) - 1, parseInt(ou_date_items[0]));
					if (ou_max_date < ou_curr_date) {
						ou_max_date = ou_curr_date;
						ou_res_name = file_name;
					}
				} else if (file_name.match(file_name_pat)) {
					roles_name = file_name.split(' ');
					roles_date_items = roles_name[0].split('.');
					roles_curr_date = new Date().setFullYear(parseInt(roles_date_items[2]), parseInt(roles_date_items[1]) - 1, parseInt(roles_date_items[0]));
					if (roles_max_date < roles_curr_date) {
						roles_max_date = roles_curr_date;
						roles_res_name = file_name;
					}
				}
			});
			readOUListFile({
				OU: ou_res_name,
				ROLES: roles_res_name
			});
		});
	});


};

Sync.prototype.updateSubjectsTable = function updateSubjectsTable() {
	var
		mysql_connection = this.connection,
		dbf_parser = new this.DBFParser(this.parse_files.SUBJECTS, "CP866"),
		lines_count = 0,
		lines_done = 0,
		lines_with_error = 0,
		all_lines_done = false;

	function lineIsDone(err) {
		if (err) {
			lines_with_error++;
			_logger.error('ERROR', {err: err});
		}
		lines_done++;

		if (all_lines_done && lines_done == lines_count) {
			_logger.info('Subjects updating is over', {
				all: lines_count,
				with_error: lines_with_error
			});
		}
	}

	function escapeRecord(record) {
		var escaped_record = {};

		record.forEach(function(column) {
			column.escaped_value = mysql_connection.escape(column.value);
			if (column.name == 'DIS') {
				escaped_record['code'] = column.escaped_value;
			} else if (column.name == 'XDIS') {
				escaped_record['name'] = mysql_connection.escape(normalizeSubjectName(column.value));
			} else if (column.name == 'KAF') {
				escaped_record['kad_code'] = column.escaped_value;
			} else {
				escaped_record[column.name] = column.escaped_value;
			}
		});
		return escaped_record;
	}

	dbf_parser.on('record', function(record) {
		lines_count++;
		var escaped_record = escapeRecord(record),
			adep_id = null,
			q_get_adep = 'SELECT id FROM academic_departments WHERE code = ' + escaped_record['kad_code'],
			q_ins_subject;

		mysql_connection.query(q_get_adep, function(err, result) {
			if (err) _logger.error('KAF NOT FOUND', q_get_adep);
			adep_id = (result.length == 1) ? result[0].id : null;

			q_ins_subject = 'INSERT INTO subjects(created_at, name, code, academic_department_id) ' +
				' VALUES (NOW(), ' +
				escaped_record['name'] + ',' +
				escaped_record['code'] + ',' +
				adep_id + ')' +
				' ON DUPLICATE KEY UPDATE ' +
				' name = ' + escaped_record['name'] + ',' +
				' code = ' + escaped_record['code'] + ',' +
				' academic_department_id = ' + adep_id;
			mysql_connection.query(q_ins_subject, lineIsDone);
		});

	});

	dbf_parser.parse();
};

Sync.prototype.updateSubjects = function updateSubjects() {
	/*Update academic departments*/
	var mysql_connection = this.connection,
		str = fs.readFileSync(this.parse_files.ACADEMIC_DEPARTMENTS, "ucs2"),
		lines_count = 0,
		lines_done = 0,
		lines_with_error = 0,
		all_lines_done = false;

	function lineIsDone(err) {
		if (err) {
			lines_with_error++;
			_logger.error('ERROR', {err: err});
		}
		lines_done++;

		if (all_lines_done && lines_done == lines_count) {
			_logger.info('Departments updating is over', {
				all: lines_count,
				with_error: lines_with_error
			});
			updateSubjectsTable();
		}
	}

	csv
		.fromString(str, this.csv_parser_options)
		.on("data", function(data) {
			lines_count++;

			var line = [],
				q_ins_dep,
				q_ou_id,
				ou_id = null,
				unescaped_key;
			for(var key in data) {
				if (data.hasOwnProperty(key)) {
					unescaped_key = encodeURIComponent(key);
					line.push(mysql_connection.escape(data[key]));
					if (line.length == 1) {
						line.push(mysql_connection.escape(addPaddingToNumber(data[key], 3)));
					} else if (line.length == 3) {
						line.push(mysql_connection.escape('Кафедра ' + data[key]));
					}
				}
			}

			line[4] = (line[4] == '\'\'') ? null : line[4];
			q_ou_id = 'SELECT id FROM organizational_units WHERE name = ' + (line[3]);

			mysql_connection.query(q_ou_id, function(err, result) {
				if (err) throw err;
				ou_id = (result.length == 1) ? result[0]['id'] : null;

				q_ins_dep = 'INSERT INTO academic_departments (created_at, code, name, ou_id, university_code)' +
				' VALUES (NOW(),' +
				line[1] + ',' +
				line[2] + ',' +
				ou_id + ',' +
				line[4] +
				') ' +
				'ON DUPLICATE KEY UPDATE ' +
				' code = ' + line[1] + ',' +
				' ou_id = ' + ou_id + ',' +
				' name = ' + line[2] + ',' +
				' university_code = ' + line[4];

				mysql_connection.query(q_ins_dep, lineIsDone);
			});
		}).on('end', function() {
			all_lines_done = true;
		});
};

Sync.prototype.updateTimetable = function updateTimetable() {

	var mysql_connection = this.connection,
		_restler = this.rest,
		req_url = this.lmsys.url,
		_args = JSON.parse(JSON.stringify(this.lmsys.post_groups_timetable)),
		localNormalizeSubjectName = this.normalizeSubjectName,
		sync_id,
		semester_id,
		_groups = {
			count: 0,
			done: 0,
			error: 0,
			items_errors: 0
		},
		sync_time = {
			start: new Date(),
			finish: null
		};

	function logInfo(data){
		var q_ins_data = 'INSERT INTO log_timetable(created_at, body, sync_id) VALUES (NOW(), ' + mysql_connection.escape(JSON.stringify(data)) +', ' + sync_id +')';
		mysql_connection.query(q_ins_data, function(err){
			if (err){
				_logger.error("LOG_PUT_ERROR", data);
			}
		});
	}

	function updateAllClassesList(){
		console.log('SELECT * FROM timetable ' +
		' INNER JOIN timetable_subjects ON timetable_subjects.id = timetable.subject_id' +
		' INNER JOIN timetable_items_professors ON timetable_items_professors.timetable_item_id = timetable.id ' +
		'   WHERE timetable_subjects.semester_id = ' + mysql_connection.escape(semester_id) +
		' AND timetable.sync_id = ' + mysql_connection.escape(sync_id))
		mysql_connection.query('SELECT * FROM timetable ' +
		' INNER JOIN timetable_subjects ON timetable_subjects.id = timetable.subject_id' +
		' INNER JOIN timetable_items_professors ON timetable_items_professors.timetable_item_id = timetable.id ' +
		'   WHERE timetable_subjects.semester_id = ' + mysql_connection.escape(semester_id) +
		' AND timetable.sync_id = ' + mysql_connection.escape(sync_id), function(err, rows){
			/*var curr_date,
				_today = new Date(),
				all_days_count = 0,
				day_type,
				class_type,
				week_type = 1,
				key,
				timetable = res.data.timetable,
				classes_info = res.data.classes_info,
				class_date, class_number, class_id,
				i = 0;
			curr_date = new Date(SUBJECT_DATES.start_date);

			do{
				day_type = curr_date.getDay() + 1;
				for (i = 1; i < 5; i++){
					class_type = i;
					key = [day_type, class_type, week_type].join('-');
					if (timetable.hasOwnProperty(key)){
						if (timetable[key].professor_id == window.getUserInfo().id
							&& timetable[key].timetable_subject_id == subject_id){
							if (curr_date > _today){
								is_future = 'future';
							}
							class_date = null;
							class_id = null;
							class_number = (all_days_count + 1);
							classes_info.forEach(function(value){
								if (value.number == class_number){
									class_date = value.date;
									class_id = value.class_id;
								}
							});

							$days.append(tmpl('day-of-month-cell', {
								class_number: class_number,
								is_future: is_future,
								class_id: class_id,
								class_date: class_date,
								class_text: (class_date == null) ? class_number : moment(class_date, 'YYYY-MM-DD').format('DD.MM')
							}));
							all_days_count++;
						}
					}
				}

				curr_date.setDate(curr_date.getDate() + 1);

				if (curr_date.getDay() == 0){ // Change week type on Monday
					week_type = (week_type == 1) ? 2 : 1;
				}
			}while(curr_date < SUBJECT_DATES.finish_date);*/
		});
	}

	function groupIsDone(error, items_error_count){
		_groups.done++;
		if (error){
			_logger.error(error.name, {error: error});
		}
		if (items_error_count){
			_groups.items_errors += items_error_count;
		}
		//console.log(_groups);
		if (_groups.done == _groups.count){
			updateAllClassesList();
			updateSyncInfo();
			sync_time.finish = new Date();
		}
	}

	function updateSyncInfo(){
		var q_upd_sync = 'UPDATE sync_timetable SET finish_time = NOW(), items_added = ' +
			mysql_connection.escape(_groups.count) +
			', errors_count = ' +  mysql_connection.escape(_groups.items_errors) +
			' WHERE id = ' + mysql_connection.escape(sync_id);
		//console.log('TIMETABLE_SYNC_DONE');
		_logger.info('TIMETABLE_SYNC_DONE', {groups: _groups});
		mysql_connection.query(q_upd_sync, function(err){_logger.info('SYNC_INFO_UPDATED')});
	}

	function getAndSaveTimeTable(group_id, enot_id) {
		var closure_args = JSON.parse(JSON.stringify(_args)),
			items = {
				count: 0,
				done: 0,
				error: 0,
				success: 0
			},
			error_count = 0;

		closure_args.data.group = enot_id;


		function timetableItemDone(error){
			items.done++;
			if (error){
				_logger.error(error.name, error);
				_groups.items_errors++;
			}
			if (items.done == items.count){
				groupIsDone(null, error_count, closure_args.data);
			}
		}

		function insertItemsProfessor(name, item_id){
			if (name.length == 0){
				timetableItemDone({name: 'LECTOR_NOT_FOUND', data:{name: name, item: item_id}});
				return;
			}else{
				if (name.length != 3){
					timetableItemDone({name: 'LECTOR_NAME_NOT_FULL', data: {name: name, item: item_id}}, closure_args.data);
				}
				name[0] = name[0] ? name[0] : '';
				name[1] = name[1] ? name[1] : '';
				name[2] = name[2] ? name[2] : '';
			}

			var q_ins_professor = 'INSERT INTO timetable_items_professors(created_at, timetable_item_id, staff_id) ' +
				' SELECT NOW() as created_at, ' +
				mysql_connection.escape(item_id) + ' as timetable_item_id,' +
				' view_users.staff_id as professor_id ' +
				' FROM view_users' +
				' WHERE ' +
				' view_users.last_name = ' + mysql_connection.escape(name[0]) +
				' AND view_users.first_name LIKE ' + mysql_connection.escape(name[1] + '%') +
				' AND view_users.middle_name LIKE ' + mysql_connection.escape(name[2] + '%') +
				' AND view_users.active = 1' +
				' AND view_users.staff_id IS NOT NULL' +
				' ORDER BY view_users.staff_id' +
				' LIMIT 1';
			mysql_connection.query(q_ins_professor, function(err, result){
				if (err) {
					timetableItemDone({
						name: 'INSERT_PROFESSOR_ERROR',
						data: {err: err, query: q_ins_professor}
					});
				}
				else if (result.insertId == 0){
					timetableItemDone({
						name: 'PROFESSOR_NOT_FOUND',
						data: {err: err, query: q_ins_professor}
					});
				}else{
					items.success++;
					timetableItemDone();
				}
			})
		}

		function insertTimetableItem(data){
			if (data.week_number == ''){
				data.week_number = null;
			}
			console.log(data.timetable_subject_id);
			var timetable_subject_id = data.timetable_subject_id,
				q_ins_timetable_item = 'INSERT INTO timetable (created_at, timetable_subject_id, ' +
					' format_id, day_type_id, week_type_id,' +
					' week_number, class_type_id,' +
					' building_id, room, sync_id) ' +
					'SELECT NOW() as created_at, ' +
					mysql_connection.escape(timetable_subject_id) + ' as timetable_subject_id,' +
					' (SELECT id FROM timetable_formats WHERE abbr = ' + mysql_connection.escape(data.format) + ') as format_id,' +
					' (SELECT id FROM timetable_days WHERE number = ' + mysql_connection.escape(data.day_number) + ') as day_type_id,' +
					' (SELECT id FROM timetable_weeks WHERE number = ' + mysql_connection.escape(data.week_type) + ') as week_type_id,' +
					mysql_connection.escape(data.week_number) + ' as week_number,' +
					' (SELECT id FROM timetable_class_types WHERE start_time = FROM_UNIXTIME(' + mysql_connection.escape(data.start_time) + ') AND ' +
					' finish_time = FROM_UNIXTIME(' + mysql_connection.escape(data.finish_time) + ') LIMIT 1) as class_type_id,' +
					' (SELECT id FROM buildings WHERE abbr = ' + mysql_connection.escape(data.building_abbr) + ' OR name = ' + mysql_connection.escape(data.building_abbr) + ') as building_id,' +
					mysql_connection.escape(data.room) + ' as room,' +
					mysql_connection.escape(sync_id) + ' as sync_id';

			mysql_connection.query(q_ins_timetable_item, function(_err, _result){
				if (_err){
					timetableItemDone({name: 'QUERY_ERROR', data: {err: _err, query: q_ins_timetable_item}});
				}else{
					if (_result.insertId == 0){
						timetableItemDone({name: 'INSERT_TIMETABLE_ITEM_RESULT_ID_IS_ZERO', data: {err: _err, query: q_ins_timetable_item}});
					}else{
						data.professor.forEach(function(name){
							name = name.replace(/\s+/gmi, ' ');
							name = name.split(' ');
							name.forEach(function(value, index){
								name[index] = value.replace(/\.|\\|,/gmi, '');
							});
							insertItemsProfessor(name,_result.insertId);
						});
					}
				}

			});
		}

		function insTimetableSubjects(data){
			var q_ins_timetable_subjects = 'INSERT INTO timetable_subjects( created_at, edu_group_id, subject_id, enot_id, since, till, semester_id) ' +
					' VALUES (NOW(),' +
					mysql_connection.escape(data.edu_group_id) + ', ' +
					mysql_connection.escape(data.subject_id) + ', ' +
					mysql_connection.escape(data.enot_id) + ', ' +
					' FROM_UNIXTIME(' + mysql_connection.escape(data.since) + '), ' +
					' FROM_UNIXTIME(' + mysql_connection.escape(data.till) + '), ' +
					mysql_connection.escape(data.semester_id) + ') ' +
					' ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID (id),' +
					' since = FROM_UNIXTIME(' + mysql_connection.escape(data.since) + '),' +
					' till = FROM_UNIXTIME(' + mysql_connection.escape(data.till) + ')'
				;
			mysql_connection.query(q_ins_timetable_subjects, function(err, __result){
				if (err){
					timetableItemDone({name: 'QUERY_ERROR', data: {error: err, query: q_ins_timetable_subjects, data: data}});
				}else{
					if (__result.insertId == 0){
						timetableItemDone({name: 'TIMETABLE_SUBJECT_INSERT_RESULT_ID_IS_ZERO', data: {error: err, query: q_ins_timetable_subjects, data: data}});
					}else{
						data.timetable_subject_id = __result.insertId;
						insertTimetableItem(data);
					}
				}
			});

		}

		function insAcademicDepartmentSubject(data, callback){
			var q_ins_subject = 'INSERT INTO subjects (academic_department_id, created_at, added_manually, name, code) ' +
				' SELECT id as academic_department_id, NOW() as created_at, 1 as added_manually,' +
				mysql_connection.escape(data.name) + ' as name, \'______\' as code' +
				' FROM academic_departments ' +
				' WHERE academic_departments.university_code IS NOT NULL ' +
				' AND ( ' +
					'CONCAT(\'Кафедра \', academic_departments.name) = ' +
						mysql_connection.escape(data.academic_department_name) +
					' OR academic_departments.additional_name = '
					+ mysql_connection.escape(data.academic_department_name) +
				') ' +
				'ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID (subjects.id)';

			_logger.info('INSERT_SUBJECT', {data: data, q: q_ins_subject});
			mysql_connection.query(q_ins_subject, function(err, result){
				if (err){
					timetableItemDone({name: 'CAN_NOT_ADD_SUBJECT_FOR_DEPARTMENT', data: {error: err, query: q_ins_subject}});
				} else if (result.insertId == 0){
					timetableItemDone({name: 'ADD_SUBJECT_FOR_DEPARTMENT_RESULT_ID_IS_ZERO', data: {query: q_ins_subject}});
				}else{
					if (callback instanceof Function){
						callback(result.insertId);
					}
				}
			});
		}

		_restler
			.post(req_url, closure_args)
			.on('complete', function(tt){
				try{
					var timetable = JSON.parse(tt);
				}catch(e){
					groupIsDone({
						name: 'JSON_PARSE_ERROR',
						data: e
					});
					return;
				}

				items.count = timetable.length;

				timetable.forEach(function(item) {

					item['0'] = localNormalizeSubjectName(item['0']);

					var q_get_kaf =
							' SELECT DISTINCT academic_department_id, subjects.id ' +
							' FROM subjects ' +
							' INNER JOIN academic_departments ON subjects.academic_department_id = academic_departments.id' +
							' WHERE subjects.name = ' + mysql_connection.escape(item['0']) +
							' AND (' +
								'CONCAT(\'Кафедра \', academic_departments.name) = ' + mysql_connection.escape(item['11']) +
								' OR ' +
								' academic_departments.additional_name = ' + mysql_connection.escape(item['11']) +
							')' +
							' AND academic_departments.university_code IS NOT NULL' +
							' GROUP BY academic_department_id';
					logInfo(item);
					mysql_connection.query(q_get_kaf, function(err, result) {
						if (err) {
							timetableItemDone({
								name: 'QUERY_ERROR',
								data: {
									err: err,
									q: q_get_kaf
								}
							});
							return;
						}

						var building_type, room;

						if (item[8].match(/.*?-\d+/ig)){
							building_type = item[8].split('-')[0];
							room = item[8].split('-')[1].trim();
						}else if (item[8].match(/\d.*?\(..\)/ig)){ //TODO: Костыль для [2 этаж (БЦ)], зставить убрать Стружкина неправильные именования
							building_type = item[8].split('(');
							room = building_type[0].trim();
							building_type = building_type[1].replace(')', '');
						}else if (item[8].match(/.*?\(.*?\)/ig)){ //TODO: Костыль для [2 этаж (БЦ)], зставить убрать Стружкина неправильные именования
							building_type = item[8].split('(');
							room = building_type[0].trim();
							building_type = building_type[1].replace(')', '');
						}else{
							building_type = item[8];
							room = null;
						}
						building_type = building_type.trim();


						if (result.length > 0){
							insTimetableSubjects({
								edu_group_id: group_id,
								subject_id: result[0].id,
								semester_id: semester_id,
								enot_id: enot_id,
								since: item[9],
								till: item[10],
								format: item[2],
								day_number: item[3],
								week_type: item[4] == '' ? 0: item[4],
								week_number: item[5],
								start_time: item[6],
								finish_time: item[7],
								professor: item[1],
								building_abbr: building_type,
								room: room
							});
						}else if (result.length == 0){
							_logger.info('DEPARTMENT_WITH_SAME_NAME_AND_SUBJECT_NOT_FOUND', {kaf: item['11'], query: q_get_kaf});
							insAcademicDepartmentSubject({
								academic_department_name: item['11'],
								name: item['0']
							}, function(subject_id){
									insTimetableSubjects({
										edu_group_id: group_id,
										subject_id: subject_id,
										semester_id: semester_id,
										enot_id: enot_id,
										since: item[9],
										till: item[10],
										format: item[2],
										day_number: item[3],
										week_type: item[4] == '' ? 0: item[4],
										week_number: item[5],
										start_time: item[6],
										finish_time: item[7],
										professor: item[1],
										building_abbr: building_type,
										room: room
									});
								});
						}else{
							timetableItemDone({
								name: 'FOUND_MORE_THAN_ONE_DEPARTMENT',
								data: {
									length: result.length,
									result: result,
									q: q_get_kaf,
									item: item,
									enot_id: enot_id
								}
							});
						}
					});
				});
			});
	}

	function getGroupDone(data) {
		if (data.error) {
			groupIsDone({
				name: 'QUERY_ERROR',
				data: data
			});
		} else if (!data.rows) {
			groupIsDone({
				name: 'GROUP_NOT_FOUND',
				data: data
			});
		}else if (data.rows.length != 1) {
			groupIsDone({
				name: 'TOO_MANY_GROUPS',
				data: data
			});
		}else{
			getAndSaveTimeTable(data.rows[0].id, data.group[6]);
		}
	}

	_restler
		.post(this.lmsys.url, this.lmsys.post_groups_list)
		.on('complete', function(groups) {
			mysql_connection.query('SELECT id FROM semesters WHERE active = 1', function(err2, rows){
				if (err2){
					_logger.log('CANT_GET_SEMESTER_ID', err2);
				}else{
					semester_id = rows[0].id;
					mysql_connection.query('INSERT INTO sync_timetable SET start_time = NOW(), created_at = NOW()', function(err, result) {
						if (err) throw err;

						var json_groups = JSON.parse(groups),
							program_code,
							profile_code;

						_groups.count = json_groups.length;
						sync_id = result.insertId;

						json_groups.forEach(function(group) {
							var str_code = group[3],
								query;

							profile_code =
								str_code[0] == '0'
									? ' AND profile_code IS NULL '
									: ' AND profile_code = ' + (mysql_connection.escape(str_code[0] + '0'));

							program_code =
								str_code[1] == '0'
									? ' AND program_code IS NULL '
									: ' AND program_code = ' + (mysql_connection.escape(str_code));

							query = 'SELECT view_edu_groups.edu_group_id as id FROM view_edu_groups' +
							' WHERE edu_form_code = ' + mysql_connection.escape(group[0]) +
							' AND institute_code = ' + mysql_connection.escape(group[1]) +
							' AND edu_speciality_code = ' + (mysql_connection.escape(group[2] + '00') ) +
							program_code +
							profile_code +
							' AND course = ' + mysql_connection.escape(group[4]);

							if (group[5] != '') {
								query += ' AND `group` = ' + mysql_connection.escape(group[5])
							}

							mysql_connection.query(query, function(err, rows) {
								getGroupDone({
									'error': err,
									'rows': rows,
									'query': query,
									'group': group
								});
							});
						});
					});
				}
			});
		});
};

Sync.prototype.updateStudents = function updateStudents(){
	var rcounter = 0;
	iconv.extendNodeEncodings();
	this.parser.on('start', function() {
		console.log('dBase file parsing has started');
	});

	this.parser.on('record', function(record) {
		rcounter++;
		if (rcounter > 5) return;
		for (var key in record){
			if (!record.hasOwnProperty(key)) continue;
			var str = '',buf = '';
			try{
				buf = new Buffer(record[key], 'CP866');
				str =  buf.toString('ucs2');
				_logger.info({'key': key, 'val':str});
			}catch (e){
				console.log(e);
			}
		}
	});

	this.parser.parse();
};

Sync.prototype.updateDPV = function(){
	var str = fs.readFileSync(this.parse_files.DPV, "ucs2"),
		mysql_connection = this.connection,
		lines = str.split("\n");
	lines.forEach(function(line){
		if (line.split("\t").length != 9){
			return;
		}
		var q_upd_dpv_name = 'UPDATE timetable_subjects ' +
			' INNER JOIN subjects ON subjects.id = timetable_subjects.subject_id' +
			' INNER JOIN view_edu_groups ON timetable_subjects.edu_group_id = view_edu_groups.edu_group_id' +
			' SET subject_normal_name = ' + mysql_connection.escape(line[1]) +
			' WHERE timetable_subjects. = ' + mysql_connection.escape();
	});
};

Sync.prototype.updateMoodleMarks = function(user, callback){
	var courseids_in_moodle = [187, 211, 207, 227, 229, 216, 186, 111],
		_restler = this.rest,
		mysql_connection = this.connection,
		_moodle_settings = JSON.parse(JSON.stringify(this.moodle_settings));

	_moodle_settings.get_user_id.data.values.push(user.email);

	_restler
		.post(_moodle_settings.get_user_id.url, {"headers": {}, data: _moodle_settings.get_user_id.data})
		.on('complete', function(users){
			var moodle_id = users[0].id,
				items_done = 0,
				items_count = courseids_in_moodle.length;

			console.log(moodle_id);

			function gradesInsDone(err){
				if (err){
					_logger.info(err);
				}
				items_done++;
				if (items_done == items_count){
					console.log('END');
					if (callback instanceof Function){
						callback();
					}
				}
			}

			function insActivitiesAndGrades(items, moodle_course_id){



				function insGrades(grades, moodle_activity_id){
					var _grades_count = grades.length,
						_grades_done = 0;

					function insGradesForCourseDone(err){
						if (err) _logger.err('GRADE_INS_ERROR');
						_grades_done++;
						if (_grades_count == _grades_done){
							gradesInsDone();
						}
					}

					grades.forEach(function(grade){
						var q_ins_grade = 'INSERT INTO moodle_grades(created_at, user_id, moodle_activity_id,' +
							' grade, dategraded_in_moodle) ' +
							' VALUES(NOW(), ' +
							mysql_connection.escape(user.user_id) + ',' +
							mysql_connection.escape(moodle_activity_id) + ',' +
							mysql_connection.escape(grade.grade) + ',' +
							'FROM_UNIXTIME(' + mysql_connection.escape(grade.dategraded) + ')) ' +
							' ON DUPLICATE KEY UPDATE' +
							' id = LAST_INSERT_ID(id),' +
							' dategraded_in_moodle = FROM_UNIXTIME(' + mysql_connection.escape(grade.dategraded) + '),' +
							' grade = ' + mysql_connection.escape(grade.grade);
						console.log(q_ins_grade);
						mysql_connection.query(q_ins_grade, insGradesForCourseDone);
					});
				}

				items.forEach(function(item){
					var q_ins_activity = 'INSERT moodle_activities(created_at, name, activityid_in_moodle,' +
						' grademax_in_moodle, moodle_edu_groups_courses_id)' +
						' VALUES (' +
						'NOW(),' +
						mysql_connection.escape(item.name) + ',' +
						mysql_connection.escape(item.activityid) + ',' +
						mysql_connection.escape(item.grademax) + ',' +
						mysql_connection.escape(moodle_course_id) + ')' +
						'ON DUPLICATE KEY UPDATE ' +
						'id = LAST_INSERT_ID(id), ' +
						'name = ' + mysql_connection.escape(item.name) +', ' +
						'grademax_in_moodle = ' + mysql_connection.escape(item.grademax);
					mysql_connection.query(q_ins_activity, function(err, result){
						if (err){
							gradesInsDone(err);
							return;
						}
						insGrades(item.grades, result.insertId);
					});
				});
			}

			function getGrades(course_id, user_id){
				var grades_url = _moodle_settings.get_grades.url,
					data = {"headers": {}, data: JSON.parse(JSON.stringify(_moodle_settings.get_grades.data))};
				data.data.courseid = course_id;
				data.data.userids.push(user_id);

				_restler
					.post(grades_url, data)
					.on('complete', function(data) {
						var q_get_groups_course_id = 'SELECT moodle_edu_groups_courses.id ' +
							' FROM moodle_edu_groups_courses' +
							' INNER JOIN moodle_courses ON moodle_courses.id = moodle_edu_groups_courses.moodle_course_id ' +
							' WHERE semester_id = 1' +
							' AND edu_group_id = (SELECT edu_group_id FROM view_users WHERE view_users.ad_login = ' + mysql_connection.escape(user.email) + ')' +
							' AND moodle_courses.courseid_in_moodle = ' + mysql_connection.escape(course_id);


						mysql_connection.query(q_get_groups_course_id, function(err, rows){
							if (err){
								gradesInsDone(err);
								return;
							}
							if (rows.length != 1){
								gradesInsDone({err: 'COURSE_NOT_FOUND_FOR_STUDENT'});
								return;
							}
							moodle_course_id = rows[0].id;
							insActivitiesAndGrades(data.items, moodle_course_id);
						})
					})
			}
			courseids_in_moodle.forEach(function(value){
				getGrades(value, moodle_id);
			});
		})
};

Sync.prototype.updateAllClassesList = function(){
	var semester_id = 2,
		sync_id = 296,
		mysql_connection = this.connection;

		mysql_connection.query('SELECT * FROM timetable ' +
			' INNER JOIN timetable_subjects ON timetable_subjects.id = timetable.timetable_subject_id' +
			' INNER JOIN timetable_items_professors ON timetable_items_professors.timetable_item_id = timetable.id ' +
			'   WHERE timetable_subjects.semester_id = ' + mysql_connection.escape(semester_id) +
			' AND timetable.sync_id = ' + mysql_connection.escape(sync_id), function(err, rows){
			rows.forEach(function(row){
				var curr_date,
					all_days_count = 0,
					day_type,
					class_type,
					week_type = 1,
					key,
					i = 0;
				curr_date = new Date(row.since);

				do{
					day_type = curr_date.getDay() + 1;

					if (timetable.hasOwnProperty(key)){
						if (timetable[key].professor_id == window.getUserInfo().id
							&& timetable[key].timetable_subject_id == subject_id){
							if (curr_date > _today){
								is_future = 'future';
							}
							class_date = null;
							class_id = null;
							class_number = (all_days_count + 1);
							classes_info.forEach(function(value){
								if (value.number == class_number){
									class_date = value.date;
									class_id = value.class_id;
								}
							});

							$days.append(tmpl('day-of-month-cell', {
								class_number: class_number,
								is_future: is_future,
								class_id: class_id,
								class_date: class_date,
								class_text: (class_date == null) ? class_number : moment(class_date, 'YYYY-MM-DD').format('DD.MM')
							}));
							all_days_count++;
						}
					}

					curr_date.setDate(curr_date.getDate() + 1);

					if (curr_date.getDay() == 0){ // Change week type on Monday
						week_type = (week_type == 1) ? 2 : 1;
					}
				}while(curr_date < row.till);
			});
		});
};

Sync.prototype.importAbiturients = function(){

};

Sync.name = '';

module.exports = Sync;
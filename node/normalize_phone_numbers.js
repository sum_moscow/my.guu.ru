var server = require('http'),
    rest = require('restler'),
    winston = require('winston'),
    Sync = require('./sync.js'),
    mysql = require('mysql'),
    _fs = require("fs"),
    config = {};

config = _fs.readFileSync('../config.json');
config = JSON.parse(config);

var
    config_index = 'local',//process.env.ENV ? process.env.ENV : 'local';
    real_config = config[config_index],
    logger = new (winston.Logger)({
            transports: [
                new winston.transports.File({filename: __dirname + '/normalize_phone_numbers.log', json: false})
            ],
        exitOnError: true
    }),
    connection = mysql.createConnection(real_config.db);


var all_count = 0,
    count = 0,
//    done = 0,
//    result_null = 0,
    done_well = 0;
connection.query("SELECT id, phone_number FROM students WHERE phone_number REGEXP '((plus-sign7)|8)?[0-9]{10}' = 0 AND phone_number IS NOT NULL", function(err, rows){
    all_count = rows.length;
    rows.forEach(function(row){
            function normalizePhoneNumber(phone){
                var new_phone = phone.replace(/[^\d]/g, '');
                if(new_phone.length < 10)
                    new_phone = null;
                else if(new_phone.length > 11)
                    new_phone = null;
                else if(new_phone.length == 10)
                    new_phone = '8' + new_phone;
                else if(new_phone[0] == 9 || new_phone[0] == 7)
                    new_phone = '8' + new_phone.substring(1);
                return new_phone;
            }
            var student_id = row.id,
                phone = row.phone_number,
                new_phone = normalizePhoneNumber(phone);
            var sql = 'UPDATE students SET phone_number = ? WHERE id = ?';
            connection.query(sql, [new_phone, student_id], function(err, result){
                console.log(err);
                if (!err)
                    logger.info(++done_well + '/' + all_count + '\t' + phone + '\tto\t' + new_phone);
            });
            count++;
        }
    );
}).on('end', function() {
    console.log('done: ' + done_well + '/' + all_count);
});

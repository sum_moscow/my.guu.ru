

var server = require('http'),
	io = require('socket.io')(server),
	winston = require('winston'),
	_ = require('underscore'),
	rest = require('restler'),
	mysql = require('mysql'),
	CronJob = require('cron').CronJob,
	Sync = require('./sync.js'),
	_fs = require("fs"),
	nodemailer = require('nodemailer'),
	smtpTransport = require('nodemailer-smtp-transport'),
	config = {},
	EMAIL_TEMPLATES_FOLDER =  '../tmpl/email/';

config = _fs.readFileSync('../config.json');
config = JSON.parse(config);


var config_index = process.env.ENV ? process.env.ENV : 'local';

	var real_config = config[config_index],
		connection = mysql.createConnection(real_config.db),
		active_users = {},
		AZURE_CLIENT_ID = real_config.azure.client_id,
		AZURE_CLIENT_SECRET = real_config.azure.client_secret,
		AZURE_RESOURCE = real_config.azure.resource;
		AZURE_URLS = real_config.azure.urls,
			transporter  = nodemailer.createTransport(smtpTransport({
				host: real_config.smtp.host,
				port: real_config.smtp.port,
				secure: false,

				auth: {
					user: real_config.smtp.user,
					pass: real_config.smtp.password
				}
			}));
connection.connect();

var logger = new (winston.Logger)({
	transports: [
		new winston.transports.File({ filename: __dirname + '/debug.log', json: false })
	],
	exceptionHandlers: [
		new winston.transports.File({ filename: __dirname + '/exceptions.log', json: false })
	],
	exitOnError: true
})//,
	sync = new Sync(connection, rest, real_config);
try {
	new CronJob('00 30 20 * * 1-5', function(){
		logger.info('Staff SYNC', 'START...' + new Date().toString());
		sync.updateStaff({
				onProgress: function(data){
					logger.info('Staff SYNC', data);
				},
				onDone: function() {
					logger.info('Staff SYNC', 'DONE!');
				}
			}
		);
	}, null, true);


	if (config_index == 'prod'){ // ONLY FOR PROD NOW!
		new CronJob('00 00 11 * * 1-7', function(){
			logger.info('Birthday congrats', 'START...' + new Date().toString());
			congratulateBirthdays();
		}, null, true);
	}
} catch(ex) {
	logger.error("CRON ERROR","cron pattern not valid");
}

function congratulateBirthdays(){
	connection.query('SELECT users.first_name, users.sex, users.middle_name, users.ad_login, users.birth_date ' +
		' FROM users' +
		' INNER JOIN view_users ON users.id = view_users.id' +
		' WHERE DAY(users.birth_date) = DAY(NOW())' +
		' AND MONTH(users.birth_date) = MONTH(NOW())' +
		' AND users.active = 1' +
		' AND view_users.is_staff = 1',
	function(err, rows){
		var bday_templates = {
			'male': _fs.readFileSync(EMAIL_TEMPLATES_FOLDER + 'bday_male.html', 'utf8'),
			'female': _fs.readFileSync(EMAIL_TEMPLATES_FOLDER + 'bday_female.html', 'utf8'),
			'unknown': _fs.readFileSync(EMAIL_TEMPLATES_FOLDER + 'bday_unknown.html', 'utf8')
		},
			emails_to_send = rows.length,
			sent_emails_count = 0;
		if (err){logger.info('ERR BIRTHDAYS')}

		rows.forEach(function(user){
			var tmpl_name = 'unknown',
				tmpl_html;
			if (user.sex == 1){
				tmpl_name = 'male';
			}else if (user.sex == '0'){
				tmpl_name = 'female';
			}
			tmpl_html = bday_templates[tmpl_name];

			transporter.sendMail({
				debug: true,
				connectionTimeout: 50000,
				greetingTimeout: 50000,
				socketTimeout: 50000,
				from: 'Поздравляем с днём рождения birthday-robot@guu.ru',
				to: user.ad_login,
				subject: 'Поздравляем с днём рождения!',
				html: tmpl_html
					.replace('{first_name}', user.first_name)
					.replace('{middle_name}', user.middle_name)
			}, function(err, info){
				if (err){
					logger.info('EMAIL SEND ERROR', user.ad_login, err);
				}
				logger.info('EMAIL_INFO', info);
				sent_emails_count++;
				if (sent_emails_count == emails_to_send){
					logger.info('Birthday congrats', 'FINISH...' + new Date().toString());
				}
			});
		});
	});
}

io.on('connection', function (socket){
	function handleQueryError(err){
		logger.log('error', err);
	}
	function pushUserUnreadMessages(login){
		var q_get_token = 'SELECT * ' +
			' FROM azure_tokens' +
			' WHERE ad_login = ' + connection.escape(login) +
			' AND expires_in < NOW()';


		connection.query(q_get_token, function(err, rows){
			if (err){
				handleQueryError(err);
				return;
			}
			if (rows.length == 1){
				var mess_args = {
					headers: {
						'client-request-id': socket.id,
						'return-client-request-id': true,
						'authorization': 'Bearer ' + rows[0].access_token,
						'Accept': '*/*'
					},
					data:{

					}
				};

				//console.log(AZURE_URLS.get_messages, mess_args);

				rest
					.get(AZURE_URLS.get_messages, mess_args)
					.on('complete', function(mess_res){
						var messages = mess_res.value,
							res_data = [];
						if (Array.isArray(messages)){
							messages.forEach(function(value){
								if (value.IsRead == false){
									res_data.push({
										from_name: value['From']['EmailAddress']['Name'] ? value['From']['EmailAddress']['Name'] : '',
										from_email: value['From']['EmailAddress']['Address'] ? value['From']['EmailAddress']['Address'] : '',
										received_time: value['DateTimeReceived'] ? value['DateTimeReceived'] : null,
										subject: value['Subject'] ? value['Subject']: '',
										link: value['WebLink']
									});
								}
							});
							socket.emit('notifications.emails', res_data);
						}
					})
			}
		});
	}

	socket.on('connection.connected', function(data){ //Получение userData

		function getLastContactsList(cb){
			var q_get_last = ' SELECT DISTINCT view_users.id as user_id, view_users.ad_login as email,  ' +
				' view_users.first_name, view_users.last_name, view_users.middle_name,  ' +
				' view_users.role, view_users.ou_id as _ou_id,  ' +
				' (SELECT organizational_units.name FROM organizational_units WHERE organizational_units.id = _ou_id) as ou_name  ' +
				' FROM view_users  ' +
				' LEFT JOIN chat_messages a ON view_users.id = a.from_user_id  ' +
				' LEFT JOIN chat_messages b ON view_users.id = b.to_user_id  ' +
				' WHERE (a.to_user_id = ' + connection.escape(socket.user.id) +
				' OR a.from_user_id = ' + connection.escape(socket.user.id) + ') ' +
				' AND view_users.id !=  ' + connection.escape(socket.user.id) +
				' ORDER BY last_name, first_name';
			connection.query(q_get_last, function(err, rows){
				if (cb instanceof Function){
					cb(rows);
				}
			});
		}

		function getColleaguesList(cb){
			if (socket.user.is_staff == 1){
				var q_get_cols = 'SELECT view_users.id as user_id, view_users.ad_login as email, ' +
					' view_users.first_name, view_users.last_name, view_users.middle_name, ' +
					'view_users.role, view_users.ou_id as _ou_id, ' +
					'(SELECT organizational_units.name FROM organizational_units WHERE organizational_units.id = _ou_id LIMIT 1) as ou_name ' +
					'FROM view_users ' +
					'INNER JOIN organizational_units ON organizational_units.id = view_users.ou_id ' +
					'WHERE organizational_units.parent_id = (SELECT organizational_units.parent_id ' +
						'FROM view_users ' +
						'INNER JOIN organizational_units ON organizational_units.id = view_users.ou_id ' +
						'WHERE view_users.id = ' + connection.escape(socket.user.id) +'  LIMIT 1)' +
						' ORDER BY last_name, first_name';
				connection.query(q_get_cols, function(err, rows){
					if (err){
						handleQueryError(err);
						return;
					}
					if (cb instanceof Function){
						cb(rows);
					}
				});
			}else if (socket.user.is_student == 1){

			}

		}


		if (data && data.user && data.user.id){
			var q_get_user = "SELECT DISTINCT view_users.*, view_users.id AS user_id " +
				" FROM view_users " +
				" INNER JOIN users ON users.id = view_users.id" +
				" INNER JOIN azure_tokens ON azure_tokens.ad_login = users.ad_login" +
				" WHERE users.token = " + connection.escape(data.user.password);


			connection.query(q_get_user,function(err, rows){
				if (err){handleQueryError(err);return;}


				if (rows.length == 1){ // Если такой пользователь действительно сущесвует - записываем его данные
					socket.user = rows[0];
					socket.user.password = data.user.password;
					active_users['u_' + socket.user.id] = socket.user;
					active_users['u_' + socket.user.id].socket_id = socket.id;
					pushUserUnreadMessages(socket.user.ad_login);
					socket.emit('chat.setActiveUsers', active_users);
					socket.broadcast.emit('chat.setUserActivity', {
						user_id: socket.user.id,
						status: true
					});
					getColleaguesList(function(arr){
						_.forEach(arr, function(value, index){
							value.is_active = active_users['u_' + value.user_id] ? true : false;
							arr[index] = value;
						});

						getLastContactsList(function(last_contacts){
							_.forEach(last_contacts, function(value, index){
								value.is_active = active_users['u_' + value.user_id] ? true : false;
								last_contacts[index] = value;
							});

							var data = {
								colleagues: arr,
								last_contacts: last_contacts
							} ;
							socket.emit('initList', data);
						});
					})
				}else{
					logger.error('AUTH', {err: 'Попытка авторизации пользователя без данных', data: data, query: q_get_user});
				}
				socket.page = data.page;
				return true;
			});
			return true;
		}
	});

	socket.on('connection.disconnect', function(){ //Получение userData
		if (typeof socket != 'undefined'){
			if (socket.hasOwnProperty('user')){
				delete active_users['u_' + socket.user.user_id];
				socket.broadcast.emit('chat.setUserActivity',{
					user_id: socket.user.user_id,
					status: false
				});
			}
		}
	});

	socket.on('chat.search', function(data){
		var escaped_q = connection.escape(data.q + '%'),
			q_get_users = 'SELECT view_users.*, view_users.id as user_id, organizational_units.name as ou_name,' +
				' CONCAT(first_name, " ", last_name, " ", middle_name) AS full_name ' +
				' FROM view_users' +
				' INNER JOIN organizational_units ON organizational_units.id = view_users.ou_id' +
				' GROUP BY view_users.id' +
				' HAVING (' +
				' first_name LIKE ' + escaped_q + ' ' +
				' OR last_name LIKE ' + escaped_q + ' ' +
				' OR middle_name LIKE ' + escaped_q + ' ' +
				' OR full_name LIKE ' + escaped_q + ' ' +
				' OR ou_name LIKE ' + escaped_q + ' ' +
				' OR role LIKE ' + escaped_q + ' )' +
				' ORDER BY id ASC' +
				' LIMIT 50';

		connection.query(q_get_users, function(err, rows){
			if (err){
				handleQueryError(err);
				return;
			}
			_.forEach(rows, function(value, index){
				value.is_active = active_users['u_' + value.id] ? true : false;
				rows[index] = value;
			});
			socket.emit('chat.searchResults', rows);
		});
	});

	socket.on('chat.message', function(data){
		if (!active_users['u_' + data.to_user_id]){
			return false;
		}
		var q_ins_message = 'INSERT INTO chat_messages(created_at, send_date, to_user_id, from_user_id, message_body)' +
			' VALUES(NOW(), NOW(),'
			+ connection.escape(data.to_user_id) + ', '
			+ connection.escape(socket.user.id) + ', '
			+ connection.escape(data.message_body) + ') ';
		connection.query(q_ins_message, function(err){
			handleQueryError({err: err, query: q_ins_message});
		});
		socket.broadcast.to(active_users['u_' + data.to_user_id].socket_id).emit('chat.newMessage', {
			from_full_name: [socket.user.first_name, socket.user.last_name].join(' '),
			from_user_id: socket.user.id,
			message_body: data.message_body,
			send_date: new Date()
		})
	});

	socket.on('logon', function(){
		socket.emit('setMicrosoftData', {
			AZURE_CLIENT_ID: AZURE_CLIENT_ID,
			AZURE_RESOURCE: AZURE_RESOURCE,
			AZURE_OAUTH_LINK: 'https://login.windows.net/common/oauth2/authorize?response_type=code&client_id='
			+ AZURE_CLIENT_ID + '&resource=' + AZURE_RESOURCE
		});
	});

	socket.on('azureAuth', function(data){
		var args = {
			headers: {
				"Content-Type": "application/x-www-form-urlencoded"
			},
			data: {
				'grant_type': "authorization_code",
				'client_id': AZURE_CLIENT_ID,
				'client_secret': AZURE_CLIENT_SECRET,
				'session_state': data.session_state,
				'code': data.code,
				'resource': AZURE_RESOURCE
			}
		};
		rest
			.post(AZURE_URLS.get_token, args)
			.on('complete', function(auth_res) {
				//console.timeEnd('common/oauth2/token');
				console.log(auth_res);
				var me_args = {
					headers: {
						'client-request-id': socket.id,
						'return-client-request-id': true,
						'authorization': 'Bearer ' + auth_res.access_token,
						'Accept': '*/*'
					},
					data:{}
				};
				rest
					.get(AZURE_URLS.user_main_info, me_args)
					.on('complete', function(me_res){
						console.log(me_res);
						var q_update_user = 'INSERT INTO azure_tokens ' +
							' (created_at, ad_login, token_type, expires_in, expires_on, not_before, resource, ' +
								'access_token, refresh_token, scope, id_token, mailbox_guid, pwd_exp)' +
							' VALUES(NOW(), '
							 + connection.escape(me_res['Id']) + ', '
							 + connection.escape(auth_res['token_type']) + ', '
							 + connection.escape(auth_res['expires_in']) + ', '
							 + 'FROM_UNIXTIME(' + connection.escape(auth_res['expires_on']) + '), '
							 + 'FROM_UNIXTIME(' +connection.escape(auth_res['not_before']) + '), '
							 + connection.escape(auth_res['resource']) + ', '
							 + connection.escape(auth_res['access_token']) + ', '
							 + connection.escape(auth_res['refresh_token']) + ', '
							 + connection.escape(auth_res['scope']) + ', '
							 + connection.escape(auth_res['id_token']) + ', '
							 + connection.escape(me_res['MailboxGuid']) + ', '
							 + connection.escape(auth_res['refresh_token_expires_in'])
							 + ' ) ON DUPLICATE KEY UPDATE '
							 + 'token_type = ' + connection.escape(auth_res['token_type']) + ', '
							 + 'expires_in = ' + connection.escape(auth_res['expires_in']) + ', '
							 + 'expires_on = FROM_UNIXTIME(' + connection.escape(auth_res['expires_on']) + '), '
							 + 'not_before = FROM_UNIXTIME(' + connection.escape(auth_res['not_before']) + '), '
							 + 'resource = ' + connection.escape(auth_res['resource']) + ', '
							 + 'access_token = ' + connection.escape(auth_res['access_token']) + ', '
							 + 'refresh_token = ' + connection.escape(auth_res['refresh_token']) + ', '
							 + 'scope = ' + connection.escape(auth_res['scope']) + ', '
							 + 'id_token = ' + connection.escape(auth_res['id_token']) + ', '
							 + 'mailbox_guid = ' + connection.escape(me_res['MailboxGuid']) + ', '
							 + 'pwd_exp = ' + connection.escape(auth_res['refresh_token_expires_in']);

						connection.query(q_update_user, function(err){
							if (err){
								handleQueryError({q:q_update_user, err: err});
								return;
							}
							socket.emit('auth', {
								login: me_res['Id'],
								password: auth_res['access_token'],
								auth_type: 'azure'
							});
						});

					});
			});
	});

	socket.on('log', function(data){
		console.log(data);
	});

	socket.on('sync.staff', function(){
		sync.updateStaff({
			onProgress: function(data){
				socket.emit('log', data);
			},
			onDone: function() {
				socket.emit('log', 'DONE!');
			}
			}
		);
	});

	socket.on('sync.timetable', function(){
		sync.updateTimetable();
	});

	socket.on('sync.dpv', function(){
		sync.updateDPV();
	});

	socket.on('sync.moodle.grades', function(){
		sync.updateMoodleMarks({
			email: socket.user.ad_login,
			user_id: socket.user.id
		}, function(){
			console.log('moodle.gradesSyncDone');
			socket.emit('moodle.gradesSyncDone');
		});
	})

});

io.listen(8080);

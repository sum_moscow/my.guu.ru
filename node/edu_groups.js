var server = require('http'),
	rest = require('restler'),
	winston = require('winston'),
	Sync = require('./sync.js'),
	mysql = require('mysql'),
	_fs = require("fs"),
	config = {};

config = _fs.readFileSync('../config.json');
config = JSON.parse(config);

var
	config_index = process.env.ENV ? process.env.ENV : 'local';
	real_config = config[config_index],
	logger = logger = new (winston.Logger)({
		transports: [
			new winston.transports.File({filename: __dirname + '/sync.log', json: true})
		],
		exceptionHandlers: [
			new (winston.transports.Console)(),
			new winston.transports.File({filename: __dirname + '/syncexceptions.log', json: false})
		],
		exitOnError: false
	}),
	connection = mysql.createConnection(real_config.db),
_sync = new Sync(connection, rest, real_config);
//_sync.updateMoodleMarks({email: 'aa_kharazyan@edu.guu.ru', user_id:45333});
//_sync.updateStudents();
_sync.updateStaff();
var server = require('http'),
    rest = require('restler'),
    winston = require('winston'),
    Sync = require('./sync.js'),
    mysql = require('mysql'),
    _fs = require("fs"),
    config = {};

config = _fs.readFileSync('../config.json');
config = JSON.parse(config);

var
    config_index = 'prod',//process.env.ENV ? process.env.ENV : 'local';
	real_config = config[config_index],
    logger = new (winston.Logger)({
        transports: [
            new winston.transports.File({filename: __dirname + '/sync.log', json: true})
        ],
        exceptionHandlers: [
            new winston.transports.File({filename: __dirname + '/syncexceptions.log', json: false})
        ],
        exitOnError: true
    }),
    connection = mysql.createConnection(real_config.db);

connection.query('(SELECT addresses.* FROM ' +
    ' addresses ' +
    ' INNER JOIN users ON users.registration_address_id = addresses.id WHERE addresses.street IS NOT NULL AND addresses.building IS NOT NULL AND addresses.street  != "" AND addresses.building != "" AND kladr IS NULL) UNION' +
    '(SELECT addresses.* FROM addresses ' +
    'INNER JOIN users ON users.residence_address_id = addresses.id WHERE addresses.street IS NOT NULL AND addresses.building IS NOT NULL AND addresses.street != "" AND addresses.building != "" AND kladr IS NULL)', function(err, rows){
    var all_count = rows.length,
        done = 0,
        result_null = 0,
            done_well = 0;
    rows.forEach(function(row){
        var _restler = rest,
            parents = [];
        function addressDone(status, path, buildingResult){
            if (!status){
                result_null++;
            }else{
                if (buildingResult != null){
                    connection.query('UPDATE addresses SET kladr = ' + connection.escape(buildingResult) +
                        ' WHERE id = ' + row.id, function(err, res){
                        done++;
                        if (err){
                            console.log('ERROR UPDATING KLADR!');
                            result_null++;
                        }else{
                            done_well++;
                        }
                    })
                }else{
                    result_null++;
                }
                console.log(done/all_count*100);
                if (done == all_count){
                    console.log({done:done, all_count:all_count, result_null:result_null,done_well:done_well});
                }
                console.log(path);
            }
        }

        function normalizeName(name){
            var _name = name.toLowerCase()
                    .replace('республика', '')
                    .replace('облость', '')
                    .replace('мо', 'московская')
                    .replace('моск.обл', 'московская')
                    .replace('область')
                    .replace('обл.', '')
                    .replace('обл', '')
                    .trim(),
                splitten = _name.split('.');
            if (splitten.length > 0){
                _name = splitten[splitten.length - 1];
            }

            splitten = _name.split(' ');
            if (splitten.length > 0){
                _name = splitten[0];
            }

            return encodeURIComponent(_name);
        }

        function getBuildingId(building, parent){
            var q = 'http://kladr-api.ru/api.php?token=559b9f6c0a69de27228b4577&limit=1&contentType=building&query=' + normalizeName(building),
                buildingResult = null;
            if (parent != null){
                q += '&' + parent.type + '=' + parent.id;
            }
            console.log(q);
            _restler
                .json(q)
                .on('complete', function(response){
                    var _parent = {}, success;
                    if (response.result != null && response.result instanceof Array && response.result.length  > 0){
                        success = true;
                        _parent = {type: 'buildingId', name: 'building', id: response.result[0].id};
                        buildingResult = response.result[0].id;
                    }else{
                        success = false;
                    }
                    parents.push(_parent);
                    addressDone(success, parents, buildingResult);
                });
        }

        function getStreetId(street, parent){
            if (street == ''){
                addressDone(false, parents);
                return;
            }
            var q = 'http://kladr-api.ru/api.php?token=559b9f6c0a69de27228b4577&limit=1&contentType=street&query=' + normalizeName(street);
            if (parent != null){
                q += '&' + parent.type + '=' + parent.id;
            }
            _restler
                .json(q)
                .on('complete', function(response){
                    var _parent = {};
                    if (response.result != null && response.result instanceof Array && response.result.length == 1){
                        _parent = {type: 'streetId', name: 'street', id: response.result[0].id};
                    }else{
                        _parent = parent;
                        addressDone(false, parents);
                        return;
                    }
                    parents.push(_parent);
                    getBuildingId(row.building, _parent);
                });
        }

        function getCityId(city, parent){
            var q = 'http://kladr-api.ru/api.php?token=559b9f6c0a69de27228b4577&limit=1&contentType=city&query=' + normalizeName(city);
            if (parent != null){
                q += '&' + parent.type + '=' + parent.id;
            }
            console.log(q);
            _restler
                .json(q)
                .on('complete', function(response){
                    var _parent = {};
                    if (response.result != null && response.result instanceof Array && response.result.length > 0){
                        _parent = {type: 'cityId', name: 'city', id: response.result[0].id};
                    }else{
                        _parent = parent;
                    }
                    parents.push(_parent);
                    getStreetId(row.street, _parent);
                });
        }


        function getDistrictId(district, _parent){
            var q = 'http://kladr-api.ru/api.php?token=559b9f6c0a69de27228b4577&limit=1&contentType=district&query=' + normalizeName(district);
            if (_parent != null) {
                q += '&' + _parent.type + '=' + _parent.id;
            }
            _restler
                .json(q)
                .on('complete', function(response){
                    var parent = {};
                    if (response.result != null && response.result instanceof Array && response.result.length  > 0){
                        parent = {type: 'districtId', name: 'district', id: response.result[0].id};
                    }else{
                        parent = _parent;
                    }
                    parents.push(parent);
                    getCityId(row.city, parent);
                });
        }


        _restler
            .json('http://kladr-api.ru/api.php?token=559b9f6c0a69de27228b4577&limit=1&contentType=region&query=' + normalizeName(row.region))
            .on('complete', function(response){
                var _parent;
                if (response.result != null && response.result instanceof Array && response.result.length == 1){
                    _parent = {type: 'regionId', name: 'region', id: response.result[0].id};
                }else{
                    _parent = null;
                }
                getDistrictId(row.district, _parent);
            });
    });
    });
//_sync.updateMoodleMarks({email: 'aa_kharazyan@edu.guu.ru', user_id:45333});
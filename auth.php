<?php

try{
	if (!isset($_REQUEST['login']) || trim($_REQUEST['login']) == '' ||
		!isset($_REQUEST['password']) || trim($_REQUEST['login']) == ''){
		throw new InvalidArgumentException('Пользователь с указанными данными не существует');
	}

	require_once 'backend/core/bin/db.php';
	require_once 'backend/core/bin/Class.Result.php';

	if (!isset($_REQUEST['auth_type']) || $_REQUEST['auth_type'] != 'azure'){
		echo json_encode(array('status' => false, 'text' => 'Неизвестный тип авторизации'));
	}
		$q_get_user = 'SELECT users.id as user_id, users.*
				FROM users
				INNER JOIN azure_tokens ON azure_tokens.ad_login = users.ad_login
				WHERE users.ad_login = :ad_login
					AND azure_tokens.access_token = :password
					AND azure_tokens.expires_on > NOW()';
		$p_get_user = $db->prepare($q_get_user);
		$result = $p_get_user->execute(array(
			':ad_login' => $_REQUEST['login'],
			':password' => $_REQUEST['password']
		));

		if ($result === FALSE) throw new DBQueryException(null, $db);
		if ($p_get_user->rowCount() != 1) throw new LogicException('Пользователь с такими данными не найден');


		$new_token = md5(uniqid($_REQUEST['login'] . date("Y-m-d H:i:s"), true));

	$q_upd_token = 'UPDATE users
				SET token = :token,
				last_auth_time = NOW()
				WHERE ad_login = :ad_login';
	$p_upd_token = $db->prepare($q_upd_token);
	$p_upd_token->execute(array(
		':token' => $new_token,
		':ad_login' => $_REQUEST['login']
	));

	if ($row_user_info = $p_get_user->fetch()){
		$_SESSION["username"] = $_REQUEST['login'];
		$_SESSION['current_user'] = $row_user_info['user_id'];
		$_SESSION['id'] = $row_user_info['user_id'];
		$_SESSION['login'] = $row_user_info['ad_login'];
		$_SESSION['p'] = $new_token;
		$_SESSION['password'] = $new_token;
		echo new Result(true, 'Данные успешно получены');
	}else{
		echo new Result(false, 'Пользователь с такими данными не найден');
	}
}catch(Exception $e){
	header("Content-Type: application/json");
	echo json_encode(array('status' => false, 'text' => $e->getMessage()));
}
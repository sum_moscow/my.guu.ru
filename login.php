<?php
	require 'backend/core/bin/db.php';
	require 'backend/core/bin/Class.Result.php';
	require 'backend/core/users/Class.User.php';


	if (isset($_REQUEST['logout']) && $_REQUEST['logout'] == true){
		User::logout();
	}

	try{
		$user = new User($db);
		header('Location: ./index.php', TRUE, 303);
	}catch(Exception $e){}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<title>Авторизация</title>
	

	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css"  id="style-resource-1">

	<link rel="stylesheet" href="assets/css/font-icons/font-awesome/css/font-awesome.min.css"  id="style-resource-001">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css"  id="style-resource-2">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic"  id="style-resource-3">
	<link rel="stylesheet" href="assets/css/bootstrap-min.css"  id="style-resource-4">
	<link rel="stylesheet" href="assets/css/neon-core-min.css"  id="style-resource-5">
	<link rel="stylesheet" href="assets/css/neon-theme-min.css"  id="style-resource-6">
	<link rel="stylesheet" href="assets/css/neon-forms-min.css"  id="style-resource-7">
	<link rel="stylesheet" href="assets/css/custom-min.css"  id="style-resource-8">
	<link rel="stylesheet" href="assets/css/skins/blue.css"  id="style-resource-99">

	<script src="assets/js/jquery-1.11.0.min.js"></script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	
	<!-- TS1410680181: Neon - Responsive Admin Template created by Laborator -->
</head>
<body class="page-body login-page login-form-fall">

<div class="login-container">
	
	<div class="login-header login-caret" style="padding-bottom: 10px;">
		<div class="login-content">

			<a href="http://guu.ru" class="logo">
				<img src="assets/images/logo_login.png" width="200" alt="" />
			</a>

			<p class="description">Необходима авторизация для входа в профиль!</p>

			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>Авторизация...</span>
			</div>

		</div>
		
	</div>
	
	<div class="login-progressbar">
		<div></div>
	</div>


	<div class="login-form">
		
		<div class="login-content">
			
			<div class="form-login-error">
				<h3>Неправильный логин или пароль</h3>
			</div>




			<div class="row" id="login-with-microsoft">
				<button id="login-with-microsoft-btn" type="button" class="btn btn-primary btn-block col-md-3" style="font-size: 42px;">
					<i class="fa fa-windows"></i> Войти
				</button>
			</div>
			<form style="display: none;" method="post" role="form" id="form_login">
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						
						<input type="text" class="form-control" name="username" id="username" placeholder="Логин" autocomplete="off" />
					</div>
					
				</div>
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						
						<input type="password" class="form-control" name="password" id="password" placeholder="Пароль" autocomplete="off" />
					</div>
				
				</div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						Войти
					</button>
				</div>
								
			</form>
			<p></p>
			<div class="alert alert-warning old-browser hidden" style="margin-top: 30px">
				<strong>Внимание!</strong>
				Ваш браузер устарел. Пожалуйста, обновите его! В противном случае Личный кабинет будет отображаться некорректно.
				Для работы с Личным кабинетом мы рекомендуем Firefox или Chrome последних версий.
			</div>
			<div class="login-bottom-links" style="padding-top: 20px">
				<p><strong>При возникновении проблем с авторизацией пишите на my@guu.ru</strong></p>
				<a href="https://gitlab.com/sum_moscow/my.guu.ru" target="_blank"><i class="fa fa-bitbucket"></i> GitLab</a>
			</div>


		</div>

	</div>
	
</div>


	<script src="assets/js/gsap/main-gsap.js" id="script-resource-1"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js" id="script-resource-2"></script>
	<script src="assets/js/bootstrap.js" id="script-resource-3"></script>
	<script src="assets/js/joinable.js" id="script-resource-4"></script>
	<script src="assets/js/resizeable.js" id="script-resource-5"></script>
	<script src="assets/js/neon-api.js" id="script-resource-6"></script>
	<script src="assets/js/cookies.min.js" id="script-resource-7"></script>
	<script src="assets/js/jquery.validate.min.js" id="script-resource-8"></script>
	<script src="<?=SUM::$DOMAIN?>:8080/socket.io/socket.io.js"></script>
	<script src="assets/js/node.js" id="script-resource-022"></script>
	<script src="assets/js/pages/login.js" id="script-resource-9"></script>
	<script src="assets/js/neon-custom.js" id="script-resource-10"></script>
	<script src="assets/js/neon-demo.js" id="script-resource-11"></script>
	<script src="assets/js/neon-skins.js" id="script-resource-12"></script>

</body>
</html>

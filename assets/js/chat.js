
active_users = {};

function defineChatEvents(){
	(function(socket){

		socket.on('chat.setActiveUsers', function(data){
			var user;
			for (var key in data){
				if (!data.hasOwnProperty(key)) continue;
				user = data[key];
				neonChat.setStatus('chat_user_' + data.user_id, 'online');
				neonChat.setStatus('chat_search_user_' + data.user_id, 'online');
				active_users[key] = true;
			}
		});

		socket.on('chat.setUserActivity', function(data){
			neonChat.setStatus('chat_user_' + data.user_id, data.status ? 'online' : 'offline');
			neonChat.setStatus('chat_search_user_' + data.user_id, data.status ? 'online' : 'offline');
			active_users['u_' + data.user_id] = data.status;
		});

		neonChat._settings = {
			last_contacts_group_id: null,
			colleagues_group_id: 1
		};

		neonChat.afterOpen = function(){
			var $this = this;
			$('.conversation-header .display-name')
				.off('click')
				.on('click', function(){
					window.showProfile($this.data('user_id'));
				});
		};

		socket.on('initList', function(data){
			var role;
			neonChat._settings.colleagues_group_id = neonChat.createGroup('Коллеги', true);
			neonChat._settings.last_contacts_group_id = neonChat.createGroup('Недавние контакты', true);
			data.colleagues.forEach(function(value){
				if (value.user_id == window.getUserInfo().user_id) return true;
				neonChat.addUser(neonChat._settings.colleagues_group_id, value.last_name + ' ' + value.first_name,
					value.is_active ? 'online' : 'offline', true, 'chat_user_' + value.user_id);
				role = value.role + ', ' + value.ou_name;
				$('#chat_user_' + value.user_id).data({
					'role': role,
					email: value.email,
					user_id: value.user_id
				}).attr('style', '');
			});
			data.last_contacts.forEach(function(value){
				if (value.user_id == window.getUserInfo().user_id) return true;
				neonChat.addUser(neonChat._settings.last_contacts_group_id, value.last_name + ' ' + value.first_name,
					value.is_active ? 'online' : 'offline', true, 'chat_user_' + value.user_id);
				role = value.role + ', ' + value.ou_name;
				$('#chat_user_' + value.user_id).data({
					'role': role,
					email: value.email,
					user_id: value.user_id
				}).attr('style', '');
			});
			neonChat.refreshUserIds();
		});

		socket.on('chat.searchResults', function(users){
			if (neonChat._settings.search_group_id){
				neonChat.removeGroup(neonChat._settings.search_group_id, '');
			}
			var role;
			neonChat._settings.search_group_id = neonChat.createGroup('Результаты поиска', true);
			users.forEach(function(value){
				neonChat.addUser(neonChat._settings.search_group_id,
					value.first_name + ' ' + value.last_name, value.is_active ? 'online':'offline', true, 'chat_search_user_' + value.user_id);
				role = value.role + ', ' + value.ou_name;
				$('#chat_search_user_' + value.user_id).data({
					'role': role,
					email: value.email,
					user_id: value.user_id
				}).attr('style', '');
			});
			window.chat_search_active = false;
			neonChat.refreshUserIds();
		});

		window.messages = [];

		socket.on('chat.newMessage', function(data){
			window.messages.push(data);

			if ($('#last_contacts_user_' + data.from_user_id).length > 0){
				neonChat.pushMessage('last_contacts_user_' + data.from_user_id, data.message_body, data.from_full_name, new Date(data.send_date), false, true);
				neonChat.renderMessages('last_contacts_user_' + data.from_user_id);
			}else if($('#chat_search_user_' + data.from_user_id).length > 0){
				neonChat.pushMessage('chat_search_user_' + data.from_user_id, data.message_body, data.from_full_name, new Date(data.send_date), true, true);
				neonChat.renderMessages('chat_search_user_' + data.from_user_id);
			}else if ($('#chat_user_' + data.from_user_id).length > 0){
				neonChat.pushMessage('chat_user_' + data.from_user_id, data.message_body, data.from_full_name, new Date(data.send_date), false, true);
				neonChat.renderMessages('chat_user_' + data.from_user_id);
			}else{
				neonChat.addUser(neonChat._settings.last_contacts_group_id, data.from_full_name, 'online', true, 'last_contacts_user_' + data.from_user_id);
				neonChat.pushMessage('last_contacts_user_' + data.from_user_id, data.message_body, data.from_full_name, new Date(data.send_date), false, true);
				$('#last_contacts_user_' + data.from_user_id).data({
					'role': data.role,
					email: data.email,
					user_id: data.from_user_id
				}).attr('style', '');

				neonChat.renderMessages('last_contacts_user_' + data.from_user_id);
			}
		});
	})(socket);
}
function MyRequisitions(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект

	_page.settings = {
		url: '',
		data: {}
	};

	_page.normalizeData = function(data){

	};




	var _super_after_open = _page.afterOpen;
	_page.afterOpen = function(){
		var $requisitions = $('#requisitions');

		$.ajax({
			url: 'api/requisitions/institute/my',
			method: 'GET',
			dataType: 'JSON',
			success: function (response) {
				response.data.forEach(function (el, i) {
					var create_date = new Date(el.create_date),
						end_date = new Date(el.change_date);
					$requisitions.find("tbody").append(
						tmpl('myRequisitions-table-row',
							{
								where: el.to_organization,
								date: create_date.getReadableText("short"),
								status_text: el.status_text
							}
						)
					).find('td.btn-info').eq(i).on('click',
						function() {
							var $modal = $('#modal-placeholder').empty().append(
								tmpl('myRequisitions-requisition-modal',
									{
										name: 'Заявка на справку',
										user_full_name: el.first_name + ' ' + el.last_name + ' ' + el.father_name,
										user_stud: el.personal_file,
										create_date: create_date.getReadableText("short"),
										end_date: end_date.getReadableText("short"),
										where: el.to_organization,
										quantity: el.count,
										status_text: el.status_text
									}
								)
							);
							$modal.find('.modal').modal('show');
							$modal.find(".input-spinner").each(function (i, el) {
								var $this = $(el),
									$minus = $this.find('button:first'),
									$plus = $this.find('button:last'),
									$input = $this.find('input'),
									minus_step = attrDefault($minus, 'step', -1),
									plus_step = attrDefault($minus, 'step', 1),
									min = attrDefault($input, 'min', null),
									max = attrDefault($input, 'max', null);
								$this.find('button').on('click', function (ev) {
									ev.preventDefault();
									var $this = $(this),
										val = $input.val(),
										step = attrDefault($this, 'step', $this[0] == $minus[0] ? -1 : 1);
									if (!step.toString().match(/^[0-9-\.]+$/)) {
										step = $this[0] == $minus[0] ? -1 : 1;
									}
									if (!val.toString().match(/^[0-9-\.]+$/)) {
										val = 0;
									}
									$input.val(parseFloat(val) + step).trigger('keyup');
								});
								$input.keyup(function () {
									if (min != null && parseFloat($input.val()) < min) {
										$input.val(min);
									}
									else if (max != null && parseFloat($input.val()) > max) {
										$input.val(max);
									}
								});
							});
							replaceCheckboxes();
							switch(el.additional_info){
								case "Номер приказа о зачислении":{
									$modal.find('#extra_1').prop("checked", "checked").parents(".radio").addClass("checked");
									break;
								}
								case "Дата зачисления/окончания":{
									$modal.find('#extra_2').prop("checked", "checked").parents(".radio").addClass("checked");
									break;
								}
								default :{
									$modal.find('#extra_3').prop("checked", "checked").parents(".radio").addClass("checked");
								}
							}
							if(el.seal_type == "Гербовая (для ПФ РФ)"){
								$modal.find('#seal_type_2').prop("checked", "checked").parents(".radio").addClass("checked");
							}else{
								$modal.find('#seal_type_1').prop("checked", "checked").parents(".radio").addClass("checked");
							}

						}
					);
				});

			}
		});
		_super_after_open.call(this);

	};



	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}
function InstituteViewRequisitions(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект

	_page.settings = {
		url: '/api/users/me/dashInfo',
		data: {}
	};

	_page.normalizeData = function(data){
		var render_data = data.data;
		render_data.unread_messages_hidden = render_data.unread_messages == null ? 'hidden' : '';
		return render_data;
	};




	var _super_after_open = _page.afterOpen;
	_page.afterOpen = function(){

		var $requisitions = $('#requisitions');

		$.ajax({
			url: 'api/requisitions/institute/all',
			method: 'GET',
			dataType: 'JSON',
			success: function (response) {
				response.data.forEach(function (el, i) {
					var $tbody = $requisitions.find("tbody"),
						create_date = new Date(el.create_date),
						end_date = new Date(el.change_date);
					$tbody.append(
						tmpl('instituteViewRequisitions-table-row',
							{
								requisition_id: el.requisition_id,
								user_full_name: el.first_name + ' ' + el.last_name + ' ' + el.father_name,
								where: el.to_organization,
								date: create_date.getReadableText("short"),
								status_text: el.status_text
							}
						)
					).find('select').eq(i).find('option').eq(el.status_id-1).prop("selected", true);
					$tbody.find("select").eq(i).on("change", function(){
						var $this = $(this);
						$.ajax({
							url: '/api/requisitions/institute/'+el.requisition_id+'/status',
							method: 'PUT',
							dataType: 'JSON',
							data: {status: $this.val()}
						});
					});
					$tbody.find('td.btn-info').eq(i).on('click',
						function() {
							var $modal = $('#modal-placeholder').empty().append(
								tmpl('instituteViewRequisitions-requisition-modal',
									{
										name: 'Заявка на справку',
										user_full_name: el.first_name + ' ' + el.last_name + ' ' + el.father_name,
										user_stud: el.personal_file,
										num: el.requisition_num,
										where: el.to_organization,
										seal_type: el.seal_type,
										additional_info: el.additional_info,
										create_date: create_date.getReadableText("short"),
										quantity: el.count,
										status_text: el.status_text
									}
								)
							);
							$modal.find('.modal').modal('show');
							$modal.find(".input-spinner").each(function (i, el) {
								var $this = $(el),
									$minus = $this.find('button:first'),
									$plus = $this.find('button:last'),
									$input = $this.find('input'),
									minus_step = attrDefault($minus, 'step', -1),
									plus_step = attrDefault($minus, 'step', 1),
									min = attrDefault($input, 'min', null),
									max = attrDefault($input, 'max', null);
								$this.find('button').on('click', function (ev) {
									ev.preventDefault();
									var $this = $(this),
										val = $input.val(),
										step = attrDefault($this, 'step', $this[0] == $minus[0] ? -1 : 1);
									if (!step.toString().match(/^[0-9-\.]+$/)) {
										step = $this[0] == $minus[0] ? -1 : 1;
									}
									if (!val.toString().match(/^[0-9-\.]+$/)) {
										val = 0;
									}
									$input.val(parseFloat(val) + step).trigger('keyup');
								});
								$input.keyup(function () {
									if (min != null && parseFloat($input.val()) < min) {
										$input.val(min);
									}
									else if (max != null && parseFloat($input.val()) > max) {
										$input.val(max);
									}
								});
							});
							$modal.find(".datepicker").each(function(i, el) {
								var $this = $(el),
									opts = {
										format: attrDefault($this, 'format', 'mm/dd/yyyy'),
										startDate: attrDefault($this, 'startDate', ''),
										endDate: attrDefault($this, 'endDate', ''),
										daysOfWeekDisabled: attrDefault($this, 'disabledDays', ''),
										startView: attrDefault($this, 'startView', 0),
										rtl: rtl(),
										language: 'ru'
									};

								$this.datepicker(opts);
								if(end_date.getDate()){
									$this.datepicker('setDate', end_date);
								}
							});
							replaceCheckboxes();
							$modal.find('select').find('option').eq(el.status_id-1).prop("selected", true);
						}
					);
				});

			}
		});


		_super_after_open.call(this);

	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}
function MoodleCourses(_url){

	var _page = Page(_url),
		response_data = []; // вызвать конструктор родителя, получить родительский объект;


	_page.settings = {
		url: '/api/moodle/grades/my',
		data: {}
	};

	_page.normalizeData = function(res){
		response_data = res.data;
		return {};
	};


	function updateProgressBars(){
		response_data.forEach(function(value){
			if (value.activityid_in_moodle != 'course') return true;
			var $progress_bar = $('[data-course-id="' + value.courseid_in_moodle + '"]').siblings('.progress');
			$progress_bar.find('.progress-bar').css('width', value.grade + '%');
		});
	}

	var _super_after_open = _page.afterOpen;
	_page.afterOpen = function(){
		var $refresh_btn = $('#refresh-moodle-courses-info'),
			btn_ladda;
		socket.on('moodle.gradesSyncDone', function(){
			$.ajax({
				url: '/api/moodle/grades/my',
				success: function(res){
					response_data = res.data;
					$('.mail-menu>li.active').click();
					updateProgressBars();
					btn_ladda.stop();
				}
			})
		});
		updateProgressBars();
		$('.mail-menu>li').on('click', function(){
			var $this = $(this),
				$subject = $this.find('a'),
				$mail_table = $('.mail-table'),
				course_id = $subject.data('course-id'),
				items = [];
			$this.addClass('active').siblings().removeClass('active');
			$('.mail-title').text($subject.text());
			response_data.forEach(function(value){
				if (value.courseid_in_moodle != course_id) return true;
				items.push({
					name: value.activity_name,
					description: value.grade == null ? 'Не оценено' : 'Оценено',
					rating: value.grade,
					max_rating: value.grademax_in_moodle
				});
			});
			tmpl('course-marks-detail', items, $mail_table.find('tbody').empty());
			$mail_table.find('tbody').append(tmpl('open-course-btn', {}));
			$mail_table.find('.go-to-moodle-course').off('click').on('click', function(){
				window.open('http://edu.guu.ru/enrol/index.php?id=' + course_id, '_blank');
			});
		});

		$refresh_btn.on('click', function(){
			btn_ladda = Ladda.create(this);
			btn_ladda.start();
			socket.emit('sync.moodle.grades');
		});
		_super_after_open.call(this);


	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}
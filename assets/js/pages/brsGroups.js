function BrsGroups(_url){

	var _page = Page(_url),
		FEATURES_0_MAX = 20,
		FEATURES_1_MIN = 50,
		FEATURES_2_MIN = 30,
		CONTROL_EVENTS_MAX = 47,
		EXAM_MAX_POINTS = 40,
		SUBJECT_DATES = {
			start_date: getBRSInfo().start_date,
			finish_date: getBRSInfo().finish_date
		},
		EXAM_FORM_TOTAL_MARK_NAMES = {
			'1':{
				'0': 'Незачтено',
				'50': 'Зачтено'
			},
			'2':{
				'0': 'Неудовлетворительно',
				'50': 'Удовлетворительно',
				'70': 'Хорошо',
				'90': 'Отлично'
			},
			'3':{
				'0': 'Неудовлетворительно',
				'40': 'Удовлетворительно',
				'50': 'Хорошо',
				'60': 'Отлично'
			}
		}
	; // вызвать конструктор родителя, получить родительский объект



	_page.settings = {
		url: '',
		data: {}
	};

	function setArrowButtonsBehavior(){
		var ARROW_UP_BTN_KEY = 38,
			ARROW_DOWN_BTN_KEY = 40,
			ARROW_ENTER_BTN_KEY = 13;
		$(document)
			.off('keydown.tables')
			.on('keydown.tables', function(e){
				var $target = $(e.target),
					$tr = $target.parents('tr'),
					target_index = $tr.find('td.editable').index($target),
					$next_tr = e.which == ARROW_DOWN_BTN_KEY || e.which == ARROW_ENTER_BTN_KEY ? $tr.next() : (e.which == ARROW_UP_BTN_KEY ? $tr.prev(): $()),
					$to_focus_td = $next_tr.find('td.editable:eq(' + target_index + ')');
				if ($target.is('td.editable')){
					$to_focus_td.focus();
					if (e.which == ARROW_ENTER_BTN_KEY){
						e.preventDefault();
					}
				}
			});
	}

	var _super_after_open = _page.afterOpen,
		addHighlightToMarks = function(){
			$('.mark').off('mouseenter').on('mouseenter', function(){
				var $this = $(this),
					Number = $this.data('class-number');
				$('.mark').removeClass('active');
				$('.mark-' + Number).addClass('active');

			})
		};

	_page.showAttendanceForm = function(){
		var $tab,
			student_group_id = $('select.select2.groups').select2('val'),
			subject_id = $('select.select2.subjects').select2('val'),
			ATT_AND_ACT_MAX = 13,
			max_ratings = {
				attendance: ATT_AND_ACT_MAX,
				activity: 0
			};

		function bindFormHandlers(){
			$('.mark').each(function(){
				var $this = $(this);
				$this
					.off('click').on('click', function(e){
					if ($this.hasClass('future')){
						e.preventDefault();
						return false;
					}
					if (e.ctrlKey){
						if ($this.hasClass('editable')){
							$this
								.addClass('warning')
								.text('Н');
						}else{
							$this
								.removeClass('warning')
								.text('')
								.focus();
						}
					}
				})
					.off('blur').on('blur', function(){
						var mark = $this.text(),
							sum_marks = 0;
						if ($this.hasClass('editable') && (mark.toLowerCase() == 'н' || mark.toLowerCase() == 'h')){
							$this
								.addClass('warning')
								.text('Н');
						}else{
							$this
								.removeClass('warning')
								.addClass('editable');
							if (mark == '') return;
							if (isNaN(parseFloat(mark))){
								$this.text('');
								showNotifier({status: false, text: 'Укажите, пожалуйста, количество баллов или поставьте "Н"'});
							}else{
								var $marks = $this.siblings('.mark');
								$marks.each(function(){
									var value = parseFloat($(this).text());
									if (!isNaN(parseFloat(value))){
										sum_marks += parseFloat(value);
									}
								});
								sum_marks += parseFloat(mark);
								if (sum_marks > max_ratings.activity){
									showNotifier({status: false, text: 'Превышена максимальная сумма баллов за активность.'});
									$this.text('');
								}
							}
						}

					});
			});

			$('.class-date').each(function(){
				var $this = $(this),
					selected_date = null,
					opts = {
						format: 'yyyy-mm-dd',
						startDate: SUBJECT_DATES.start_date,
						endDate: SUBJECT_DATES.finish_date,
						daysOfWeekDisabled: attrDefault($this, 'disabledDays', '0'),
						startView: attrDefault($this, 'startView', 0),
						language: 'ru',
						rtl: rtl(),
						weekStart: 1
					};

				$this.datepicker(opts);

				if ($this.data('class-date') != ''){
					selected_date = new Date($this.data('class-date'));
					$this.datepicker('setDate', selected_date);
				}
				$this.on('changeDate', function(e){
						$this
							.data('class-date', e.format('yyyy-mm-dd'))
							.siblings('.class-number-alias')
							.text(e.format('dd.mm'))
					});
			});
		}

		function appendStudents(students){
			students.forEach(function(value, index){
				var st_line = [],
					is_future = '', contenteditable = 'true';
				$('.days-header td').each(function(index){
					var $this = $(this);
					if ($this.hasClass('future')){
						is_future = 'future';
						contenteditable = 'false';
					}
					st_line.push({
						contenteditable: contenteditable,
						number: (index + 1),
						is_future: is_future
					});
				});
				st_line = tmpl('attendance-student-line', {
					id: value.user_id,
					user_id: value.user_id,
					student_id: value.student_id,
					number: (index + 1),
					first_name: value.first_name,
					last_name: value.last_name,
					mark_tds: tmpl('attendance-student-line-editable-cell', st_line)
				});
				$('#attendance').find(' tbody').append(st_line);
				bindFormHandlers();
			});

		}

		function fillAttendanceFormWithData(){
			$.ajax({
				 url: '/api/brs/edu_groups/' + student_group_id + '/subjects/' + subject_id + '/attendance',
				 success: function(res){
					 res.data.forEach(function(value){
						 var $td = $('.student-' + value.student_id + ' .mark-' + value.class_number);
						 if (value.mark == 0){
							 $td
								 .addClass('warning')
								 .removeClass('editable')
								 .attr('contenteditable', false)
								 .text('Н');
						 }else{
							 $td.text(value.mark);
						 }
					 });
				 }
			});
		}

		function appendBonusRatingCol($table){
			$table.find('.months-header').append(tmpl());
		}

		function setMaxRatings(_max_ratings){
			if (_max_ratings){
				var att_max = parseInt(_max_ratings.attendance_max),
					act_max = parseInt(_max_ratings.activity_max);
				if (isNaN(att_max) && isNaN(act_max)) return;
				if (isNaN(att_max)){
					max_ratings.activity = act_max;
					max_ratings.attendance = ATT_AND_ACT_MAX - act_max;
					return;
				}
				if (isNaN(act_max)){
					max_ratings.attendance = att_max;
					max_ratings.activity = ATT_AND_ACT_MAX - att_max;
					return;
				}
				max_ratings.attendance = att_max;
				max_ratings.activity = act_max;
			}
		}

		$('.text-loading').removeClass('hidden');

		$.ajax({
			url: '/api/brs/edu_groups/'+ student_group_id + '/subjects/' + subject_id +'/form_data',
			success: function(res){
				$('.text-loading').addClass('hidden');
				$('.panels-heading').removeClass('hidden');
				var $main_table_tbody = tmpl('main-table-tbody', $.extend(res.data.group_info, {
						subject_id: $('select.select2.subjects').val(),
						subject_name: $('select.select2.subjects option:selected').text(),
						brs_semester: window.getBRSInfo().semester_number,
						subject_academic_department: res.data.class_info.academic_department_name
					})),
					$attendance_tbody = tmpl('attendance-table-tbody', {}),
					$header = $attendance_tbody.find('.months-header'),
					$days = $attendance_tbody.find('.days-header'),
					curr_date,
					_today = new Date(),
					is_future = '',
					all_days_count = 0,
					day_type, class_type, week_type = 1,
					key,
					timetable = res.data.timetable,
					classes_info = res.data.classes_info,
					class_date, class_number, class_id,
					i = 0;

				$.each(timetable, function(index, value){
					if (value.timetable_subject_id == subject_id && value.professor_id == getUserInfo().id){
						SUBJECT_DATES.start_date = new Date(value.since);
						SUBJECT_DATES.finish_date = new Date(value.till);
					}
				});
				curr_date = new Date(SUBJECT_DATES.start_date);

				do{
					day_type = curr_date.getDay() + 1;
					for (i = 1; i < 5; i++){
						class_type = i;
						key = [day_type, class_type, week_type].join('-');
						if (timetable.hasOwnProperty(key)){
							if (timetable[key].professor_id == window.getUserInfo().id
								&& timetable[key].timetable_subject_id == subject_id){
								if (curr_date > _today){
									is_future = 'future';
								}
								class_date = null;
								class_id = null;
								class_number = (all_days_count + 1);
								classes_info.forEach(function(value){
									if (value.number == class_number){
										class_date = value.date;
										class_id = value.class_id;
									}
								});

								$days.append(tmpl('day-of-month-cell', {
									class_number: class_number,
									is_future: is_future,
									class_id: class_id,
									class_date: class_date,
									class_text: (class_date == null) ? class_number : moment(class_date, 'YYYY-MM-DD').format('DD.MM')
								}));
								all_days_count++;
							}
						}
					}

					curr_date.setDate(curr_date.getDate() + 1);

					if (curr_date.getDay() == 0){ // Change week type on Monday
						week_type = (week_type == 1) ? 2 : 1;
					}
				}while(curr_date < SUBJECT_DATES.finish_date);

				$tab = $('#attendance');
				$tab.html($attendance_tbody);
				$('#export-table')
					.html($main_table_tbody)
					.removeClass('hidden');
				appendStudents(res.data.students);
				fillAttendanceFormWithData();
				setMaxRatings(res.data.max_ratings);

				$('.attendance.save').off('click').on('click', function(){
					var ladda = Ladda.create(this);
						$this = $(this);
					if ($this.hasClass('disabled')) return;

					var send_data = {
						students: [],
						classes: [],
						subject_id: subject_id,
						student_group_id: student_group_id
					};
					$tab.find('.student').each(function(index){
						var $student = $(this);
						send_data.students.push({
							student_id: $student.data('student-id'),
							marks: []
						});
						$student.find('.mark:not(.future)').each(function(){
							var $mark = $(this);
							send_data.students[index].marks.push($mark.text() == 'Н' ? 0 : parseFloat($mark.text()));
						});
					});
					$tab.find('.class-number').each(function(){
						var $number_td = $(this);
						send_data.classes.push({
							number: $number_td.data('class-number'),
							date: $number_td.find('.class-date').data('class-date')
						});
					});
					ladda.start();
					$.ajax({
						url: '/api/brs/edu_groups/' + student_group_id + '/subjects/' + subject_id +  '/attendance',
						type: 'POST',
						data: JSON.stringify(send_data),
						contentType: 'application/json',
						success: showNotifier
					}).always(function(){
						ladda.stop();
					});
				});
				addHighlightToMarks();
			}
		});
	};

	_page.showProgressForm = function(){


		var student_group_id = $('select.select2.groups').select2('val'),
			subject_id = $('select.select2.subjects').select2('val'),
			$progress = $('#progress'),
			$progress_head = $progress.find('thead'),
			$progress_body = $progress.find('tbody');


		function buildProgressTable(data){
			$progress_body.empty();
			$progress_head.find('.control-event-header').remove();
			if (data.events.length == 0){
				$progress_body.append(tmpl('no-control-events-line',{}));
				$('.save.brs-progress').addClass('disabled');
				return;
			}else{
				$('.save.brs-progress').removeClass('disabled');
			}
			data.students.forEach(function(value, index) {
				value.number = (index + 1);
				$progress_body.append(tmpl('progress-student-line', value));
			});

			data.events.forEach(function(value, index){
				value.number = index + 1;
				value.active_till = moment(value.active_till, 'YYYY-MM-DD').format('DD/MM/YYYY');
				value.event_date = moment(value.event_date, 'YYYY-MM-DD').format('DD/MM/YYYY');
				var $th = tmpl('control-event-header', value).data(value);
				$progress_head.find('tr').append($th);
				$('.student-line').append(tmpl('control-event-student-line-editable-cell',  value));
			});

			data.marks.forEach(function(value){
				$progress_body.find('.student-' + value.student_id + ' .control-mark-' + value.control_event_id).text(value.mark);
			});

			$progress_body.find('.control-mark').on('blur', function(){
				var $this = $(this),
					value = parseFloat($this.text()),
					max = $this.data('max-rating');

				if ($this.text().trim() == ''){
					return true;
				}
				if (isNaN(value)){
					showNotifier({status: false, text: 'Указанное значение не является числом. Введите, пожалуйста, корректкное число'});
					$this.text('');
				} else if (value > max){
					showNotifier({status: false, text: 'Введенное значение больше максимально допустимого (' + max +'). Введите, пожалуйста, корректное значение.'});
					$this.text('');
				}
			});
			$progress_head.find('.control-event-info').each(function(){
				var $btn = $(this),
					$td = $btn.parents('th');
				$btn.popover({
					placement: 'bottom',
					trigger: 'click',
					html: true,
					content: tmpl('control-event-popover-content', $td.data()).get(0).outerHTML
				});
			});
		}
		$.ajax({
			url: 'api/brs/edu_groups/' + student_group_id + '/subjects/' + subject_id + '/progress',
			type: 'GET',
			success: function(res){
				buildProgressTable(res.data);
			}
		});

		$('.save.brs-progress').off('click').on('click', function(){
			var ladda = Ladda.create(this),
				send_data = {
					marks: [],
					student_group_id : student_group_id,
					subject_id: subject_id
				};
			$progress_body.find('.control-mark').each(function(){
				var $td = $(this);
				send_data.marks.push({
					control_event_id: $td.data('control-event-id'),
					mark: parseFloat($td.text()),
					student_id: $td.parents('.student-line').data('student-id')
				});
			});
			ladda.start();
			$.ajax({
				url: '/api/brs/edu_groups/' + student_group_id + '/subjects/' + subject_id + '/progress',
				data: send_data,
				type: 'POST',
				success: showNotifier
			}).always(function(){
				ladda.stop();
			});
		});

	};

	_page.showSettings = function(){

		var control_event_types = [],
			student_group_id = $('select.select2.groups').select2('val'),
			subject_id = $('select.select2.subjects').select2('val'),
			$settings = $('#settings').empty(),
			$tmpl = tmpl('settings-form', {}, $settings);

		function updateControlEventsSumLine(){
			var $control_events = $('.control-event'),
				sum_rating = 0,
				td_class = '';

			$control_events.each(function(){
				var val = parseFloat($(this).find('.rating-input').val());
				val = isNaN(val) ? 0 : val;
				sum_rating += val;
			});
			$('.control-events-sum').remove();

			if (sum_rating > CONTROL_EVENTS_MAX){
				td_class = 'danger';
			}

			$control_events.filter(':last').after(tmpl('control-events-rating-sum', {
				sum: sum_rating,
				td_class: td_class,
				max_control_events: CONTROL_EVENTS_MAX
			}));
		}

		function updateFormHandlers(){
			if ($.isFunction($.fn.datepicker)) {
				$(".datepicker").each(function(i, el) {
					var $this = $(el),
						opts = {
							format: 'dd/mm/yyyy',
							startDate: moment(attrDefault($this, 'startDate', ''), 'YYYY-MM-DD').format('DD/MM/YYYY'),
							endDate: moment(attrDefault($this, 'endDate', ''), 'YYYY-MM-DD').format('DD/MM/YYYY'),
							daysOfWeekDisabled: attrDefault($this, 'disabledDays', ''),
							startView: attrDefault($this, 'startView', 0),
							language: 'ru',
							rtl: rtl(),
							autoclose: true
						},
						$n = $this.next(),
						$p = $this.prev();
					$this.datepicker(opts);
					if ($n.is('.input-group-addon') && $n.has('a')) {
						$n.on('click', function(ev) {
							ev.preventDefault();
							$this.datepicker('show');
						});
					}
					if ($p.is('.input-group-addon') && $p.has('a')) {
						$p.on('click', function(ev) {
							ev.preventDefault();
							$this.datepicker('show');
						});
					}
				});
			}

			$settings.find('select.to-select2:not(.select2)').each(function(){
				var $this = $(this),
					$tr = $this.parents('.control-event');
				$this
					.select2()
					.on('change', function(){
						var $this_select = $(this),
							min_rating = $this_select.find(':selected').data('min-rating');
						$tr
							.find('.rating-input')
							.attr('placeholder', '> ' + (min_rating - 1) + ' баллов')
							.val('')
							.off('blur')
							.on('blur', function(){
								var $input = $(this);
								if (this.value < min_rating){
									$('#wrong-max-rating').modal('show');
									$input.val(min_rating);
								}
								updateControlEventsSumLine();
							})

					});

			});
			$settings.find('.remove-item:not(.handled)').off('click').on('click', function(){
				var $this = $(this),
					$tr = $this.parents('tr');
				$this.toggleClass('btn-danger btn-success');
				if ($this.hasClass('btn-danger')){
					$this.html('<i class="entypo-trash"></i> Удалить');
					$tr
						.data('to-remove', 0)
						.removeClass('to-remove')
						.find('td:not(.remove-item)')
						.removeClass('btn-danger');
				}else{
					$this.html('<i class="entypo-arrows-ccw"></i> Восстановить');
					$tr
						.data('to-remove', 1)
						.addClass('to-remove')
						.find('td:not(.remove-item)')
						.addClass('btn-danger');
				}
			}).addClass('handled');
		}

		function getSelectWithControlTypes(){
			if (control_event_types.length == 0) return false;
			var $select = tmpl('control-types-select', {}),
				current_group = '',
				$opt_group = null;

			control_event_types.forEach(function(value){
				if (current_group != value.subject_types){
					$opt_group = tmpl('control-types-optgroup', value);
					current_group = value.subject_types;
					$select.append($opt_group);
					$opt_group.append(tmpl('control-types-option', value));
				}else{
					$opt_group.append(tmpl('control-types-option', value));
				}
			});
			return $select;
		}

		function buildControlEventsList(events){
			var $add_event_btn = $tmpl.find('.add-control-event'),
				ratings_sum = 0;
			events.forEach(function(value, index){
				value.number = (index + 1);
				value.event_date = moment(value.event_date, 'YYYY-MM-DD').format('DD/MM/YYYY');
				value.active_till = moment(value.active_till, 'YYYY-MM-DD').format('DD/MM/YYYY');
				value.semester_start_date = moment(SUBJECT_DATES.start_date).format('YYYY-MM-DD');
				value.semester_finish_date = moment(SUBJECT_DATES.finish_date).format('YYYY-MM-DD');

				ratings_sum += parseFloat(value.max_rating);

				var $line = tmpl('control-event-line', value),
					$select = getSelectWithControlTypes();
				$line.find('.types-of-control')
					.html($select)
					.find('select')
					.select2()
					.select2('val', value.control_type_id);

				$add_event_btn.before($line);
				updateFormHandlers();
				updateControlEventsSumLine();
			});
		}

		function buildProjectFeaturesList(features){
			var count_in_groups = [0, 0, 0];
			features.forEach(function(value){
				count_in_groups[value.group_number]++;
				value.number = count_in_groups[value.group_number];

				var $line = tmpl('project-feature-line', value),
					$select = tmpl('project-features-list-' + value.group_number, {}),
					$add_feature_btn = $tmpl.find('.add-project-feature[data-feature-group="' + value.group_number + '"]');

				//try to select in select list
				$select
					.find('option').filter(function() {
						//may want to use $.trim in here
						return $(this).attr('value') == value.name;
					}).prop('selected', true);
					/*.select2()
					.select2('val', value.name);*/

				if ($select.select2('val') == null){
					$select = $('<p contenteditable="true"></p>').html(value.name);
				}

				$line
					.find('.feature-name')
					.append($select);

				$add_feature_btn.before($line);
				$select.select2();
				updateFormHandlers();
			});
		}

		function setMaxRatings(max_ratings){
			if (max_ratings.attendance_max != null){
				$('#attendance-max-rating').val(max_ratings.attendance_max);
			}
			if (max_ratings.activity_max != null){
				$('#activity-max-rating').val(max_ratings.activity_max);
			}

		}

		$settings.find('.add-project-feature').off('click').on('click', function(){
			var $btn = $(this);
			tmpl('project-feature-line', {
				group_number: $btn.data('feature-group'),
				number: $('.project-features-line-' + $btn.data('feature-group')+':not(.hidden)').length + 1
			})
				.insertBefore($btn)
				.find('.feature-name')
					.html(tmpl('project-features-list-' + $btn.data('feature-group'), {}).get(0).outerHTML);
			updateFormHandlers();
			$settings.find('select.project-features-list')
				.on('change', function(e){
					if (e.val == -1){
						var $td = $(this).parents('td');
						$td
							.attr('contenteditable', true)
							.addClass('editable')
							.text('')
							.focus();
					}
				});
		});

		$settings.find('.add-control-event').off('click').on('click', function(){
			$('.control-events-sum').remove();
			var $btn = $(this);
			tmpl('control-event-line', {
				number: $('.control-event').length + 1,
				semester_start_date: moment(SUBJECT_DATES.start_date).format('YYYY-MM-DD'),
				semester_finish_date: moment(SUBJECT_DATES.finish_date).format('YYYY-MM-DD')
			})
				.insertBefore($btn)
				.find('.types-of-control')
				.html(getSelectWithControlTypes());
			updateFormHandlers();
		});

		$settings
			.find('#project-exists')
			.on('change', function(e){
				if ($(this).is(':checked')){
					$settings.find('.project-features-table').slideDown();
				}else{
					$settings.find('.project-features-table').slideUp();
				}
		});

		$settings
			.find('.max-rating-input')
			.on('blur', function(){
				var MAX_SUM_RATING = 13,
					$input = $(this),
					value = parseInt($input.val()),
					$another_input = $settings.find('.max-rating-input').not($input),
					another_value = parseInt($another_input.val());
				if (isNaN(value)){
					showNotifier({status: false, text: 'Введите, пожалуйста, корректное число'});
					$input.val(0);
					$another_input.val(MAX_SUM_RATING);
				}else{
					if ((another_value + value) > MAX_SUM_RATING){
						showNotifier({status: false, text: 'Извините, но сумма баллов не может превышать ' + MAX_SUM_RATING + ' баллов.'});
						$input.val(MAX_SUM_RATING - another_value);
					}else if((another_value + value) < MAX_SUM_RATING){
						showNotifier({status: false, text: 'Вы задали сумму, которая меньше максимального (' + MAX_SUM_RATING + '). Вы отбираете баллы у студентов. :)'});
					}
				}
			});

		$('.save.settings').off('click').on('click', function(){
			var $this = $(this),
				$errors_ul = $('<ul></ul>'),
				max_rating_sum = 0,
				project_exists = $settings.find('#project-exists:checked').length == 1,
				send_data = {
					subject_id: subject_id,
					student_group_id: student_group_id,
					has_project: project_exists,
					for_activity: $('#activity-max-rating').val(),
					for_attendance: $('#attendance-max-rating').val(),
					events: [],
					features:[]
				},
				all_types_of_project_feature, all_max_ratings = true;
			if ($this.hasClass('disabled')) return;
			$settings.find('.control-event').each(function(index){
				var $tr = $(this),
					__max_rating = parseFloat($tr.find('.rating-input').val());

				if (isNaN(__max_rating)){
					showNotifier({status: false, text: 'У контрольного мероприятия не указан максимальный балл. Мероприятие будет сохранено с минимальным баллом.'});
					__max_rating =  parseInt($tr.find('option:selected').data('min-rating'));
				}

				send_data['events[' + index +']'] = {
					control_event_id: $tr.data('control-event-id'),
					control_event_type: $tr.find('.types-of-control select').select2('val'),
					description: $tr.find('.description-text').text(),
					event_date: $tr.find('.event-date').datepicker('getDate').getMysqlFormat(),
					active_till: $tr.find('.active-till').datepicker('getDate').getMysqlFormat(),
					max_rating: __max_rating,
					to_remove: $tr.data('to-remove')
				};
				if (!$tr.data('to-remove')){
					max_rating_sum += __max_rating;
				}
			});

			if (project_exists){
				var features_groups_sum = {_0: 0, _1: 0, _2: 0},
					type_0_exists = $settings.find('.project-feature.feature-0:not(.to-remove)').length > 0,
					type_1_exists = $settings.find('.project-feature.feature-1:not(.to-remove)').length > 0,
					type_2_exists = $settings.find('.project-feature.feature-2:not(.to-remove)').length > 0;

				all_types_of_project_feature = type_0_exists && type_1_exists && type_2_exists;

				$settings.find('.project-feature').each(function(index){
					var $tr  = $(this),
						feature_max_rating = parseFloat($tr.find('.rating-input').val());

					if (!$tr.hasClass('to-remove')){
						features_groups_sum['_' + $tr.data('feature-group')] += !isNaN(feature_max_rating) ? feature_max_rating : 0;
						all_max_ratings = all_max_ratings && (!isNaN(feature_max_rating) && feature_max_rating > 0) || $tr.hasClass('to-remove');
					}

					send_data['features[' + index +']'] = {
						feature_id: $tr.data('feature-id'),
						feature_name: $tr.find('.project-features-list').length ? $tr.find('.project-features-list').select2('val') : $tr.find('.feature-name').text(),
						description: $tr.find('.description-text').text(),
						max_rating: $tr.find('.rating-input').val(),
						feature_group: $tr.data('feature-group'),
						to_remove: $tr.data('to-remove')
					};
				});

			}

			if (max_rating_sum > CONTROL_EVENTS_MAX){
				$errors_ul.append('<li>Максимальное количество баллов за контрольные мероприятия составляет <strong>' + CONTROL_EVENTS_MAX + ' баллов</strong>.</li>');
			}
			if (project_exists){
				if (!all_types_of_project_feature){
					$errors_ul.append('<li>Необходимо указать как минимум по одной характеристике из каждой группы оценивания курсовой работы.</li>');
				}
				if (!all_max_ratings){
					$errors_ul.append('<li>Не указаны максимальные баллы за характеристики курсовой работы.</li>');
				}
				if (features_groups_sum['_0'] < 1){
					$errors_ul.append('<li>Сумма баллов характеристик из первой группы (формальные признаки) должно быть <strong>больше 0</strong>.</li>');
				}
				if (features_groups_sum['_0'] > FEATURES_0_MAX){
					$errors_ul.append('<li>Сумма баллов характеристик из первой группы (формальные признаки) должно быть <strong>не более 20</strong>.</li>');
				}
				if (features_groups_sum['_1'] < FEATURES_1_MIN){
					$errors_ul.append('<li>Сумма баллов характеристик из второй группы (содержательные признаки) должно быть <strong>не менее 50</strong>.</li>');
				}
				if (features_groups_sum['_2'] < FEATURES_2_MIN){
					$errors_ul.append('<li>Сумма баллов характеристик из третьей группы (защита работы) должно быть <strong>не более 20</strong>.</li>');
				}
				if (features_groups_sum['_0'] + features_groups_sum['_1'] + features_groups_sum['_2'] != 100){
					$errors_ul.append('<li>Сумма баллов характеристик курсовой работы должна <strong>быть 100</strong>.</li>');
				}
			}
			if ($errors_ul.find('li').length != 0){
				$('#settings-wrong')
					.modal('show')
					.find('.modal-text')
					.empty()
					.append($errors_ul);
			}else{
				var ladda = Ladda.create(this);
				ladda.start();
				$.ajax({
					url: '/api/brs/edu_groups/' + student_group_id + '/subjects/' + subject_id + '/settings',
					type: 'POST',
					data: send_data,
					success: function(res){
						showNotifier(res);
						var $to_remove = $('.to-remove'),
							$st_tab = $('.settings-tab');
						if ($to_remove.length > 0){
							$to_remove.hide('slow', function(){
								$to_remove.remove();
							});
							$st_tab.click();
						}else{
							$st_tab.click();
						}
					}
				}).always(function(){
					ladda.stop();
				});
			}

		});

		$.ajax({
			url: 'api/brs/edu_groups/' + student_group_id + '/subjects/' + subject_id +'/settings',
			success: function(res){
				control_event_types = res.data.control_events.event_types;
				var $project_exists = $settings.find('#project-exists');
				if (res.data.project.exists){
					$settings.find('.project-features-table').show();
					$project_exists.prop('checked', true);
					buildProjectFeaturesList(res.data.project.features);
				}else{
					$settings.find('.project-features-table').hide();
					$project_exists.prop('checked', false);
				}
				$project_exists
					.labelauty({
						checked_label: "Курсовая работа по дисциплине",
						unchecked_label: "Курсовая работа по дисциплине"
					});
				buildControlEventsList(res.data.control_events.events);
				setMaxRatings(res.data.max_ratings);
				updateFormHandlers();
				$('[data-toggle="popover"]').popover();
			}
		});

	};

	_page.showProjectForm = function(){
		var MIN_SUM_TO_RETRY = 40, //меньше этого - пересдача
			student_group_id = $('select.select2.groups').select2('val'),
			subject_id = $('select.select2.subjects').select2('val');

		function calculateMarks($lines){
			if ($lines == undefined){
				$lines = $('.student.project-line');
			}
			$lines.each(function(){
				var $this = $(this),
					$marks = $this.find('.feature-mark'),
					$sum = $this.find('.feature-marks-sum'),
					$final_mark = $this.find('.final-mark'),
					line_sum = 0,
					curr_val,
					all_empty = true,
					mark_text = 'Неудовл.';
				$marks.each(function(){
					curr_val = parseFloat(this.innerText);
					if (!isNaN(curr_val)){
						all_empty = false
					}
					line_sum += (isNaN(curr_val)) ? 0 : curr_val;
				});
				if (!all_empty){
					if (line_sum >= MIN_SUM_TO_RETRY && line_sum < 60){
						mark_text = 'Удовл.';
					}else if (line_sum >= 60 && line_sum < 80){
						mark_text = 'Хорошо';
					}else if (line_sum >= 80){
						mark_text = 'Отлично';
					}
				}else{
					line_sum = '';
					mark_text = '';
				}
				$sum.text(line_sum);
				$final_mark.text(mark_text);
			});
		}

		function calculateRetries(){}

		function initChanceCalendars(){
			var calendar_opts = {
					format: 'yyyy-mm-dd',
					startDate: window.getBRSInfo().start_date,
					endDate: window.getBRSInfo().finish_date,
					daysOfWeekDisabled: '0',
					startView: '0',
					language: 'ru',
					rtl: rtl(),
					weekStart: 1
				};
			$('.chance-date:not(.calendar)').each(function(){
				var $td = $(this),
					$cal = $td.find('.chance-date-calendar');
				$cal.datepicker(calendar_opts);
				$cal.on('changeDate', function(e){
					$td.find('.chance-date-text').text(e.format('dd.mm.yyyy'));
				});
				$td.addClass('calendar');
			});

			$('.chance-date-calendar-all').datepicker(calendar_opts).on('changeDate', function(e){
				var $this_table = $(this).parents('table');
				$this_table.find('.chance-date').each(function(){
					var $td = $(this);
					$td.find('.chance-date-calendar').datepicker('update', e.format('yyyy-mm-dd'));
					$td.find('.chance-date-text').text(e.format('dd.mm.yyyy'));
				});
			});
		}

		function appendStudents(students, themes){
			var mark_tds = '',
				counter = 1,
				$project_table = $('#project'),
				themes_by_student_id = {};
			$project_table.find('.student').remove();

			themes.forEach(function(value){
				themes_by_student_id['_' + value.student_id] = value.theme;
			});

			$('.feature').each(function(){
				var $this = $(this);
				mark_tds += '<td class="feature-mark editable feature-' + $this.data('feature-id') +'" contenteditable data-feature-id="' + $this.data('feature-id') +'"></td>';
			});
			mark_tds += '<td class="feature-marks-sum"></td>';
			mark_tds += '<td class="final-mark"></td>';

			students.forEach(function(value){
				value.number = counter++;
				value.chance_type = 1;
				value.theme = themes_by_student_id['_' + value.student_id];
				tmpl('project-student-line', value)
					.append(mark_tds)
					.appendTo($project_table);
			});
			$('.student.project-line').each(function(){
				var $this = $(this);
				$this.find('.feature-mark').on('blur', function(){
					var $td = $(this),
						$feature = $('.feature.feature-' + $td.data('feature-id')),
						input_rating = parseFloat($td.text());
					if (isNaN(input_rating) && $td.text() != ''){
						showNotifier({status: false, text: 'Укажите, пожалуйста, число от 0 до ' + $feature.data('max-rating')});
						$td.text('');
					}else{
						if (input_rating > parseFloat($feature.data('max-rating'))){
							showNotifier({status: false, text: 'Укажите, пожалуйста, число, от 0 до ' + $feature.data('max-rating')});
							$td.text('');
						}else{
							calculateMarks($this);
						}
					}
				});
			})
		}


		function fillProjectMarksWithData(marks){
			var marks_with_chance_type = {
				'_1': [],
				'_2': [],
				'_3': []
			};

			marks.forEach(function(value){
				marks_with_chance_type['_' + value.chance_type].push(value);
			});

			//build first chances
			marks_with_chance_type['_1'].forEach(function(value){
				var $st_lint = $('.student-' + value.student_id + '.chance-type-1'),
					chance_date = value.chance_date ? moment(value.chance_date, 'YYYY-MM-DD'): '';
				$st_lint.find('.chance-date-calendar').datepicker('setDate', chance_date._d);
				chance_date = chance_date.format('DD.MM.YYYY');
				value.mark = isNaN(parseInt(value.mark)) ? '' : value.mark;
				$st_lint.find('.chance-date-text').text(chance_date);
				$st_lint.find('.feature-mark.feature-' + value.feature_id).text(value.mark);
			});
			calculateMarks();
			calculateRetries();
		}

		$.ajax({
			url: '/api/brs/edu_groups/' + student_group_id + '/subjects/' + subject_id +'/project',
			success: function(res){
				var project = res.data.project,
					$prj = $('#project');
				$prj.find('tbody').empty();
				if (project.exists == false || project.features.length == 0){
					$prj.find('tbody').empty().append(tmpl('no-project-line', {}));
					$('.save.project').addClass('disabled');
				}else{
					$('.save.project').removeClass('disabled');
					$prj.removeClass('hidden');
					$prj.find('.feature-mark').remove();
					$prj.find('.tr-features-line').empty().append(tmpl('feature-td', project.features));
					$prj.find('.features-group').each(function(){
						var $group = $(this),
							group_number = $group.data('group-number');
						$group.attr('colspan', $('.feature.feature-group-' + group_number).length);
					});
					appendStudents(res.data.students, res.data.themes);
					initChanceCalendars();
					fillProjectMarksWithData(res.data.marks);
				}
			}
		});

		$('.save.project').off('click').on('click', function(){
			var ladda = Ladda.create(this),
				$student_lines = $('.student.project-line'),
				send_data = [];
			$student_lines.each(function(){
				var $this = $(this),
					chance_date = $this.find('.chance-date-calendar').datepicker('getDate'),
					obj = {
						student_id: $this.data('student-id'),
						theme: $this.find('.project-theme').text(),
						chance_date: chance_date.getMysqlFormat(),
						chance_type: 1,
						marks: []
					};
				$this.find('.feature-mark').each(function(){
					var $mark = $(this),
						mark_float = parseFloat($mark.text());
					mark_float = isNaN(mark_float) ? null : mark_float;
					obj.marks.push({id: $mark.data('feature-id'), value: mark_float})
				});
				send_data.push(obj);
			});
			ladda.start();
			$.ajax({
				url: 'api/brs/edu_groups/' + student_group_id + '/subjects/' + subject_id + '/project_marks',
				type: 'POST',
				data: JSON.stringify(send_data),
				contentType: 'application/json',
				success: showNotifier
			}).always(function(){
				ladda.stop();
			});

		});

	};

	_page.showExamForm = function(){

		var student_group_id = $('select.select2.groups').select2('val'),
			subject_id = $('select.select2.subjects').select2('val'),
			$exam = $('#exam'),
			$exam_body = $exam.find('tbody');

		function calculateMarkTexts(lines){
			var exam_form_id = $('.select2.exam-form').select2('val');
			if (isNaN(parseInt(exam_form_id))){
				showNotifier({status: false, text: 'Выберите тип итогового мероприятия для выставления оценки по пятибальной шкале'});
				return false;
			}
			lines.each(function(){
				var $line = $(this),
					marks_sum = 0,
					mark_texts_array = EXAM_FORM_TOTAL_MARK_NAMES[String(exam_form_id)],
					marks_text = '';
				$line.find('.mark-total-text').text('');

				$line.find('.mark').each(function(){
					if ($(this).text() == '' || marks_sum === false){
						marks_sum = false;
						return true;
					}
					var val = parseFloat($(this).text());
					marks_sum += isNaN(val) ? 0 : val;
				});

				if (marks_sum === false) {
					return true;
				}

				$line.find('.marks-sum').text(marks_sum);
				$.each(mark_texts_array, function(key, value){
					if (marks_sum >= parseFloat(key)){
						marks_text = value;
					}
				});
				$line.find('.mark-total-text').text('').text(marks_text);
			});
		}

		function buildExamTable(data) {
			$exam_body.empty();
			data.students.forEach(function(value, index) {
				value.number = (index + 1);
				$exam_body.append(tmpl('exam-student-line', value));
			});

			$.each(data.marks, function(key, value) {
				$exam_body.find('.activity-rating.st-' + value.student_id).text(value.activity);
				$exam_body.find('.attendance-rating.st-' + value.student_id).text(value.attendance);
				$exam_body.find('.progress-rating.st-' + value.student_id).text(value.progress);
				value.exam = value.exam == null ? '' : value.exam;
				$exam_body.find('.exam-mark.st-' + value.student_id).text(value.exam);
				value.date_of_change = value.date_of_change == null || value.exam == '' ? '' : moment(value.date_of_change).format('DD/MM/YYYY HH:mm:ss');
				$exam_body.find('.date-of-change.st-' + value.student_id).text(value.date_of_change);
			});

			$exam_body.find('.exam-mark').on('blur', function() {
				var $this = $(this),
					exam_mark = parseFloat($this.text()),
					$tr = $this.parents('.student-line');
				if (exam_mark > EXAM_MAX_POINTS){
					showNotifier({status: false, text: 'Превышен максимальный балл за мероприятие. Исправлено на ' + EXAM_MAX_POINTS});
					$this.text(EXAM_MAX_POINTS);
				}
				calculateMarkTexts($tr);
			});
			calculateMarkTexts($('.student-line.exam-line'));
			$('.exam-form').on('change', function(){
				calculateMarkTexts($('.student-line.exam-line'));
			});
		}

		$.ajax({
			url: 'api/brs/edu_groups/' + student_group_id + '/subjects/' + subject_id + '/exam',
			type: 'GET',
			success: function(res){
				if (res.data.exam_form_id != null){
					$('.exam-form').select2('val', res.data.exam_form_id);
				}
				buildExamTable(res.data);
			}
		});

		$('.save.exam').off('click').on('click', function(){
			var send_data = {
				marks: [],
				exam_form_id: $('.exam-form').select2('val')
			};
			$exam_body.find('.exam-mark').each(function(){
				var $td = $(this),
					exam_mark = parseFloat($td.text());
				if (exam_mark > EXAM_MAX_POINTS){
					showNotifier({status: false, text: 'Превышен максимальный балл за мероприятие. Исправлено на ' + EXAM_MAX_POINTS});
					$td.text(EXAM_MAX_POINTS);
					exam_mark = EXAM_MAX_POINTS;
				}
				send_data.marks.push({
					mark: exam_mark,
					student_id: $td.parents('.student-line').data('student-id')
				});
			});

			$.ajax({
				url: '/api/brs/edu_groups/' + student_group_id + '/subjects/' + subject_id + '/exam',
				data: JSON.stringify(send_data),
				contentType: 'application/json',
				type: 'POST',
				success: showNotifier
			})
		});
	}

	_page.afterOpen = function(){
		function appendMyGroups(res){
			var $groups = $('.select2.groups').html("<option></option>"),
				$subjects = $('.select2.subjects').html("<option></option>"),
				render_data = [];
			res.data.forEach(function(value){
				value.student_group_name = value.program ? value.program : (value.profile ? value.profile : value.edu_speciality_name);
				render_data.push(value);
			});
			$groups
				.append(tmpl('my-groups-option', render_data))
				.select2()
				.on('change', function(e){
					$.ajax({
						url: '/api/brs/edu_groups/' + e.val + '/subjects',
						success: appendSubjectsWithGroup
					});
				});
			$subjects.select2().select2('disable');
		}

		function appendSubjectsWithGroup(res){
			$('.panels-heading, .panel-body, #export-table').addClass('hidden');
			var $subjects = $('select.subjects').html("<option></option>");
			$subjects
				.select2('destroy')
				.append(tmpl('subjects-with-group-option', res.data))
				.select2()
				.select2('enable')
				.off('change')
				.on('change', _page.showAttendanceForm);

			if (res.data.length == 1){
				$subjects.select2('val', res.data[0].subject_id);
				_page.showAttendanceForm();
			}
		}

		$.ajax({
			url: 'api/brs/my_groups',
			dataType: 'JSON',
			success: appendMyGroups
		});

		var $tab_items = $('.tab-item');
		$tab_items.each(function(){
			var $this = $(this);

			$this.off('click').on('click', function(){
				function changeTab(){
					$('.tab-pane').removeClass('active').eq($tab_items.index($this)).addClass('active');
					if ($this.data('click-handler') && (_page.hasOwnProperty($this.data('click-handler')))){
						if (_page[$this.data('click-handler')] instanceof Function){
							_page[$this.data('click-handler')].call();
							$('.panel-body').removeClass('hidden');
						}
					}
				}
				changeTab();
			});
		});

		$('.print.btn').on('click', function(){
			$('.select2').addClass('hidden-print');
			$('table').addClass('hidden-print');
			$('table:visible').removeClass('hidden-print');
			window.print();
		});

		setArrowButtonsBehavior();

		_super_after_open.call(this);

	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}
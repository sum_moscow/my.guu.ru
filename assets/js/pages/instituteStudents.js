function InstituteStudents(_url){


	var photo_file = null,
		photo_file_name = null,
		file = null,
		file_name = null;

	function handleFileSelect(evt) {
		var files = evt.target.files; // FileList object
		for(var i = 0, f; f = files[i]; i++) {
			var reader = new FileReader();
			reader.onload = (function(the_file) {
				return function(e) {
					var img = tmpl('document-scan-img', {}).find('img');
					img.get(0).src = e.target.result;
					$('.document-image-wrapper').html(img);
					file = e.target.result;
					file_name = the_file.name;
				};
			})(f);
			// Read in the image file as a data URL.
			reader.readAsDataURL(f);
		}
	}

	var _page = Page(_url),
		ALL_INPUTS_TO_FILL = 13,
		agreements = [
			{text: 'Не согласен', 'icon_class': 'cancel', 'btn_class': 'warning'},
			{text: 'Согласен', 'icon_class': 'check', 'btn_class': 'success'}
		]; // вызвать конструктор родителя, получить родительский объект;

	_page.settings = {
		url: '',
		data: {

		}
	};
	_page.filtering = {
		start_date: moment.unix(0),
		end_date: moment.unix(2140000000)
	};

	var _super_after_open = _page.afterOpen;

	_page.normalizeData = function(res){

	};


	function addFilteringFunction(){
		var DATE_COLUMN_INDEX = 6;
		$.fn.dataTableExt.afnFiltering.push(
			function(oSettings, aData){
				var dateStart = _page.filtering.start_date;
				var dateEnd = _page.filtering.end_date;
				// aData represents the table structure as an array of columns, so the script access the date value
				// in the first column of the table via aData[0]
				var evalDate = moment(aData[DATE_COLUMN_INDEX], 'DD/MM/YYYY HH:MM:SS').unix();

				return dateStart.unix() <= evalDate && evalDate <= dateEnd.unix();

			});

	}

	_page.afterOpen = function(){

		var params = {},
			query_params = {};



		$.getMultiScripts([
				'jquery.dataTables.min.js',
				'dataTables.bootstrap.js',
				'datatables/TableTools.min.js',
				'datatables/responsive/js/datatables.responsive.js',
				'datatables/jquery.dataTables.columnFilter.js',
				'datatables/lodash.min.js'
			], function(){

				addFilteringFunction();

				var students_data_table,
					$students_table = $('#students-table'),
					$real_table = $students_table.find('table'),
					search_type = 'last_name';


				function getGroupNormalName(val){
					if (val.program != null){
						return val.program + ' ' + val.course + '-' + val.group + ' ('+ val.institute_abbr + ')';
					}else if (val.profile != null){
						return val.profile + ' ' + val.course + '-'  + val.group + ' ('+ val.institute_abbr + ')';
					}else{
						return val.edu_speciality_name + ' ' + val.course + '-'  + val.group + ' ('+ val.institute_abbr + ')';
					}
				}

				function getSelects(){
					$.ajax({
						url: 'api/student_groups/my',
						data: params,
						success: buildSelects
					});
				}

				function buildSelects(res){
					if (res.status == false){
						showNotifier(res);
						return;
					}
					$.each(res.data, function(key, values){
						var $select = $('select.filter-select.' + key)
							.select2('destroy');
						$select.find('option').prop('disabled', 'disabled');
						values.forEach(function(val){
							if (key == 'groups'){
								val.name = getGroupNormalName(val);
							}
							if (val.name == null) return true;
							var option_value = key == 'edu_specialties' ? val.name : val.id;
							var $option = $select.find('option[value="' + option_value +'"]');
							if ($option.length == 0){
								$select.append('<option value="' + option_value + '">' + val.name + '</option>');
							}else{
								$option.prop('disabled', false);
							}
						});
						$select.select2({
							allowClear: true
						})
							.off('change')
							.on('change', function(e){
								var $this = $(this);
								if(e.val == null){
									delete params[$this.data('name')];
									delete query_params[$this.data('request-param')];
								}else{
									params[$this.data('name')] = e.val;
									query_params[$this.data('request-param')] = e.val;
								}
								getSelects();
								if ($select.is('.groups')){
									if (e.val == null){
										emptyTableWithStudents();
									}else{
										$.ajax({
											url: '/api/gupmsr/student_group/' + e.val + '/students',
											success: buildStudentsTable
										});
									}
								}
							});
						var $selected_option = $select.find('option[value="' + params[$select.data('name')] + '"]');
						if (!$selected_option.prop('disabled')){
							$select.select2('val', params[$select.data('name')])
						}
					});
				}

				function emptyTableWithStudents(){
					if (students_data_table){
						$students_table.find('.filtering-row').remove();
						students_data_table.fnDestroy();
					}
					$students_table
						.find('table')
						.addClass('hidden')
						.find('tbody')
						.empty();
				}

				function buildStudentsTable(res){
					emptyTableWithStudents();
					if (!res.hasOwnProperty('data')) return;
					$('.btn-show-group').removeClass('active');
					var $st_table = $students_table.find('tbody'),
						$item;
					$st_table
						.empty();
					res.data.forEach(function(value, index){
						var required = ['last_name', 'first_name', 'birth_date', 'series',
								'number', 'issue_date' ,'issue_place', 'registration_address',
								'residence_address', 'phone_number'],
							additional = ['doc_type_id', 'snils', 'email'];
						value.list_number = index + 1;

						value.phone_number = value.phone_number.length == 11 && value.phone_number[0] == '8' ? value.phone_number.substring(1) : value.phone_number;

						value.update_date = moment(new Date(value.last_update_time)).format('DD/MM/YYYY HH:MM:SS');
						value.birth_date = moment(new Date(value.birth_date)).format('DD/MM/YYYY');
						value.group_name = getGroupNormalName(value);
						if (value.issue_date != '' && value.issue_date != null){
							value.issue_date = moment(new Date(value.issue_date)).format('DD/MM/YYYY');
						}
						value.filled = 0;


						value.agreement_text = !value.agreement ? agreements[0].text : agreements[1].text;
						value.agreement_icon_class = !value.agreement ? agreements[0].icon_class : agreements[1].icon_class;
						value.agreement_class = !value.agreement ? agreements[0].btn_class : agreements[1].btn_class;


						value.agreement_int = value.agreement ? 1 : 0;
						value.doc_type_id = (value.doc_type_id == null) ? value.doc_type_id : value.doc_type_id;
						required.forEach(function(attr_name){
							value.filled += (value[attr_name] == null || value[attr_name] == '') ? 0 : 1;
						});

						additional.forEach(function(attr_name){
							value.filled += (value[attr_name] == null || value[attr_name] == '') ? 0 : 1;
						});

						if (value.filled >= required.length && value.filled < ALL_INPUTS_TO_FILL){
							value.filled_class = 'info';
						}else if(value.filled == ALL_INPUTS_TO_FILL){ //13 - всего полей
							value.filled_class = 'success';
						}else{
							value.filled_class = 'warning';
						}

						value.filled_text = value.filled + '/' + ALL_INPUTS_TO_FILL;

						if (!value.student_status){
							if (value.last_sync_time != null){
								value.student_status = 'Выгружен';
								value.message = 'Дата последней выгрузки: ' + new Date(value.last_sync_time).getReadableText('genitive', true)
									+ '.';
							}else{
								value.student_status = 'Не выгружался';
							}
						}

						$item = tmpl('student-line', value);


						$item.find('.edit-student-info-btn').data('value', value).on('click', function(){
							var $this = $(this),
								value =  $this.data('value');

							if (value.document_scan_ext != null){
								value.document_scan_img = tmpl('document-scan-img', value);
							}

							var $modal_body = tmpl('edit-student-info', value);
							$modal_body.find('.select2.document').select2().select2('val', value.doc_type_id);

							if($.isFunction($.fn.datepicker)){
								$modal_body.find(".datepicker").each(function(i,el){
									var $this=$(el),opts={
											format: attrDefault($this,'format','dd/mm/yyyy'),
											startDate: attrDefault($this,'startDate',''),
											endDate: attrDefault($this,'endDate',''),
											daysOfWeekDisabled: attrDefault($this,'disabledDays',''),
											startView: attrDefault($this,'startView', 0),
											rtl:rtl()
										},
										$n=$this.next(),
										$p=$this.prev();
									$this.datepicker(opts);
									if($n.is('.input-group-addon')&&$n.has('a')){
										$n.on('click',function(ev){
											ev.preventDefault();$this.datepicker('show');
										});
									}
									if($p.is('.input-group-addon')&&$p.has('a')){
										$p.on('click',function(ev){
											ev.preventDefault();
											$this.datepicker('show');
										});
									}});
							}

							$modal_body
								.find('#field-file')
								.on('change', handleFileSelect);

							$modal_body.find('.copy-from-registration').on('click', function(){
								$modal_body
									.find('.residence')
									.each(function(){
										var $this = $(this);
										$this.val($modal_body.find('.registration.' + $this.data('type')).val());
										$this.data('id', $modal_body.find('.registration.' + $this.data('type')).data('id'));
									})
							});
							$modal_body.find('.student-agreement-btn').on('click', function(){
								var $this = $(this);
								if ($this.data('agreement') == 1){
									$this
										.removeClass('btn-success')
										.addClass('btn-warning')
										.html('<span>' + agreements[0].text + '</span><i class="entypo-' + agreements[0].icon_class +'"></i>')
										.data('agreement', 0)
								}else{
									$this
										.removeClass('btn-warning')
										.addClass('btn-success')
										.html('<span>' + agreements[1].text + '</span><i class="entypo-' + agreements[1].icon_class +'"></i>')
										.data('agreement', 1);
								}
							});

							$modal_body
								.find('#photo-file-field')
								.on('change', function(evt){
									var files = evt.target.files; // FileList object
									for(var i = 0, f; f = files[i]; i++) {
										var reader = new FileReader();
										reader.onload = (function(the_file) {
											return function(e) {
												document.getElementById('user-photo').src = e.target.result;
												photo_file = e.target.result;
												photo_file_name = the_file.name;
											};
										})(f);
										reader.readAsDataURL(f);
									}
								});

							$modal_body.find('.typeahead').each(function(){
								var $input = $(this),
									$parent = $input.parents('.input-group').prev();

								$input.kladr({
									token: '559b9f6c0a69de27228b4577',
									parentId: $parent.length > 0 && $parent.find('input').data('id') ? $parent.find('input').data('id') : null,
									parentInput: $parent.find('input'),
									type: $.kladr.type[$input.data('type')],
									change: function(obj){
										$input.data('id', obj.id);
										if ($input.data('type') == 'building'){
											$input.siblings('.postcode').val(obj.zip);
										}
									}
								});
							});

							$modal_body.find('.save-and-send').on('click', function(){

								var send_data = {
									user_id: value.user_id,
									student_id: value.student_id,
									doc_type_id: $modal_body.find('.document').select2('val'),
									agreement: $modal_body.find('.student-agreement-btn').hasClass('btn-success') ? 1 : 0
								}, new_value = {};

								$modal_body.find('input').each(function(){
									var $this = $(this);
									send_data[this.name] = this.value;
									if ($this.hasClass('typeahead')){
										send_data[this.name + '_kladr'] = $this.data('id') != null ? $this.data('id') : '';
									}
								});

								new_value = $.extend(value, send_data);

								send_data.issue_date = send_data.issue_date ? moment(send_data.issue_date, 'DD/MM/YYYY').format('YYYY-MM-DD') : '0000-00-00';
								send_data.birth_date = send_data.birth_date ? moment(send_data.birth_date, 'DD/MM/YYYY').format('YYYY-MM-DD') : '0000-00-00';


								new_value.country = send_data['registration_country'];
								new_value.region = send_data['registration_region'];
								new_value.district = send_data['registration_district'];
								new_value.city = send_data['registration_city'];
								new_value.street = send_data['registration_street'];
								new_value.building = send_data['registration_building'];
								new_value.flat = send_data['registration_flat'];


								new_value.res_country = send_data['residence_country'];
								new_value.res_region = send_data['residence_region'];
								new_value.res_district = send_data['residence_district'];
								new_value.res_city = send_data['residence_city'];
								new_value.res_street = send_data['residence_street'];
								new_value.res_building = send_data['residence_building'];
								new_value.res_flat = send_data['residence_flat'];

								new_value.residence_kladr_id = send_data['residence_building_kladr_id'];
								new_value.registration_kladr_id = send_data['registration_building_kladr_id'];

								new_value.agreement_text = !send_data.agreement ? agreements[0].text : agreements[1].text;
								new_value.agreement_icon_class = !send_data.agreement ? agreements[0].icon_class : agreements[1].icon_class;
								new_value.agreement_class = !send_data.agreement ? agreements[0].btn_class : agreements[1].btn_class;


								new_value.document_scan_ext = file_name != null ? file_name.split('.').pop() : null;
								new_value.photo_ext = photo_file_name != null ? photo_file_name.split('.').pop() : null;


								var prompt_texts = [];
								if (file == null && new_value.document_scan_ext == null && value.document_scan_ext){
									prompt_texts.push('без скана паспорта')
								}
								if (photo_file == null && new_value.photo_ext == null && value.photo_ext){
									prompt_texts.push('без фотографии')
								}

								if (prompt_texts.length != 0){
									var send_agreement = confirm('Вы уверены, что хотите сохранить анкету без ' + prompt_texts.join(' и ') + '?');
									if (!send_agreement){
										return true;
									}
								}


								send_data['file'] = file;
								send_data['photo_file'] = photo_file;
								send_data['file_name'] = file_name;
								send_data['photo_file_name'] = photo_file_name;

								file = null;
								file_name = null;
								photo_file = null;
								photo_file_name = null;

								$.ajax({
									url: '/api/students/' + value.student_id,
									type: 'POST',
									data: JSON.stringify(send_data),
									contentType: 'application/json',
									success: function(res){
										if (res.status == 1){
											var $st = $('.student-' + new_value.user_id);
											$('.edit-student-passport-info').modal('hide');
											new_value.filled = 0;
											required.forEach(function(attr_name){
												new_value.filled += (value[attr_name] == null || value[attr_name] == '') ? 0 : 1;
											});

											additional.forEach(function(attr_name){
												new_value.filled += (value[attr_name] == null || value[attr_name] == '') ? 0 : 1;
											});

											if (file_name != null){
												file_name = file_name.split('.');
												new_value.document_scan_ext = file_name[file_name.length - 1];
											}

											if (value.filled >= required.length && value.filled < ALL_INPUTS_TO_FILL){
												value.filled_class = 'info';
											}else if(value.filled == ALL_INPUTS_TO_FILL){ //13 - всего полей
												value.filled_class = 'success';
											}else{
												value.filled_class = 'warning';
											}

											value.filled_text = value.filled + '/' + ALL_INPUTS_TO_FILL;
											$st.find('.update-date').text(new Date().getReadableText('genitive', true));
											$st.find('.filled')
												.text(new_value.filled+'/'+ALL_INPUTS_TO_FILL)
												.removeClass()
												.addClass('filled ' +  value.filled_class);
											$st.find('.agreement-td')
												.html('<span>' + new_value.agreement_text
													+ '</span><i class="entypo-'+new_value.agreement_icon_class
													+'"></i>');
											$this.data('value', new_value);
										}
										showNotifier(res);
									}
								})
							});


							$('.edit-student-passport-info')
								.empty()
								.append($modal_body)
								.removeClass('hidden')
								.modal('show');
						});
						$st_table.append($item);
					});
					students_data_table = $students_table.find('table').dataTable({
						"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Все"]],
						"sPaginationType": "bootstrap",
						initComplete: function(){
							var _selected_values = {},
								_datatable = this,
								$filtering_line = tmpl('filtering-row', {}),
								unique_values = [],
								values = $students_table.find('table').DataTable().data().toArray();
							values.forEach(function(value){
								value.forEach(function(param, param_index){
									if (!unique_values[param_index]){
										unique_values[param_index] = [];
									}
									if(unique_values[param_index].indexOf(param) == -1){
										unique_values[param_index].push(param);
									}
								})
							});
							unique_values.forEach(function(value, index){
								var $td = $filtering_line.find('td:eq(' + index + ')');
								switch($td.data('filter-type')){
									case 'td-select2': {
										var $select2 = $('<select multiple><option></option></select>');
										value.forEach(function(option){
											$select2.append('<option>' + option + '</option>')
										});
										$td.find('button').on('click', function(){
											$(this).toggleClass('active');
										}).popover({
											animation: true,
											html: true,
											content: $select2.get(0).outerHTML,
											placement: 'bottom',
											title: 'Фильтрация: ' + $students_table.find('th:eq(' + index + ')').text()
										}).on('shown.bs.popover', function() {
											var $select = $td.find('select');
											if ($select.is('.select2')){
												$select.select2('destroy');
											}
											if (_selected_values['_' + index]){
												$.each(_selected_values['_' + index], function(i,option_value){
													$select.find("option[value='" + option_value + "']").prop("selected", true);
												});
											}
											$select.select2({
												allowClear: true,
												width: 250
											}).on('change', function(e){
												_selected_values['_' + index] = [];
												e.val.forEach(function(value){
													if (value != ''){
														_selected_values['_' + index].push(value);
													}
												});
												var regex = '';
												if (_selected_values['_' + index].length != 0){
													regex = [];
													_selected_values['_' + index].forEach(function(param){
														regex.push($.fn.dataTable.util.escapeRegex(param));
													});
													regex = regex.join('|');
													$td.find('button')
														.removeClass('btn-default')
														.addClass('btn-info')
														.attr('title', 'Отфильтровано: ' + e.val.join(', '));
												}else{
													$td.find('button')
														.removeClass('btn-info')
														.addClass('btn-default')
														.attr('title', 'Фильтр не задан');
												}
												_datatable.api().column(index).every( function () {
													this
														.search(regex, true, false)
														.draw();
												});
											});
											$select.select2('val', _selected_values['_' + index]);
											$students_table.find('.popover .arrow').remove();
										});
										break;
									}
									case 'td-date': {
										function cb(start, end) {
											$td.find('a span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
											_page.filtering.start_date = start;
											_page.filtering.end_date = end;
											students_data_table.fnDraw();
										}
										$td.find('a').daterangepicker({
											"showDropdowns": true,
											ranges: {
												'Сегодня': [moment(), moment()],
												'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
												'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
												'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
												'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
												'Все': [moment.unix(0), moment.unix(2140000000).endOf('year')]
											},

											"startDate": "01/01/1970",
											"endDate": "31/12/2037",
											locale:{
												"format": "DD/MM/YYYY",
												"separator": " - ",
												"applyLabel": "Применить",
												"cancelLabel": "Отменить",
												"fromLabel": "С",
												"toLabel": "По",
												"customRangeLabel": "Диапазон дат",
												"daysOfWeek": [
													"ВС",
													"ПН",
													"ВТ",
													"СР",
													"ЧТ",
													"ПТ",
													"СБ"
												],
												"monthNames": [
													"Январь",
													"Февраль",
													"Март",
													"Апрель",
													"Май",
													"Июнь",
													"Июль",
													"Август",
													"Сентябрь",
													"Октябрь",
													"Ноябрь",
													"Декабрь"
												],
												"firstDay": 1
											}

										}, cb);
										break;
									}
								}
							});
							$students_table.find('thead').append($filtering_line);
						}
					});
					$students_table.find('table').removeClass('hidden');

				}


				show_loading_bar(60);
				getSelects();

				$('.get-students-list-btn').on('click', function(){
					$.ajax({
						url: 'api/gupmsr/students/search',
						data: query_params,
						success: buildStudentsTable
					});
				});

				$('.clear-all-selects').on('click', function(){
					$('.select2').select2('val', '');
				});

				function searchStudent(){
					var data = {};
					data[search_type] = $('.search-student-passport').val();
					$.ajax({
						url: 'api/gupmsr/students/search',
						data: data,
						success: buildStudentsTable
					});
				}

				$('.search-student-passport').on('keypress', function(e){
					if (e.which == 13){
						searchStudent();
					}
				});

				$('.search-student-passport-btn').on('click', searchStudent);

				$('.search-type-item').on('click', function(){
					var
						$this = $(this),
						$btn_text = $this.parents('.input-group-btn').find('.btn-text');
					$btn_text.text('Поиск по: ' + $this.text());
					search_type = $this.data('value');
				});

			});

		_super_after_open.call(this);
	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}
function MoodleGroups(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект;


	_page.settings = {
		url: null,
		data: {}
	};
	var _super_after_open = _page.afterOpen;


	_page.afterOpen = function(){
		$('.professor-' + getUserInfo().id).removeClass('hidden');

		$('.subjects').on('change', function(e){
			$.ajax({
				url: '/api/moodle/groups/grades/' + e.val,
				success: function(res){
					if (res){
						var _data = res.data,
							final_object = {
								users: {},
								events: {},
								grades: []
							},
							arr_index;
						_data.forEach(function(value){
							$('.open-moodle').off('click').on('click', function(){
								window.open('http://edu.guu.ru/enrol/index.php?id=' + value.courseid_in_moodle, '_blank');
							});
							arr_index = '_' + value.user_id;
							if (!final_object.users.hasOwnProperty(arr_index)){
								final_object.users[arr_index] = {
									first_name: value.first_name,
									last_name: value.last_name,
									middle_name: value.middle_name,
									user_id: value.user_id
								};
							}
							arr_index = '_' + value.activityid_in_moodle;
							if (!final_object.events.hasOwnProperty(arr_index)){
								final_object.events[arr_index] = {
									moodle_activity_id: value.moodle_activity_id,
									moodle_course_id: value.moodle_course_id,
									activityid_in_moodle: value.activityid_in_moodle,
									activity_name: value.activity_name
								};
							}
							final_object.grades.push({
								user_id: value.user_id,
								activityid_in_moodle: value.activityid_in_moodle,
								grade: value.grade,
								grademax_in_moodle: value.grademax_in_moodle,
							});
						});
					}else{
						return;
					}
					tmpl('main-table-tbody', {
						subject_name: $('.subjects :selected').text(),
						professor_first_name: getUserInfo().first_name,
						professor_last_name: getUserInfo().last_name,
						professor_father_name: getUserInfo().middle_name,
					}, $('#export-table').removeClass('hidden'));

					var $table = $('#moodle-students').removeClass('hidden'),
						$tbody = $table.find('tbody'),
						$thead_tr = $table.find('thead tr'),
						activities_td = [], tds = '';

					$.each(final_object.events, function(name, value){
						tmpl('moodle-activity-th', {activity_name: value.activity_name, activity_id: value.activityid_in_moodle}, $thead_tr);
						activities_td.push('activity-' + value.activityid_in_moodle);
					});

					$.each(final_object.users, function(name, user){
						tds = '';
						activities_td.forEach(function(td_class){
							tds += '<td class="' + td_class + '-user-' + user.user_id +'"></td>';
						});
						tmpl('moodle-user-line', {first_name: user.first_name, last_name: user.last_name, middle_name: user.middle_name}).append(tds).appendTo($tbody);
					});

					final_object.grades.forEach(function(grade){
						if (grade.grade == null) return;
						$('.activity-' + grade.activityid_in_moodle + '-user-' + grade.user_id).text(grade.grade + '/' + grade.grademax_in_moodle);
					});
				}
			});
		});
		$('.btn.print').on('click', function(){
			window.print();
		});

		$('#refresh-moodle-courses-info').on('click', function(){
			btn_ladda = Ladda.create(this);
			btn_ladda.start();
			socket.emit('sync.moodle.grades');
		});
		_super_after_open.call(this);
	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}
function InstituteStats(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект

	_page.settings = {
		url: '/api/users/me/dashInfo',
		data: {}
	};

	_page.normalizeData = function(data){
		var render_data = data.data;
		render_data.unread_messages_hidden = render_data.unread_messages == null ? 'hidden' : '';
		return render_data;
	};



	var _super_after_open = _page.afterOpen;
	_page.afterOpen = function() {
		$.ajax({
			url: '/api/science/stats?format=for_graph',
			method: 'GET',
			success: function(res){
				var chart,
					$chart = $("#chart"),
					$chart_back = $("#chart-back"),
					colors = Highcharts.getOptions().colors,
					data = res.data;
				data.forEach(function(e,i){
					data[i].index = i;
				});
				function setChart(data) {
					var len = chart.series.length;
					for(var i = 0; i < len; i++){
						chart.series[0].remove(false);
					}
					chart.redraw();
					if(data.series){
						chart.setTitle(null, {text: 'Медиана по '+data.name});
						for(var i = 0; i < data.series.length; i ++ ){
							chart.addSeries({
								name: 'Кафедра '+data.series[i].name,
								data: data.series[i].data,
								color: colors[i],
								academic_department: data.series[i].academic_department_ou_id
							});
						}
					} else {
						chart.setTitle(null, {text: 'Медиана по ВУЗу'});
						for(var i = 0; i < data.length; i ++ ){
							chart.addSeries({
								name: data[i].name,
								data: data[i].data,
								color: colors[i],
								index: data[i].index
							});
						}
					}
				}

				function buildProfessorsTable(academic_department_id, academic_department_name){
					var $table_body = $("#attestation-table").find("tbody");
					$("#attestation-table-header").text(academic_department_name);
					$.ajax({
						url: '/api/science/stats/academic_departments/' + academic_department_id,
						method: 'GET',
						success: function(res){
							$table_body.empty();
							res.data.forEach(function(staff){
								$table_body.append(
									tmpl('instituteStats-tableRow', {
										staff_id: staff.staff_id,
										user_id: staff.user_id,
										name: staff.last_name+" "+staff.first_name+" "+staff.middle_name,
										kpk: staff.indicators[0].value,
										knk: staff.indicators[1].value
									})
								);
							});
							$.bootstrapSortable(true);
						}
					})
				}

				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'chart',
						type: 'column'
					},
					title: {text: 'Аттестация'},
					subtitle: {text: 'Медиана по ВУЗу'},
					xAxis: {
						type: 'category',
						categories: ['Квалификационный научный коэффициент', 'Квалификационный педагогический коэффициент']
					},
					yAxis: { title: {text: ''}	},
					credits: {enabled: false},
					legend: {
						borderColor: '#ccc',
						borderWidth: 1,
						shadow: false
					},

					plotOptions: {
						column: {
							groupPadding: 0.1,
							cursor: 'pointer',
							point: {
								events: {
									click: function() {
										if (!$chart.hasClass("drilled-down")) {
											$chart.addClass("drilled-down");
											$chart_back.removeClass("hidden");
											setChart(data[this.series.options.index].drilldown);
										}
										else{
											var $attestation_table = $("#attestation-table");
											$attestation_table.show();
											buildProfessorsTable(this.series.options.academic_department, this.series.options.name);
											$(window).scrollTop($attestation_table.position().top);
										}
									}
								}
							},
							dataLabels: {
								enabled: true,
								color: '#FFFFFF',
								format: '{point.y:.1f}',
								y: 30,
								style: {
									fontFamily: 'Verdana, sans-serif',
									fontSize: '13px',
									textShadow: 'none'
								}
							}
						}
					},
					tooltip: {
						borderRadius: 0,
						backgroundColor: '#fff',
						headerFormat: '<span style="font-size:11px">{series.name}</span>',
						pointFormat: ''
					},
					series: data
				});
				$chart_back.on("click", function(){
					$chart.removeClass("drilled-down");
					$chart_back.addClass("hidden");
					setChart(data);
				})
			}
		});

		_super_after_open.call(this);
	};
	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}
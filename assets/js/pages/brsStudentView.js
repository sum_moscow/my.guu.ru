function BrsStudentView(_url){

	var _page = Page(_url), // вызвать конструктор родителя, получить родительский объект;
	ATTENDANCE_RATING = 0.2;

	_page.settings = {
		url: '',
		data: {}
	};
	var _super_after_open = _page.afterOpen,
		selected_semester_id = null,
		selected_subject_id = null;


	function buildMarksTables(res){
		var data = res.data,
			$att_table = $('#attendance-marks-tbody'),
			$events_table = $('#control-events-tbody'),
			arr = [], ce_arr = [],
			sum = 0, max_sum = 0;
		//Build attendance list
		$('.control-event-line, .class-line').remove();

		data.attendance.forEach(function(value){
			value.class_date = (value.class_date == '0000-00-00' || value.class_date == '') ? '#' + value.class_number : new Date(value.class_date).getReadableText();
			value.mark = (value.mark == null) ? ATTENDANCE_RATING : parseFloat(value.mark);
			sum += parseFloat(value.mark);
			arr.push(value);
		});
		arr.push({class_date: 'Итого', mark: sum.toFixed(2)});

		sum = 0;
		data.control_events.forEach(function(value){
			value.mark = parseFloat(value.mark);
			value.mark = isNaN(value.mark) ? 0 : value.mark;

			value.max_rating = parseFloat(value.max_rating);
			value.max_rating = isNaN(value.max_rating) ? 0 : value.max_rating;

			ce_arr.push(value);
			sum += value.mark;
			max_sum += value.max_rating;
		});

		ce_arr.push({control_event_name: 'Итого', mark: sum, max_rating: max_sum});

		tmpl('attendance-tr', arr, $att_table);
		tmpl('control-event-tr', ce_arr, $events_table);
		$('.tables-tab').removeClass('hidden');
	}

	function getMarks(){
		$.ajax({
			url: '/api/brs/me/' + selected_semester_id + '/subjects/' + selected_subject_id,
			success: buildMarksTables
		})
	}

	function buildSubjectsListInSemester(subjects){
		var $tabs = $('.subjects-tabs').empty();
		tmpl('subject-line', subjects, $tabs.empty());
		$('.tables-tab').addClass('hidden');
		$('.subject-item').on('click', function(){
			selected_subject_id = $(this).data('subject-id');
			getMarks();
		});
	}

	function getSubjectsListInSemester(){
		$.ajax({
			url: '/api/brs/me/' + selected_semester_id + '/subjects/',
			success: function(res){
				buildSubjectsListInSemester(res.data);
			}
		});
	}


	_page.afterOpen = function(){
		var $semesters = $('.semesters');
		$.ajax({
			url: '/api/brs/me/semesters/',
			success: function(res){
				$semesters.append('<option></option>');
				res.data.forEach(function(value){
					$semesters.append(tmpl('brs-semester-item', value));
				});
				$('.tables-tab').addClass('hidden');
				$semesters.on('change', function(e){
					debugger;
					selected_semester_id = e.val;
					getSubjectsListInSemester();
				});
			}
		});
		_super_after_open.call(this);

	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}
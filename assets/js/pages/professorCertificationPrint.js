function ProfessorCertificationPrint(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект

	_page.settings = {
		url: '',
		data: {}
	};

	_page.normalizeData = function(data){

	};




	var _super_after_open = _page.afterOpen;
	_page.afterOpen = function(){

		var staff = {staff_id: "me", user_id: window.getUserInfo().id};

		if(typeof(window._current_page.getURL().getObject().properties.staff_id) != "undefined")
			staff.staff_id = window._current_page.getURL().getObject().properties.staff_id;
		if(typeof(window._current_page.getURL().getObject().properties.user_id) != "undefined")
			staff.user_id = window._current_page.getURL().getObject().properties.user_id;

		$.ajax({
			url: 'api/users/' + staff.user_id + '/fullInfo',
			method: 'GET',
			dataType: 'JSON',
			success: function(resp){
				var birthday = new Date(resp.data[0].birth_date);
				resp.data[0].birthday = resp.data[0].birth_date !== null ? birthday.getReadableText("genitive") : $('<span style="color: red">ЗАПОЛНИТЕ В ЛИЧНЫХ ДАННЫХ</span>');
				$("#attestation_info").append(
					tmpl('professorCertificationPrint-info', resp.data[0])
				);
			}
		});

		$.ajax({
			url: 'api/science/answers/' + staff.staff_id + '?mod=details',
			method: 'GET',
			dataType: 'JSON',
			success: function(resp) {
				var verified_status = resp.data.verification.verified ? "Подтвержден" : "Не подтвержден";
				$("#print_button")
					.after(resp.data.verification.can_verify ? resp.data.verification.verified ? $('<button id="verify" class="btn btn-primary pull-right hidden-print verified">Опровергнуть</button>') : $('<button id="verify" class="btn btn-primary pull-right hidden-print">Подтвердить</button>') : "")
					.after($('<h4 id="verify_status" class="col-xs-3 pull-right">'+verified_status+'</h4>'));
				$("#verify").on("click", verifyAttestation);
				fillAttestationTable(resp.data);
			}
		});

		$("#print_button").on("click", function(){
			window.print();
		});

		function verifyAttestation(){
			var $this = $(this),
				is_verified = $this.hasClass("verified");
			$.ajax({
				url: '/api/science/staff/' + staff.staff_id + '/periods/0/verification',
				method: 'PUT',
				data: {value: is_verified ? 0 : 1},
				success: function(){
					$this.toggleClass("verified").text(is_verified?"Подтвердить":"Опровергнуть");
					$("#verify_status").text(is_verified?"Не подтвержден":"Подтвержден");
				}
			});
		}

		function fillAttestationTable(answers){
			var $table_body = $("#attestation_list").find("tbody");
			$.each(answers.tree, function(i,elem){
				$table_body.append(
					tmpl('professorCertificationPrint-section', elem)
				);
				elem.items.forEach(buildTableRow);
			});
			$("#kpk").text(answers.final[0].result);
			$("#knk").text(answers.final[1].result);

			function buildTableRow(element, index, array, parent_index){
				var row_num = typeof(parent_index) != "undefined" ? parent_index + "." + parseInt(index+1) : parseInt(index+1),
					value = "";

				switch(true){
					case (typeof(element.value_item_id) != "undefined" && element.value_item_id !== null):{
						value = element.value_item_name;
						break;
					}
					case (typeof(element.value_numeric) != "undefined" && element.value_numeric !== null):{
						value = element.value_numeric;
						value += (value != "" && "point_per_unit" in element && element.unit_of_measure != null) ? " "+element.unit_of_measure : "";
						break;
					}
					case (typeof(element.value_boolean) != "undefined" && element.value_boolean !== null):{
						value = element.value_boolean == 1 ? $('<i class=\"fa fa-check\"></i>') : $('<i class=\"fa fa-times\"></i>');
						break;
					}
				}

				$table_body.append(
					tmpl('professorCertificationPrint-row', {
						row_num: row_num,
						name: element.name,
						unit_of_measure: "unit_of_measure" in element && element.unit_of_measure != null ? element.unit_of_measure : "",
						value: value,
						point_per_unit: "point_per_unit" in element && element.point_per_unit != null ? element.point_per_unit : "",
						points: "points" in element && element.points != null ? element.points : "",
						max_points: "max_points" in element && element.max_points != null ? element.max_points : ""
					})
				);
				if("items" in element){
					element.items.forEach(function(e,i,a){
						buildTableRow(e,i,a,row_num);
					});
				}

			}
		}

		_super_after_open.call(this);

	};



	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}
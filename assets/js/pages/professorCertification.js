function ProfessorCertification(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект

	_page.settings = {
		url: '',
		data: {}
	};

	_page.normalizeData = function(data){
	};




	var _super_after_open = _page.afterOpen;
	_page.afterOpen = function(){
		$("#print_page_button").on("click", function(){
			window._URLObserver.open("professorCertificationPrint.php");
		});


		$.ajax({
			url: 'api/science/questions',
			method: 'GET',
			dataType: 'JSON',
			success: initCertifications
		});

		function initCertifications(response){

			function buildFormBlock(questions){
				var output = "";
				$.each(questions, function(key, question){
					if("items" in question){
						output += '<div class="form-wrap">';
						if('name' in question){
							output += '<div class="form-group form-header"><div class="col-sm-3 control-label"><span>'+question.name+':</span></div><div class="col-sm-5 form-control-static"><span data-max_points="'+question.max_points+'"></span> <i class="fa fa fa-star"></i></div></div>';
						}
						output += buildFormBlock(question.items);
						output += '</div>';
					}
					else{
						output += buildFormUnit(question);
					}
				});
				return output;
			}

			function buildFormUnit(unit){
				var unit_output = '';
				unit_output += '<div class="form-group"><label for="field-'+unit.id+'" class="col-sm-3 control-label">'+unit.name+'</label><div class="col-sm-5">';
				switch(unit.type_id) {
					case '1':{
						unit_output += '<div class="make-switch" data-on-label="<i class=\'entypo-check\'></i>" data-off-label="<i class=\'entypo-cancel\'></i>"><input id="field-'+unit.id+'" type="checkbox" name="item-'+unit.id+'" data-point="'+unit.point_per_unit+'"></div>';
						break;
					}
					case '2':{
						unit_output += '<select id="field-'+unit.id+'" name="item-'+unit.id+'" class="select2" data-allow-clear="true" data-placeholder="Выберите одну, если есть"><option></option>';
						if('options' in unit){
							unit.options.forEach(function(option){
								unit_output += '<option id="option-'+option.id+'" value="'+option.id+'" data-point="'+option.point_per_unit+'">'+option.name+'</option>';
							});
						}
						unit_output += '</select>';
						break;
					}
					case '3':{
						unit_output += '<input id="field-'+unit.id+'" class="form-control" type="number" min="0" name="item-'+unit.id+'" data-point_per_unit="'+unit.point_per_unit+'" data-max_points="'+unit.max_points+'" style="width: 100px" placeholder="'+unit.unit_of_measure+'">';
						break;
					}
				}
				unit_output += '</div><div class="col-sm-2"><span class="weight" title="Баллов"><i class="fa fa fa-star"></i><span>0</span></span></div></div>';
				return unit_output;
			}

			function buildForm(){
				$.each(response.data, function(key, value){
					if(key == 1){
						$('<li class="active"><a href="#section-'+key+'" class="handled" data-toggle="tab">'+value.name+'</a></li>').appendTo($("#wrapper").children(".nav"));
					}
					else{
						$('<li><a href="#section-'+key+'" class="handled" data-toggle="tab">'+value.name+'</a></li>').appendTo($("#wrapper").children(".nav"));
					}
				});

				var output = '';
				$.each(response.data, function(k){
					if(k == 1){
						output += '<div class="tab-pane active" id="section-'+k+'">';
					}
					else{
						output += '<div class="tab-pane" id="section-'+k+'">';
					}
					output += '<h3>'+response.data[k].description+'</h3>' +
					'<form id="section_'+k+'_form" role="form" class="form-horizontal">' +
					'<div class="form-group form-header"><div class="col-sm-3 control-label"><span>Общее количество балов по разделу:</span></div><div class="col-sm-5 form-control-static"><span></span> <i class="fa fa fa-star"></i></div></div>' +
					'<hr>';
					output += buildFormBlock(response.data[k].questions);
					output += '<div class="form-group"><div class="col-sm-offset-3 col-sm-5 text-right"><button id="section_save" type="button" class="btn btn-success btn-icon ladda-button save_btn hidden-print" data-style="slide-up">Сохранить <i class="entypo-floppy"></i></button> ';
					if(typeof(response.data[parseInt(k)+1]) != 'undefined')
						output += '<button type="button" class="btn btn-info btn-icon icon-right next_btn">Дальше<i class="entypo-right"></i></button>';
					output += '</div></div>';
					output += '</form></div>';
				});
				$(output).appendTo($("#wrapper").children(".tab-content"));
				output = null;
			}

			buildForm();

			$('.make-switch').bootstrapSwitch();
			if($.isFunction($.fn.select2)){
				$(".select2").each(function(i, el){
					var $this = $(el),
						opts = {
							allowClear: attrDefault($this, 'allowClear', false)
						};

					$this.select2(opts);
					$this.addClass('visible');
				});

				if($.isFunction($.fn.niceScroll)){
					$(".select2-results").niceScroll({
						cursorcolor: '#d4d4d4',
						cursorborder: '1px solid #ccc',
						railpadding: {right: 3}
					});
				}
			}

			$(".tab-content")
				.find("input.form-control").on("input", changeRate)
				.end().find("input[type='checkbox']").on("change", changeRate)
				.end().find("select").on("change", changeRate);

			$.ajax({
				url: 'api/science/answers/me',
				method: 'GET',
				dataType: 'JSON',
				success: function(res){
					res.data.forEach(function(answer){
						var $field = $("#field-"+answer.mask_question_id);
						if(answer.value_numeric !== null){
							$field.val(answer.value_numeric);
						}
						else if(answer.value_boolean !== null){
							$field.parents('.make-switch').bootstrapSwitch('setState', answer.value_boolean == 1, true)
						}
						else if(answer.value_item_id !== null){
							$field.select2("val", answer.value_item_id)
						}
						changeRate($field);
					})
				}
			});

			function changeRate(t){
				var $this = typeof(t.attr) == 'undefined' ? $(this) : t,
					$form_group = $this.parents(".form-group"),
					$point_field = $form_group.find(".weight").children("span"),
					max_points,
					point_per_unit,
					value,
					points;

				if($this.is("select")){
					if($this.val() !== ""){
						points = parseFloat($("#option-"+$this.val()).data("point"));
					}
					else{
						points = 0;
					}
				}
				else if($this.is("input[type='checkbox']")){
					value = $this.prop("checked") ? 1 : 0;
					point_per_unit = parseFloat($this.data("point"));
					points = value*point_per_unit;
				}
				else{
					value = $this.val() !== "" ? parseFloat($this.val()) : 0;
					point_per_unit = parseFloat($this.data("point_per_unit"));
					max_points = parseFloat($this.data("max_points"));
					points = value*point_per_unit >= max_points ? max_points : value*point_per_unit;
				}
				$point_field.text(points.toFixed(2));
				points ? $form_group.addClass("has-weight") : $form_group.removeClass("has-weight");
				countPoints($form_group);
			}

			function countPoints($this){
				var $header = $this.prevAll(".form-header").eq(-1).find(".form-control-static").children("span"),
					$form_groups = $this.parent().children(".form-group"),
					$form_wraps = $this.parent().children(".form-wrap"),
					max_points = $header.data("max_points") ? parseFloat($header.data("max_points")) : 9999999999,
					total = 0;

				$form_groups.find(".weight").children("span").each(function(){
					total += isNaN(parseFloat($(this).text())) ? 0 : parseFloat($(this).text());
				});
				$form_wraps.children(".form-header").find(".form-control-static").children("span").each(function(){
					total += isNaN(parseFloat($(this).text())) ? 0 : parseFloat($(this).text());
				});
				if(total > max_points)
					total = max_points;
				$header.text(total);
				if($this.parent().is(".form-wrap")){
					countPoints($this.parent())
				}
			}

			$(".next_btn").on("click", function(){
					var index = $(".next_btn").index($(this));
					$(".nav-tabs").children().removeClass("active").eq(index+1).addClass("active");
					$(".tab-pane").removeClass("active").eq(index+1).addClass("active");
					$(window).scrollTop(0);
				}
			);
			var ladda;
			$(".save_btn").on('click', function(){
					ladda = Ladda.create(this);
					ladda.start();
					saveData();
				}
			);

			function saveData(){
				var data = [];
				$("#wrapper").find("select, input.form-control").each(function(){
					if($(this).val()){
						data.push({name: $(this).attr("name"), value: $(this).val()})
					}
				}).end().find("input[type='checkbox']").each(function(){
					data.push({name: $(this).attr("name"), value: $(this).prop("checked")})
				});
				$.ajax({
					url: 'api/science/answers/me',
					method: 'POST',
					data: data,
					dataType: 'JSON',
					success: function(resp){
						$('.kpk-label').text(resp.data.final[0].result);
						$('.knk-label').text(resp.data.final[1].result);
						$('.kpk-progress .progress-bar').css('width', resp.data.final[0].result + '%');
						$('.knk-progress .progress-bar').css('width', resp.data.final[1].result + '%');
						$('#certification-sum-rating').modal('show');
					}
				}).always(function(){
					ladda.stop();
				});
			}

		}

		_super_after_open.call(this);
	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}
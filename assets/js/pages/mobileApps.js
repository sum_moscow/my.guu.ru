function MobileApps(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект;


	_page.settings = {
		url: '/api/users/me/apps',
		data: {}
	};
	var _super_after_open = _page.afterOpen;

	_page.normalizeData = function(res){
		var _html = $('<div>');
		res.data.forEach(function(value){
			_html.append(tmpl('user-item', value));
		});
		return {stars_html: _html};
	};


	_page.afterOpen = function(){
		$.ajax({
			url: 'api/mobileapps/token/android/',
			success: function(res){
				$('#android-qr-code').html('<img src="https://chart.googleapis.com/chart?chs=220x220&cht=qr&chl=' + res.data.token + '&choe=UTF-8"> <p><a href="#" id="refresh-token">Обновить (выход на всех устройствах)</a></p>');
				$('#refresh-token').on('click', function(){
					$.ajax({
						url: 'api/mobileapps/token/android/regenerate',
						success: function(_res){
							$('#android-qr-code').find('img').attr('src', 'https://chart.googleapis.com/chart?chs=220x220&cht=qr&chl=' + _res.data.token + '&choe=UTF-8');
						}
					});
				});
			}
		});
		_super_after_open.call(this);
	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}
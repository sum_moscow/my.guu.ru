String.prototype.capitalize = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
};
String.prototype.cut = function(chars_count){
	if(this.length > chars_count){
		var one_side_chars_num = Math.floor(chars_count / 2);
		return this.substr(0, one_side_chars_num) + ' ... ' + this.substr(chars_count - one_side_chars_num);
	}else{
		return this;
	}
};
Date.prototype.getMonthName = function(type){
	if (isNaN(this.getTime())){
		return '';
	}
	var short_months = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
		infinitive_months = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
		genitive_months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
		months_array;
	if (type == 'short'){
		months_array = short_months;
	}else if (type == 'genitive'){
		months_array = genitive_months;
	}else{
		months_array = infinitive_months;
	}
	return months_array[this.getMonth()];
};
Date.prototype.getReadableText = function(type, with_time){
	if (isNaN(this.getTime())){
		return '';
	}
	var short_months = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
		infinitive_months = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
		genitive_months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
		months_array,
		time;
	if (type == 'short'){
		months_array = short_months;
	}else if (type == 'genitive'){
		months_array = genitive_months;
	}else{
		months_array = infinitive_months;
	}
	if (with_time){
		var hours = this.getHours(),
			minutes = this.getMinutes(),
			secs = this.getSeconds();

		hours = (hours < 10 ? '0' + String(hours): hours);
		minutes = (minutes < 10 ? '0' + String(minutes): minutes);
		secs = (secs < 10 ? '0' + secs : String(secs));
		time = [hours, minutes, secs].join(':');
	}else{
		time = '';
	}
	return [this.getDate(), months_array[this.getMonth()], this.getFullYear(), time].join(' ');
};
Date.prototype.getInFormat = function() {
	return this.getDate() + '-' + (this.getMonth() + 1) + '-' + this.getFullYear();
};
Date.prototype.getMysqlFormat = function(){
	return moment(this).format('YYYY-MM-DD');
};

window.ALLOW_URL_HOSTS = ['my.guu.ru', 'lk.guu.ru', 'my.guu.local', 'dev.my.guu.ru'];
window.SITE_URL = 'my.guu.ru';

$.ajaxSetup({'global':true});

function showProfile(user_id){
	var KNK_INDICATOR_VALUE_ID = 2,
		KPK_INDICATOR_VALUE_ID = 1;
	$.ajax({
		url: '/api/users/' + user_id + '/fullInfo',
		success: function(res){
			res.data[0].sticker = res.data[0].is_student ? $('<span class="sticker label label-info">Студент</span>') : $('<span class="sticker label label-default">Сотрудник</span>');
			res.data[0].staff_visible = res.data[0].is_staff ? '' : 'hidden';
			var $modal = tmpl('user-info-modal', res.data[0]);
			$('#user-info-modal')
				.empty()
				.append($modal)
				.modal('show', {backdrop: 'static'})
				.css('max-height', jQuery(window).height());

			if (res.data[0].is_staff){
				$.ajax({
					url: '/api/science/rating/' + res.data[0].staff_id + '/periods/0',
					success: function(_res){
						var ratings = {
							knk: 0,
							kpk: 0
						};
						_res.data.forEach(function(value){
							if (value.indicator_id == KPK_INDICATOR_VALUE_ID){
								ratings.kpk = value.value;
							}else if (value.indicator_id == KNK_INDICATOR_VALUE_ID){
								ratings.knk = value.value;
							}
						});
						$modal.find('.kpk-label').text(ratings.kpk);
						$modal.find('.knk-label').text(ratings.knk);
						$modal.find('.kpk-progress .progress-bar').css('width', ratings.kpk + '%');
						$modal.find('.knk-progress .progress-bar').css('width', ratings.knk + '%');
					}
				});
			}
		}
	});

}

function showNotifier(response){
	var opts = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-top-full-width",
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	};
	var fn = response.status ? 'success': 'warning';
	toastr[fn](response.text, opts);
}

function tmpl(template_type, items, addTo, direction){

	var	htmlEntities = function(str) {
			return String(str + '').replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
		},
		replaceTags = function(html, object){
			$.each(object, function(key, value){
				var key_expr = new RegExp('{'+key+'}', 'gim');
				if ($.type(value) == 'string'){
					value = htmlEntities(value);
				}else if (value instanceof jQuery){
					var _value = [];
					value.each(function(){
						"use strict";
						_value.push(this.outerHTML) ;
					});
					value = _value.join('');
				}else if (value == null){
					value = '';
				}
				html = html ? html.replace(key_expr, value) : '';
			});
			var not_handled_keys = new RegExp(/\{.*?\}/gim);
			html = html ? html.replace(not_handled_keys, '') : '';
			return html;
		},

		result = '',
		html_val = (typeof template_type == 'object') ? template_type.tmpl : $('#tmpl-'+template_type).html(), //Р”РѕР±Р°РІР»СЏСЋ РІРѕР·РјРѕР¶РЅРѕСЃС‚СЊ РїРµСЂРµРґР°С‡Сѓ СЃСЂР°Р·Сѓ С‚РµР»Р° С€Р°Р±Р»РѕРЅР°, Р° РЅРµ СЃРµР»РµРєС‚РѕСЂР°
		comments = new RegExp(/(?:\/\*(?:[\s\S]*?)\*\/)|(?:([\s;])+\/\/(?:.*)$)/gim),
		spaces = new RegExp('\\s{2,}','igm');
	if(html_val === undefined || items === undefined){
		console.group('tmpl_error');
		console.log('error in '+template_type);
		console.log('items', items);
		console.log('addTo', addTo);
		console.log('html_val', html_val);
		console.log('inputs', {template_type: template_type, items:items, addTo:addTo, direction:direction});
		console.groupEnd();
	}
	html_val = html_val ? html_val.replace(comments,'') : '';
	html_val = html_val ? html_val.replace(spaces,'').trim() : '';
	if (Array.isArray(items)){
		var i, items_length = items.length;
		for (i = 0; i < items_length; i++){
			result += replaceTags(html_val, items[i]);
		}
	} else {
		result = replaceTags(html_val, items);
	}
	result = $(result);
	if (addTo == null || addTo == undefined){
		return result;
	}
	if(direction == 'prepend'){
		addTo.prepend(result);
	}else{
		addTo.append(result);
	}
	return result;
}

$(document)
	.on('drop dragover', function (e) {
		e.preventDefault();
		return false;
	});
$(document).ajaxError(function(event, jqXHR, ajaxSettings, thrownError){
	console.log('ajax error',{
		'event' : event,
		'jqXHR' : jqXHR,
		'ajaxSettings' : ajaxSettings,
		'thrownError' : thrownError
	});
	showNotifier({
		status: false,
		text: 'Извините... Произошла ошибка. Мы не смогли получить данные от сервера, попробуйте еще раз. :)'
	});
});
$(document).ajaxSuccess(function(event, request, settings) {
	if(settings.dataType === 'JSON'){
		var response = request.responseJSON;
		if(response.status == 0){
			showNotifier(response);
			if (response.data.hasOwnProperty('refresh') && response.data['refresh'] == true){
				window.location.reload();
			}
		}
	}
});
$(document).ajaxSend(function(){
	$('.neon-loading-bar').show(0);
	var random_pct = 25 + Math.round(Math.random() * 30);
	show_loading_bar(random_pct);
	show_loading_bar(random_pct + 40);

});
$(document).ajaxComplete(function(){
	console.log('complete');
	show_loading_bar(100);
	$('.neon-loading-bar').hide(0); //for render
});


function initYandexMetricaCounter(){
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				var params = w.getUserInfo();
				params['_' + w.getUserInfo().id] = true;
				w.yaCounter29425770 = new Ya.Metrika({id:29425770,
					webvisor:true,
					clickmap:true,
					trackLinks:true,
					accurateTrackBounce:true,
					trackHash:true,
					params:params
				});
			} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
}


function refreshNotifications(){
	var messages = window.notifications.email;

	$('.unread-emails-count').text(messages.length >= 10 ? '10+' : messages.length).data('messages', messages);
	var $cont = $('.unread-emails');

	tmpl('email-dropdown-notification', messages, $cont);
	$('.unread-emails-tile').data('end', messages.length);

	$('.tile-email .tile-stats').on("click", function(){
		var $timeline = $("#index_timeline");

		$timeline.slideUp("slow", function(){
			$timeline.empty();
			messages.forEach(function(notification) {
				var date = new Date(notification.received_date);
				$timeline.append(
					tmpl('timeline-email-row',
						{
							receive_date: notification.received_date,
							readable_receive_date: date.getReadableText("short"),
							readable_receive_time: date.toLocaleTimeString().slice(0,-3),
							from_name: notification.from_name,
							from_email: notification.from_email,
							subject: notification.subject,
							link: notification.link
						}
					)
				)
			});
			$('.go-to-email-btn').remove();
			$("#index_timeline").after(tmpl('open-email-btn', {}));
			$timeline.slideDown("slow");
		})
	});

	if (window.updateTiles){
		$('.tile-email').show('slow');
		window.updateTiles();
	}
}

$(document).ready(function(){
	var $window = $(window),
		$chat = $('#chat');
	window.chat_search_active = false;

	window.notifications = {
		email: [],
		requisitions: [],
		marks: [],
		timetable: []
	};

	function URL(url){
		if (url instanceof URL){
			return url;
		}
		var url_object,
			url_string,
			getPropertiesFromString = function(search){
				search = search.substring(1);
				var str;
				try{
					str = JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
				}catch(e){str = ''}
				return str;
			};
		if (typeof url === 'object'){
			var new_url = [];
			if (url.hasOwnProperty('properties') && url.hasOwnProperty('page_type')){
				$.each(url.properties, function(key,value){
					new_url.push(key + '=' + value);
				});
				url_string =  url.page_type + '?' + new_url.join('&');
				url_object = url;
			}else{
				throw new Error('Invalid URL object');
			}
		}else if (typeof url === 'string'){
			var a = document.createElement('a');
			a.href = url;
			var pathname = a.pathname.split('/').pop().replace(/\//g, '');
			if (ALLOW_URL_HOSTS.indexOf(a.hostname) == 1){
				throw new Error('Invalid URL. URL should be on my.guu.ru or my.guu.com');
			}
			else{
				url_string = url;
				url_object = {
					hostname: a.hostname,
					page_type: pathname ? pathname : 'index',
					pathname: pathname ? pathname : 'index',
					search: a.search,
					properties: getPropertiesFromString(a.search)
				};
			}
		}else{
			throw new Error('Invalid URL. Argument should be string or object');
		}
		return {
			getStandardString: function(){
				if(url_object.properties){
					var standard_url = [],
						keys = [];
					for (var _key in url_object.properties){
						if (url_object.properties.hasOwnProperty(_key)){
							keys.push(_key);
						}
					}
					keys.sort();
					var key;
					for (var i = 0; i < keys.length; i++){
						key = keys[i];
						standard_url.push(key + '=' + url_object.properties[key]);
					}
					return url_object.pathname + '?' + standard_url.join('&');
				} else {
					return url_object.pathname;
				}
			},
			getObject: function(){
				return url_object;
			},
			getString: function(){
				return url_string;
			},
			getPathName: function(){
				return url_object.page_type.replace('.php', '');
			}
		}
	}

	function URLObserver(){
		var page_was_here = false, //Переменная для отслеживания, была ли страница до того, как был запущен очередной open()
			base_bound = false;

		window.addEventListener('popstate', function(e){
			var _url;
			if (e.state){
				_url = new URL(e.state.url);
				window._URLObserver.open(_url.getStandardString(), true);
			}
			return false;
		});
		/**
		 * Проверяем, чтобы URL, на который переходим принадлежал нам.
		 */
		var updateMenu = function(){
				var $main_menu = $('#main-menu'),
					$next_menu = $main_menu.find('a[href="' + window.location.pathname +'"]');
				$main_menu.find('>li, li.active').removeClass('active');
				$main_menu.find('>li').removeClass('active');
				$next_menu.parent().addClass('active').parents('.root-level').addClass('active');
			},
			loadPage = function(_url, back_effect, callback){
				show_loading_bar(10);
				var page_type = _url.getPathName(),
					url_string = _url.getStandardString();

				window._resources.load(page_type, function(){
					if (window._current_page){
						window._current_page.close();
					}
					window._current_page = new window[page_type.capitalize()](_url);
					window._current_page.open(back_effect);
					updateMenu();
					if(callback instanceof Function){
						callback();
					}

				});


			};
		return {
			open: function(url_string, back_effect){

				var _url = new URL(url_string);

				if (window._current_page){ // Если это НЕ самая первая открываемая страница
					if(window._current_page.getURL()){
						$window.off('resize', this.resize);
					}
					if (!back_effect){
						window.history.pushState({'url': url_string}, '' , url_string);
					}
				} else {
					window.history.replaceState({'url' : url_string}, '', url_string);
				}
				loadPage(_url, back_effect);
			}
		}
	}

	function Resources(){
		/*Константы*/
		var JS_FOLDER = '/assets/js/pages/',
			TMPL_FOLDER = '/tmpl/',

			TMPL_EXT = '.html',
			JS_EXT = '.js',
			/** Loaded files */
			tmpls = [],
			js = [];
		return {
			getTmpl: function (page, callback){
				if (tmpls.indexOf(page) == -1){
					$.ajax({
						url: page,
						dataType: 'html',
						success: function(html) {
							$('body').append(html);
							if (callback instanceof Function){
								callback();
							}
						}
					});
					tmpls.push(page);
				}else{ // Есл указанный рексур у нас уже есть - мы выполм сразу callback
					if (callback instanceof Function){
						callback();
					}
				}
			},
			getJS: function(page, callback){
				if (js.indexOf(page) == -1){
					$.getScript(page, callback);
					js.push(page);
				} else if (callback instanceof Function){
					callback();
				}
			},
			load: function(page, callback){ // Последовательно загружает необходимые ресурсы
				var _this = this;
				page = page.replace('.php', '');
				_this.getTmpl(TMPL_FOLDER + page + TMPL_EXT, function(){
					_this.getJS(JS_FOLDER + page + JS_EXT, function(){
						if (callback instanceof Function){ // Выполним переданный callback только на последнем этапе
							callback();
						}
					});
				});
			},
			logStacks: function(){
				console.log(tmpls, js);
			}
		}
	}

	//TODO: Закончить вывод уведомлений по БРС и расписанию
	function getNotifications(){
		$.ajax({
			url: '/api/users/me/notifications',
			success: function(res){
				var marks = [],
					changes = [],
					days = ['понедельник', 'вторник', 'среду', 'четверг', 'пятницу', 'субботу'];
				//TIMETABLE
				if (res.data.timetable && res.data.timetable.length > 0){
					$('.timetable-notifications').removeClass('hidden');
					$('.timetable-changes-count').text(res.data.timetable.length);
					res.data.timetable.forEach(function(value, index){
						changes[index] = value;
						value.day_type = parseInt(value.day_type) - 1;
						changes[index].day_name = days[value.day_type];
					});
					tmpl('timetable-dropdown-notification', changes, $('.timetable-changes'));
				}

				//BRS
				if (res.data.brs && res.data.brs.length > 0){
					$('.brs-notifications').removeClass('hidden');
					$('.brs-new-marks-count').text(res.data.brs.length);

					res.data.brs.forEach(function(value, index){
						marks[index] = value;
						marks[index].change_date_readable = new Date(value.change_date).getReadableText('genitive');
					});
					tmpl('brs-dropdown-notification', marks, $('.brs-new-marks'));
				}
			}
		});
	}

	function connectWithNode(){
		socket.emit('connection.connected', {
			user: window.getUserInfo ? window.getUserInfo() : {}
		});

		socket.on('notifications.emails', function(messages){
			messages.forEach(function(value, index){
				messages[index].received_date = value.received_time;
				messages[index].received_time = new Date(value.received_time).getReadableText('short');
			});

			window.notifications.email = messages;
			refreshNotifications();
		});

		socket.on('log', function(d){
			console.log(d);
		});

		socket.on('alert', function(data){
			alert(data);
		});
		defineChatEvents();
	}

	$.getMultiScripts = function(resources, callback) {
		$.getScript( '/assets/js/multiscript.php?files[]=' + resources.join('&files[]='), callback);
	};

	/**
	 * Создаем экзепляры класса обработчика URL и загрузчика ресурсов
	 */
	window._resources = new Resources();
	window._URLObserver = new URLObserver();
	window._URLObserver.open(window.location.href);
	window.transition_end = 'transitionend webkitTransitionEnd oTransitionEnd otransitionend';


	$('a:not(.handled)').on('click', function(e){
		if (e.button == 0 && e.ctrlKey == false){ // Обрабатываем только нажатие левой кнопкой мыши и когда не зажат ctrl
			var href = this.href,
				host = this.hostname,
				page_URL = href.split('/').pop(),
				target = this.target,
				re = new RegExp(/javascript/ig),
				re_email = new RegExp(/mailto:/ig);
			//Если через href="#", то нам не нужно, чтобы это писалось в адрес,
			// т к архитектура очень чувствительна к изменению адреса
			if(href.substr(href.length - 1) === '#' && href[0] != '#' ){
				//e.preventDefault();
				return true;
			}else if (re.test(page_URL) == false && re_email.test(page_URL) == false){
				console.log(page_URL);
				if((ALLOW_URL_HOSTS.indexOf(host) != -1) && target !== '_blank'){
					window._URLObserver.open(page_URL);
					e.preventDefault();
					return false;
				} else if(target !== '_blank') {
					e.preventDefault();
					return false;
				}
			} else {
				e.preventDefault();
				return false;
			}
		}else{
			return false;
		}
		return true;
	}).addClass('handled');
	$chat.attr({
		'data-current-user': getUserInfo().name,
		'data-user-id': getUserInfo().user_id
	});
	$chat.find('.search-people-in-chat').on('keypress', function(e){
		if (e.which == 13 && !window.chat_search_active){
			socket.emit('chat.search', {q: this.value});
		}
	});

	$('.main-exit-btn').on('click', function(){
		$.ajax({
			url: '/login.php?logout=true',
			success: function(){
				window.location.href = 'https://login.microsoftonline.com/logout.srf';
			}
		});
	});

	connectWithNode();
	getNotifications();
	initYandexMetricaCounter();
	//authInMoodle();
});


function Page(_url){
	var page_data = {},
		page_type = _url.getPathName(),
		slide_effect = {
			'default' : 'right',
			'forward' : 'right',
			'back' : 'left'
		},
		default_settings = {
			slide_effect: slide_effect.default,
			bench_type: page_type
		},
		observer,
		observer_settings = {
			attributes: true,
			characterData: true,
			childList: true,
			subtree: true,
			attributeFilter: ['class', 'src']
		};

	return {
		$bench: null,
		createBench: function(data){
			var settings = $.extend(default_settings, this.settings, data);
			this.$bench = tmpl(page_type + '-page', settings);
			return this.$bench.hide();
		},
		normalizeData: function(res){
			return res.data;
		},
		open: function(){
			var _this = this;
			if (this.settings.url){
				$.ajax({
					url: this.settings.url,
					data: this.settings.data ? this.settings.data: {},
					dataType: 'JSON',
					success: function(res){
						if (window._current_page.getURL().getPathName() != _this.getURL().getPathName()) return;
						var data = _this.normalizeData(res);
						_this.createBench(data);
						$('#page-content').append(_this.$bench);
						_this.$bench.slideDown('slow', function(){
							_this.afterOpen();
						});
					}
				});
			}else{
				if (window._current_page.getURL().getPathName() != this.getURL().getPathName()) return;
				this.createBench(this.settings.data);
				$('#page-content').append(_this.$bench);
				_this.$bench.slideDown('slow', function(){
					_this.afterOpen();
				});
			}
		},
		render: null,
		afterOpen: function(){
			$(window).on('resize', this.resize);
			var _this = this;
			observer = new window.MutationObserver(function(){
				var $$bench = _this.$bench;
				$$bench.find('a:not(.handled)').on('click', function(e){
					if (e.button == 0 && e.ctrlKey == false){ // Обрабатываем только нажатие левой кнопкой мыши и когда не зажат ctrl
						var href = this.href,
							host = this.hostname,
							page_URL = href.split('/').pop(),
							target = this.target,
							re = new RegExp(/javascript/ig);
						//Если через href="#", то нам не нужно, чтобы это писалось в адрес,
						// т к архитектура очень чувствительна к изменению адреса
						if (href[0] == '#') return;
						if(href.substr(href.length - 1) === '#'){
							return;
						}else if (re.test(page_URL) == false){
							if((ALLOW_URL_HOSTS.indexOf(host) != -1) && target !== '_blank'){
								window._URLObserver.open(page_URL);
								e.preventDefault();
								return false;
							} else if(target !== '_blank') {
								e.preventDefault();
								return false;
							}
						} else {
							e.preventDefault();
							return false;
						}
					}else{
						return false;
					}
					return true;
				}).addClass('handled');

				// Input Mask
				if($.isFunction($.fn.inputmask)){
					$("[data-mask]:not(.done)").each(function(i, el){
						var $this = $(el).addClass('done'),
							mask = $this.data('mask').toString(),
							opts = {
								numericInput: attrDefault($this, 'numeric', false),
								radixPoint: attrDefault($this, 'radixPoint', ''),
								rightAlignNumerics: attrDefault($this, 'numericAlign', 'left') == 'right'
							},
							placeholder = attrDefault($this, 'placeholder', ''),
							is_regex = attrDefault($this, 'isRegex', '');


						if(placeholder.length){
							opts[placeholder] = placeholder;
						}

						switch(mask.toLowerCase()){
							case "phone":
								mask = "(999) 999-9999";
								break;

							case "currency":
							case "rcurrency":

								var sign = attrDefault($this, 'sign', '$');;

								mask = "999,999,999.99";

								if($this.data('mask').toLowerCase() == 'rcurrency'){
									mask += ' ' + sign;
								}
								else{
									mask = sign + ' ' + mask;
								}

								opts.numericInput = true;
								opts.rightAlignNumerics = false;
								opts.radixPoint = '.';
								break;

							case "email":
								mask = 'Regex';
								opts.regex = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+\\.[a-zA-Z]{2,4}";
								break;

							case "fdecimal":
								mask = 'decimal';
								$.extend(opts, {
									autoGroup		: true,
									groupSize		: 3,
									radixPoint		: attrDefault($this, 'rad', '.'),
									groupSeparator	: attrDefault($this, 'dec', ',')
								});
						}

						if(is_regex){
							opts.regex = mask;
							mask = 'Regex';
						}

						$this.inputmask(mask, opts);
					});
				}

			});
			observer.observe(_this.$bench.get(0), observer_settings);
			$('.to-select2:not(.not-auto)').each(function(){
				$(this).select2({
					allowClear: $(this).data('allow-clear') == true
				});
			});
			/*
			 $('.make-switch').off('click').on('click', function(){
			 if ($(this).hasClass('deactivate')) return false;
			 $(this).find('.switch-off,.switch-on').toggleClass('switch-off switch-on');
			 });*/
		},
		beforeClose: function(){

		},
		close: function(){
			this.beforeClose();
			if (observer){
				observer.disconnect();
			}
			if (this.$bench instanceof jQuery){
				this.$bench.slideUp('slow', function(){
					$(this).remove();
				});
			}
			return this;
		},
		resize: function(){
			var sidebar_left = $("#sidebar-left");
			sidebar_left.find(".viewport-v").height(sidebar_left.height()-$("#session").outerHeight()-35);
		},
		getURL: function(){
			return _url;
		},
		getBench: function(){
			return this.$bench;
		}
	}
}
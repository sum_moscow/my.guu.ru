//! moment.js locale configuration
//! locale : russian (ru)
//! author : Viktorminator : https://github.com/Viktorminator
//! Author : Menelion Elens?le : https://github.com/Oire

(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(require('../moment')) :
		typeof define === 'function' && define.amd ? define(['moment'], factory) :
			factory(global.moment)
}(this, function (moment) { 'use strict';


	function plural(word, num) {
		var forms = word.split('_');
		return num % 10 === 1 && num % 100 !== 11 ? forms[0] : (num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20) ? forms[1] : forms[2]);
	}
	function relativeTimeWithPlural(number, withoutSuffix, key) {
		var format = {
			'mm': withoutSuffix ? '������_������_�����' : '������_������_�����',
			'hh': '���_����_�����',
			'dd': '����_���_����',
			'MM': '�����_������_�������',
			'yy': '���_����_���'
		};
		if (key === 'm') {
			return withoutSuffix ? '������' : '������';
		}
		else {
			return number + ' ' + plural(format[key], +number);
		}
	}
	function monthsCaseReplace(m, format) {
		var months = {
				'nominative': '������_�������_����_������_���_����_����_������_��������_�������_������_�������'.split('_'),
				'accusative': '������_�������_�����_������_���_����_����_�������_��������_�������_������_�������'.split('_')
			},
			nounCase = (/D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/).test(format) ?
				'accusative' :
				'nominative';
		return months[nounCase][m.month()];
	}
	function monthsShortCaseReplace(m, format) {
		var monthsShort = {
				'nominative': '���_���_����_���_���_����_����_���_���_���_���_���'.split('_'),
				'accusative': '���_���_���_���_���_����_����_���_���_���_���_���'.split('_')
			},
			nounCase = (/D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/).test(format) ?
				'accusative' :
				'nominative';
		return monthsShort[nounCase][m.month()];
	}
	function weekdaysCaseReplace(m, format) {
		var weekdays = {
				'nominative': '�����������_�����������_�������_�����_�������_�������_�������'.split('_'),
				'accusative': '�����������_�����������_�������_�����_�������_�������_�������'.split('_')
			},
			nounCase = (/\[ ?[��] ?(?:�������|���������|���)? ?\] ?dddd/).test(format) ?
				'accusative' :
				'nominative';
		return weekdays[nounCase][m.day()];
	}

	var ru = moment.defineLocale('ru', {
		months : monthsCaseReplace,
		monthsShort : monthsShortCaseReplace,
		weekdays : weekdaysCaseReplace,
		weekdaysShort : '��_��_��_��_��_��_��'.split('_'),
		weekdaysMin : '��_��_��_��_��_��_��'.split('_'),
		monthsParse : [/^���/i, /^���/i, /^���/i, /^���/i, /^��[�|�]/i, /^���/i, /^���/i, /^���/i, /^���/i, /^���/i, /^���/i, /^���/i],
		longDateFormat : {
			LT : 'HH:mm',
			LTS : 'LT:ss',
			L : 'DD.MM.YYYY',
			LL : 'D MMMM YYYY �.',
			LLL : 'D MMMM YYYY �., LT',
			LLLL : 'dddd, D MMMM YYYY �., LT'
		},
		calendar : {
			sameDay: '[������� �] LT',
			nextDay: '[������ �] LT',
			lastDay: '[����� �] LT',
			nextWeek: function () {
				return this.day() === 2 ? '[��] dddd [�] LT' : '[�] dddd [�] LT';
			},
			lastWeek: function (now) {
				if (now.week() !== this.week()) {
					switch (this.day()) {
						case 0:
							return '[� �������] dddd [�] LT';
						case 1:
						case 2:
						case 4:
							return '[� �������] dddd [�] LT';
						case 3:
						case 5:
						case 6:
							return '[� �������] dddd [�] LT';
					}
				} else {
					if (this.day() === 2) {
						return '[��] dddd [�] LT';
					} else {
						return '[�] dddd [�] LT';
					}
				}
			},
			sameElse: 'L'
		},
		relativeTime : {
			future : '����� %s',
			past : '%s �����',
			s : '��������� ������',
			m : relativeTimeWithPlural,
			mm : relativeTimeWithPlural,
			h : '���',
			hh : relativeTimeWithPlural,
			d : '����',
			dd : relativeTimeWithPlural,
			M : '�����',
			MM : relativeTimeWithPlural,
			y : '���',
			yy : relativeTimeWithPlural
		},
		meridiemParse: /����|����|���|������/i,
		isPM : function (input) {
			return /^(���|������)$/.test(input);
		},
		meridiem : function (hour, minute, isLower) {
			if (hour < 4) {
				return '����';
			} else if (hour < 12) {
				return '����';
			} else if (hour < 17) {
				return '���';
			} else {
				return '������';
			}
		},
		ordinalParse: /\d{1,2}-(�|��|�)/,
		ordinal: function (number, period) {
			switch (period) {
				case 'M':
				case 'd':
				case 'DDD':
					return number + '-�';
				case 'D':
					return number + '-��';
				case 'w':
				case 'W':
					return number + '-�';
				default:
					return number;
			}
		},
		week : {
			dow : 1, // Monday is the first day of the week.
			doy : 7  // The week that contains Jan 1st is the first week of the year.
		}
	});

	return ru;

}));
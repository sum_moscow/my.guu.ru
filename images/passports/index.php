<?php

	if (!isset($_REQUEST['id'])){
		header("HTTP/1.0 404 Not Found");
		die();
	}

	require_once "../../backend/core/bin/db.php";
	require_once "../../backend/core/bin/Class.AbstractModule.php";
	require_once "../../backend/core/users/Class.AbstractUser.php";
	require_once "../../backend/core/users/Class.User.php";
	require_once "../../backend/core/users/Class.Staff.php";
	require_once "../../backend/students/Class.StudentObject.php";
	require_once "../../backend/core/users/Class.Professor.php";
	require_once "../../backend/core/bin/Class.Result.php";


function getMimeType($file_ext) {
	$mime_types_map = array(
		'bm' => 'image/bmp',
		'bmp' => 'image/bmp',
		'boo' => 'application/book',
		'dwg' => 'image/x-dwg',
		'dxf' => 'image/x-dwg',
		'gif' => 'image/gif',
		'jfif' => 'image/pjpeg',
		'jfif-tbnl' => 'image/jpeg',
		'jpe' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'jpg' => 'image/jpeg',
		'jps' => 'image/x-jps',
		'jut' => 'image/jutvision',
		'nap' => 'image/naplps',
		'naplps' => 'image/naplps',
		'nif' => 'image/x-niff',
		'niff' => 'image/x-niff',
		'pbm' => 'image/x-portable-bitmap',
		'pct' => 'image/x-pict',
		'pcx' => 'image/x-pcx',
		'pdf' => 'application/pdf',
		'pict' => 'image/pict',
		'qif' => 'image/x-quicktime',
		'qti' => 'image/x-quicktime',
		'qtif' => 'image/x-quicktime',
		'rast' => 'image/cmu-raster',
		'rf' => 'image/vnd.rn-realflash',
		'rgb' => 'image/x-rgb',
		'tif' => 'image/tiff',
		'tiff' => 'image/tiff',
		'xbm' => 'image/xbm',
		'xpm' => 'image/xpm',
		'x-png' => 'image/png',
		'xwd' => 'image/x-xwd'
	);
	return $mime_types_map[$file_ext];
}

	$student_info = StudentObject::searchConfidentialInfoStudent($__db,
		new User($__db, null),
		array('id' => $_REQUEST['id']))->getData();


	if (count($student_info) == 0){
		header("HTTP/1.0 404 Not Found");
		die();
	}

	$file_name = $_REQUEST['id'] . '.' . $student_info[0]['document_scan_ext'];

	if (!file_exists($file_name)){
		header("HTTP/1.0 404 Not Found");
		die();
	}else{
		header('Content-Type: ' . getMimeType($student_info[0]['document_scan_ext']));
		readfile($file_name);
	}